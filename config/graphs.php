<?php


return [

    'font1-regular' => env('GRAPHS_FONT1_REGULAR', '/usr/share/fonts/truetype/noto/NotoSans-Regular.ttf'),
    'font1-bold' => env('GRAPHS_FONT1_BOLD', '/usr/share/fonts/truetype/noto/NotoSans-Bold.ttf'),
    'font1-italic' => env('GRAPHS_FONT1_ITALIC', '/usr/share/fonts/truetype/noto/NotoSans-Italic.ttf'),

    'font2-regular' => env('GRAPHS_FONT2_REGULAR', '/usr/share/fonts/truetype/ubuntu/Ubuntu-M.ttf'),
    'font2-bold' => env('GRAPHS_FONT2_BOLD', '/usr/share/fonts/truetype/ubuntu/Ubuntu-B.ttf'),
    'font2-italic' => env('GRAPHS_FONT2_ITALIC', '/usr/share/fonts/truetype/ubuntu/Ubuntu-I.ttf'),

    'font3-regular' => env('GRAPHS_FONT3_REGULAR', '/usr/share/fonts/truetype/liberation2/LiberationSans-Regular.ttf'),
    'font3-bold' => env('GRAPHS_FONT3_BOLD', '/usr/share/fonts/truetype/liberation2/LiberationSans-Bold.ttf'),
    'font3-italic' => env('GRAPHS_FONT3_ITALIC', '/usr/share/fonts/truetype/liberation2/LiberationSans-Italic.ttf'),

];