<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */
    'google' => [
        'streetview' => [
            'apikey' => env('STREETVIEW_API_KEY', ''),
            'urlsecret' => env('STREETVIEW_API_SECRET', ''),
        ],
        'geocode' => [
            'apikey' => env('GEOCODE_API_KEY', ''),
        ],
        'maps' => [
            'apikey' => env('MAPS_API_KEY', ''),
            'urlsecret' => env('MAPS_API_SECRET', ''),
        ],

        'sheets' => [
            'application_name' => env('GOOGLE_APPLICATION_NAME', 'Twitter Bots'),
            'scopes' => [Google\Service\Sheets::SPREADSHEETS],
            'service_account' => env('GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION', ''),
            'project' => env('GOOGLE_PROJECT_NAME', ''),
        ],
    ],

    'fediApi' => [
        'ciclovia18' => [
            'host' => env('CICLOVIA_API_HOST', ''),
            'apikey' => env('CICLOVIA_API_KEY', ''),
        ],

        'everyflight' => [
            'host' => env('EVERYFLIGHT_API_HOST', ''),
            'apikey' => env('EVERYFLIGHT_API_KEY', ''),
        ],

        'cdf' => [
            'host' => env('CDF_API_HOST', ''),
            'apikey' => env('CDF_API_KEY', ''),
        ],

        'ladiaria' => [
            'host' => env('LADIARIA_API_HOST', ''),
            'apikey' => env('LADIARIA_API_KEY', ''),
        ],

        'lynch' => [
            'host' => env('LYNCH_API_HOST', ''),
            'apikey' => env('LYNCH_API_KEY', ''),
        ],

        'meteobot' => [
            'host' => env('METEOBOT_API_HOST', ''),
            'apikey' => env('METEOBOT_API_KEY', ''),
        ],

        'reloj' => [
            'host' => env('RELOJ_API_HOST', ''),
            'apikey' => env('RELOJ_API_KEY', ''),
        ],

        'scihub' => [
            'host' => env('SCIHUB_API_HOST', ''),
            'apikey' => env('SCIHUB_API_KEY', ''),
        ],

        'test' => [
            'host' => env('TEST_API_HOST', ''),
            'apikey' => env('TEST_API_KEY', ''),
        ],

        'ute' => [
            'host' => env('UTE_API_HOST', ''),
            'apikey' => env('UTE_API_KEY', ''),
        ],

        'uvindex' => [
            'host' => env('UVINDEX_API_HOST', ''),
            'apikey' => env('UVINDEX_API_KEY', ''),
        ],

        'wikipediaEn' => [
            'host' => env('WIKIPEDIAEN_API_HOST', ''),
            'apikey' => env('WIKIPEDIAEN_API_KEY', ''),
        ],

        'wikipediaEsTop' => [
            'host' => env('WIKIPEDIA_ES_TOP_HOST', ''),
            'apikey' => env('WIKIPEDIA_ES_TOP_KEY', ''),
        ],
    ],

    'twitter' => [
        'defancify' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('DEFANCIFY_TWITTER_TOKEN', ''),
            'secret' => env('DEFANCIFY_TWITTER_SECRET', ''),
            'bot_user_id' => env('DEFANCIFY_TWITTER_USERID', ''),
        ],
        'everylotmvd' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('EVERYLOTMVD_TWITTER_TOKEN', ''),
            'secret' => env('EVERYLOTMVD_TWITTER_SECRET', ''),
            'bot_user_id' => env('EVERYLOTMVD_TWITTER_USERID', ''),
        ],
        'everypurchase_presi' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('EVERYPURCHASE_PRESI_TWITTER_TOKEN', ''),
            'secret' => env('EVERYPURCHASE_PRESI_TWITTER_SECRET', ''),
            'bot_user_id' => env('EVERYPURCHASE_PRESI_TWITTER_USERID', ''),
        ],
        'everypurchase_legis' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('EVERYPURCHASE_LEGIS_TWITTER_TOKEN', ''),
            'secret' => env('EVERYPURCHASE_LEGIS_TWITTER_SECRET', ''),
            'bot_user_id' => env('EVERYPURCHASE_LEGIS_TWITTER_USERID', ''),
        ],
        'everypurchase_def' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('EVERYPURCHASE_DEF_TWITTER_TOKEN', ''),
            'secret' => env('EVERYPURCHASE_DEF_TWITTER_SECRET', ''),
            'bot_user_id' => env('EVERYPURCHASE_DEF_TWITTER_USERID', ''),
        ],
        'nomenclator' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('NOMENCLATOR_TWITTER_TOKEN', ''),
            'secret' => env('NOMENCLATOR_TWITTER_SECRET', ''),
            'bot_user_id' => env('NOMENCLATOR_TWITTER_USERID', ''),
        ],

        'autores' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('AUTORESUY_TWITTER_TOKEN', ''),
            'secret' => env('AUTORESUY_TWITTER_SECRET', ''),
            'bot_user_id' => env('AUTORESUY_TWITTER_USERID', ''),
        ],

        'polalert' => [
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('POLALERT_TWITTER_TOKEN', ''),
            'secret' => env('POLALERT_TWITTER_SECRET', ''),
        ],

        'peteco' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('PETECO_TWITTER_TOKEN', ''),
            'secret' => env('PETECO_TWITTER_SECRET', ''),
            'bot_user_id' => env('PETECO_TWITTER_USERID', ''),
        ],

        'everyuruguayan' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('EVERYURUGUAYAN_TWITTER_TOKEN', ''),
            'secret' => env('EVERYURUGUAYAN_TWITTER_SECRET', ''),
            'bot_user_id' => env('EVERYURUGUAYAN_TWITTER_USERID', ''),
        ],

        'everyairport' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('AIRPORT_TOKEN', ''),
            'secret' => env('AIRPORT_SECRET', ''),
            'bot_user_id' => env('AIRPORT_USERID', ''),
        ],

        'randombakeries' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_MIENTRAS_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_MIENTRAS_CONSUMER_SECRET', ''),
            'token' => env('BAKERY_TWITTER_TOKEN', ''),
            'secret' => env('BAKERY_TWITTER_SECRET', ''),
            'bot_user_id' => env('BAKERY_USERID', ''),
        ],

        'cdf' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('CDF_TWITTER_TOKEN', ''),
            'secret' => env('CDF_TWITTER_SECRET', ''),
            'bot_user_id' => env('CDF_USERID', ''),
        ],

        'ipap' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('IPAP_TWITTER_TOKEN', ''),
            'secret' => env('IPAP_TWITTER_SECRET', ''),
            'bot_user_id' => env('IPAP_USERID', ''),
        ],

        'webmonitor' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_YALOVES_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_YALOVES_CONSUMER_SECRET', ''),
            'token' => env('WEB_MONITOR_TOKEN', ''),
            'secret' => env('WEB_MONITOR_SECRET', ''),
            'bot_user_id' => env('WEB_MONITOR_USERID', ''),
        ],

        'indprop' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_YALOVES_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_YALOVES_CONSUMER_SECRET', ''),
            'token' => env('INDPROP_TOKEN', ''),
            'secret' => env('INDPROP_SECRET', ''),
            'bot_user_id' => env('INDPROP_USERID', ''),
        ],

        'test' => [
            'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/twitterapio.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('TEST_TWITTER_TOKEN', ''),
            'secret' => env('TEST_TWITTER_SECRET', ''),
            'bot_user_id' => env('TEST_TWITTER_USERID', ''),
        ],
    ],

    'bitly' => [
        'token' => env('BITLY_TOKEN', ''),
    ],

    'flickr' => [
        'key' => env('FLICKR_API_KEY', ''),
    ],

    'ambienteUy' => [
        'certPath' => env('METEO_CERT_PATH', '/usr/share/ca-certificates/RapidSSLGlobalTLSRSA4096SHA2562022CA1.crt'),
    ],

];
