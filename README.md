# RUNNING BOTS
* @BigBo_Energy
* @Carrasco_Vuelos
* @LaDiaria
* @SciHub
*

# DEAD BOTS
* @MeteoBot
* @Cada_Uruguayo

# POSSIBLE BOTS
* https://www.newspapers.com/clippings/
    ```
    curl 'https://www.newspapers.com/api/clipping/list?count=25&sort=modified-desc&product_id=1' --compressed -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'Referer: https://www.newspapers.com/clippings/' -H 'DNT: 1' -H 'Sec-GPC: 1' -H 'Alt-Used: www.newspapers.com' -H 'Connection: keep-alive' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' -H 'TE: trailers'
    ```