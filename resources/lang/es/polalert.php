<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Politicos Alert (BigBo_Politics) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the polalert command (BigBo_Politics)
    |
    */
    'name' => 'nombre',
    'description' => 'bio',
    'location' => 'ubicación',
    'now-verified' => 'ha pasado a estar verificado ☑️',
    'not-verified-anymore' => 'ha dejado de estar verificado ❌',
    'has-changed' => 'ha cambiado su',
    'from' => 'de',
    'to' => 'a',
    'now-follows' => 'ahora sigue',
    'has-started-following' => 'ha empezado a seguir',
    'gave-follow' => 'le dio follow',
    'suspended-account' => 'Cuenta suspendida',
    'has-stopped-following' => 'ha dejado de seguir',
    'does-not-follow-anymore' => 'ya no sigue',
    'stopped-following' => 'dejó de seguir',
    'removed-account' => 'Cuenta eliminada',
    'in-the-last-hour' => 'en la última hora',
    'since-one-hour-ago' => 'desde hace una hora',
    'since-yesterday' => 'desde ayer',
    'in-the-last-day' => 'en el último día',
    'has-increased-their-number-of-followers-perc' => 'ha incrementado su número de seguidores un :perc%',
    'has-increased-their-followers-perc' => 'ha incrementado sus followers en un :perc%',
    'has-grown-perc-followers' => "ha ganado un :perc% de followers",
    'grew-perc-their-followers' => 'aumentó en un :perc% sus seguidores',
    'has-lost-perc-their-followers' => "ha perdido un :perc% de sus seguidores",
    'lost-perc-their-followers' => "perdió un :perc% de sus followers",
    'perc-followers-stopped-following' => ":perc% de sus followers dejó de seguirle",
    'has-increased-their-number-of-followers-count' => "ha incrementado +:count su número de seguidores",
    'has-increased-their-followers-count' => "ha incrementado sus followers en +:count",
    'has-gained-count-followers' => "ha ganado :count followers",
    'increased-count-their-followers' => "aumentó en :count sus seguidores",
    'has-lost-count-followers' => "ha perdido :count de sus seguidores",
    'lost-count-followers' => "perdió :count de sus followers",
    'count-followers-stopped-following' => ":count de sus followers dejaron de seguir la cuenta",
    'old-avatar-of' => 'Viejo avatar de',
    'new-avatar-of' => 'Nuevo avatar de',
];
