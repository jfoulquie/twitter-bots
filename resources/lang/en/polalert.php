<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Politicos Alert (BigBo_Politics) Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the polalert command (BigBo_Politics)
    |
    | Pronouns are supported, see Constants on TwitterUser model, 0 = their, 1 = her, 2 his
    |
    */
    'has-increased-their-number-of-followers-perc' => '{0}has increased their number of followers a :perc%|{1}has increased her number of followers a :perc%|{2}has increased his number of followers a :perc%',
];
