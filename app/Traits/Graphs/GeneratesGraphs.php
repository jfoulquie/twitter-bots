<?php

namespace App\Traits\Graphs;

use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Graph\PieGraph;
use Amenadiel\JpGraph\Plot\AccBarPlot;
use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\GroupBarPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use Amenadiel\JpGraph\Plot\PiePlot;
use Amenadiel\JpGraph\Plot\PiePlotC;
use Amenadiel\JpGraph\Themes\Theme;
use App\Models\Inumet\Observation;
use Carbon\CarbonInterval;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use RuntimeException;

use function Safe\ob_end_clean;
use function Safe\ob_start;

trait GeneratesGraphs
{
    protected Theme $theme;

    protected function createGraph(int $width = 1280, int $height = 720) : Graph
    {
        // Create the Graph.
        return new Graph($width, $height);
    }

    protected function createPieGraph(int $width = 1280, int $height = 720) : PieGraph
    {
        return new PieGraph($width, $height);
    }

    protected function createPieChart(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', PieGraph $graph = null) : PieGraph
    {
        // Create the Pie Graph.
        if (is_null($graph)) {
            $graph = $this->createPieGraph();
        }
        $graph->SetTheme($this->theme);

        // Set A title for the plot
        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        // Create pie plot
        $pieChart = new PiePlot($data);

        // Setup the labels to be displayed
        $pieChart->SetLabels($labels);

        // Add and stroke
        $graph->Add($pieChart);

        return $graph;
    }

    protected function createPieChartC(array $data, array $labels, string $title = '', string $midTitle = '', PieGraph $graph = null) : PieGraph
    {
        // Create the Pie Graph.
        if (is_null($graph)) {
            $graph = $this->createPieGraph();
        }
        $graph->SetTheme($this->theme);

        // Set A title for the plot
        $graph->title->Set($title);
        // $graph->subtitle->Set($subtitle);
        // $graph->subsubtitle->Set($subsubtitle);

        // Create pie plot
        $pieChart = new PiePlotC($data);
        $pieChart->SetMid($midTitle);
        // Setup the labels to be displayed
        $pieChart->SetLabels($labels);

        // Add and stroke
        $graph->Add($pieChart);

        return $graph;
    }

    protected function createBarChart(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', string $xTitle = '', string $yTitle = '', string $xScale = 'textlin', Graph $graph = null) : Graph
    {
        // Create the bar chart Graph if not given
        if ($graph === null) {
            $graph = $this->createGraph();
        }

        // Now specify the X-scale and Y-scales explicitly
        $max = collect($data)->flatten()->max();
        $yMin = 1;
        $yMax = 1;

        // Only do this for percentages
        if ($max < 1) {
            $min = collect($data)->flatten()->min();
            $step = 0.05;
            $yMin = 0;
            $yMax = $step;
            while ($yMax < $max) {
                $yMax += $step;
            }

            // Y axis should not start on zero
            if ($min < 0) {
                $yMin = $step;
                while ($yMin > $min) {
                    $yMin -= $step;
                }
            }
        }

        $graph->SetScale($xScale, $yMin, $yMax);
        $graph->SetTheme($this->theme);

        // Create the bar plots
        if (count($data) > 1) {
            $plots = [];
            foreach ($data as $key => $series) {
                $plot = new BarPlot($series);
                if (is_string($key)) {
                    $plot->SetLegend($key);
                }
                $plots[] = $plot;
            }
            // Create the grouped bar plot
            $plot = new GroupBarPlot($plots);
        } else {
            $plot = new BarPlot(reset($data));
        }

        // Width of the bars
        $plot->SetWidth(0.85);

        // add the plot(s) to the graph
        $graph->Add($plot);
        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        // Specify X-labels
        $graph->xaxis->SetTickLabels($labels);

        // Specify axis titles
        $graph->xaxis->title->Set($xTitle);
        $graph->yaxis->title->Set($yTitle);

        return $graph;
    }

    protected function createAccBarChart(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', string $xTitle = '', string $yTitle = '', string $xScale = 'textlin', Graph $graph = null) : Graph
    {
        // Create the bar chart Graph if not given
        if ($graph === null) {
            $graph = $this->createGraph();
        }

        // Now specify the X-scale and Y-scales explicitly
        $max = collect($data)->flatten()->max();
        $yMin = 1;
        $yMax = 1;

        // Only do this for percentages
        if ($max < 1) {
            $min = collect($data)->flatten()->min();
            $step = 0.05;
            $yMin = 0;
            $yMax = $step;
            while ($yMax < $max) {
                $yMax += $step;
            }

            // Y axis should not start on zero
            if ($min < 0) {
                $yMin = $step;
                while ($yMin > $min) {
                    $yMin -= $step;
                }
            }
        }

        $graph->SetScale($xScale, $yMin, $yMax);
        $graph->SetTheme($this->theme);

        // Create the bar plots
        $plots = [];
        foreach ($data as $key => $series) {
            $plot = new BarPlot($series);
            if (is_string($key)) {
                $plot->SetLegend($key);
            }
            $plots[] = $plot;
        }
        // Create the grouped bar plot
        $plot = new AccBarPlot($plots);

        // Width of the bars
        $plot->SetWidth(0.85);

        // add the plot(s) to the graph
        $graph->Add($plot);
        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        // Specify X-labels
        $graph->xaxis->SetTickLabels($labels);

        // Specify axis titles
        $graph->xaxis->title->Set($xTitle);
        $graph->yaxis->title->Set($yTitle);

        return $graph;
    }

    public function createLinePlot(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', string $scale = 'intlin', ?array $xScale = null, array $yScale = [1, 1], Graph $graph = null) : Graph
    {
        if ($graph === null) {
            $graph = $this->createGraph();
        }

        // Now specify the X-scale explicitly and Y-scale automatically
        if ($xScale === null) {
            $xScale = [min($labels), max($labels)];
        }
        $graph->SetScale($scale, $yScale[0], $yScale[1], $xScale[0], $xScale[1]);
        $graph->SetTheme($this->theme);

        foreach ($data as $key => $series) {
            $line = new LinePlot($series, $labels);
            if (is_string($key)) {
                $line->SetLegend($key);
            }
            $graph->Add($line);
        }

        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        return $graph;
    }

    public function createLinePlotMultiAxis(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', string $scale = 'intlin', ?array $xScale = null, array $yScale = [1, 1], Graph $graph = null) : Graph
    {
        if ($graph === null) {
            $graph = $this->createGraph();
        }

        // Now specify the X-scale explicitly and Y-scale automatically
        if ($xScale === null) {
            $xScale = [min($labels), max($labels)];
        }
        $graph->SetScale($scale, $yScale[0], $yScale[1], $xScale[0], $xScale[1]);
        $graph->SetTheme($this->theme);

        $i = -1;
        foreach ($data as $key => $series) {
            $line = new LinePlot($series, $labels);
            if (is_string($key)) {
                $line->SetLegend($key);
            }

            if ($i < 0) {
                $graph->Add($line);
            } else {
                $graph->AddY($i, $line);
                $graph->SetYScale($i, 'lin');
            }
            $i++;
        }

        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        return $graph;
    }

    protected function createStackedLinePlot(array $data, array $labels, string $title = '', string $subtitle = '', string $subsubtitle = '', Graph $graph = null) : Graph
    {
        // Create the bar chart Graph.
        if ($graph === null) {
            $graph = $this->createGraph();
        }

        // Now specify the X-scale and Y-scales explicitly
        $graph->SetScale('intlin', 1, 1, min($labels), max($labels));

        $graph->SetTheme($this->theme);
        $graph->graph_theme->fillLinePlots();

        $plots = [];
        foreach ($data as $key => $series) {
            $line = new LinePlot($series, $labels);
            $line->SetLegend($key);
            $plots[] = $line;
        }

        $plot = new AccLinePlot($plots);

        $graph->Add($plot);
        $graph->title->Set($title);
        $graph->subtitle->Set($subtitle);
        $graph->subsubtitle->Set($subsubtitle);

        return $graph;
    }

    private function getYearlyTicks(array $labels) : array
    {
        $majorPositions = [];
        $minorPositions = [];
        $min = collect($labels)->min();
        $max = collect($labels)->max();

        $date = Carbon::parse($min);
        while ($date->timestamp <= $max) {
            $date->addMonth();
            if ($date->month === 1) {
                if ($date->year % 2 === 0) {
                    $majorPositions[] = $date->timestamp;
                } else {
                    $minorPositions[] = $date->timestamp;
                }
            }
        }

        return [$majorPositions, $minorPositions];
    }

    /**
     *
     * @param array<int, string> $labels
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @return array<int, array<int, int|float|string>>
     */
    protected function getDailyTicks(array $labels) : array
    {
        $majorPositions = [];
        $minorPositions = [];
        $min = collect($labels)->min();
        $max = collect($labels)->max();

        $date = Carbon::parse($min)->hour(0)->minute(0)->second(0);
        while ($date->timestamp <= $max) {
            if ($date->hour === 0) {
                $majorPositions[] = $date->timestamp;
            } else {
                $minorPositions[] = $date->timestamp;
            }
            $date->addHours(12);
        }
        if ($date->day % 2 === 0) {
            $majorPositions[] = $date->timestamp;
        } else {
            $minorPositions[] = $date->timestamp;
        }
        return [$majorPositions, $minorPositions];
    }

    /**
     *
     * @param array<int, string> $labels
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @return array<int, array<int, int|float|string>>
     */
    protected function getHourlyTicks(array $labels, $tz = 'America/Montevideo') : array
    {
        $majorPositions = [];
        $minorPositions = [];
        $min = collect($labels)->min();
        $max = collect($labels)->max();

        $date = Carbon::parse($min, $tz)->startOfDay();
        while ($date->timestamp <= $max) {
            if ($date->hour % 2 === 0) {
                $majorPositions[] = $date->timestamp;
            } else {
                $minorPositions[] = $date->timestamp;
            }
            $date->addHour();
        }
        if ($date->hour % 2 === 0) {
            $majorPositions[] = $date->timestamp;
        } else {
            $minorPositions[] = $date->timestamp;
        }
        return [$majorPositions, $minorPositions];
    }

    /**
     *
     * @param array $labels
     * @param \Carbon\CarbonInterval $interval
     * @return array<int, array<int, int|float|string>>
     */
    protected function getCustomIntervalTicks(array $labels, CarbonInterval $interval, string $tz = null) : array
    {
        $majorPositions = [];
        $minorPositions = [];
        $min = collect($labels)->min();
        $max = collect($labels)->max();

        $date = Carbon::createFromTimestamp($min, $tz)->hour(0)->minute(0)->second(0);

        $unit = 'second';
        if ($interval->years > 0) {
            $unit = 'year';
        } elseif ($interval->months > 0) {
            $unit = 'month';
        } elseif ($interval->dayz > 0) {
            $unit = 'day';
        } elseif ($interval->hours > 0) {
            $unit = 'hour';
        } elseif ($interval->minutes > 0) {
            $unit = 'minute';
        }
        $unitPlural = $unit . ($unit === 'day' ? 'z' : 's');
        $add = 'add' . ucfirst($unit) . 's';

        while ($date->timestamp <= $max) {
            if ($date->$unit % ($interval->$unitPlural) === 0) {
                $majorPositions[] = $date->timestamp;
            } else {
                $minorPositions[] = $date->timestamp;
            }
            $date->$add($interval->$unitPlural / 2);
        }
        return [$majorPositions, $minorPositions];
    }

    protected function getDailyWeatherIconSeries(Collection $timeSeries)
    {
        // Add weather icon
        $day = -1;
        return $timeSeries->mapWithKeys(function (int $ts) use (&$day) {

            $date = Carbon::createFromTimestamp($ts, 'America/Montevideo');
            if ($day === $date->day) {
                return[$ts => false];
            }
            $day = $date->day;
            // Prado Station
            $observations = Observation::fromStationId(211)
                ->whereBetween('updated_at', [$date->copy()->startOfDay()->timestamp, $date->copy()->endOfDay()->timestamp])
                ->get();

            if ($observations->isEmpty()) {
                return [$ts => false];
            }
            $icon = $observations->mode('IconoEstadoActual');
            // $icon = $observations->when($observations->count() === 24, function (Collection $collection, int $value) {
            // return $collection->filter(fn ($o, $i) => $i > 6);
            // })->mode('IconoEstadoActual');
            $mostCommon = new Observation();
            $mostCommon->IconoEstadoActual = Arr::random($icon);

            if (empty($mostCommon->iconName)) {
                return [$ts => false];
            }

            return [$ts => resource_path('images/weather-icons/' . $mostCommon->iconName)];
        })->filter();
    }

    protected function getHourlyWeatherIconSeries(Collection $timeSeries)
    {
        // Add weather icon
        $hour = -1;
        return $timeSeries->mapWithKeys(function (int $ts) use (&$hour) {

            $date = Carbon::createFromTimestamp($ts, 'America/Montevideo');
            if ($hour === $date->hour) {
                return[$ts => false];
            }
            $hour = $date->hour;
            // Prado Station
            $observations = Observation::fromStationId(211)
                ->whereBetween('updated_at', [$date->copy()->startOfHour()->timestamp, $date->copy()->endOfHour()->timestamp])
                ->get();

            if ($observations->isEmpty()) {
                return [$ts => false];
            }
            $icon = $observations->mode('IconoEstadoActual');
            // $icon = $observations->when($observations->count() === 24, function (Collection $collection, int $value) {
            // return $collection->filter(fn ($o, $i) => $i > 6);
            // })->mode('IconoEstadoActual');
            $mostCommon = new Observation();
            $mostCommon->IconoEstadoActual = Arr::random($icon);

            if (empty($mostCommon->iconName)) {
                return [$ts => false];
            }

            return [$ts => resource_path('images/weather-icons/' . $mostCommon->iconName)];
        })->filter();
    }

    protected function getDailyTempAndWindSeries(Collection $timeSeries)
    {
        // Add weather icon
        $day = -1;
        return $timeSeries->mapWithKeys(function (int $ts) use (&$day) {

            $date = Carbon::createFromTimestamp($ts, 'America/Montevideo');
            if ($day === $date->day) {
                return[$ts => false];
            }
            $day = $date->day;
            // Prado Station
            $o = Observation::fromStationId(211)
                ->whereBetween('updated_at', [$date->copy()->startOfDay()->timestamp, $date->copy()->endOfDay()->timestamp])
                ->get();

            if ($o->isEmpty()) {
                return [$ts => false];
            }
            $min = $o->min('TempAire');
            $max = $o->max('TempAire');
            $wind = $o->median('IntViento');
            // $precip = $o->sum('precipHoraria');

            $msg = sprintf('%d°/%d° ~%dkm/h', $min, $max, $wind);

            return [$ts => $msg];
        })->filter();
    }

    protected function getHourlyTempAndWindSeries(Collection $timeSeries)
    {
        // Add weather icon
        $hour = -1;
        return $timeSeries->mapWithKeys(function (int $ts) use (&$hour) {

            $date = Carbon::createFromTimestamp($ts, 'America/Montevideo');
            if ($hour === $date->hour) {
                return[$ts => false];
            }
            $hour = $date->hour;
            // Prado Station
            $o = Observation::fromStationId(211)
                ->whereBetween('updated_at', [
                    $date->copy()->startOfHour()->timestamp,
                    $date->copy()->endOfHour()->timestamp,
                ])->get();

            if ($o->isEmpty()) {
                return [$ts => false];
            }
            $tmp = $o->avg('TempAire');
            $wind = $o->median('IntViento');

            $msg = sprintf('%d° ~%dkm/h', $tmp, $wind);

            return [$ts => $msg];
        })->filter();
    }

    protected function getGraphImage(Graph $graph) : string
    {
        ob_start();
        $graph->Stroke();
        $g = ob_get_contents();
        ob_end_clean();

        if (!is_string($g)) {
            throw new RuntimeException('Error generating a graph: ' . class_basename($graph));
        }
        return $g;
    }
}
