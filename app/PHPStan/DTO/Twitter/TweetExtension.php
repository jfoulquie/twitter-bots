<?php

declare(strict_types=1);

namespace App\PHPStan\DTO\Twitter;

use App\DTO\Twitter\Tweet;
use PhpParser\Node\Expr\MethodCall;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Type\ArrayType;
use PHPStan\Type\BooleanType;
use PHPStan\Type\DynamicMethodReturnTypeExtension;
use PHPStan\Type\FloatType;
use PHPStan\Type\IntegerType;
use PHPStan\Type\MixedType;
use PHPStan\Type\NullType;
use PHPStan\Type\StringType;
use PHPStan\Type\Type;
use PHPStan\Type\TypeCombinator;
use PHPStan\Type\UnionType;

class TweetExtension implements DynamicMethodReturnTypeExtension
{
    public function getClass(): string
    {
        return Tweet::class;
    }

    public function isMethodSupported(MethodReflection $methodReflection): bool
    {
        return $methodReflection->getName() === 'get';
    }

    public function getTypeFromMethodCall(
        MethodReflection $methodReflection,
        MethodCall $methodCall,
        Scope $scope
    ): Type {
        // if (count($methodCall->getArgs()) === 0) {
        //     return new ArrayType(new StringType(), new UnionType([new StringType(), new FloatType(), new IntegerType()]));
        // }

        if ($methodCall->getArgs()[0]->value instanceof \PhpParser\Node\Scalar\String_) {
            $arg = $methodCall->getArgs()[0]->value->value;
            return match ($arg) {
                'created_at' => new StringType(),
                'id' => new IntegerType(),
                'id_str' => new StringType(),
                'text' => new StringType(),
                'full_text' => new StringType(),
                'truncated' => new BooleanType(),
                'entities' => new ArrayType(new StringType(), new MixedType()),
                'source' => new StringType(),
                'in_reply_to_status_id' => TypeCombinator::union(new IntegerType(), new NullType()),
                'in_reply_to_status_id_str' => TypeCombinator::union(new StringType(), new NullType()),
                'in_reply_to_user_id' => TypeCombinator::union(new IntegerType(), new NullType()),
                'in_reply_to_user_id_str' => TypeCombinator::union(new StringType(), new NullType()),
                'in_reply_to_screen_name' => TypeCombinator::union(new StringType(), new NullType()),
                'user' => new ArrayType(new StringType(), new MixedType()),
                'geo' => TypeCombinator::union(new StringType(), new NullType()),
                'coordinates' => TypeCombinator::union(new StringType(), new NullType()),
                'place' => TypeCombinator::union(new StringType(), new NullType()),
                'contributors' => TypeCombinator::union(new StringType(), new NullType()),
                'is_quote_status' => new BooleanType(),
                'retweet_count' => new IntegerType(),
                'favorite_count' => new IntegerType(),
                'favorited' => new BooleanType(),
                'retweeted' => new BooleanType(),
                'lang' => new StringType(),
                default => TypeCombinator::union(new ArrayType(new StringType(), new MixedType()), new BooleanType(), new IntegerType(), new StringType(), new NullType()),
            };
        }
        return TypeCombinator::union(new ArrayType(new StringType(), new MixedType()), new BooleanType(), new IntegerType(), new StringType(), new NullType());
    }
}
