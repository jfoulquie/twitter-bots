<?php

declare(strict_types=1);

namespace App\PHPStan\DTO\Twitter;

use App\DTO\Twitter\User;
use PhpParser\Node\Expr\MethodCall;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Type\ArrayType;
use PHPStan\Type\BooleanType;
use PHPStan\Type\DynamicMethodReturnTypeExtension;
use PHPStan\Type\IntegerType;
use PHPStan\Type\MixedType;
use PHPStan\Type\NullType;
use PHPStan\Type\StringType;
use PHPStan\Type\Type;
use PHPStan\Type\TypeCombinator;

class UserExtension implements DynamicMethodReturnTypeExtension
{
    public function getClass(): string
    {
        return User::class;
    }

    public function isMethodSupported(MethodReflection $methodReflection): bool
    {
        return $methodReflection->getName() === 'get';
    }

    public function getTypeFromMethodCall(
        MethodReflection $methodReflection,
        MethodCall $methodCall,
        Scope $scope
    ): Type {
        // if (count($methodCall->getArgs()) === 0) {
        //     return new ArrayType(new StringType(), new UnionType([new StringType(), new FloatType(), new IntegerType()]));
        // }

        if ($methodCall->getArgs()[0]->value instanceof \PhpParser\Node\Scalar\String_) {
            $arg = $methodCall->getArgs()[0]->value->value;
            return match ($arg) {
                'id' => new IntegerType(),
                'id_str' => new StringType(),
                'name' => new StringType(),
                'screen_name' => new StringType(),
                'location' => new StringType(),
                'description' => new StringType(),
                'url' => TypeCombinator::union(new StringType(), new NullType()),
                'created_at' => new StringType(),
                'time_zone' => new StringType(),
                'lang' => new StringType(),
                'status' => new ArrayType(new StringType(), new MixedType()),
                'entities' => new ArrayType(new StringType(), new ArrayType(new StringType(), new MixedType())),
                'protected' => new BooleanType(),
                'followers_count' => new IntegerType(),
                'friends_count' => new IntegerType(),
                'listed_count' => new IntegerType(),
                'created_at' => new StringType(),
                'favourites_count' => new IntegerType(),
                'utc_offset' => TypeCombinator::union(new StringType(), new NullType()),
                'time_zone' => TypeCombinator::union(new StringType(), new NullType()),
                'geo_enabled' => new BooleanType(),
                'verified' => new BooleanType(),
                'statuses_count' => new IntegerType(),
                'lang' => TypeCombinator::union(new StringType(), new NullType()),
                'contributors_enabled' => new BooleanType(),
                'is_translator' => new BooleanType(),
                'is_translation_enabled' => new BooleanType(),
                'profile_background_color' => new StringType(),
                'profile_background_image_url' => new StringType(),
                'profile_background_image_url_https' => new StringType(),
                'profile_background_tile' => new BooleanType(),
                'profile_image_url' => new StringType(),
                'profile_image_url_https' => new StringType(),
                'profile_link_color' => new StringType(),
                'profile_sidebar_border_color' => new StringType(),
                'profile_sidebar_fill_color' => new StringType(),
                'profile_text_color' => new StringType(),
                'profile_use_background_image' => new BooleanType(),
                'has_extended_profile' => new BooleanType(),
                'default_profile' => new BooleanType(),
                'default_profile_image' => new BooleanType(),
                'following' => new BooleanType(),
                'follow_request_sent' => new BooleanType(),
                'notifications' => new BooleanType(),
                'translator_type' => new StringType(),
                'withheld_in_countries' => new ArrayType(new IntegerType(), new StringType()),
                default => TypeCombinator::union(new ArrayType(new StringType(), new MixedType()), new BooleanType(), new IntegerType(), new StringType(), new NullType()),
            };
        }
        return TypeCombinator::union(new ArrayType(new StringType(), new MixedType()), new BooleanType(), new IntegerType(), new StringType(), new NullType());
    }
}
