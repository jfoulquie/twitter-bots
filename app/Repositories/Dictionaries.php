<?php

namespace App\Repositories;

use App\Dictionaries\Dictionary;
use Illuminate\Support\Collection;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

use function Safe\json_decode;
use function Safe\preg_split;

use TypeError;

class Dictionaries
{
    private Collection $dictionaries;

    public function __construct()
    {
        $this->dictionaries = new Collection();
    }

    public function translate(string $text) : string
    {
        $originalText = collect(preg_split('//u', $text, -1, PREG_SPLIT_NO_EMPTY));
        return $originalText->reject(function (string $char) {
            $overlays = [
                '̴', // COMBINING TILDE OVERLAY U+0334
                '̵', // COMBINING SHORT STROKE OVERLAY U+0335
                '̶', // COMBINING LONG STROKE OVERLAY U+0336
                '̷', // COMBINING SHORT SOLIDUS OVERLAY U+0337
                '̸', // COMBINING LONG SOLIDUS OVERLAY U+0338
                '᪾', // COMBINING PARENTHESES OVERLAY U+1ABE
                '⃒', // COMBINING LONG VERTICAL LINE OVERLAY U+20D2
                '⃓', // COMBINING SHORT VERTICAL LINE OVERLAY U+20D3
                '⃘', // COMBINING RING OVERLAY U+20D8
                '⃙', // COMBINING CLOCKWISE RING OVERLAY U+20D9
                '⃚', // COMBINING ANTICLOCKWISE RING OVERLAY U+20DA
                '⃥', // COMBINING REVERSE SOLIDUS OVERLAY U+20E5
                '⃦', // COMBINING DOUBLE VERTICAL STROKE OVERLAY U+20E6
                '⃪', // COMBINING LEFTWARDS ARROW OVERLAY U+20EA
                '⃫', // COMBINING LONG DOUBLE SOLIDUS OVERLAY U+20EB
                '̳', // COMBINING DOUBLE LOW LINE
            ];
            return in_array($char, $overlays);
        })->map(function (string $char) {
            try {
                return $this->getDictionaryFromLetter($char)->$char;
            } catch (TypeError $e) {
                return $char;
            }
        })->implode('');
    }

    private function getDictionaryFromLetter(string $letter) : Dictionary
    {
        $this->getAllDictionaries();

        return $this->dictionaries->filter->hasChar($letter)->first();
    }

    private function getAllDictionaries() : void
    {
        if ($this->dictionaries->isNotEmpty()) {
            return;
        }

        // Retrieve all files on resources/dicts
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(resource_path('dicts'))) as $filename) {
            // filter out "." and ".."
            if ($filename->isDir()) {
                continue;
            }
            $file = $filename->openFile();
            $dict = new Dictionary(json_decode($file->fread($file->getSize()), true));
            // Add only the class name
            $this->dictionaries->put($filename->getFilename(), $dict);
        }
        $this->dictionaries = $this->dictionaries->sortKeys();
    }
}
