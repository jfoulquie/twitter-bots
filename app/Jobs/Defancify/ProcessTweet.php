<?php

namespace App\Jobs\Defancify;

use App\DTO\Twitter\Tweet;
use App\Repositories\Dictionaries;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use RuntimeException;
use Safe\Exceptions\JsonException;

use function Safe\json_decode;

class ProcessTweet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $tweetStr;
    private Tweet $tweet;
    private bool $dontReply;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $tweetStr, bool $dontReply = false)
    {
        $this->tweetStr = $tweetStr;
        $this->dontReply = $dontReply;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TwitterApio $api) : void
    {
        Log::debug('Handling tweet');
        try {
            $tweetArr = json_decode($this->tweetStr, true);
        } catch (JsonException $e) {
            Log::notice("Tweet couldn't be decoded: " . $this->tweetStr);
            return;
        }
        $this->tweet = new Tweet($tweetArr);

        // Only process replies
        if (!$this->tweet->isReply()) {
            Log::info("Tweet {$this->tweet->getStatusId()} is not a reply, ignoring");
            return;
        }

        // Don't reply to yourself
        if ($this->tweet->getReplyUserId() === Arr::get($api->getConfig(), 'bot_user_id', '')) {
            Log::info('Original tweet is from the bot, ignore');
            return;
        }

        Log::debug('Retrieving original tweet');
        $originalArr = $api->get('statuses/show', [
            'id' => $this->tweet->getReplyStatusId(),
            'tweet_mode' => 'extended',
        ]);
        if (!is_array($originalArr)) {
            Log::warning("Could not retrieve the original tweet {$this->tweet->getReplyStatusId()} from @{$this->tweet->getReplyUsername()}");
            return;
        }

        Log::debug('Translating tweet');
        $original = new Tweet($originalArr);
        $translation = app(Dictionaries::class)->translate($original->getText());

        // Don't reply to normal tweets
        if ($original->getText() === $translation) {
            Log::info('Not a fancy tweet, only a mention');
            $api->post('favorites/create', ['id' => $this->tweet->getStatusId()]);
            return;
        }

        $tweets = $this->getTweets($translation);
        $reply = $this->tweet->getStatusId();
        foreach ($tweets as $tweet) {
            Log::debug('Sending reply: ' . $tweet);
            if (!$this->dontReply) {
                // Send reply
                $response = $api->post('statuses/update', [
                    'status' => $tweet,
                    'in_reply_to_status_id' => $reply,
                ]);
                if (!is_array($response)) {
                    throw new RuntimeException('Error while tweeting');
                }
                $reply = Arr::get($response, 'id_str');
            }
        }
    }

    private function getTweets(string $translation) : array
    {
        $tweets = [];
        $tweet = '';
        $originalText = collect(mb_str_split('@' . $this->tweet->getAuthor()->getUsername() . ' ' . $translation))->reverse();
        while ($originalText->isNotEmpty()) {
            /** @phpstan-ignore-next-line */
            while (Tweet::countChars($tweet) <= 265 && $originalText->isNotEmpty()) { // Being on the safe side
                $tweet .= $originalText->pop();
            }
            // Another tweet is coming, cut the string on the previous word
            /** @phpstan-ignore-next-line */
            if ($originalText->isNotEmpty()) {
                $lastSpacePos = mb_strrpos($tweet, ' ');
                if ($lastSpacePos !== false) {
                    $remaining = trim(mb_substr($tweet, $lastSpacePos));
                    $tweet = mb_substr($tweet, 0, $lastSpacePos);
                }

                if (!empty($remaining)) {
                    $remainingArray = mb_str_split(Tweet::mbStrrev($remaining));
                    foreach ($remainingArray as $char) {
                        $originalText->push($char);
                    }
                }
            }
            $tweets[] = $tweet;
            $tweet = '';
        }

        if (count($tweets) > 1) {
            foreach ($tweets as $i => &$tweet) {
                $tweet .= ' (' . ($i + 1) . '/' . count($tweets) . ')';
            }
        }

        return $tweets;
    }
}
