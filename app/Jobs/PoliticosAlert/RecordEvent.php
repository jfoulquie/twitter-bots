<?php

namespace App\Jobs\PoliticosAlert;

use App\Models\PoliticosAlert\Event;
use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use j3j5\TwitterApio;

use Sentry;

use Throwable;

class RecordEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private TwitterUser $user;
    private string $type;
    private string $changedKey;
    private string $oldValue;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TwitterUser $user, string $type, string $changedKey, string $oldValue)
    {
        $this->user = $user;
        $this->type = $type;
        $this->changedKey = $changedKey;
        $this->oldValue = $oldValue;
        // For follows/unfollows, add a random delay so different
        // users are mixed up on the timeline and it doesn't machine-gun-tweet
        if (in_array($type, [Event::FOLLOW, EVENT::UNFOLLOW])) {
            $this->delay(random_int(1, 900));
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TwitterApio $api)
    {
        // Store the event on the db and tweet the change
        $event = Event::create([
            'user_id' => $this->user->id,
            'type' => $this->type,
            'key' => $this->changedKey,
            'old_value' => $this->oldValue,
        ]);

        // Log::debug($event->getStatusText());

        $tweetOptions = [
            'status' => $event->getStatusText(),
        ];

        // Attach the new avatars when a user changes it
        if ($this->type === Event::PROFILE && $this->changedKey === 'avatar') {
            try {
                $mediaIds = [];
                $altText = trans('polalert.old-avatar-of') . ' @' . $event->user->username . " ({$event->user->name})";
                $mediaIds[] = $this->uploadPic($api, $this->user->getAvatarFile(), $altText);

                $altText = trans('polalert.new-avatar-of') . ' @' . $event->user->username . "({$event->user->name})";
                $response = Http::retry(3, mt_rand(1000, 5000))->get($this->user->avatar)->throw();
                $mediaIds[] = $this->uploadPic($api, $response->body(), $altText);

                // Replace the old avatar locally
                Storage::delete($this->user->avatar_filepath);
                Storage::put($this->user->avatar_filepath, $response->body());

                if (count($mediaIds) === 2) {
                    $tweetOptions['media_ids'] = implode(',', $mediaIds);
                }
            } catch (Throwable $e) {
                Sentry::captureException($e);
            }
        }
        $api->post('statuses/update', $tweetOptions);

        $event->tweeted_at = now();
        $event->save();
    }

    private function uploadPic(TwitterApio $api, string $blob, string $altText) : string
    {
        // Upload old avatar pic
        $mediaResponse = $api->uploadMedia([
            'media' => $blob,
        ]);
        // Add alt text
        $api->addMetadata([
            'media_id' => $mediaResponse['media_id_string'],
            'alt_text' => [
                'text' => $altText,
            ],
        ]);

        return $mediaResponse['media_id_string'];
    }
}
