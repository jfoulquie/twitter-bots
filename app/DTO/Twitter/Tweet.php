<?php

namespace App\DTO\Twitter;

use App\Services\TwitterHelper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

class Tweet extends Fluent
{
    /**
     * App\DTO\Twitter\Tweet
     *
     * @property-read int $id
     * @property-read string $id_str
     * @property-read string $text
     * @property-read array $user
     * @property-read string $in_reply_to_user_id_str
     * @property-read string $in_reply_to_status_id_str
     * @property-read string $in_reply_to_screen_name
     *
     * @phpstan-type UserArray array{id: int, id_str: string, name: string, screen_name: string, location: string, description: string, url: string|null, created_at: string, time_zone: string, lang: string, entities: array<string, array>, protected: bool, followers_count: int, friends_count: int, listed_count: int, created_at: string, favourites_count: int, utc_offset: string|null, time_zone: string|null, geo_enabled: bool, verified: bool, statuses_count: int, lang: string|null, contributors_enabled: bool, is_translator: bool, is_translation_enabled: bool, profile_background_color: string,profile_background_image_url: string, profile_background_image_url_https: string, profile_background_tile: bool,  profile_image_url: string, profile_image_url_https: string, profile_link_color: string, profile_sidebar_border_color: string, profile_sidebar_fill_color: string, profile_text_color: string, profile_use_background_image: bool, has_extended_profile: bool, default_profile: bool, default_profile_image: bool, following: bool, follow_request_sent: bool, notifications: bool, translator_type: string, withheld_in_countries: array}
     *
     * @phpstan-type TweetArray array{created_at: string, id: int<1, max>, id_str: string, text: string, full_text: string, truncated: bool, entities: array<string, array>, source: string, in_reply_to_status_id: int<1, max>|null, in_reply_to_status_id_str: string|null, in_reply_to_user_id: int<1, max>|null, in_reply_to_user_id_str: string|null, in_reply_to_screen_name: string|null, user: UserArray, geo: string|null, coordinates: string|null, place: string|null, contributors: string|null, is_quote_status: bool, retweet_count: int<0, max>, favorite_count: int<0, max>, favorited: bool, retweeted: bool, lang: string}
     *
     */
    public const MAX_LENGTH = 280;
    private User $tweetAuthor;

    public function getStatusId() : string
    {
        return $this->get('id_str', '');
    }

    public function getText() : string
    {
        if (!empty($this->get('full_text'))) {
            return $this->get('full_text', '');
        }
        return $this->get('text', '');
    }

    public function getAuthor() : User
    {
        if (empty($this->tweetAuthor)) {
            $this->tweetAuthor = new User($this->get('user', []));
        }
        return $this->tweetAuthor;
    }

    public function isReply() : bool
    {
        return $this->getReplyStatusId() !== '';
    }

    public function getReplyStatusId() : string
    {
        return (string) $this->get('in_reply_to_status_id_str');
    }

    public function getReplyUserId() : string
    {
        return (string) $this->get('in_reply_to_user_id_str');
    }

    public function getReplyUsername() : string
    {
        return (string) $this->get('in_reply_to_screen_name');
    }

    public function getCreatedAt() : Carbon
    {
        return Carbon::parse($this->get('created_at'));
    }

    public static function countChars(string $text) : int
    {
        return app(TwitterHelper::class)->countChars($text);
    }

    public static function mbStrrev(string $string, string $encoding = null) : string
    {
        return app(TwitterHelper::class)->mbStrrev($string, $encoding);
    }
}
