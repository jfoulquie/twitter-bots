<?php

namespace App\DTO\Twitter;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Fluent;

/**
 * {
        "entities": {
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 0,
        "friends_count": 0,
        "listed_count": 0,
        "created_at": "Fri Sep 07 15:46:14 +0000 2012",
        "favourites_count": 6,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 39,
        "lang": null,
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
        "profile_image_url_https": "https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": true,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none",
        "withheld_in_countries": []
    }
 */
class User extends Fluent
{
    /**
     * App\DTO\Twitter\User
     *
     *
     * @extends \Illuminate\Support\Fluent<string, int|string|array|bool|null>
     *
     * @property-read int $id
     * @property-read string $id_str
     * @property-read string $name
     * @property-read string $screen_name
     * @property-read string $location
     * @property-read string $url
     * @property-read string $created_at
     * @property-read string $time_zone
     * @property-read string $lang
     * @property-read array $entities
     * @property-read string $profile_background_color
     * @property-read string $profile_background_image_url
     * @property-read string $profile_background_image_url_https
     * @property-read string $profile_image_url
     * @property-read string $profile_image_url_https
     * @property-read string $profile_link_color
     * @property-read string $profile_sidebar_border_color
     * @property-read string $profile_sidebar_fill_color
     * @property-read string $profile_text_color
     * @property-read string $translator_type
     *
     * @phpstan-type UserArray array{id: int, id_str: string, name: string, screen_name: string, location: string, description: string, url: string|null, created_at: string, time_zone: string, lang: string, entities: array<string, array>, protected: bool, followers_count: int, friends_count: int, listed_count: int, created_at: string, favourites_count: int, utc_offset: string|null, time_zone: string|null, geo_enabled: bool, verified: bool, statuses_count: int, lang: string|null, contributors_enabled: bool, is_translator: bool, is_translation_enabled: bool, profile_background_color: string,profile_background_image_url: string, profile_background_image_url_https: string, profile_background_tile: bool,  profile_image_url: string, profile_image_url_https: string, profile_link_color: string, profile_sidebar_border_color: string, profile_sidebar_fill_color: string, profile_text_color: string, profile_use_background_image: bool, has_extended_profile: bool, default_profile: bool, default_profile_image: bool, following: bool, follow_request_sent: bool, notifications: bool, translator_type: string, withheld_in_countries: array}
     *
     */
    private ?string $expandedUrl = null;

    public function getId() : string
    {
        return $this->get('id_str', '');
    }

    public function getUsername() : string
    {
        return $this->get('screen_name', '');
    }

    public function getName() : string
    {
        return $this->get('name', '');
    }

    public function getLocation() : string
    {
        return (string) $this->get('location');
    }

    public function getAvatar() : string
    {
        return str_replace('_normal', '_400x400', (string) $this->get('profile_image_url_https'));
    }

    public function getBanner() : string
    {
        return (string) $this->get('profile_background_image_url_https');
    }

    public function getUrl() : string
    {
        if (empty($this->get('url'))) {
            return '';
        }

        if ($this->expandedUrl !== null) {
            return $this->expandedUrl;
        }

        $url = data_get($this, 'entities.url.urls.0.expanded_url');
        if ($url !== null && $url !== '') {
            return $url;
        }
        $url = (string) $this->get('url');
        if (mb_strpos($url, 't.co') !== false) {
            $response = Http::head($url);
            $this->expandedUrl = (string) $response->effectiveUri();
            return $this->expandedUrl;
        }

        return $url;
    }

    public function getCreatedAt() : Carbon
    {
        return Carbon::parse($this->get('created_at'));
    }

    public function getLastTweet() : ?Tweet
    {
        if (!empty($this->get('status'))) {
            return new Tweet($this->get('status'));
        }

        return null;
    }
}
