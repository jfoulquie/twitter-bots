<?php

namespace App\Graphs\Misc;

use Amenadiel\JpGraph\Graph\RectPattern;
use Amenadiel\JpGraph\Plot\Gradient;

/**
 * @class RectPatternGradient
 * // Implements a solid band with a gradient
 */
class RecPatternGradient extends RectPattern
{
    private string $from;
    private string $to;
    private int $style;
    protected int $gradient;

    public function __construct(string $aColorFrom, string $aColorTo, int $aWeight = 1, int $style = GRAD_HOR)
    {
        $this->from = $aColorFrom;
        $this->to = $aColorTo;
        $this->style = $style;
        parent::__construct($aColorFrom, $aWeight);
    }

    public function SetFillGradient(string $aFromColor, string $aToColor, int $aStyle = GRAD_HOR) : void
    {
        $this->from = $aFromColor;
        $this->to = $aToColor;
        $this->style = $aStyle;
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Image\Image $aImg
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function DoPattern($aImg)
    {
        $gradient = new Gradient($aImg);

        $gradient->FilledRectangle(
            $this->rect->x,
            $this->rect->y,
            $this->rect->xe,
            $this->rect->ye,
            $this->from,
            $this->to,
            $this->style
        );
    }
}
