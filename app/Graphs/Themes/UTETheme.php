<?php

namespace App\Graphs\Themes;

/**
 * JPGraph v4.0.3 compaible
 * @author j3j5
 */

use Amenadiel\JpGraph\Plot\AccBarPlot;
use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use Amenadiel\JpGraph\Plot\PiePlot;
use Amenadiel\JpGraph\Plot\PiePlotC;
use Amenadiel\JpGraph\Plot\PlotLine;
use Amenadiel\JpGraph\Plot\PlotMark;
use OzdemirBurak\Iris\Color\Hex;

class UTETheme extends BaseTheme
{
    protected const LEGEND_FONTSIZE = 18;

    private bool $fillLinePlots = false;

    public function fillLinePlots(bool $fill = true) : void
    {
        $this->fillLinePlots = $fill;
    }

    /**
     *
     * @return array<int, string>
     */
    public function GetColorList() : array
    {
        return [
            '#ff7f00',  // biomasa
            '#ffef3d',  // solar
            '#4daf4a',  // eólica
            '#3cd7df',  // hidráulica
            '#999999',  // térmica
            '#f781bf',  // importaciones
            '#984ea3',  // DEMANDA
        ];
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function SetupGraph($graph)
    {
        $graph->SetFrame(true, $this->bgColor);
        $graph->SetBox(true, $this->bgColor);
        $graph->SetMarginColor($this->bgColor);
        // Default margin
        $graph->SetMargin(
            120,    // Left
            80,     // Right
            160,    // Top
            80      // Bottom
        );

        // Fonts
        $this->setUserFonts($graph);

        $this->setTitleStyles($graph);
        $graph->subtitle->SetMargin(6);
        $graph->subsubtitle->SetMargin(8);

        // legend
        $posX = 0.5;
        $posY = 0.96;
        if ($graph->img->original_width / $graph->img->original_height > 2) {
            $posY = 0.95;
        }
        $graph->legend->Pos($posX, $posY, 'center', 'bottom');
        $graph->legend->SetFrameWeight(0);
        $graph->legend->SetFillColor($this->bgColor);
        $graph->legend->SetColor($this->fontColor);
        $graph->legend->SetFont(FF_USERFONT3, FS_NORMAL, self::LEGEND_FONTSIZE);
        $graph->legend->SetLayout(LEGEND_HOR);
        $graph->legend->SetShadow(false);
        $graph->legend->SetMarkAbsSize(10);
        $graph->legend->SetLineWeight(10);

        $this->setupDefaultAxisStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\PieGraph $graph
     * @return void
     */
    public function SetupPieGraph($graph)
    {
        $graph->SetFrame(false);

        $this->setUserFonts($graph);
        $this->setTitleStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function PreStrokeApply($graph)
    {
        foreach ($graph->plots as $plot) {
            // Fix bug where PlotLine's legends don't get added
            if ($plot instanceof PlotLine) {
                $dummyPlotMark = new PlotMark();
                $lineStyle = 2;
                // $legend = ,
                $graph->legend->Add(
                    (fn () => $this->legend)->call($plot),
                    (fn () => $this->color)->call($plot),
                    $dummyPlotMark,
                    $lineStyle
                );
            }
        }

        if ($graph->legend->HasItems()) {
            // Make some room for the legend at the bottom
            $graph->SetMargin(
                $graph->img->raw_left_margin,
                $graph->img->raw_right_margin,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin + 40
            );
        }

        if (reset($graph->plots) instanceof LinePlot || reset($graph->plots) instanceof AccLinePlot) {
            $graph->xaxis->SetWeight(2);
            $graph->xgrid->SetColor($this->gridColor);
            $graph->xgrid->SetFill(true, 'white@0.99', 'white@0.99');
            $graph->xgrid->Show(true, true);

            $graph->xaxis->SetLabelMargin(20);
            $graph->xaxis->SetTitleMargin(30);
        }
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function ApplyPlot($plot)
    {
        switch (get_class($plot)) {
            case AccBarPlot::class:
                foreach ($plot->plots as $plot) {
                    $this->ApplyPlot($plot);
                }
                break;
            case BarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
                $plot->Clear();
                $plot->SetColor('#FFFFFF@0.99');
                $color = $this->GetNextColor();
                $hexColor = new Hex($color);
                $plot->SetFillGradient((string) $hexColor->saturate(30), (string) $hexColor->desaturate(20), GRAD_VER);
                // $plot->value->SetColor($this->fontColor);
                $plot->SetWidth(2);
                break;
            case AccLinePlot::class:
                $this->fillLinePlots();
                /**
                 * For some reason, AccLinePlot has $plots as a protected
                 * property so it's not possible to apply the style to all individual
                 * LinePlots inside, so this naughty trick is required to access
                 * the protected property.
                 *
                 * Forgive Me (OO) Father For I Have Sinned 🙏
                 */
                $theme = $this;
                (function () use ($theme) {
                    foreach ($this->plots as $plot) {
                        $theme->ApplyPlot($plot);
                    }
                })->call($plot);
                break;
            case LinePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\LinePlot $plot */
                $this->applyStylesToLinePlot($plot);
                break;
            case PiePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\PiePlot $plot */
                // Size of the chart itself
                $plot->SetSize(0.35);
                $plot->SetCenter(0.5, 0.5);
                $plot->SetSliceColors($this->GetThemeColors());
                // Enable and set policy for guide-lines. Make labels line up vertically
                // and force guide lines to always being used
                // $plot->SetGuideLines(true, false, true);
                // $plot->SetGuideLinesAdjust(5.4);
                $plot->ShowBorder(false);

                // This method adjust the position of the labels. This is given as fractions
                // of the radius of the Pie. A value < 1 will put the center of the label
                // inside the Pie and a value >= 1 will pout the center of the label outside the
                // Pie. By default the label is positioned at 0.5, in the middle of each slice.
                $plot->SetLabelPos(1);

                // Setup the label formats and what value we want to be shown (The absolute)
                // or the percentage.
                $plot->SetLabelType(PIE_VALUE_PERCENTAGE);
                $plot->value->Show();
                $plot->value->SetFont(FF_USERFONT3, FS_NORMAL, 14);
                $plot->value->SetColor($this->fontColor);
                break;
            case PiePlotC::class:
                /** @var \Amenadiel\JpGraph\Plot\PiePlotC $plot */
                // Set size of pie
                $plot->SetSize(0.46);
                $plot->SetSliceColors($this->GetThemeColors());
                $plot->ShowBorder();

                // This method adjust the position of the labels. This is given as fractions
                // of the radius of the Pie. A value < 1 will put the center of the label
                // inside the Pie and a value >= 1 will pout the center of the label outside the
                // Pie. By default the label is positioned at 0.5, in the middle of each slice.
                $plot->SetLabelPos(0.455);

                // Values
                $plot->value->Show();
                $plot->value->SetFont(FF_USERFONT3, FS_NORMAL, 12);
                $plot->value->SetColor($this->fontColor);
                $plot->value->SetFormatCallback(function ($value) {
                    [$name, $perc] = explode("\n", $value);
                    $perc = floatval(str_replace('%', '', $perc));

                    if ($perc < 2) {
                        return '';
                    }
                    return $name;
                });

                // Setup the title on the center circle
                $plot->midtitle->SetFont(FF_USERFONT3, FS_BOLD, 14);

                $plot->SetMidSize(0.65);
                // Set color for mid circle
                $plot->SetMidColor($this->bgColor);
                // Explode all slices 2 pixels
                $plot->ExplodeAll(2);
                break;
            default:
                break;
        }
    }

    private function applyStylesToLinePlot(LinePlot $plot) : void
    {
        $plot->Clear();
        $color = $this->GetNextColor();

        switch($plot->legend) {
            case 'Demanda':
                $this->setStyleForDemandPlot($plot, $color);
                break;
            case 'Demanda del día':
                $color = '#984ea3';
                $hexColor = new Hex($color);
                $plot->SetColor($color);
                $plot->SetWeight(6);
                $plot->SetBarCenter();

                $plot->mark->SetType(MARK_FILLEDCIRCLE);
                $plot->mark->SetColor($this->fontColor . '@0.25');
                $plot->mark->SetFillColor("$color@0.1");
                $plot->mark->SetSize(6);

                $plot->value->Show();
                $plot->value->SetMargin(20);
                $plot->value->SetFormat('%01.2f GW');
                $plot->value->SetColor((string) $hexColor->desaturate(15)->darken(10));
                $plot->value->SetFont(FF_USERFONT3, FS_NORMAL, self::LEGEND_FONTSIZE);
                break;
            default:
                $plot->SetColor('#238b45'); // For price spot
                $plot->SetWeight(4);
                if ($this->fillLinePlots) {
                    $hexColor = new Hex($color);
                    $plot->SetColor($this->fontColor);
                    $plot->SetWeight(1);
                    $plot->SetFillGradient((string) $hexColor->saturate(10), (string) $hexColor->desaturate(30));
                }
        }
    }

    private function setStyleForDemandPlot(LinePlot $plot, string $color) : void
    {
        $plot->SetColor($color);
        $plot->mark->SetType(MARK_FILLEDCIRCLE);
        $plot->mark->SetFillColor($color);
        if ($plot->numpoints <= 50) {
            $markWidth = 5;
        } elseif ($plot->numpoints > 50 && $plot->numpoints <= 100) {
            $markWidth = 4;
        } elseif ($plot->numpoints > 100 && $plot->numpoints <= 150) {
            $markWidth = 3;
        } else {
            $markWidth = 2;
        }
        $plot->mark->SetWidth($markWidth);

        $plot->SetWeight(3);
    }
}
