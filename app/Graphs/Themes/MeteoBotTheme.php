<?php

namespace App\Graphs\Themes;

/**
 * JPGraph v4.0.3 compaible
 * @author j3j5
 */

use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\LinePlot;

/**
 * Theme class for @meteoBotUY Graphs.
 */
class MeteoBotTheme extends BaseTheme
{
    private string $mode = 'accent';

    private array $colorList;

    private array $coldColorList = [
        1 => ['#002f61'],
        2 => ['#002f61', '#3dea28'],
        3 => ['#002f61', '#00929a', '#3dea28'],
        4 => ['#002f61', '#007393', '#00b194', '#3dea28'],
        5 => ['#002f61', '#00638c', '#00929a', '#00c189', '#3dea28'],
        6 => ['#002f61', '#005985', '#008097', '#00a599', '#00cb80', '#3dea28'],
    ];

    private array $warmColorList = [
        1 => ['#580000'],
        2 => ['#580000', '#e5a50a'],
        3 => ['#580000', '#b25700', '#e5a50a'],
        4 => ['#580000', '#993d00', '#c67100', '#e5a50a'],
        5 => ['#580000', '#8b2f00', '#b25700', '#d07e00', '#e5a50a'],
        6 => ['#580000', '#822700', '#a34700', '#bf6700', '#d48500', '#e5a50a'],
    ];

    private array $accentColorList = [
        1 => ['#f0027f'],
        2 => ['#f0027f', '#7fc97f'],
        3 => ['#f0027f', '#7fc97f', '#beaed4'],
        4 => ['#f0027f', '#7fc97f', '#beaed4', '#fdc086'],
        5 => ['#f0027f', '#7fc97f', '#beaed4', '#fdc086', '#386cb0'],
        6 => ['#f0027f', '#7fc97f', '#beaed4', '#fdc086', '#386cb0', '#bf5b17'],
        7 => ['#f0027f', '#7fc97f', '#beaed4', '#fdc086', '#386cb0', '#bf5b17', '#666666'],
    ];

    private int $totalPlots = -1;

    public function setTotalPlots(int $total) : void
    {
        $this->totalPlots = $total;
    }

    public function setColorMode(string $mode) : void
    {
        $this->mode = $mode;
    }

    /**
     *
     * @return array<int, string>
     */
    public function GetColorList() : array
    {
        $this->colorList = $this->{"{$this->mode}ColorList"};

        $colorList = end($this->colorList);
        if ($this->totalPlots > 0) {
            $colorList = isset($this->colorList[$this->totalPlots])
                ? $this->colorList[$this->totalPlots]
                : end($this->colorList);
        }

        return array_reverse($colorList);
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function SetupGraph($graph)
    {
        $graph->SetFrame(true, $this->bgColor);
        $graph->SetBox(true, $this->bgColor);
        $graph->SetMarginColor($this->bgColor);
        // Default margin
        $graph->SetMargin(
            120,    // Left
            80,     // Right
            115,    // Top
            80      // Bottom
        );

        // Fonts
        $this->setUserFonts($graph);
        $this->setTitleStyles($graph, FF_USERFONT1);

        // legend
        $graph->legend->SetFrameWeight(0);
        $graph->legend->Pos(0.5, 0.95, 'center', 'bottom');
        $graph->legend->SetFillColor($this->bgColor);
        $graph->legend->SetColor($this->fontColor);
        $graph->legend->SetFont(FF_USERFONT1, FS_NORMAL, self::LEGEND_FONTSIZE);
        $graph->legend->SetLayout(LEGEND_HOR);
        $graph->legend->SetShadow(false);
        $graph->legend->SetMarkAbsSize(10);
        $graph->legend->SetLineWeight(10);

        $this->setupDefaultAxisStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function PreStrokeApply($graph)
    {
        if ($graph->legend->HasItems()) {
            // Make some room for the legend at the bottom
            $graph->SetMargin(
                $graph->img->raw_left_margin,
                $graph->img->raw_right_margin,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin + 40
            );
        }

        if (reset($graph->plots) instanceof LinePlot || reset($graph->plots) instanceof AccLinePlot) {
            $graph->xaxis->SetWeight(2);
            $graph->xgrid->SetColor($this->gridColor);
            $graph->xgrid->SetFill(true, 'white@0.99', 'white@0.99');
            $graph->xgrid->Show(true, true);

            $graph->xaxis->SetLabelMargin(20);
            $graph->xaxis->SetTitleMargin(30);
        }
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function ApplyPlot($plot)
    {
        switch (get_class($plot)) {
            case LinePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\LinePlot $plot */
                $plot->Clear();
                $color = $this->GetNextColor();
                $plot->SetColor($color);
                $weight = 2;
                // Highlight the latest plot
                if ($this->color_index === $this->totalPlots) {
                    $weight = 6;
                }
                $plot->SetWeight($weight);
                break;
            default:
        }
    }
}
