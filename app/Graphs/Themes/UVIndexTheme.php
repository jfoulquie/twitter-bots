<?php

namespace App\Graphs\Themes;

/**
 * JPGraph v4.0.3 compaible
 * @author j3j5
 */

use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use App\Graphs\Misc\RecPatternGradient;
use Illuminate\Support\Arr;

/**
 * Theme class for @UVIndexMVD graphs.
 */
class UVIndexTheme extends BaseTheme
{
    private int $plots = 0;

    /**
     *
     * @return array<int, string>
     */
    public function GetColorList() : array
    {
        return [
            '#e01c25',
            '#e01c25',
        ];
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function PreStrokeApply($graph)
    {
        if ($graph->legend->HasItems()) {
            // Make some room for the legend at the bottom
            $graph->SetMargin(
                $graph->img->raw_left_margin,
                $graph->img->raw_right_margin,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin + 40
            );
        }

        if (reset($graph->plots) instanceof LinePlot || reset($graph->plots) instanceof AccLinePlot) {
            $graph->xaxis->SetWeight(2);
            $graph->xgrid->SetColor($this->gridColor);
            $graph->xgrid->SetFill(true, 'white@0.99', 'white@0.99');
            $graph->xgrid->Show(true, true);

            $graph->xaxis->SetLabelMargin(20);
            $graph->xaxis->SetTitleMargin(30);
        }
        $graph->yaxis->SetTitleMargin(40);

        if (count($graph->ynaxis) > 0) {
            // Make some room for the right axis(es)
            $graph->SetMargin(
                $graph->img->raw_left_margin - 40,
                $graph->img->raw_right_margin + count($graph->ynaxis) * 40,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin
            );
        }

        foreach ($graph->ynaxis as $i => $axis) {
            $axis->title->SetColor($this->fontColor);
            $axis->title->SetFont(FF_USERFONT3, FS_NORMAL, BaseTheme::AXIS_TITLE_FONTSIZE);
            $axis->SetTitleMargin(70);
            $axis->SetColor($this->axisColor, $this->fontColor);
            $axis->SetFont(FF_USERFONT3, FS_NORMAL, BaseTheme::AXIS_FONTSIZE);
            $axis->SetTickSide(SIDE_RIGHT);
            $axis->SetLabelMargin(10);
            $axis->SetWeight(2);
        }

        // // Change normal bands for gradient bands
        // // Fighting against the library... 😎 #casiHacker
        // foreach ($graph->bands as $i => $band) {
        //     $colors = Arr::wrap((fn () => (fn () => $this->color)->call($this->prect))->call($band));
        //     (fn () => $this->prect = new RecPatternGradient(Arr::last($colors), Arr::first($colors)))->call($band);
        // }
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function ApplyPlot($plot)
    {
        switch (get_class($plot)) {
            case LinePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\LinePlot $plot */
                $plot->Clear();
                $color = $this->GetNextColor();
                $plot->SetColor($color);
                $plot->SetWeight(5);
                // Hide the plots after the first (UV Index)
                if ($this->plots !== 0) {
                    $plot->SetWeight(0);
                }
                $this->plots++;
                break;
            default:
        }
    }
}
