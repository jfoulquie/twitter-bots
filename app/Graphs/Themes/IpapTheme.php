<?php

namespace App\Graphs\Themes;

/**
 * JPGraph v4.0.3 compaible
 * @author j3j5
 */

use Amenadiel\JpGraph\Plot\AccBarPlot;
use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\GroupBarPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use Amenadiel\JpGraph\Plot\PiePlot;
use Amenadiel\JpGraph\Plot\PiePlot3D;
use OzdemirBurak\Iris\Color\Hex;

class IpapTheme extends BaseTheme
{
    private int $iterations = 0;
    private array $barPlotColors;
    private bool $fillLinePlots = false;

    public function __construct()
    {
        parent::__construct();
        // Don't do anything by default
        $this->barPlotColors = [$this->GetColorList()];
    }

    public function fillLinePlots(bool $fill = true) : void
    {
        $this->fillLinePlots = $fill;
    }

    /**
     *
     * @return array<int, string>
     */
    public function GetColorList() : array
    {
        return [
            '#c0504e',
            '#435ebc',
            '#8064a3',
            '#4bacc6',
            '#9bbb60',
            '#e5a50a',
            '#999999',
        ];
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function SetupGraph($graph)
    {
        $graph->SetFrame(true, $this->bgColor);
        $graph->SetBox(true, $this->bgColor);
        $graph->SetMarginColor($this->bgColor);
        // Default margin
        $graph->SetMargin(
            120,    // Left
            80,     // Right
            115,    // Top
            80      // Bottom
        );

        // Fonts
        $this->setUserFonts($graph);

        $this->setTitleStyles($graph);

        // legend
        $graph->legend->SetFrameWeight(0);
        $graph->legend->Pos(0.5, 0.95, 'center', 'bottom');
        $graph->legend->SetFillColor($this->bgColor);
        $graph->legend->SetColor($this->fontColor);
        $graph->legend->SetFont(FF_USERFONT3, FS_NORMAL, self::LEGEND_FONTSIZE);
        $graph->legend->SetLayout(LEGEND_HOR);
        $graph->legend->SetShadow(false);
        $graph->legend->SetMarkAbsSize(10);
        $graph->legend->SetLineWeight(10);

        $this->setupDefaultAxisStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\PieGraph $graph
     * @return void
     */
    public function SetupPieGraph($graph)
    {
        $graph->SetFrame(false);

        $this->setUserFonts($graph);
        $this->setTitleStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function PreStrokeApply($graph)
    {
        if ($graph->legend->HasItems()) {
            // Make some room for the legend at the bottom
            $graph->SetMargin(
                $graph->img->raw_left_margin,
                $graph->img->raw_right_margin,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin + 40
            );
        }

        if (reset($graph->plots) instanceof LinePlot || reset($graph->plots) instanceof AccLinePlot) {
            $graph->xaxis->SetWeight(2);
            $graph->xgrid->SetColor($this->gridColor);
            $graph->xgrid->SetFill(true, 'white@0.99', 'white@0.99');
            $graph->xgrid->Show(true, true);

            $graph->xaxis->SetLabelMargin(20);
            $graph->xaxis->SetTitleMargin(30);
        }
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function ApplyPlot($plot)
    {
        switch (get_class($plot)) {
            case GroupBarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\GroupBarPlot $plot */
                $this->setColorsBasedOnPlots($plot->plots);
                // $plot->setColor($this->GetColorList());
                foreach ($plot->plots as $plot) {
                    $this->ApplyPlot($plot);
                    $this->iterations++;
                }
                break;
            case AccBarPlot::class:
                foreach ($plot->plots as $plot) {
                    $this->ApplyPlot($plot);
                }
                break;
            case BarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
                $plot->Clear();
                // Remove border
                $plot->SetColor('#FFFFFF@0.99');
                $plot->SetFillColor($this->getColorListForPlots());
                $plot->value->Show();
                $plot->value->SetFont(FF_USERFONT3, FS_NORMAL, 10);
                $plot->value->SetColor($this->fontColor);
                $plot->SetWidth(0.7);
                break;
            case AccLinePlot::class:
                $this->fillLinePlots();
                /**
                 * For some reason, AccLinePlot has $plots as a protected
                 * property so it's not possible to apply the style to all individual
                 * LinePlots inside, so this naughty trick is required to access
                 * the protected property.
                 *
                 * Forgive Me (OO) Father For I Have Sinned 🙏
                 */
                $theme = $this;
                (function () use ($theme) {
                    foreach ($this->plots as $plot) {
                        $theme->ApplyPlot($plot);
                    }
                })->call($plot);
                break;
            case LinePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\LinePlot $plot */
                $plot->Clear();
                $color = $this->GetNextColor();
                $plot->SetColor($color);
                $plot->SetWeight(3);
                if ($this->fillLinePlots) {
                    $plot->SetFillColor($color);
                }

                break;
            case PiePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\PiePlot $plot */
                // Size of the chart itself
                $plot->SetSize(0.35);
                $plot->SetCenter(0.49, 0.57);
                $plot->SetSliceColors($this->GetThemeColors());
                // Enable and set policy for guide-lines. Make labels line up vertically
                // and force guide lines to always beeing used
                $plot->SetGuideLines(true, false, true);
                // $plot->SetGuideLinesAdjust(5.4);
                $plot->ShowBorder(false);

                // This method adjust the position of the labels. This is given as fractions
                // of the radius of the Pie. A value < 1 will put the center of the label
                // inside the Pie and a value >= 1 will pout the center of the label outside the
                // Pie. By default the label is positioned at 0.5, in the middle of each slice.
                $plot->SetLabelPos(1);

                // Setup the label formats and what value we want to be shown (The absolute)
                // or the percentage.
                $plot->SetLabelType(PIE_VALUE_PERCENTAGE);
                $plot->value->Show();
                $plot->value->SetFont(FF_USERFONT3, FS_NORMAL, 16);
                $plot->value->SetColor($this->fontColor);

                break;
            case PiePlot3D::class:
                /** @var \Amenadiel\JpGraph\Plot\PiePlot3D $plot */
                $plot->SetSliceColors($this->GetThemeColors());
                break;
            default:
        }
    }

    /**
     *
     * @param array<int, \Amenadiel\JpGraph\Plot\Plot> $plots
     * @return void
     */
    protected function setColorsBasedOnPlots(array $plots) : void
    {
        $colors = $this->GetColorList();
        $this->barPlotColors = [$colors];
        for ($i = 1; $i < count($plots); $i++) {
            $colors = array_map(fn (string $color) => (string) (new Hex($color))->lighten($i * 20), $colors);
            $this->barPlotColors[] = $colors;
        }

        $this->barPlotColors = array_reverse($this->barPlotColors);
    }

    protected function getColorListForPlots() : array
    {
        return $this->barPlotColors[$this->iterations];
    }
}
