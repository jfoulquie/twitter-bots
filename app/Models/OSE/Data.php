<?php

namespace App\Models\OSE;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    protected $connection = 'ose';
    protected $guarded = ['id'];

    protected $casts = [
        'observed_at' => 'date',
        'record' => 'array',
    ];
}
