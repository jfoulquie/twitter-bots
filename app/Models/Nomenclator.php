<?php

namespace App\Models;

use App\Services\TwitterHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Nomenclator
 *
 * @property int $id
 * @property int $via
 * @property string $nombre_abreviado
 * @property string $especificacion
 * @property string $nombre
 * @property string $nombre_clasificacion
 * @property string $significado
 * @property int $comienzo_num
 * @property int $fin_num
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator query()
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereComienzoNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereEspecificacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereFinNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereNombreAbreviado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereNombreClasificacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereSignificado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nomenclator whereVia($value)
 * @mixin \Eloquent
 */
class Nomenclator extends Model
{
    use HasFactory;

    protected $connection = 'everylot';
    protected $guarded = ['id'];
    public $timestamps = false;
    //

    public function getTweets(string $user = 'everylotMVD') : array
    {
        return app(TwitterHelper::class)->splitTextIntoTweets("@$user $this->significado");
    }

    public static function findStreetOnNomenclator(string $nombre, int $depth = 0) : ?Nomenclator
    {
        $nombre = str_replace([
            'BENITO RIQUET',
            'BOULEVARD',
            'CMNO CIBILS',
            'DIECIOCHO',
            'PROFESOR DOCTOR ENRIQUE CLAVEAUX',
            'CORONEL JUAN BELINZON',
            'DOCTOR JUAN PAULLIER',
            'CANDIDO JOANICO',
        ], [
            'BENITO RIQUE',
            'BULEVAR',
            'AV CIBILS',
            '18',
            'PROF. DR. ENRIQUE CLAVEAUX',
            'CNEL. JUAN BELINZON',
            'JUAN PAULLIER',
            'JOANICO',
        ], $nombre);

        if ($depth > 2) {
            return null;
        }

        $query = self::where('nombre', 'like', $nombre)->orWhere('nombre_abreviado', 'like', $nombre);
        $nomenclator = $query->get();
        if ($nomenclator->isEmpty()) {
            switch ($depth) {
                case 0:
                    $nombre = self::possibleReplacements($nombre);
                    break;
                case 1:
                    $nombre = '%' . $nombre;
                    break;
                case 2:
                    $nombre = $nombre . '%';
                    break;
            }
            return self::findStreetOnNomenclator($nombre, ++$depth);
        } elseif ($nomenclator->count() === 1) {
            return $nomenclator->first();
        } else {
            Log::notice($nomenclator->count() . ' matches found for ' . $nombre);
        }
        return null;
    }

    private static function possibleReplacements(string $name) : string
    {
        $wordNumbers = [
            'UNO',
            'DOS',
            'TRES',
            'CUATRO',
            'CINCO',
            'SEIS',
            'SIETE',
            'OCHO',
            'NUEVE',
            'DIEZ',
            'ONCE',
            'DOCE',
            'TRECE',
            'CATORCE',
            'QUINCE',
            'DIECISEIS',
            'DIECISIETE',
            'DIECIOCHO',
            'DIECINUEVE',
            'VEINTE',
            'VEINTIUNO',
            'VEINTIDOS',
            'VEINTITRES',
            'VEINTICUATRO',
            'VEINTICINCO',
            'VEINTISEIS',
            'VEINTISIETE',
            'VEINTIOCHO',
            'VEINTINUEVE',
            'TREINTA',
            'TREINTA Y UNO',
        ];
        $numbers = array_map(fn ($n) => (string) $n, \range(1, 31));

        $original = [
            'CNEL',
            'CORONEL',
            'CAMINO',
            'CNO',
            'TENIENTE',
            'PSJE',
            'PASAJE',
            'RAMBLA',
            'RBLA',
            'DR ',
            'DR. ',
            'DOCTOR ',
            'GRAL',
            'GENERAL',
            'ARQ ',
            'ARQUITECTO ',
            'PROF ',
            'PROFESOR ',
            'ING ',
            'ING. ',
            'INGENIERO',
            'GDOR',
            'GOVERNADOR',
        ];
        $original = array_merge($original, $wordNumbers);

        $replace = [
            'CORONEL',
            'CNEL',
            'CNO',
            'CAMINO',
            'TTE',
            'PASAJE',
            'PSJE',
            'RBLA',
            'RAMBLA',
            'DOCTOR ',
            'DOCTOR ',
            'DR ',
            'GENERAL',
            'GRAL',
            'ARQUITECTO ',
            'ARQ ',
            'PROFESOR ',
            'PROF ',
            'INGENIERO',
            'INGENIERO',
            'ING.',
            'GOVERNADOR',
            'GDOR',
        ];
        $replace = array_merge($replace, $numbers);
        return str_replace($original, $replace, $name);
    }
}
