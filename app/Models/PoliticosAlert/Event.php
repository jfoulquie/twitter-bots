<?php

namespace App\Models\PoliticosAlert;

use App\Services\TwitterHelper;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use function Safe\json_decode;
use Sentry;
use Sentry\EventHint;

use Sentry\Severity;

/**
 * App\Models\PoliticosAlert\Event
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property string $type
 * @property string $key
 * @property string $old_value
 * @property string|null $tweeted_at
 * @property-read string $emoji
 * @property-read string $follow_emoji
 * @property-read string $nice_key
 * @property-read mixed $profile_emoji
 * @property-read string $unfollow_emoji
 * @property-read \App\Models\PoliticosAlert\TwitterUser $user
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereOldValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTweetedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUserId($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use HasFactory;

    public const PROFILE = 'profile';
    public const FOLLOW = 'follow';
    public const UNFOLLOW = 'unfollow';
    public const GROWTH = 'growth';

    public const HOURLY = 'hourly';
    public const DAILY = 'daily';

    protected $connection = 'pol-alert';
    protected $guarded = ['id'];

    public function user() : BelongsTo
    {
        return $this->belongsTo(TwitterUser::class, 'user_id');
    }

    public function getStatusText() : string
    {
        return match ($this->type) {
            self::PROFILE => $this->getStatusForProfile(),
            self::FOLLOW => $this->getStatusForFollow(),
            self::UNFOLLOW => $this->getStatusForUnfollow(),
            self::GROWTH => $this->getStatusForGrowth(),
            default => throw new Exception('Unknown type'),
        };
    }

    private function getStatusForProfile() : string
    {
        if ($this->key === 'verified') {
            $text = "{$this->emoji} «{$this->user->name}» https://twitter.com/{$this->user->username} ";
            $text .= $this->user->verified
                ? trans('polalert.now-verified')
                : trans('polalert.not-verified-anymore');
            return $text;
        }

        $showOldValueKeys = ['username', 'name', 'location', 'url', 'avatar', 'banner'];
        $text = "{$this->emoji} «{$this->user->name}» https://twitter.com/{$this->user->username} " . trans('polalert.has-changed');
        if (in_array($this->key, $showOldValueKeys)) {
            $text .= " {$this->nice_key}";
            if (!empty($this->old_value)) {
                $text .= ' ' . trans('polalert.from') . " « {$this->old_value} »";
            }
            $text .= ' ' . trans('polalert.to') . " « {$this->user->{$this->key}} »";
        } else {
            $text .= " {$this->nice_key} " . trans('polalert.to') . " «{$this->user->{$this->key}}»";
        }

        return $text;
    }

    private function getStatusForFollow() : string
    {
        if (!is_string($this->old_value)) {
            throw new Exception('Follow event must have old_value');
        }
        $follow = json_decode($this->old_value, true);
        $follow['name'] = app(TwitterHelper::class)->safeText($follow['name']);
        $text = "{$this->emoji} «{$this->user->name}» (https://twitter.com/{$this->user->username}) " . Arr::random([trans('polalert.now-follows'), trans('polalert.has-started-following'), trans('polalert.gave-follow')]);
        $text .= ' ' . trans('polalert.to') . " «{$follow['name']}» (https://twitter.com/{$follow['username']})";

        return $text;
    }

    private function getStatusForUnfollow() : string
    {
        if (!is_string($this->old_value)) {
            throw new Exception('Unfollow event must have old_value');
        }
        $unfollow = json_decode($this->old_value, true);
        $unfollow['name'] = app(TwitterHelper::class)->safeText($unfollow['name']);

        $text = "{$this->emoji} «{$this->user->name}» https://twitter.com/{$this->user->username} " . Arr::random([trans('polalert.has-stopped-following'), trans('polalert.does-not-follow-anymore'), trans('polalert.stopped-following')]);
        $text .= ' ' . trans('polalert.to') . " «{$unfollow['name']}» ﹫{$unfollow['username']}";
        if (isset($unfollow['error_title'])) {
            switch ($unfollow['error_title']) {
                case 'Forbidden':
                    $text .= ' (' . Arr::random(['🚷', '⛔', '🛑', '🔇', '🚫']) . ' ' . trans('polalert.suspended-account') . ')';
                    break;
                case 'Not Found Error':
                    $text .= ' (' . Arr::random(['🪦', '💀', '⚰️', '😵']) . ' ' . trans('polalert.removed-account') . ')';
                    break;
                default:
                    Sentry::captureMessage('Unknown error title for user', Severity::info(), EventHint::fromArray(['extra' => $unfollow]));
                    break;
            }
        } else {
            $text .= " (https://twitter.com/{$unfollow['username']})";
        }
        return $text;
    }

    public function getNiceKeyAttribute() : string
    {
        switch ($this->key) {
            case 'name':
            case 'description':
            case 'location':
                return trans('polalert.' . $this->key);
            case 'url':
            case 'username':
            case 'avatar':
            case 'banner':
            default:
                return $this->key;
        }
    }

    public function getStatusForGrowth()  : string
    {
        $difference = $this->user->followers_count - intval($this->old_value);
        $percentage = $difference / intval($this->old_value) * 100;

        $percNumber = round(abs($percentage));
        $countNumber = abs($difference);

        $timeframe = match ($this->key) {
            Event::HOURLY => Arr::random([trans('polalert.in-the-last-hour'), trans('polalert.since-one-hour-ago')]),
            Event::DAILY => Arr::random([trans('polaert.since-yesterday'), trans('polalert.in-the-last-day')]),
            default => throw new Exception('Unknown timeframe: ' . $this->old_value),
        };

        if ($percNumber >= TwitterUser::HOURLY_THRESHOLD_PERC || $percNumber >= TwitterUser::DAILY_THRESHOLD_PERC) {
            // Over the percentage threshold
            if ($percentage > 0) {
                $emoji = '📈';
                $action = Arr::random([
                    trans_choice('polalert.has-increased-their-number-of-followers-perc', $this->user->pronoun, ['perc' => $percNumber]),
                    trans_choice('polalert.has-increased-their-followers-perc', $this->user->pronoun, ['perc' => $percNumber]),
                    trans_choice('polalert.has-grown-perc-followers', $this->user->pronoun, ['perc' => $percNumber]),
                    trans_choice('polalert.grew-perc-their-followers', $this->user->pronoun, ['perc' => $percNumber]),
                ]);
            } else {
                $emoji = '📉';
                $action = Arr::random([
                    trans_choice('polalert.has-lost-perc-their-followers', $this->user->pronoun, ['perc' => $percNumber]),
                    trans_choice('polalert.lost-perc-their-followers', $this->user->pronoun, ['perc' => $percNumber]),
                    trans_choice('polalert.perc-followers-stopped-following', $this->user->pronoun, ['perc' => $percNumber]),
                ]);
            }
        } else {
            // Growth coming from threshold count
            if ($difference > 0) {
                $emoji = '📈';
                $action = Arr::random([
                    trans_choice('polalert.has-increased-their-number-of-followers-count', $this->user->pronoun, ['count' => $countNumber]),
                    trans_choice('polalert.has-increased-their-followers-count', $this->user->pronoun, ['count' => $countNumber]),
                    trans_choice('polalert.has-gained-count-followers', $this->user->pronoun, ['count' => $countNumber]),
                    trans_choice('polalert.increased-count-their-followers', $this->user->pronoun, ['count' => $countNumber]),
                ]);
            } else {
                $emoji = '📉';
                $action = Arr::random([
                    trans_choice('polalert.has-lost-count-followers', $this->user->pronoun, ['count' => $countNumber]),
                    trans_choice('polalert.lost-count-followers', $this->user->pronoun, ['count' => $countNumber]),
                    trans_choice('polalert.count-followers-stopped-following', $this->user->pronoun, ['count' => $countNumber]),
                ]);
            }
        }
        $actualData = trans('polalert.from') . " {$this->old_value} " . trans('polalert.to') . " {$this->user->followers_count}";

        $text = "⚠️ FOLLOWER ALERT ⚠️ «{$this->user->name}» https://twitter.com/{$this->user->username} " . PHP_EOL . PHP_EOL;
        $text .= "🤨 $emoji " . ucfirst($action) . " ($actualData) $timeframe";

        return $text;
    }

    public function getEmojiAttribute() : string
    {
        return $this->{"{$this->type}_emoji"};
    }

    public function getProfileEmojiAttribute() : string
    {
        if ($this->key === 'verified') {
            return '✅';
        }
        $emojis = ['🪞', '💄', '🤡', '🤵', '👨‍🎨', '👩‍🎨'];

        return Arr::random($emojis);
    }

    public function getFollowEmojiAttribute() : string
    {
        $emojis = ['🫂', '🧍💛🧍‍♀️', '🧍💛🧍', '🧍‍♀️💛🧍‍♀️', '🧑‍🤝‍🧑', '👬', '👭', '👫'];

        return Arr::random($emojis);
    }

    public function getUnfollowEmojiAttribute() : string
    {
        $emojis = ['💔', '⁉️', '🥺', '🙋 🙋‍♂️', '🙋‍♀️🙋', '‍‍‍🏃‍‍‍'];

        return Arr::random($emojis);
    }

    public function getOldValueAttribute(?string $value) : ?string
    {
        if (is_string($value) && $this->key === 'avatar') {
            return str_replace('_normal', '_400x400', $value);
        }

        return $value;
    }
}
