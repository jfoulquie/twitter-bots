<?php

namespace App\Models\PoliticosAlert;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PoliticosAlert\FriendsList
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array $list
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList query()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList whereList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendsList whereUserId($value)
 * @mixin \Eloquent
 */
class FriendsList extends Model
{
    use HasFactory;
    protected $connection = 'pol-alert';

    protected $guarded = ['id'];

    protected $casts = [
        'list' => 'array',
    ];
}
