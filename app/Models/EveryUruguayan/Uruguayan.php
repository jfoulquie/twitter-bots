<?php

namespace App\Models\EveryUruguayan;

use App\DTO\Twitter\Tweet;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

use function Safe\shuffle;

/**
 * App\Models\Uruguayan
 *
 * @property int $id
 * @property int $hogar_id
 * @property int $persona_id
 * @property string $identificacion
 * @property string $departamento
 * @property string|null $barrio
 * @property int $personas_vivienda
 * @property string $region
 * @property bool $estudia
 * @property string $tipo_centro
 * @property int $beca
 * @property string|null $tipo_beca
 * @property string $alfabetización
 * @property string|null $universidad
 * @property int $termino_universidad
 * @property string|null $actividad
 * @property string $ascendencia
 * @property string|null $identidad_genero
 * @property int $edad
 * @property string $sexo
 * @property string|null $condicion_actividad
 * @property bool $pobre
 * @property bool $indigente
 * @property int|null $jornada_laboral
 * @property float|null $ingresos
 * @property string|null $razon_dejar_trabajo
 * @property string|null $insti_salud
 * @property bool $aporta_caja
 * @property string|null $caja_aportes
 * @property string|null $user_id
 * @property string|null $username
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @property string|null $acceso_insti_salud
 * @property int|null $emergencia
 * @property string|null $acceso_emergencia
 * @property-read string $acceso_insti_salud_text
 * @property-read string $actividad_ingresos_text
 * @property-read string $actividad_text
 * @property-read string $alfabetizacion_text
 * @property-read string $ascendencia_text
 * @property-read string $edad_text
 * @property-read string $genero_gramatical
 * @property-read string $genero_text
 * @property-read string $identidad_text
 * @property-read string $ingresos_text
 * @property-read string $institucion_salud
 * @property-read string $lat
 * @property-read string $long
 * @property-read string $personas_hogar_text
 * @property-read string $pobreza_text
 * @property-read string $residencia_text
 * @property-read string $salud_text
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAccesoEmergencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAccesoInstiSalud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereActividad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAlfabetización($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAportaCaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAscendencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereBarrio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereBeca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCajaAportes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCondicionActividad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereDepartamento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereEdad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereEmergencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereEstudia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereHogarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereIdentidadGenero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereIdentificacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereIndigente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereIngresos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereInstiSalud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereJornadaLaboral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan wherePersonaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan wherePersonasVivienda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan wherePobre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereRazonDejarTrabajo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereSexo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTerminoUniversidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoBeca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTweetedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereUniversidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereUsername($value)
 * @property string $alfabetizacion
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAlfabetizacion($value)
 * @property-read string $aportes_text
 * @property string|null $tipo_universidad
 * @property string $años_aprobados_uni
 * @property string|null $carrera
 * @property string|null $asistencia_fdocente
 * @property int $finalizo_fdocente
 * @property string|null $tipo_fdocente
 * @property string|null $años_aprobados_fd
 * @property string|null $carrera_fd
 * @property string|null $tiempo_dejo_empleo
 * @property string $categoria_anterior_trabajo
 * @property int $aportaba_caja
 * @property bool $trabajo_antes
 * @property-read string $categoria_anterior_trabajo_text
 * @property-read string $educacion_text
 * @property-read string $estudios_text
 * @property-read string $razon_dejar_trabajo_text
 * @property-read string $universidad_nombre
 * @property-read string $universidad_text
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAportabaCaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistenciaFdocente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosFd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosUni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCarrera($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCarreraFd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCategoriaAnteriorTrabajo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereFinalizoFdocente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTiempoDejoEmpleo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoFdocente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoUniversidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTrabajoAntes($value)
 * @property string|null $asistio_preescolar
 * @property string|null $tipo_centro_preescolar
 * @property bool $recibe_comida_preescolar
 * @property int $cantidad_comidas_semanales_desayuno_pre
 * @property int $cantidad_comidas_semanales_almuerzo_pre
 * @property int $cantidad_comidas_semanales_merienda_pre
 * @property string|null $asistio_primaria
 * @property bool $finalizo_primaria
 * @property string $años_aprobados_primaria
 * @property string $años_aprobados_primaria_especial
 * @property string|null $tipo_centro_primaria
 * @property bool $recibe_comida_primaria
 * @property int $cantidad_comidas_semanales_desayuno_pri
 * @property int $cantidad_comidas_semanales_almuerzo_pri
 * @property int $cantidad_comidas_semanales_merienda_pri
 * @property string|null $asistio_media
 * @property string|null $razon_no_termino_media
 * @property string|null $tipo_liceo
 * @property string|null $años_aprobados_media_basico
 * @property string|null $años_aprobados_bachillerato
 * @property-read bool $asiste_preescolar
 * @property-read string $centro_preescolar
 * @property-read string $comidas_preescolar_text
 * @property-read string $comidas_primaria_text
 * @property-read string $preescolar_text
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioPreescolar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosBachillerato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosMediaBasico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosPrimariaEspecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesAlmuerzoPre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesAlmuerzoPri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesDesayunoPre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesDesayunoPri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesMeriendaPre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadComidasSemanalesMeriendaPri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereFinalizoPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereRazonNoTerminoMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereRecibeComidaPreescolar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereRecibeComidaPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentroPreescolar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentroPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoLiceo($value)
 * @property-read bool $asiste_liceo
 * @property-read bool $asiste_primaria
 * @property-read string $centro_media
 * @property-read string $centro_primaria
 * @property-read string $educacion_media_text
 * @property-read string $primaria_text
 * @property string|null $parentesco
 * @property int|null $cantidad_trabajos
 * @property string|null $categoria_trabajo
 * @property string|null $sector_publico
 * @property string|null $tamano_empresa
 * @property string|null $lugar_de_trabajo
 * @property int|null $ciuo_08_trabajo
 * @property string|null $forma_tributacion
 * @property string|null $bachillerato_tecnologico_curso
 * @property string|null $asistio_ed_tecnica
 * @property int $finalizo_ed_tecnica
 * @property string|null $tipo_centro_ed_tecnica
 * @property string|null $años_aprobados_ed_tecnica
 * @property string|null $ed_media_curso
 * @property string|null $asistio_terciario
 * @property string|null $tipo_centro_terciario
 * @property string|null $años_aprobados_terciario
 * @property string|null $terciario_curso
 * @property string|null $asistio_posgrado
 * @property int $finalizo_posgrado
 * @property string|null $tipo_centro_posgrado
 * @property string|null $años_aprobados_posgrado
 * @property string|null $posgrado_curso
 * @property-read string $max_education_level
 * @property-read string $max_education_text
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioEdTecnica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioPosgrado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAsistioTerciario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosEdTecnica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosPosgrado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereAñosAprobadosTerciario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereBachilleratoTecnologicoCurso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCantidadTrabajos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCategoriaTrabajo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereCiuo08Trabajo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereEdMediaCurso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereFinalizoEdTecnica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereFinalizoPosgrado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereFormaTributacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereLugarDeTrabajo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereParentesco($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan wherePosgradoCurso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereSectorPublico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTamanoEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTerciarioCurso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentroEdTecnica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentroPosgrado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Uruguayan whereTipoCentroTerciario($value)
 * @property-read string|null $barrio_text
 * @mixin \Eloquent
 */
class Uruguayan extends Model
{
    use HasFactory;
    use HasEducation, HasHealth, HasJob;

    public const MASCULINO = 'masculino';
    public const FEMENINO = 'femenino';

    protected $connection = 'everyuruguayan';
    protected $guarded = [];
    public $timestamps = false;

    protected $casts = [
        'aporta_caja' => 'bool',
        'edad' => 'int',
        'estudia' => 'bool',
        'ingresos' => 'float',
        'jornada_laboral' => 'int',
        'personas_vivienda' => 'int',
        'trabajo_antes' => 'bool',
        'recibe_comida_preescolar' => 'bool',
        'finalizo_primaria' => 'bool',
        'recibe_comida_primaria' => 'bool',
    ];

    public function getTweet() : string
    {
        $mandatory = [
            $this->residencia_text,
            $this->identidad_text,
            $this->alfabetizacion_text,
        ];

        $mandatory = array_filter($mandatory);

        $optional = [
            $this->actividad_ingresos_text,
            $this->ascendencia_text,
            $this->salud_text,
            $this->aportes_text,
            $this->educacion_text,
        ];

        $totalMandatory = array_reduce($mandatory, function ($carry, $sentence) {
            $chars = $sentence === '' ? 0 : Tweet::countChars($sentence) + 1;
            return $carry + $chars;
        }, 0);

        shuffle($optional);

        $tweet = $mandatory;
        $total = $totalMandatory;
        foreach ($optional as $sentence) {
            if (!empty($sentence) && Tweet::countChars($sentence) + $total < 280) {
                $tweet[] = $sentence;
                $total += Tweet::countChars($sentence) + 1;
            }
        }

        shuffle($tweet);
        $text = implode(' ', $tweet);

        return trim($text);
    }

    public function getIdentidadTextAttribute() : string
    {
        return $this->genero_text . ' y ' . strtolower($this->edad_text) . '.';
    }

    public function getEdadTextAttribute() : string
    {
        if ($this->edad > 0) {
            return "Tengo {$this->edad} " . Str::plural('año', $this->edad);
        }
        return Arr::random(['Aún', 'Todavía']) . ' no ' . Arr::random(['cumplí un', 'tengo un']) . ' año';
    }

    public function getResidenciaTextAttribute() : string
    {
        $text = 'Vivo en ';
        if (!empty($this->barrio_text)) {
            $text .= "{$this->barrio_text} ({$this->departamento})";

            if ($this->personas_hogar_text) {
                if ($this->personas_vivienda > 1) {
                    $text .= ' con';
                }
                $text .= " {$this->personas_hogar_text}";
            }

            if ($this->pobreza_text !== '') {
                $text .= ' y ' . strtolower($this->pobreza_text);
            } else {
                $text .= '.';
            }

            return $text;
        }
        $text .= $this->departamento;

        if ($this->personas_hogar_text) {
            if ($this->personas_vivienda > 1) {
                $text .= ' con';
            }
            $text .= " {$this->personas_hogar_text}";
        }

        if ($this->pobreza_text !== '') {
            $text .= ' y ' . strtolower($this->pobreza_text);
        } else {
            $text .= '.';
        }

        return $text;
    }

    public function getGeneroTextAttribute() : string
    {
        $text = 'Soy ';

        if ($this->edad < 2) {
            return $text . 'un bebé';
        } elseif ($this->edad >= 2 && $this->edad < 14) {
            $text .= $this->sexo === 'Hombre' ? 'un niño' : 'una niña';
            return $text;
        } elseif ($this->edad >= 14 && $this->edad < 18) {
            $text .= $this->genero_gramatical === self::FEMENINO
                ? 'una adolescente'
                : 'un adolescente';
            return $text;
        }

        if (!empty($this->identidad_genero)) {
            switch ($this->identidad_genero) {
                case 'Mujer':
                    $text .= 'mujer';
                    break;
                case 'Varón':
                    $text .= 'hombre';
                    break;
                case 'Varón trans':
                    $text .= 'un varón trans';
                    break;
                case 'Mujer trans':
                    $text .= 'una mujer trans';
                    break;
                case 'Otra':
                case 'Sin dato':
                case 'No sabe / No contesta':
                default:
                    return '';
            }
        } else {
            $text .= $this->sexo;
        }

        return ucfirst(strtolower($text));
    }

    public function getPersonasHogarTextAttribute() : string
    {
        if ($this->personas_vivienda <= 1) {
            $text = 'sol';
            $text .= $this->genero_gramatical === self::FEMENINO ? 'a' : 'o';
            return $text;
        } elseif ($this->personas_vivienda == 2) {
            return 'otra persona';
        }

        return ($this->personas_vivienda - 1) . ' personas';
    }

    public function getAscendenciaTextAttribute() : string
    {
        switch ($this->ascendencia) {
            case 'Indígena':
                return 'Tengo ascendencia índigena.';
            case 'Asiática o amarilla':
                // return 'Tengo ascendencia asiática.';
            case 'Afro o negra':
                // return 'Tengo ascendencia afro.';
            case 'Blanca':
                // return 'Tengo ascendencia caucásica.';
            case 'Sin dato':
            default:
                return '';
        }
    }

    public function getPobrezaTextAttribute() : string
    {
        if ($this->pobre) {
            if ($this->indigente) {
                return 'Soy indigente.';
            } else {
                return 'Soy pobre.';
            }
        }
        return '';
    }

    public function getBarrioTextAttribute() : ?string
    {
        switch ($this->barrio) {
            // Artículo masculino
            case 'Centro':
            case 'Cerrito':
            case 'Cerro':
            case 'Paso de la Arena  ':
            case 'Paso de las Duranas':
                return "el {$this->barrio}";

                // Artículo femenino
            case 'Unión':
                return "la {$this->barrio}";
                // Tildes y nombres cortos
            case 'Capurro, Bella Vista':
                return Arr::random(['Capurro', 'Bella Vista']);
            case 'Casabo, Pajas Blancas':
                return Arr::random(['Casabó', 'Pajas Blancas']);
            case 'Castro, P. Castellanos':
                return Arr::random(['Castro', 'Pérez Castellanos']);
            case 'Colon Centro y Noroeste':
                return 'Colón Centro y Noroeste';
            case 'Colon Sureste, Abayuba':
                return 'Colón Sureste';
            case 'Conciliacion':
                return 'Conciliación';
            case 'Cordon':
                return 'Cordón';
            case 'Ituzaingo':
                return 'Ituzaingó';
            case 'Jardines del Hipodromo':
                return 'Jardines del Hipódromo';
            case 'La Paloma, Tomkinson':
                return 'La Paloma';
            case 'Maroñas, Parque Guaraní':
                return 'Maroñas';
            case 'Mercado Modelo, Bolivar':
                return 'Mercado Modelo';
            case 'Parque Rodo':
                return 'el Parque Rodó';
            case 'Pque. Batlle, V. Dolores':
                return 'Villa Dolores';
            case 'Peñarol, Lavalleja':
                return 'Peñarol';
            case 'Pta. Rieles, Bella Italia':
                return Arr::random(['Punta de Rieles', 'Bella Italia']);
            case 'Prado, Nueva Savona':
                return Arr::random(['Prado', 'Nueva Savona']);
            case 'Tres Ombues, Victoria':
                return 'Tres Ombúes';
            case 'Villa García, Manga Rur.':
                return 'Villa García';
            case 'Villa Muñoz, Retiro':
                return 'Villa Muñoz';
            default:
                break;
        }
        return $this->barrio;
    }

    public function getGeneroGramaticalAttribute() : string
    {
        $m = self::MASCULINO;
        $f = self::FEMENINO;
        if (empty($this->identidad_genero)) {
            // No gender identity, trust sex
            return $this->sexo === 'Hombre'
                ? $m
                : $f;
        }

        // There's gender, check it and use it
        switch ($this->identidad_genero) {
            case 'Mujer':
            case 'Mujer trans':
            case 'Otra':
                return $f;
            case 'Varón':
            case 'Varón trans':
            case 'Sin dato':
            case 'No sabe / No contesta':
            default:
                return $m;
        }
    }

    private function forceString(?string $value) : string
    {
        if (is_null($value)) {
            return '';
        }
        return $value;
    }

    public function getLatAttribute() : string
    {
        return $this->getCoordinates()['lat'];
    }

    public function getLongAttribute() : string
    {
        return $this->getCoordinates()['long'];
    }

    private function getCoordinates() : array
    {
        switch ($this->departamento) {
            case 'Artigas':
                return [
                    'lat' => '-30.39692486262745',
                    'long' => '-56.46325220556882',
                ];
            case 'Canelones':
                return [
                    'lat' => '-34.527448087095685',
                    'long' => '-56.27715266336302',
                ];
            case 'Cerro Largo':
                return [
                    'lat' => '-32.36712764479953',
                    'long' => '-54.14034345741404',
                ];
            case 'Colonia':
                return [
                    'lat' => '-34.44620560102517',
                    'long' => '-57.78285393313647',
                ];
            case 'Durazno':
                return [
                    'lat' => '-33.36408874689155',
                    'long' => '-56.51717225867838',
                ];
            case 'Flores':
                return [
                    'lat' => '-33.516886976078275',
                    'long' => '-56.8965140486351',
                ];
            case 'Florida':
                return [
                    'lat' => '-34.09503413089246',
                    'long' => '-56.2206763726899',
                ];
            case 'Lavalleja':
                return [
                    'lat' => '-34.365487622753406',
                    'long' => '-55.24428912272088',
                ];
            case 'Maldonado':
                return [
                    'lat' => '-34.91365150748848',
                    'long' => '-54.956967650270805',
                ];
            case 'Montevideo':
                return $this->getMontevideoCoordinates();
            case 'Paysandú':
                return [
                    'lat' => '-32.30844524418372',
                    'long' => '-58.09857899763117',
                ];
            case 'Río Negro':
                return [
                    'lat' => '-32.696688807314466',
                    'long' => '-57.63128811656796',
                ];
            case 'Rivera':
                return [
                    'lat' => '-30.90228383258154',
                    'long' => '-55.543236198877764',
                ];
            case 'Rocha':
                return [
                    'lat' => '-34.48690769145875',
                    'long' => '-54.322500505808556',
                ];
            case 'Salto':
                return [
                    'lat' => '-31.38165654653194',
                    'long' => '-57.96633363935452',
                ];
            case 'San José':
                return [
                    'lat' => '-34.33647089813306',
                    'long' => '-56.70762328770086',
                ];
            case 'Soriano':
                return [
                    'lat' => '-33.52884392111657',
                    'long' => '-58.22360015033757',
                ];
            case 'Tacuarembó':
                return [
                    'lat' => '-31.721008027381828',
                    'long' => '-55.98433808342921',
                ];
            case 'Treinta y Tres':
                return [
                    'lat' => '-33.23371913719927',
                    'long' => '-54.38846724148145',
                ];
            default:
                return [
                    'lat' => '-32.81438967822801',
                    'long' => '-56.00490983914575',
                ];
        }
    }

    private function getMontevideoCoordinates() : array
    {
        switch ($this->barrio) {
            case 'Aguada':
                return [
                    'lat' => '-34.8913040243133',
                    'long' => '-56.187501142414405',
                ];
            case 'Aires Puros':
                return [
                    'lat' => '-34.853871500790994',
                    'long' => '-56.189339240777215',
                ];
            case 'Atahualpa':
                return [
                    'lat' => '-34.8657959926491',
                    'long' => '-56.18976841516174',
                ];
            case 'Barrio Sur':
                return [
                    'lat' => '-34.911466479208286',
                    'long' => '-56.19106931966805',
                ];
            case 'Bañados de Carrasco':
                return [
                    'lat' => '-34.840150990276555',
                    'long' => '-56.0782171536097',
                ];
            case 'Belvedere':
                return [
                    'lat' => '-34.84948571783261',
                    'long' => '-56.22359529571762',
                ];
            case 'Brazo Oriental':
                return [
                    'lat' => '-34.86291015821452',
                    'long' => '-56.17904090482227',
                ];
            case 'Buceo':
                return [
                    'lat' => '-34.89973181044902',
                    'long' => '-56.128885457694935',
                ];
            case 'Capurro, Bella Vista':
                return [
                    'lat' => '-34.87386880091008',
                    'long' => '-56.209801265157004',
                ];
            case 'Carrasco':
                return [
                    'lat' => '-34.88996923223546',
                    'long' => '-56.05478976631784',
                ];
            case 'Carrasco Norte':
                return [
                    'lat' => '-34.87371282845713',
                    'long' => '-56.06789319615116',
                ];
            case 'Casabo, Pajas Blancas':
                return [
                    'lat' => '-34.884919458799814',
                    'long' => '-56.27333738545104',
                ];
            case 'Casavalle':
                return [
                    'lat' => '-34.81784099289031',
                    'long' => '-56.17486201237044',
                ];
            case 'Castro, P. Castellanos':
                return [
                    'lat' => '-34.857220050457144',
                    'long' => '-56.15855803864289',
                ];
            case 'Centro':
                return [
                    'lat' => '-34.905803790556895',
                    'long' => '-56.19133194852996',
                ];
            case 'Cerrito':
                return [
                    'lat' => '-34.85288266693781',
                    'long' => '-56.17123031824328',
                ];
            case 'Cerro':
                return [
                    'lat' => '-34.887341248631785',
                    'long' => '-56.26019248842951',
                ];
            case 'Ciudad Vieja':
                return [
                    'lat' => '-34.90653126679127',
                    'long' => '-56.203475881871675',
                ];
            case 'Colon Centro y Noroeste':
                return [
                    'lat' => '-34.80252817614074',
                    'long' => '-56.220624150184406',
                ];
            case 'Colon Sureste, Abayuba':
                return [
                    'lat' => '-34.77878392142658',
                    'long' => '-56.210228677115175',
                ];
            case 'Conciliacion':
                return [
                    'lat' => '-34.823033617208445',
                    'long' => '-56.23502258394228',
                ];
            case 'Cordon':
                return [
                    'lat' => '-34.902970618942106',
                    'long' => '-56.179545439423094',
                ];
            case 'Flor de Maroñas':
                return [
                    'lat' => '-34.85038776772083',
                    'long' => '-56.127731948563785',
                ];
            case 'Ituzaingo':
                return [
                    'lat' => '-34.848303840408775',
                    'long' => '-56.147461787652716',
                ];
            case 'Jacinto Vera':
                return [
                    'lat' => '-34.87380182607549',
                    'long' => '-56.170654935573815',
                ];
            case 'Jardines del Hipodromo':
                return [
                    'lat' => '-34.83864960927221',
                    'long' => '-56.132304989056834',
                ];
            case 'La Blanqueada':
                return [
                    'lat' => '-34.88494266760209',
                    'long' => '-56.159658642950504',
                ];
            case 'La Comercial':
                return [
                    'lat' => '-34.889169750879155',
                    'long' => '-56.169348232252084',
                ];
            case 'La Figurita':
                return [
                    'lat' => '-34.874584984079206',
                    'long' => '-56.17782282200334',
                ];
            case 'La Paloma, Tomkinson':
                return [
                    'lat' => '-34.861803644460664',
                    'long' => '-56.2617051651632',
                ];
            case 'La Teja':
                return [
                    'lat' => '-34.86258995949955',
                    'long' => '-56.23182722207394',
                ];
            case 'Larrañaga':
                return [
                    'lat' => '-34.87905303514632',
                    'long' => '-56.16092003734447',
                ];
            case 'Las Acacias':
                return [
                    'lat' => '-34.84104074271178',
                    'long' => '-56.16046093550831',
                ];
            case 'Las Canteras':
                return [
                    'lat' => '-34.86357031625541',
                    'long' => '-56.106279152838106',
                ];
            case 'Lezica, Melilla':
                return [
                    'lat' => '-34.78668093555489',
                    'long' => '-56.25545586527284',
                ];
            case 'Malvín':
                return [
                    'lat' => '-34.89246571038392',
                    'long' => '-56.09901113198478',
                ];
            case 'Malvín Norte':
                return [
                    'lat' => '-34.8775941826168',
                    'long' => '-56.118139612447656',
                ];
            case 'Manga':
                return [
                    'lat' => '-34.80682300939121',
                    'long' => '-56.13809061091802',
                ];
            case 'Manga, Toledo Chico':
                return [
                    'lat' => '-34.76420695605422',
                    'long' => '-56.14342173252156',
                ];
            case 'Maroñas, Parque Guaraní':
                return [
                    'lat' => '-34.860999661393826',
                    'long' => '-56.122510718777086',
                ];
            case 'Mercado Modelo, Bolivar':
                return [
                    'lat' => '-34.86758954971897',
                    'long' => '-56.156018547181624',
                ];
            case 'Nuevo Paris':
                return [
                    'lat' => '-34.84097271430359',
                    'long' => '-56.244905890040165',
                ];
            case 'Palermo':
                return [
                    'lat' => '-34.91054465179351',
                    'long' => '-56.17998592562275',
                ];
            case 'Parque Rodo':
                return [
                    'lat' => '-34.912252318336755',
                    'long' => '-56.1665672465792',
                ];
            case 'Paso de la Arena':
                return [
                    'lat' => '-34.836159125142764',
                    'long' => '-56.27265360502431',
                ];
            case 'Paso de las Duranas':
                return [
                    'lat' => '-34.84602107150677',
                    'long' => '-56.20249340155605',
                ];
            case 'Peñarol, Lavalleja':
                return [
                    'lat' => '-34.825658255205205',
                    'long' => '-56.2019813248949',
                ];
            case 'Piedras Blancas':
                return [
                    'lat' => '-34.82128748201427',
                    'long' => '-56.14007188019676',
                ];
            case 'Pocitos':
                return [
                    'lat' => '-34.911380520592445',
                    'long' => '-56.145336846701696',
                ];
            case 'Pque. Batlle, V. Dolores':
                return [
                    'lat' => '-34.89393730321813',
                    'long' => '-56.15300844827333',
                ];
            case 'Prado, Nueva Savona':
                return [
                    'lat' => '-34.859863825373715',
                    'long' => '-56.20584523857776',
                ];
            case 'Pta. Rieles, Bella Italia':
                return [
                    'lat' => '-34.8249030029035',
                    'long' => '-56.10247613594721',
                ];
            case 'Punta Carretas':
                return [
                    'lat' => '-34.92139697808958',
                    'long' => '-56.15415749510406',
                ];
            case 'Punta Gorda':
                return [
                    'lat' => '-34.898652943345695',
                    'long' => '-56.08127515077347',
                ];
            case 'Reducto':
                return [
                    'lat' => '-34.877525186338765',
                    'long' => '-56.18720503067683',
                ];
            case 'Sayago':
                return [
                    'lat' => '-34.83652012678104',
                    'long' => '-56.215193634765264',
                ];
            case 'Tres Cruces':
                return [
                    'lat' => '-34.89377078142991',
                    'long' => '-56.16633785634403',
                ];
            case 'Tres Ombues, Victoria':
                return [
                    'lat' => '-34.85998908692362',
                    'long' => '-56.240198861220826',
                ];
            case 'Unión':
                return [
                    'lat' => '-34.876409284583694',
                    'long' => '-56.14292428004719',
                ];
            case 'Villa Española':
                return [
                    'lat' => '-34.86496885066281',
                    'long' => '-56.1460833383426',
                ];
            case 'Villa García, Manga Rur.':
                return [
                    'lat' => '-34.77808353868332',
                    'long' => '-56.06313709727067',
                ];
            case 'Villa Muñoz, Retiro':
                return [
                    'lat' => '-34.887925451496265',
                    'long' => '-56.17679104198447',
                ];
            default:
                return [
                    'lat' => '-34.900831396268224',
                    'long' => '-56.16458554858006',
                ];
        }
    }
}
