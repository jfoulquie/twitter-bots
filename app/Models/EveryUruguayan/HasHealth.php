<?php

namespace App\Models\EveryUruguayan;

use Illuminate\Support\Arr;

trait HasHealth
{
    public function getInstitucionSaludAttribute() : string
    {
        switch ($this->insti_salud) {
            case 'IAMC':
                return 'Me atiendo en una mutualista';
            case 'HOSPITAL POLICIAL O MILITAR':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return 'Me atiendo en el ' . strtolower($this->insti_salud);
            case 'MSP/ASSE':
                return 'Me atiendo en ASSE';
            case '7':
                return 'No cuento con asistencia de salud en ninguna institución';
            case 'SEGURO MEDICO PRIVADO':
                return 'Dispongo de un seguro médico privado';
            case 'POLICINICA MUNICIPAL':
                return 'Me atiendo en una policlínica municipal';
            case 'AREA SALUD BPS':
                return '';
            case '99':
            default:
                return '';
        }
    }

    public function getAccesoInstiSaludTextAttribute() : string
    {
        $hogar = ['hogar', 'casa', 'nucleo familiar'];
        switch ($this->acceso_insti_salud) {
            case 'A través de FONASA, miembro de este hogar':
            case 'A través de FONASA, miembro de otro hogar':
                return 'a través de FONASA';
            case 'Paga el empleador de un miembro del hogar':
                return 'porque paga el empleador de un miembro de mi ' . Arr::random($hogar);
            case 'Paga un miembro de este hogar':
                return '';
                // return 'porque paga un miembro de mi ' . Arr::random($hogar);
            case 'A través de FONASA, miembro de este hogar, miembro de este h':
            case 'A través de FONASA, miembro de otro hogar, pagando complemen':
                return 'a través de FONASA (pagando complemento)';
            case 'A través de un miembro de otro hogar':
            case 'A través de otro que no es miembro del hogar':
            case 'A través de un miembro de este hogar':
            case 'Pagando arancel':
            case 'Por bajos recursos':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return str_replace(['este'], ['mi'], $this->acceso_insti_salud);
            default:
                return '';
        }
    }

    public function getSaludTextAttribute() : string
    {
        $text = $this->institucion_salud;
        if (!empty($this->acceso_insti_salud_text)) {
            $text .= ' ' . str_replace('fonasa', 'FONASA', strtolower($this->acceso_insti_salud_text)) . '.';
        } elseif (!empty($text)) {
            $text .= '.';
        }

        if ($this->emergencia) {
            $text .= ' Tengo emergencia porque ' . str_replace('iamc', 'mutualista', strtolower($this->acceso_emergencia)) . '.';
        }

        return $text;
    }

    public function getAccesoEmergenciaAttribute(?string $value) : string
    {
        switch ($value) {
            case 'Para el empleador de un miembro del hogar':
                return ($this->personas_vivienda <= 1) ? 'La paga mi empleador' : 'La paga el empleador de un miembro de mi hogar';
            case 'Está incluida en la cuota de la IAMC':
                return 'está incluida en mi mutualista';
            case 'Paga un miembro de este hogar':
                if ($this->personas_vivienda <= 1) {
                    return 'La pago aparte';
                } elseif ($this->edad < 18 && $this->personas_vivienda === 2) {
                    // Nos tomamos la licencia artística de suponer que si viven dos, el hombre o mujer mayor será el padre o la madre.
                    // En la vida real no tiene porqué, pero hace esta lógica más sencilla, por ahora
                    $tutor = Uruguayan::where('hogar_id', $this->hogar_id)->where('persona_id', '!=', $this->persona_id)->first();
                    if ($tutor instanceof Uruguayan) {
                        $text = 'La paga mi ';
                        $text .= $tutor->genero_gramatical === Uruguayan::MASCULINO ? 'padre' : 'madre';
                    } else {
                        // I don't think we'll ever enter here but whatevs
                        $text = 'La pago yo';
                    }
                    return $text;
                }
                return 'La paga alguien de mi hogar';
            case 'Paga un miembro de otro hogar':
                return 'La paga alguien de otro hogar';
            default:
                return '';
        }
    }
}
