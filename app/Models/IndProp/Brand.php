<?php

namespace App\Models\IndProp;

use App\DTO\Twitter\Tweet;
use App\Services\MIEM\Helper;
use App\Services\MIEM\Parser;
use App\Services\TwitterHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function Safe\base64_decode;
use function Safe\preg_match;

/**
 * App\Models\IndProp\Brand
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $requestNumber
 * @property string $brand
 * @property array $niceClasses
 * @property string $requestType
 * @property string|null $requestSubType
 * @property string|null $signType
 * @property \Illuminate\Support\Carbon $receptionDate
 * @property string|null $status
 * @property string $owner
 * @property string|null $ownerNationality
 * @property string|null $ownerAddress
 * @property string|null $logo
 * @property string|null $requestUrl
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereNiceClasses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereOwnerAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereOwnerNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereReceptionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereRequestNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereRequestSubType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereRequestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereRequestUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereSignType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereUpdatedAt($value)
 * @property int $trimmed
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereTrimmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereTweetedAt($value)
 * @method static Builder|Brand toBeTrimmed()
 * @method static Builder|Brand tweetable()
 * @property-read string $logo_binary
 * @property-read string $logo_extension
 * @property-read string $mime_logo
 * @property-read string $owner_nationality
 * @property-read string $request_sub_type
 * @property-read string $request_type
 * @property-read string $show_url
 * @property-read string $sign_type
 * @property-read array<int, int> $nice_numbers
 * @property-read array $nice_numbers
 * @mixin \Eloquent
 */
class Brand extends Model
{
    use HasFactory;

    protected $connection = 'ind-prop';
    protected $guarded = ['id'];

    protected $casts = [
        'requestNumber' => 'int',
        'niceClasses' => 'array',
        'receptionDate' => 'datetime',
    ];

    public function getTweets() : array
    {
        // $this->requestSubType is nullable, shoudln't be tweetable with that on null
        $status = '📆 ' . ucfirst($this->receptionDate->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY')) . PHP_EOL;
        if ($this->requestSubType === 'primaria') {
            $status .= '‼️ Nueva solicitud de «' . mb_strtoupper($this->requestType) . '»';
        } else {
            $status .= '🔄 ' . ucfirst((string) $this->requestSubType) . ' de «' . mb_strtoupper($this->requestType) . '»';
        }
        $status .= ' tipo «' . (string) $this->signType . '»' . PHP_EOL;
        if (!empty($this->brand)) {
            $status .= '🪧 Marca: ' . app(TwitterHelper::class)->safeText($this->brand) . PHP_EOL;
        }
        $status .= '🧍 Titular: ' . app(TwitterHelper::class)->safeText($this->owner) . ' (' . $this->ownerNationality . ')' . PHP_EOL;
        $status .= '🚦 Estado: ' . $this->status . PHP_EOL;
        $status .= '🗂️ Niza: ' . implode(', ', $this->nice_numbers) . PHP_EOL;
        $status .= '🔗 ' . $this->show_url; // . PHP_EOL;
        // $status .= '📃 '. $this->requestUrl;

        $maxTweetLength = 269;
        if (Tweet::countChars($status) > $maxTweetLength) {
            $lines = explode(PHP_EOL, $status);
            $text = '';
            foreach ($lines as $line) {
                // Add back the new line
                $line .= PHP_EOL;

                if (Tweet::countChars($line) > $maxTweetLength) {
                    // The line itself is bigger than the max length, split it
                    $sentences = explode('.', $line);
                    foreach ($sentences as $sentence) {
                        $sentence .= '. ';
                        if (Tweet::countChars($text) + Tweet::countChars($sentence) <= $maxTweetLength) {
                            $text .= $sentence;
                        } else {
                            // Add the tweet, and reset the text
                            $tweets[] = $text;
                            $text = $sentence;
                        }
                    }
                } else {
                    // Replace urls for the char counting, Twitter uses t.co shortener so all urls become 23 chars
                    $lineShortened = str_replace((string) $this->requestUrl, str_repeat('*', 23), $line);
                    // Shall we add the new line on the same tweet or create a new one?
                    if (Tweet::countChars($text) + Tweet::countChars($lineShortened) <= $maxTweetLength) {
                        $text .= $line;
                        $text = trim($text, "\t\r\0\x0B");
                    } else {
                        // Add the tweet, and reset the text
                        $tweets[] = trim($text);
                        $text = $line;
                    }
                }
            }
            // Add remaining text
            $tweets[] = trim($text);
        } else {
            $tweets[] = $status;
        }
        $tweetsCount = count($tweets);
        if ($tweetsCount > 1) {
            foreach ($tweets as $i => &$status) {
                $status .= ' 🧵(' . ($i + 1) . '/' . $tweetsCount . ')';
            }
            unset($status);
        }

        return $tweets;
    }

    public function requestType() : Attribute
    {
        return Attribute::make(
            get: fn ($value) : string => trim(mb_strtolower((string) $value)),
        );
    }

    public function requestSubType() : Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) : string => trim(mb_strtolower((string) $value)),
        );
    }

    public function signType() : Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) : string => trim(mb_strtolower((string) $value)),
        );
    }

    public function ownerNationality() : Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) : string => app(Helper::class)->getCountryInfo((string) $value)
        );
    }

    public function logoBinary() : Attribute
    {
        return Attribute::make(
            get: function () : string {
                if (preg_match('/data:(?<mime>\w+\/\w+);base64,/', (string) $this->logo, $matches)) {
                    $image = str_replace("data:{$matches['mime']};base64,", '', (string) $this->logo);
                    return base64_decode($image);
                }
                return '';
            },
        );
    }

    public function mimeLogo() : Attribute
    {
        return Attribute::make(
            get: function () : string {
                if (preg_match('/data:(?<mime>\w+\/\w+);base64,/', (string) $this->logo, $matches)) {
                    return $matches['mime'];
                }
                return '';
            }
        );
    }

    public function logoExtension() : Attribute
    {
        return Attribute::make(
            get: fn () : string => str_replace('image/', '', $this->mime_logo)
        );
    }

    public function showUrl() : Attribute
    {
        $query = [
            'seq' => 'MA',
            'type' => 'M',
            'series' => '1',
            'number' => $this->requestNumber,
        ];
        // ?seq=PA&type=s&series=1&number=4976
        return Attribute::make(
            get: fn () : string => Parser::BRAND_SHOW . '?' . http_build_query($query)
        );
    }

    public function niceNumbers() : Attribute
    {
        return Attribute::make(
            get: fn () : array => array_keys($this->niceClasses)
        );
    }

    /**
     * Scope a query to only brands with logos to be trimmed.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTweetable(Builder $query) : Builder
    {
        return $query->whereNull('tweeted_at')->whereNotNull('requestSubType');
    }
}
