<?php

namespace App\Models\EveryFlight;

use App\Events\EveryFlight\FlightAdded;
use App\Events\EveryFlight\FlightStatusChanged;
use App\Events\EveryFlight\FlightTimeChanged;
use App\Services\EveryFlight\FlightTimes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\EveryFlight\Flight
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $type
 * @property Carbon|null $departure_time
 * @property Carbon|null $etd
 * @property Carbon|null $atd
 * @property Carbon|null $arrival_time
 * @property Carbon|null $eta
 * @property Carbon|null $ata
 * @property string|null $departure_city
 * @property string|null $arrival_city
 * @property string $airline
 * @property string|null $airline_logo
 * @property string $flight_number
 * @property string $status
 * @property-read string $actual_time
 * @property-read string $description
 * @property-read string $emoji
 * @property-read string $estimated_time
 * @property-read string $most_accurate_time
 * @property-read string $time
 * @method static \Illuminate\Database\Eloquent\Builder|Flight newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight query()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAirline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAirlineLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereArrivalCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereArrivalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAtd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereDepartureCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereDepartureTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereEta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereEtd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereFlightNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereUpdatedAt($value)
 * @property-read Carbon $carbon_time
 * @property-read string $clean_flight_number
 * @property-read string $radar_url
 * @property string|null $aircraft
 * @property-read \Illuminate\Support\Carbon|null $carbon_actual_time
 * @property-read \Illuminate\Support\Carbon|null $carbon_estimated_time
 * @property-read Carbon $carbon_most_accurate_time
 * @property-read string $city
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAircraft($value)
 * @mixin \Eloquent
 */
class Flight extends Model
{
    use HasFactory;

    public const ARRIVAL = 'arrival';
    public const DEPARTURE = 'departure';

    public const TYPES = [
        self::ARRIVAL,
        self::DEPARTURE,
    ];

    protected $guarded = ['id'];
    protected $connection = 'everyflight';

    protected $casts = [
        'arrival_time' => 'datetime',
        'departure_time' => 'datetime',
        'etd' => 'datetime',
        'atd' => 'datetime',
        'eta' => 'datetime',
        'ata' => 'datetime',
    ];

    protected array $calendarTranslations = [
        'sameDay' => '[hoy (]D [de] MMMM[) a las] LT',
        'nextDay' => '[mañana (]D [de] MMMM[) a las] LT',
        'nextWeek' => '[la semana que viene (]D [de] MMMM[) a las] LT',
        'lastDay' => '[ayer (]D [de] MMMM[) a las] LT',
        'lastWeek' => '[la semana pasada (]D [de] MMMM[) a las] LT',
        'sameElse' => 'D [de] MMMM [a las] LT',
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function (Flight $flight) {
            FlightAdded::dispatch($flight);
        });

        static::updated(function (Flight $flight) {
            if ($flight->wasChanged('status')) {
                FlightStatusChanged::dispatch($flight, $flight->getOriginal('status'));
            } else {
                if ($flight->wasChanged('etd') && !empty($flight->getOriginal('etd'))) {
                    FlightTimeChanged::dispatch($flight);
                }

                if ($flight->wasChanged('eta') && !empty($flight->getOriginal('eta'))) {
                    FlightTimeChanged::dispatch($flight);
                }
            }
        });
    }

    public function getStatusAttribute(string $value) : string
    {
        return mb_strtolower($value);
    }

    public function getEmojiAttribute() : string
    {
        switch ($this->status) {
            case FlightStatus::STATUS_LANDED:
                return '🛬';
            case FlightStatus::STATUS_DEPARTED:
                return '🛫';
            case FlightStatus::STATUS_LASTCALL:
                return '🛫🔔‼️';
            case FlightStatus::STATUS_CHECKING_IN:
                return '🛫🧳';
            case FlightStatus::STATUS_DELAYED:
                return $this->isArrival() ? '🛬🕟‼️' : '🛫🕟‼️';
            case FlightStatus::STATUS_CANCELLED:
                return '❌✈️‼️';
            case FlightStatus::STATUS_SURROUNDINGS:
                return '🛬🏙️';
            case FlightStatus::STATUS_PREBOARDING:
            case FlightStatus::STATUS_BOARDING:
                return '🛫🚶';
            case FlightStatus::STATUS_CLOSED:
                return '🛫🔒🚪';
            case FlightStatus::STATUS_ESTIMATED:
            case FlightStatus::STATUS_ONTIME:
                return $this->isArrival() ? '🛬' : '🛫';
        }
        return '';
    }

    public function isArrival() : bool
    {
        return $this->type === self::ARRIVAL;
    }

    public function isDeparture() : bool
    {
        return $this->type === self::DEPARTURE;
    }

    public function shouldIncludeRadarLink() : bool
    {
        // For departures, we only show the radar link once the flight has departed
        if ($this->isDeparture() && $this->status === FlightStatus::STATUS_DEPARTED) {
            return true;
        }

        // For arrivals, we show the radar link for all statuses once the flight departed
        // from the airport of origin
        $statuses = [
            FlightStatus::STATUS_ONTIME,
            FlightStatus::STATUS_DELAYED,
            FlightStatus::STATUS_ESTIMATED,
            FlightStatus::STATUS_SURROUNDINGS,
            FlightStatus::STATUS_DIVERTED,
        ];
        /** @var \App\Services\EveryFlight\FlightTimes $flightTimes */
        $flightTimes = app(FlightTimes::class);
        if ($this->isArrival() && in_array($this->status, $statuses, true) && $flightTimes->isLive($this)) {
            return true;
        }

        return false;
    }

    public function getAirlineAttribute(string $value) : string
    {
        switch ($value) {
            case 'COMPAÑÍA HOLANDESA DE AVIACIÓN':
                return 'KLM';
            default:
                return $value;
        }
    }

    public function getDepartureCityAttribute(?string $value) : ?string
    {
        return $this->cleanCityName($value);
    }

    public function getArrivalCityAttribute(?string $value) : ?string
    {
        return $this->cleanCityName($value);
    }

    public function getCityAttribute() : string
    {
        return (string) ($this->isArrival() ? $this->departure_city : $this->arrival_city);
    }

    private function cleanCityName(?string $city) : ?string
    {
        switch ($city) {
            case 'Aeoroparque':
                return 'Buenos Aires (Aeroparque)';
            default:
                return $city;
        }
    }

    public function getDescriptionAttribute() : string
    {
        $description = "{$this->flight_number} de {$this->airline} ";
        if ($this->isArrival()) {
            $description .= "procedente de {$this->departure_city} con destino Montevideo";
        } else {
            $description .= "con destino {$this->arrival_city}";
        }
        return $description;
    }

    public function getCarbonTimeAttribute() : Carbon
    {
        if ($this->isArrival()) {
            /** @phpstan-ignore-next-line */
            return $this->arrival_time;
        }
        /** @phpstan-ignore-next-line */
        return $this->departure_time;
    }

    public function getCarbonEstimatedTimeAttribute() : ?Carbon
    {
        if ($this->isArrival()) {
            return $this->eta;
        }
        return $this->etd;
    }

    public function getCarbonActualTimeAttribute() : ?Carbon
    {
        if ($this->isArrival()) {
            return $this->ata;
        }
        return $this->atd;
    }

    public function getCarbonMostAccurateTimeAttribute() : Carbon
    {
        if ($this->carbonActualTime instanceof Carbon) {
            return $this->carbonActualTime;
        }

        if ($this->carbonEstimatedTime instanceof Carbon) {
            return $this->carbonEstimatedTime;
        }

        return $this->carbonTime;
    }

    public function getTimeAttribute() : string
    {
        if ($this->isArrival()) {
            $time = $this->arrival_time;
        } else {
            $time = $this->departure_time;
        }

        if (!$time instanceof Carbon) {
            return '';
        }

        return $time->locale('es_UY')->calendar(now(), $this->calendarTranslations);
    }

    public function getEstimatedTimeAttribute() : string
    {
        if ($this->isArrival()) {
            $time = $this->eta;
        } else {
            $time = $this->etd;
        }

        if (!$time instanceof Carbon) {
            return '';
        }

        return $time->locale('es_UY')->calendar(now(), $this->calendarTranslations);
    }

    public function getActualTimeAttribute() : string
    {
        if ($this->isArrival()) {
            $time = $this->ata;
        } else {
            $time = $this->atd;
        }

        if (!$time instanceof Carbon) {
            return '';
        }

        return $time->locale('es_UY')->calendar(now(), $this->calendarTranslations);
    }

    public function getMostAccurateTimeAttribute() : string
    {
        if ($this->actualTime !== '') {
            return $this->actualTime;
        }

        if ($this->estimatedTime !== '') {
            return $this->estimatedTime;
        }

        return $this->time;
    }

    public function getCleanFlightNumberAttribute() : string
    {
        return str_replace(['-', ' '], '', $this->flight_number);
    }

    public function getRadarUrlAttribute() : string
    {
        return 'https://www.radarbox.com/flight/' . $this->clean_flight_number;
    }
}
