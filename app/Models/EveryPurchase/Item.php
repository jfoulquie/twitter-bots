<?php

namespace App\Models\EveryPurchase;

use App\Services\TwitterHelper;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Item
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $provider
 * @property string|null $variation
 * @property string|null $quantity
 * @property string|null $amount_per_unit
 * @property string|null $total_amount
 * @property int $purchase_id
 * @property-read \App\Models\Purchase $purchase
 * @method static \Illuminate\Database\Eloquent\Builder|Item newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Item newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Item query()
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereAmountPerUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item wherePurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereVariation($value)
 * @property int|null $provider_id
 * @method static \Illuminate\Database\Eloquent\Builder|Item whereProviderId($value)
 * @property-read \App\Models\EveryPurchase\Provider|null $providerInfo
 * @mixin \Eloquent
 */
class Item extends Model
{
    use HasFactory;

    protected $connection = 'everypurchase';
    protected $guarded = [];
    public $timestamps = false;

    public function purchase() : BelongsTo
    {
        return $this->belongsTo(Purchase::class);
    }

    public function providerInfo() : BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }

    protected function provider() : Attribute
    {
        return Attribute::make(
            get: fn ($value) => app(TwitterHelper::class)->safeText($value)
        );
    }
}
