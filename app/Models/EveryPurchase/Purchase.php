<?php

namespace App\Models\EveryPurchase;

use App\DTO\Twitter\Tweet;
use App\Services\UserAgent;
use DiDom\Document;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use function Safe\preg_match;
use function Safe\preg_replace;
use SimpleXMLElement;

use Throwable;

/**
 * App\Models\Purchase
 *
 * @property int $id
 * @property int $arce_id
 * @property string $purchase_id
 * @property string $description
 * @property string $buyer
 * @property string $buyer_sub
 * @property string $type
 * @property string $link
 * @property Carbon $published_at
 * @property string|null $total_amount
 * @property Carbon|null $bought_at
 * @property string|null $user_id
 * @property string|null $username
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @property-read string $good_description
 * @property-read array $tweets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EveryPurchase\Item[] $items
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase query()
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereArceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereBoughtAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereBuyer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereBuyerSub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase wherePurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereTweetedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purchase whereUsername($value)
 * @property-read string $clean_description
 * @property-read string $formatted_bought_at
 * @method static Builder|Purchase notTweetedYet()
 * @property int|null $source_id
 * @method static Builder|Purchase whereSourceId($value)
 * @property-read \App\Models\EveryPurchase\PurchaseSource $source
 * @property-read string $formatted_type
 * @mixin \Eloquent
 */
class Purchase extends Model
{
    use HasFactory;

    public const ID_REGEX = '#http://www\.comprasestatales\.gub\.uy/consultas/detalle/id/(?<id>\d+)#';

    protected $connection = 'everypurchase';
    protected $guarded = [];
    protected $casts = [
        'bought_at' => 'datetime',
        'published_at' => 'datetime',
    ];
    public $timestamps = false;

    /**
     * Undocumented function
     *
     * @param \SimpleXMLElement $domItem
     * @throws \Exception
     * @return self
     */
    public static function createOrUpdateFromRSSItem(SimpleXMLElement $domItem, PurchaseSource $source) : self
    {
        DB::beginTransaction();
        $data = [
            'description' => preg_replace('#<br/>.*$#', '', (string) $domItem->description),
            'link' => (string) $domItem->link,
            'source_id' => $source->id,
        ];
        $pubDate = Carbon::parse((string) $domItem->pubDate);
        if ($pubDate instanceof Carbon) {
            $data['published_at'] = $pubDate->setTimezone('America/Montevideo');
        }
        // Get numeric ID
        if (preg_match(self::ID_REGEX, $domItem->link, $matches)) {
            $data['arce_id'] = $matches['id'];
        }

        // Extract info from title
        $titleData = explode('-', $domItem->title);
        if (count($titleData) < 2) {
            throw new Exception('Unknown title format: ' . $domItem->title);
        }
        if (preg_match('#\d+/\d{4}#', $titleData[0], $matches)) {
            $data['purchase_id'] = $matches[0];
            $data['type'] = trim(str_replace($data['purchase_id'], '', $titleData[0]));
        }
        /** @var string $buyerData */
        $buyerData = str_replace($titleData[0] . '-', '', $domItem->title);
        $buyerData = explode('|', $buyerData);
        $data['buyer'] = trim($buyerData[0]);
        $data['buyer_sub'] = trim(Arr::get($buyerData, '1', ''));

        if (!isset($data['purchase_id']) || empty($data['purchase_id'])) {
            // Invalid purchase
            DB::rollBack();
            return new Purchase();
        }

        // parse the page to get the rest of the info
        $purchase = self::updateOrCreateFromLink($data['link'], $data);
        DB::commit();
        return $purchase;
    }

    public static function updateOrCreateFromLink(string $link, array $data = []) : self
    {
        $data['link'] = $link;
        /** @var \App\Services\UserAgent $userAgent */
        $userAgent = resolve(UserAgent::class);

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($link);

        if (!$response->successful()) {
            $response->throw();
        }
        try {
            $document = new Document($response->body());
            $wellChildren = $document->first('.well')->children();
            foreach ($wellChildren as $child) {
                if (preg_match('/Fecha (?:de Compra|Resolución)/', $child->text())) {
                    $date = html_entity_decode($child->first('strong')->text(), ENT_HTML5);
                    $boughtAt = Carbon::createFromFormat('d/m/Y', $date);
                    if ($boughtAt instanceof Carbon) {
                        $data['bought_at'] = $boughtAt->setTimezone('America/Montevideo')->startOfDay();
                    }
                }

                if (mb_strpos($child->text(), 'Monto Total') !== false) {
                    $data['total_amount'] = str_replace(['$ ', ',00'], ['$', ''], html_entity_decode($child->first('strong')->text(), ENT_HTML5));
                }
            }

            $purchase = self::updateOrCreate(['link' => $data['link']], $data);

            $domItems = $document->find('.item  .desc-item');
            foreach ($domItems as $domItem) {
                $title = $domItem->first('h3');
                foreach ($title->findInDocument('span') as $span) {
                    $span->remove();
                }
                $itemData['title'] = trim($title->text(), " \n\r\t\v\0 "); // Includes &nbsp;
                $itemData['provider'] = $domItem->first('.provider-name > strong')->text();
                $doc = $domItem->first('.provider-name > span')->text();
                if (preg_match('/\(RUT\s(?<rut>\d+)\)/', $doc, $match)) {
                    $provider = Provider::updateOrCreate(['doc' => $match['rut']], ['name' => $itemData['provider']]);
                } elseif (preg_match('/\([\w\s]+(?<doc>\d+)\)/u', $doc, $match)) {
                    $provider = Provider::updateOrCreate(['doc' => $match['doc']], ['name' => $itemData['provider']]);
                } else {
                    $provider = Provider::where('name', $itemData['provider'])->first();
                }
                if ($domItem->first('.list-inline')->has('li > strong')) {
                    $itemData['variation'] = $domItem->first('.list-inline')->first('li > strong')->text();
                }
                $details = $domItem->find('.list-inline')[1];
                $itemData['quantity'] = self::getCleanQuantity($details->child(1)->text());
                $itemData['amount_per_unit'] = str_replace(['$ ', ',00'], ['$', ''], $details->child(3)->text());
                $itemData['total_amount'] = str_replace(['$ ', ',00'], ['$', ''], $details->child(5)->text());
                $itemData['purchase_id'] = $purchase->id;
                if ($provider instanceof Provider) {
                    $itemData['provider_id'] = $provider->id;
                }

                Item::updateOrCreate(Arr::only($itemData, ['purchase_id', 'total_amount', 'quantity']), $itemData);
            }
        } catch (Throwable $e) {
            Log::error('Error retrieving info from Detail page', $data);
            DB::rollBack();
            throw $e;
        }

        return $purchase;
    }

    protected static function getCleanQuantity(?string $value) : ?string
    {
        if ($value === null) {
            return $value;
        }
        $value = str_replace(',00', '', $value);
        if (mb_strpos($value, 'UNIDAD') !== false) {
            $qty = intval(str_replace('UNIDAD', '', $value));
            if ($qty > 1) {
                return str_replace('UNIDAD', 'uds.', $value);
            }
            return str_replace('UNIDAD', 'ud.', $value);
        }
        return (string) $value;
    }

    public function items() : HasMany
    {
        return $this->hasMany(Item::class);
    }

    public function source() : BelongsTo
    {
        return $this->belongsTo(PurchaseSource::class, 'source_id', 'id');
    }

    public function getTweetsAttribute() : array
    {
        $tweets = [$this->buyer_sub . ' compró '];

        $descLimit = 160;
        if ($this->items->isEmpty()) {
            return [];
        } elseif ($this->items->count() === 1) {
            $tweets[0] .= $this->items->first()->title . ' - '
            . Str::limit($this->good_description, $descLimit, '…')
            . "\n\n📆 " . $this->formatted_bought_at
            . "\n🧮 " . $this->items->first()->quantity
            . "\n💸 " . $this->items->first()->total_amount
            . "\n🏪 " . $this->items->first()->provider
            . "\n" . $this->formatted_type
            . "\n🔗 " . $this->link;
        } else {
            $tweets[0] .= $this->items->count() . ' items por 💸' . $this->total_amount
                . ' (' . Str::limit($this->clean_description, $descLimit, '…') . ')'
                . "\n\n📆 " . $this->formatted_bought_at
                . "\n" . $this->formatted_type
                . "\n🔗" . $this->link;
            foreach ($this->items as $item) {
                $tweet = $item->title;
                if (!empty($this->variation)) {
                    $tweet .= ' - ' . Str::limit($this->variation, 140, '…');
                }
                $tweet .= "\n🧮 " . $item->quantity
                . "\n💸 " . $item->total_amount
                // . ' (Precio un. s/ imp.: ' . $this->amount_per_unit . ")"
                . "\n🏪 " . $item->provider;
                $tweets[] = $tweet;
            }
            $total = count($tweets);
            for ($i = 0; $i < $total; $i++) {
                $tweets[$i] .= "\n🧵 " . ($i + 1) . "/$total";
            }
        }

        // Make sure we aren't over the limit on the first tweet
        while (Tweet::countChars($tweets[0]) >= 270) { // Being on the safe side
            $lines = explode("\n", $tweets[0]);
            $firstLine = reset($lines);
            $newFirstLine = mb_substr($firstLine, 0, -2) . '…';
            $tweets[0] = str_replace($firstLine, $newFirstLine, $tweets[0]);
        }

        return $tweets;
    }

    public function getFormattedBoughtAtAttribute() : string
    {
        if ($this->bought_at instanceof Carbon) {
            return $this->bought_at->isoFormat('D/M/YYYY');
        }
        return '';
    }

    public function getCleanDescriptionAttribute() : string
    {
        $desc = $this->description;
        if ($start = mb_stripos($desc, 'Por consulta')) {
            $desc = trim(mb_substr($desc, 0, $start));
        }

        return $desc;
    }

    public function getGoodDescriptionAttribute() : string
    {
        $desc = $this->clean_description;
        if ($this->items->count() === 1 && !empty($this->items->first()->variation)) {
            $desc = $this->items->first()->variation;
        }
        return $desc;
    }

    public function getDescriptionAttribute(string $value) : string
    {
        // Replace dots with a ONE-DOT-LEADER (U+2024) to avoid links on description
        $value = str_replace('.', '․', $value);
        // Replace @ with ﹫ to avoid mentions
        $value = str_replace('@', '﹫', $value);
        return preg_replace('#<br/>.*$#', '', $value);
    }

    public function getBuyerSubAttribute(string $value) : string
    {
        switch ($value) {
            case '':
            case 'Unidad Reguladora de Servic.de Comunicaciones-URSEC':
                return 'URSEC';
            case 'Presidencia de la República y Oficinas Dependientes':
                return 'Presidencia y Otros';
            case 'Oficina de Planeamiento y Presupuesto':
                return 'OPP';
            case 'Instituto Nacional de Estadística':
                return 'INE';
            case 'Secretaría Nacional del Deporte':
            case 'Casa Militar':
            case 'AGESIC':
            case 'Oficina Nacional del Servicio Civil':
            default:
                return $value;
        }
    }

    public function getFormattedTypeAttribute() : string
    {
        switch ($this->type) {
            case 'Compra Directa':
                $emoji = '👉';
                break;
            default:
                $emoji = '🧾';
                break;
        }
        return "$emoji {$this->type}";
    }

    public function scopeNotTweetedYet(Builder $query) : Builder
    {
        return $query->whereNull('tweeted_at');
    }
}
