<?php

namespace App\Models\Inumet;

use App\Traits\EloquentTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Inumet\Variable
 *
 * @property int $id
 * @property string $variable
 * @property string $name
 * @property string|null $unit
 * @method static \Illuminate\Database\Eloquent\Builder|Variable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Variable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Variable query()
 * @method static \Illuminate\Database\Eloquent\Builder|Variable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variable whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variable whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variable whereVariable($value)
 * @mixin \Eloquent
 */
class Variable extends Model
{
    use EloquentTrait;
    use HasFactory;

    protected $connection = 'inumet';
    protected $guarded = [];
    public $timestamps = false;
}
