<?php

namespace App\Models\Inumet;

use App\Traits\EloquentTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Inumet\Station
 *
 * @property int $id
 * @property string $codename
 * @property string $name
 * @property string|null $lat
 * @property string|null $long
 * @property string|null $altitude
 * @property string|null $department
 * @property string|null $OMM
 * @property string|null $OACI
 * @property string|null $codigoPluviometrico
 * @property int|null $active
 * @property string|null $administrator
 * @property string|null $ISO3166
 * @property int|null $tipomet
 * @property int|null $tipopluvio
 * @property int|null $tipoExterna
 * @property int|null $tipoAeronautica
 * @property int|null $automatica
 * @property string|null $timezone
 * @property string|null $adminShort
 * @method static \Illuminate\Database\Eloquent\Builder|Station newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station query()
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereAdminShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereAdministrator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereAltitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereAutomatica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereCodename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereCodigoPluviometrico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereISO3166($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereOACI($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereOMM($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereTipoAeronautica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereTipoExterna($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereTipomet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereTipopluvio($value)
 * @mixin \Eloquent
 */
class Station extends Model
{
    use EloquentTrait;
    use HasFactory;

    protected $connection = 'inumet';
    protected $guarded = [];
    public $timestamps = false;
}
