<?php

namespace App\Models\Inumet;

use App\Services\Inumet;
use App\Traits\EloquentTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * App\Models\Inumet\Observation
 *
 * @property int $id
 * @property string $updated_at
 * @property int $station_id
 * @property string|null $Visibilidad
 * @property string|null $DirViento
 * @property string|null $IntViento
 * @property string|null $IntRafaga
 * @property string|null $IntVientMaxHora
 * @property string|null $TempAire
 * @property string|null $HumRelativa
 * @property string|null $TempPtoRocio
 * @property string|null $TensionVapAgua
 * @property string|null $PresAtmEst
 * @property string|null $PresAtmMar
 * @property string|null $IconoEstadoActual
 * @property string|null $Nubes
 * @property string|null $Cielo
 * @property string|null $precipHoraria
 * @property-read string $iconName
 * @method static Builder|Observation fromStation(\App\Models\Inumet\Station $station)
 * @method static Builder|Observation fromStationId(int $stationId)
 * @method static Builder|Observation newModelQuery()
 * @method static Builder|Observation newQuery()
 * @method static Builder|Observation query()
 * @method static Builder|Observation whereCielo($value)
 * @method static Builder|Observation whereDirViento($value)
 * @method static Builder|Observation whereHumRelativa($value)
 * @method static Builder|Observation whereIconoEstadoActual($value)
 * @method static Builder|Observation whereId($value)
 * @method static Builder|Observation whereIntRafaga($value)
 * @method static Builder|Observation whereIntVientMaxHora($value)
 * @method static Builder|Observation whereIntViento($value)
 * @method static Builder|Observation whereNubes($value)
 * @method static Builder|Observation wherePrecipHoraria($value)
 * @method static Builder|Observation wherePresAtmEst($value)
 * @method static Builder|Observation wherePresAtmMar($value)
 * @method static Builder|Observation whereStationId($value)
 * @method static Builder|Observation whereTempAire($value)
 * @method static Builder|Observation whereTempPtoRocio($value)
 * @method static Builder|Observation whereTensionVapAgua($value)
 * @method static Builder|Observation whereUpdatedAt($value)
 * @method static Builder|Observation whereVisibilidad($value)
 * @property-read string $icon_name
 * @mixin \Eloquent
 */
class Observation extends Model
{
    use EloquentTrait;
    use HasFactory;

    protected $connection = 'inumet';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * Get the user's first name.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function iconName(): Attribute
    {
        return Attribute::make(
            get: fn ($value) : string => str_replace('svg', 'png', Arr::get(app(Inumet::class)->icons, (string) $this->IconoEstadoActual, '')),
        );
    }

    /**
     * Scope a query to only include observations from a given station.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopeFromStation(Builder $query, Station $station) : void
    {
        $query->where('station_id', $station->id);
    }

    /**
     * Scope a query to only include observations from a given station.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopeFromStationId(Builder $query, int $stationId) : void
    {
        $query->where('station_id', $stationId);
    }
}
