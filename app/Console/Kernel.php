<?php

namespace App\Console;

use App\Console\Commands\LitClock\PostTime;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use function Safe\date_sun_info;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) : void
    {
        // Every Lot
        // $schedule->command('everylot:post')->everyThirtyMinutes()->runInBackground();
        // Every Purchase
        // $schedule->command('everypurchase:update')->everyMinute()->runInBackground();
        // $schedule->command('everypurchase:post')->everyFiveMinutes()->runInBackground();

        // Autores
        // $schedule->command('autores:post')->cron('0 */2 * * *')->runInBackground();

        // BigBo Politics
        // $schedule->command('pol-alert:update')->twiceDaily(10, 20)->runInBackground();
        // $schedule->command('pol-alert:update --no-friends')->hourly()->runInBackground();

        // IPAP
        // $schedule->command('ipap:update-sources')->everyThreeHours()->when(function () {
        // return now()->day < 12;
        // })->runInBackground();
        // $schedule->command('ipap:update-sheet')->twiceDailyAt(9, 18, 50)->runInBackground();
        // Run 15 minutes after update-sheet
        // $schedule->command('ipap:post')->twiceDailyAt(10, 19, 5)->runInBackground();

        // EveryUruguayan
        // $schedule->command('everyuru:post')->hourlyAt(30)->runInBackground();

        // EveryFlight
        // $schedule->command('everyflight:update')->everyMinute()->runInBackground();
        // $schedule->command('everyflight:post')->everyMinute()->runInBackground();

        // UV Index
        $sunInfo = date_sun_info(now()->timezone('America/Montevideo')->getTimestamp(), -34.90706976618914, -56.18601472514095);
        $sunrise = Carbon::createFromTimestamp($sunInfo['sunrise'])->timezone('America/Montevideo');
        $sunset = Carbon::createFromTimestamp($sunInfo['sunset'])->timezone('America/Montevideo');
        // Only run during the hours of the day
        $schedule->command('uv:post')
            ->everyFifteenMinutes()
            ->when(fn () => now()->between($sunrise, $sunset->copy()->addMinutes(15)))
            ->runInBackground();
        // Post the chart an hour and 10mins after sunset every day
        $schedule->command('uv:post-chart')
            ->dailyAt($sunset->copy()->addHour()->addMinutes(10)->format('H:i'))
            ->runInBackground();

        // Every Airport
        // $schedule->command('everyairport:post')->hourlyAt(7)->when(fn () => mt_rand(0, 100) <= 70)->runInBackground();

        // PanaderiasUY
        // $schedule->command('bakery:post')
        // ->hourlyAt(15)
        // ->when(function () {
        // $hour = now()->timezone(config('app.scheduling_timezone'))->hour;
        // return $hour >= 6 && $hour <= 20;
        // })
        // ->runInBackground();

        $schedule->command('cdf:post')
            ->hourlyAt(43)
            ->when(fn (): bool => in_array(now()->timezone(config('app.scheduling_timezone'))->hour, [9, 15, 21], true))
            ->runInBackground();

        // $schedule->command('meteo:post')->dailyAt('09:01')->runInBackground();

        // INUMET
        $schedule->command('inumet:get-current')->everyFifteenMinutes()->runInBackground();
        // $schedule->command('inumet:backup ' . config('github.inumet-gist'))->hourlyAt(2)->runInBackground();

        // MIEM / RegistraBot
        // $schedule->command('registra:parse-miem')->everyFifteenMinutes()->runInBackground();
        // $schedule->command('registra:post')->everyFifteenMinutes()->runInBackground();

        // UTE
        // $schedule->command('ute:update-power-info')->everyFiveMinutes()->runInBackground();
        // Update the avatar 4 times every hour and the banner twice every hour
        // $schedule->command('ute:update-charts')->hourlyAt([1,31])->runInBackground();   // no options will update banner AND avatar
        // $schedule->command('ute:update-charts --avatar-only')->hourlyAt([16, 46])->runInBackground();
        $schedule->command('ute:post --daily')->dailyAt('09:29')->runInBackground();
        $schedule->command('ute:post --weekly')->weeklyOn(1, '9:01')->runInBackground();

        // Free Peteco
        // $schedule->command('twitter:tweeted-ago Peteco_Petaca --hashtag=FreePeteco --start_date=2022-09-15')->twiceDailyAt(10, 18);

        // $schedule->command('web:up https://ebanking.brou.com.uy/frontend/loginStep1')->everyFiveMinutes()->runInBackground();

        // La Diaria
        $schedule->command('ladiaria:update')->everyFiveMinutes()->runInBackground();
        $schedule->command('ladiaria:covers')->dailyAt('07:59')->runInBackground();

        // Sci-hub, run daily at 12:06
        $schedule->command('scihub:update-domain-list')->dailyAt('12:06')->runInBackground();

        // Reloj Literario
        $schedule->command('litclock:percentage')->dailyAt('15:31');
        $schedule->command('litclock:post-time')->hourly();

        $schedule->command('litclock:post-time')->everyMinute()->when(function () {
            $currentMinute = now()->minute;
            if ($currentMinute === 0) {
                return false;
            }

            $timesPosted = Cache::get(PostTime::$cacheKey);
            if ($timesPosted > 1) {
                $error = 'Already posted ' . $timesPosted . ' at ' . PostTime::$cacheKey;
                Log::debug($error);
                return false;
            }
            return random_int(0, 100) + $currentMinute > 90;
        });

        # Wikipedia Alert
        $schedule->command('wiki:notability-deletion')->everyThirtyMinutes()->runInBackground();

        # Wikipedia ES Top Views
        $schedule->command('wiki:top-views 10')->dailyAt('09:03')->runInBackground();

        # @Ciclovia18
        $schedule->command('bici:post --type=hourly')->hourlyAt('32')->runInBackground();
        $schedule->command('bici:post --type=daily')->dailyAt('09:17')->runInBackground();
        $schedule->command('bici:post --type=weekly')->weeklyOn(1, '9:02')->runInBackground();

        # OSE
        // $schedule->command('ose:get-data')->dailyAt('16:07')->runInBackground();
        $schedule->command('ose:get-data')->hourlyAt('04')->runInBackground();

        # Lynch
        $schedule->command('lynch:weather')->dailyAt('12:17')->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() : void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return config('app.scheduling_timezone');
    }
}
