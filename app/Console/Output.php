<?php

namespace App\Console;

use Symfony\Component\Console\Output\Output as SymfonyOutput;
use Symfony\Component\Console\Output\OutputInterface;

class Output extends SymfonyOutput implements OutputInterface
{
    protected function doWrite(string $message, bool $newLine) : void
    {
        echo $message;
        if ($newLine) {
            echo PHP_EOL;
        }
    }
}
