<?php

namespace App\Console\Commands\Inumet;

use App\Models\Inumet\Station;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use RuntimeException;

use function Safe\file_get_contents;
use function Safe\json_decode;

class ImportStations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inumet:import-stations {--file=resources/data/estaciones-inumet.json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from all stations from INUMET';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->option('file');
        if (!is_string($file)) {
            throw new RuntimeException('File needs to be a string');
        }
        $data = json_decode(file_get_contents($file), true);

        foreach ($data['estaciones'] as $estacion) {
            dump($estacion);
            Station::updateOrCreate(['id' => $estacion['id']], [
                'codename' => Arr::get($estacion, 'Estacion'),
                'name' => Arr::get($estacion, 'NombreEstacion'),
                'lat' => Arr::get($estacion, 'Latitud'),
                'long' => Arr::get($estacion, 'Longitud'),
                'altitude' => Arr::get($estacion, 'Altitud'),
                'department' => Arr::get($estacion, 'departamento'),

                'OMM' => Arr::get($estacion, 'codigoOMM'),
                'OACI' => Arr::get($estacion, 'idOACI'),
                'codigoPluviometrico' => Arr::get($estacion, 'codigoPluviometrico'),

                'active' => Arr::get($estacion, 'activa'),
                'administrator' => Arr::get($estacion, 'administracion'),
                'adminShort' => Arr::get($estacion, 'gerenreg'),
                'ISO3166' => Arr::get($estacion, 'depto_codISO3166'),

                'tipomet' => Arr::get($estacion, 'tipomet'),
                'tipopluvio' => Arr::get($estacion, 'tipopluvio'),
                'tipoExterna' => Arr::get($estacion, 'tipoExterna'),
                'tipoAeronautica' => Arr::get($estacion, 'tipoAeronautica'),
                'automatica' => Arr::get($estacion, 'automatica'),
                'timezone' => Arr::get($estacion, 'husoHorario'),
            ]);
        }
        return 0;
    }
}
