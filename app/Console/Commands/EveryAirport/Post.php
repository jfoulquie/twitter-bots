<?php

namespace App\Console\Commands\EveryAirport;

use App\DTO\Twitter\Tweet;
use App\Services\Google\Maps;
use App\Services\Google\MapsShortener;
use App\Services\Google\Places;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;

use function Safe\file_get_contents;
use function Safe\file_put_contents;
use function Safe\json_decode;
use function Safe\json_encode;
use function Safe\unlink;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyairport:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post a new airport on every run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Places $places, Maps $maps, TwitterApio $twitter)
    {
        $airportsPath = storage_path('app/everyairport/airports.json');
        $tweetedPath = storage_path('app/everyairport/tweeted');
        /** @var array<int, array<string, mixed>> $airportsJson */
        $airportsJson = json_decode(file_get_contents($airportsPath), true);
        $airports = collect($airportsJson);
        $tweeted = explode("\n", file_get_contents($tweetedPath));
        $airport = $airports->reject(fn ($item) => in_array($item['objectID'], $tweeted))->random();

        $name = "{$airport['name']} airport ({$airport['iata_code']}), {$airport['city']}, {$airport['country']}";
        $bias = "circle:1000@{$airport['_geoloc']['lat']},{$airport['_geoloc']['lng']}";
        $response = $places->findPlace($name, $bias);
        $airportPlaceId = Arr::get($response, 'candidates.0.place_id');
        if ($airportPlaceId === null) {
            $this->error('No airport found for ' . $name . ' (bias: ' . $bias . ')');
            $this->error(json_encode($response));
            return 1;
        }
        $response = $places->getDetails($airportPlaceId);
        $airportGeo = Arr::get($response, 'result.geometry');
        /** @var array<int, array<string, mixed>> $photos */
        $photos = Arr::get($response, 'result.photos', []);
        $airportPhotosGoogle = collect($photos)->map(function ($item) {
            $attr = reset($item['html_attributions']);
            $reference = $item['photo_reference'];
            return compact('reference', 'attr');
        })->shuffle();

        $refs = $airportPhotosGoogle->pluck('reference')->toArray();
        $limit = 4;
        $images = $places->getImages($airport['iata_code'], $refs, $limit);
        $tweetImgs = [];
        foreach ($images as $ref => $path) {
            $mediaResponse = $twitter->uploadMedia([
                'media' => file_get_contents($path),
            ]);

            // dump($mediaResponse);

            // dump($airportPhotosGoogle->firstWhere('reference', $ref));
            // Add alt text
            // $twitter->addMetadata([
            // 'media_id' => $mediaResponse['media_id_string'],
            // 'alt_text' => [
            // 'text' => $airportPhotosGoogle->firstWhere('reference', $ref)['attr'],
            // ],
            // ]);

            $tweetImgs[] = $mediaResponse['media_id_string'];
        }
        $status = $name . PHP_EOL;

        $tweet = null;
        if (!empty($tweetImgs)) {
            $tweetOptions = [
                'status' => $status,
                'lat' => $airportGeo['location']['lat'],
                'long' => $airportGeo['location']['lng'],
                'display_coordinates' => true,
                'media_ids' => implode(',', $tweetImgs),
            ];
            $tweetResponse = $twitter->post('statuses/update', $tweetOptions);
            if (!is_array($tweetResponse)) {
                Log::error('Something went wrong when tweeting the image', Arr::wrap($tweetResponse));
                $this->error('Something went wrong when tweeting the image');
                return -1;
            }
            $tweet = new Tweet($tweetResponse);
        }

        // Upload Map
        $staticMap = $maps->getImage($airport['iata_code'], $airportGeo['viewport']);
        $mediaResponse = $twitter->uploadMedia([
            'media' => file_get_contents($staticMap),
        ]);
        $mapsUrl = $this->getGoogleMapsUrl($airportGeo['location']['lat'], $airportGeo['location']['lng']);

        // If the previous tweet was a success, then we can tweet the map as a reply, otherwise
        // we tweet the name, the url and the map as only tweet
        if ($tweet instanceof Tweet) {
            $tweetOptions['status'] = '@every_airport 🗺️ ' . $mapsUrl;
            $tweetOptions['in_reply_to_status_id'] = $tweet->getStatusId();
        } else {
            $tweetOptions['status'] = $status . PHP_EOL . '🗺️ ' . $mapsUrl;
        }

        if (Arr::get($mediaResponse, 'media_id_string') !== null) {
            $tweetOptions['media_ids'] = $mediaResponse['media_id_string'];
        }
        $tweetResponse = $twitter->post('statuses/update', $tweetOptions);

        // Add the airport to the tweeted file
        file_put_contents($tweetedPath, $airport['objectID'] . "\n", FILE_APPEND);

        unlink($staticMap);
        foreach ($images as $ref => $path) {
            unlink($path);
        }

        if ($this->output->isVerbose()) {
            $this->info($name);
        }

        return 0;
    }

    private function getGoogleMapsUrl(float $lat, float $long) : string
    {
        $baseurl = 'https://www.google.com/maps/@';
        $query = [
            'api' => 1,
            'map_action' => 'map',
            'center' => "$lat,$long",
            'basemap' => 'satellite',
        ];

        $longUrl = $baseurl . '?' . http_build_query($query);

        try {
            return app(MapsShortener::class)->shorten($longUrl);
        } catch (\Exception $e) {
            return $longUrl;
        }
    }
}
