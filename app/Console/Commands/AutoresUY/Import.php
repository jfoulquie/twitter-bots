<?php

namespace App\Console\Commands\AutoresUY;

use App\Models\Obra;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fopen;
use function Safe\json_encode;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autores:import {csvfile}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the list of authors from CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        $count = LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
        })->count();
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
        })->skip(1)->each(
            /** @param array{0: string, 1: string, 2?: string, 3?: string, 4?: string, 5?: string, 6?: string} $line */
            function ($line) {
                if (!is_array($line)) {
                    return;
                }
                try {
                    $this->output->progressAdvance();
                    Obra::firstOrCreate(
                        ['path' => str_replace('http:', 'https:', $line[0])],
                        [
                            'titulo' => trim($line[1]),
                            'subtitulo' => trim($line[2]) ?: null,
                            'autores' => trim($line[3]) ?: null,
                            'fecha_publicacion' => trim($line[4]) ?: null,
                            'tipo_obra' => trim($line[5]),
                            'estado_derechos_obra' => trim($line[6]) ?: null,
                        ]
                    );
                } catch (QueryException $e) {
                    $this->error('QueryException: ' . $e->getMessage());
                    $this->error(json_encode($line));
                    return;
                }
            }
        );
        $this->output->progressFinish();

        return 0;
    }
}
