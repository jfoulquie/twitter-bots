<?php

namespace App\Console\Commands\UTE;

use App\Services\UTE\Portal;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use function Safe\fclose;
use function Safe\fopen;
use function Safe\fputcsv;

class UpdatePowerInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ute:update-power-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Portal $portal)
    {
        /** @phpstan-ignore-next-line */
        app()->log->pushProcessor(static function (array $record) {
            $record['message'] = "[UTE] {$record['message']}";
            return $record;
        });

        $powerData = $portal->getInstantPower()->toArray();
        $demandData = $portal->getInstantDemand()->toArray();
        $forget = [
            'Delete',
            'FecModif',
            'FecModif_OldValue',
            'Hash',
            'Identificador',
            'Nuevo',
            'DemandaMaxima',
            'DemandaMinima',
            'HoraDemandaMaxima',
            'HoraDemandaMinima',
        ];
        foreach ($forget as $key) {
            unset($demandData[$key]);
        }
        Log::info('powerData', $powerData);
        Log::info('demandData', $demandData);
        $lastUpdated = Carbon::createFromFormat('H:i', $demandData['HoraAccesoScada'] ?? '', 'America/Montevideo') ?: now();
        $data = array_merge([$lastUpdated->toIso8601String()], $powerData, $demandData);
        $filepath = storage_path('app/ute/last.csv');
        $addHeaders = !file_exists($filepath);

        $res = fopen($filepath, 'a');

        if ($addHeaders) {
            fputcsv($res, array_merge(['date'], array_keys($powerData), array_keys($demandData)));
        }
        fputcsv($res, $data);

        fclose($res);

        return Command::SUCCESS;
    }
}
