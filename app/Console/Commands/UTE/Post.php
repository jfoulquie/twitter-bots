<?php

namespace App\Console\Commands\UTE;

use App\Services\FediApi;
use App\Services\TwitterHelper;
use App\Services\UTE\ADME;
use App\Services\UTE\Graphs;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;
use RuntimeException;

use function Safe\file_put_contents;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ute:post {--daily} {--weekly} {--spot} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post tweets for the UTE account';

    private FediApi $api;
    private ADME $adme;
    private Graphs $graphFactory;
    private string $filePrefix = 'bigbo_energy';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FediApi $api, Graphs $graphFactory, ADME $adme)
    {
        $this->api = $api;
        $this->adme = $adme;
        Pluralizer::useLanguage('spanish');

        $this->graphFactory = $graphFactory;
        if ($this->option('daily')) {
            $this->postDailySummary();
        }

        if ($this->option('weekly')) {
            $this->postWeeklySummary();
        }

        if ($this->option('spot')) {
            $this->postMonthlySpotPrice();
        }
        return Command::SUCCESS;
    }

    private function postDailySummary() : bool|array
    {
        $start = now()->timezone('America/Montevideo')->subDay();
        $end = now()->timezone('America/Montevideo');

        $dryRun = (bool) $this->option('dry-run');

        $data = $this->adme->getDataForInterval($start, $end);
        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(Distribución por fuente)';
        $subsubtitle = $start->locale('es_UY')->isoFormat('ll');

        // Smooth the series
        $smoothData = $data->map(fn ($series) => $series->smoother(3));
        $lineGraph = $this->graphFactory->getHistoricalDistributionLine($smoothData, 1920, 1080, $title, $subtitle, $subsubtitle);
        $data->forget('Demanda');
        $sumData = $data->mapWithKeys(fn (Collection $series, string $key) => [$key => $series->sum()]);
        $totalGenerated = $sumData->sum();
        $percs = $sumData->mapWithKeys(fn (float $value, string $key) => [$key => round($value / $totalGenerated * 100, 2)]);

        if (!$dryRun) {
            // Upload the graph
            $filepath = $this->filePrefix . '-daily-' . $start->format('Y-m-d') . '.jpg';
            $uuid = Storage::disk('uploadcare')->putGetUuid($filepath, $lineGraph);
            if (empty($uuid)) {
                throw new RuntimeException('Error while uploading the line graph');
            }
        }

        // // Add alt text
        $alt = 'Gráfico de líneas que muestra la generación de energía eléctrica en Uruguay ';
        $alt .= 'durante el día de ayer.' . PHP_EOL;
        foreach ($data as $source => $series) {
            $sourceAlt = $this->getAltForSourceDaily($source, $data, $percs);
            if (!empty($sourceAlt)) {
                $alt .= $sourceAlt . PHP_EOL;
            }
        }

        // Tweet
        $status = 'Generación eléctrica en 🇺🇾 durante el día de ayer ('
            . $start->locale('es_UY')->isoFormat('ll') . ')' . PHP_EOL . PHP_EOL;

        $icons = [
            'Eólica' => '🌬️🟢',
            'Hidráulica' => '💧🔵',
            'Solar' => '🌞🟡',
            'Biomasa' => '💩🟠',
            'Térmica' => '🔥⚫️',
            'Importación' => '💸🌸',
        ];
        $status .= '------------🍃------------' . PHP_EOL;
        $percs->sortDesc()->each(function ($value, $key) use (&$status, $icons) {
            if (!in_array($key, ['Térmica', 'Importación'])) {
                $status .= "{$icons[$key]} {$key}: {$value}%\n";
            }
        });
        $status .= '------------🌫️------------' . PHP_EOL;
        $status .= "{$icons['Térmica']} Térmica: {$percs['Térmica']}%\n";
        $status .= '------------💵------------' . PHP_EOL;
        $status .= "{$icons['Importación']} Importación: {$percs['Importación']}%\n";

        if (!$dryRun) {
            $options = ['media' => [[
                'mediaType' => 'image/jpeg',
                'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
                'name' => $alt,
            ]]];
            $response = $this->api->publishPost($status, $options);
            Log::debug('tweetresponse', Arr::wrap($response));
            return $response->created();
        }

        $this->info($status);
        $this->line($alt);
        $filename = storage_path('app/ute/post-daily-summary' . now()->format('y-m-d-h:i:s') . '.png');
        file_put_contents($filename, $lineGraph);
        $this->info($filename);
        return [];
    }

    private function postWeeklySummary() : bool|array
    {
        $start = now()->timezone('America/Montevideo')->subWeek()->startOfWeek();
        $end = now()->timezone('America/Montevideo')->subWeek()->endOfWeek();

        $data = $this->adme->getDataForInterval($start, $end->copy()->addDay());
        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(Distribución por fuente)';
        $subsubtitle = 'Semana del ' . $start->locale('es_UY')->isoFormat('D');
        $subsubtitle .= ' al ' . $end->locale('es_UY')->isoFormat('LL');

        // Group and sum by day
        $dayData = $data->map(
            fn ($series) => $series->groupBy(
                fn (float $value, int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('Y-m-d')
            )->filter(
                /** @phpstan-ignore-next-line */
                fn (Collection $dayHours, string $date) => Carbon::createFromFormat('Y-m-d', $date, 'America/Montevideo')->isBetween($start, $end)
            )->mapWithKeys(
                fn (Collection $dayHours, string $date) => [
                    // Data points every 10 min in MW, convert to GW, plus, get the average for the hour (6 data points every hour)
                    Carbon::createFromFormat('Y-m-d', $date, 'America/Montevideo')->timestamp => ($dayHours->sum()) / 6 / 1000, /** @phpstan-ignore-line */
                ]
            )
        );

        // Extract demand data
        $data->forget('Demanda');
        $accBarGraph = $this->graphFactory->getWeeklyChart($dayData, 1920, 1080, $title, $subtitle, $subsubtitle);

        $sumData = $data->mapWithKeys(fn (Collection $series, string $key) => [$key => $series->sum()]);
        $totalGenerated = $sumData->sum();
        $percs = $sumData->mapWithKeys(fn (float $value, string $key) => [$key => round($value / $totalGenerated * 100, 2)]);
        $mediaResponse = null;
        if (!$this->option('dry-run')) {
            // Upload the graph
            $filepath = $this->filePrefix . '-weekly-' . $start->format('Y-m-d') . '_' . $end->format('Y-m-d') . '.jpg';
            $uuid = Storage::disk('uploadcare')->putGetUuid($filepath, $accBarGraph);

            if (empty($uuid)) {
                throw new RuntimeException('Error while uploading the line graph');
            }
        }

        // Add alt text
        $alt = 'Gráfico de líneas que muestra la generación de energía eléctrica en Uruguay ';
        $alt .= 'durante la semana pasada (';
        $alt .= $start->locale('es_UY')->isoFormat('D') . ' al ';
        $alt .= $end->locale('es_UY')->isoFormat('LL') . ').' . PHP_EOL;
        foreach ($dayData as $source => $series) {
            $sourceAlt = $this->getAltForSourceWeekly($source, $dayData, $percs);
            if (!empty($sourceAlt)) {
                $alt .= $sourceAlt . PHP_EOL;
            }
        }

        // Tweet
        $status = 'Generación eléctrica en 🇺🇾 durante la semana pasada ('
            . $start->locale('es_UY')->isoFormat('D') . ' al '
            . $end->locale('es_UY')->isoFormat('LL') . ')' . PHP_EOL . PHP_EOL;

        $icons = [
            'Eólica' => '🌬️',
            'Hidráulica' => '💧',
            'Solar' => '🌞',
            'Biomasa' => '💩',
            'Térmica' => '🔥',
            'Importación' => '💸',
        ];
        $status .= '------------🍃------------' . PHP_EOL;
        $percs->sortDesc()->each(function ($value, $key) use (&$status, $icons) {
            if (!in_array($key, ['Térmica', 'Importación']) && isset($icons[$key])) {
                $status .= "{$icons[$key]} {$key}: {$value}%\n";
            }
        });
        $status .= '------------🌫️------------' . PHP_EOL;
        $status .= "{$icons['Térmica']} Térmica: {$percs['Térmica']}%\n";
        $status .= '------------💵------------' . PHP_EOL;
        $status .= "{$icons['Importación']} Importación: {$percs['Importación']}%\n";

        if (!$this->option('dry-run')) {
            $options = ['media' => [[
                'mediaType' => 'image/jpeg',
                'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
                'name' => $alt,
            ]]];
            $tweetResponse = $this->api->publishPost($status, $options);
            Log::debug('tweetresponse', Arr::wrap($tweetResponse));
            return [];
        }

        $this->info($status);
        $this->line(PHP_EOL);
        $this->line($alt);
        $filename = storage_path('app/ute/post-weekly-summary' . now()->format('Y-m-d-H:i:s') . '.png');
        file_put_contents($filename, $accBarGraph);
        $this->line(PHP_EOL);
        $this->info($filename);
        return [];
    }

    private function postMonthlySpotPrice() : bool|array
    {
        $month = now('America/Montevideo')->subMonth();
        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getSpotPrice($month);

        /** @var \Illuminate\Support\Collection<int, float> $dataAvg */
        $dataAvg = $data->smoother(6, 'avg');
        $dataSeries = collect([$dataAvg]);

        $title = 'Precios SPOT sancionado de la energía eléctrica';
        $subtitle = ucfirst($month->locale('es_UY')->isoFormat('MMMM YYYY'));
        $subsubtitle = 'Valor medio cada 6 horas en U$S/MWh';

        $lastMonthData = app(ADME::class)->getSpotPrice($month->copy()->subMonth());
        $twoMonthsAgoData = app(ADME::class)->getSpotPrice($month->copy()->subMonths(2));

        $lineGraph = $this->graphFactory->getSpotLine($dataSeries, 1920, 1080, $title, $subtitle, $subsubtitle);

        $mediaResponse = null;
        if (!$this->option('dry-run')) {
            // Upload the graph
            $mediaResponse = $this->api->uploadMedia([
                'media' => $lineGraph,
            ]);
            Log::debug('mediaResponse', Arr::wrap($mediaResponse));
            if (!isset($mediaResponse['media_id_string'])) {
                throw new RuntimeException('Error while uploading the line graph');
            }
        }

        // Add alt text
        $alt = 'Gráfico de líneas que muestra la evolución del precio SPOT de exportacion ';
        $alt .= '(expresado en U$S/MWh) durante ' . ucfirst($month->locale('es_UY')->isoFormat('MMMM YYYY')) . '.';
        $alt .= ' En la gráfica, el precio alcanzó ' . round((float) $dataAvg->max(), 2) . ' U$D/MWh en su punto ';
        $alt .= 'más alto aunque el precio medio para el mes fue de ' . round((float) $dataAvg->avg(), 2);
        $alt .= ' U$D/MWh (la mediana fue ' . round((float) $dataAvg->median(), 2) . ' U$D/MWh).';

        if (!$this->option('dry-run')) {
            $this->api->addMetadata([
                'media_id' => $mediaResponse['media_id_string'],
                'alt_text' => [
                    'text' => $alt,
                ],
            ]);
        }

        $status = 'El valor medio para el SPOT sancionado durante el mes de ' . ucfirst($month->locale('es_UY')->isoFormat('MMMM'));
        $status .= ' fue de ' . round((float) $dataAvg->avg(), 2) . ' U$D/MWh, ';
        $comparison = '';
        if (round((float) $dataAvg->avg()) > round((float) $lastMonthData->avg())) {
            if (round((float) $dataAvg->avg()) > round((float) $twoMonthsAgoData->avg())) {
                $comparison = 'más alto que los dos meses anteriores ('
                    . round((float) $lastMonthData->avg(), 2) . ' y '
                    . round((float) $twoMonthsAgoData->avg(), 2) . ' U$D/MWh)';
            } else {
                $comparison = 'más alto que el mes anterior ('
                    . round((float) $lastMonthData->avg(), 2) . ' U$D/MWh)';
            }
        } elseif (round((float) $dataAvg->avg()) < round((float) $lastMonthData->avg())) {
            if (round((float) $dataAvg->avg()) < round((float) $twoMonthsAgoData->avg())) {
                $comparison = 'más bajo que los dos meses anteriores ('
                    . round((float) $lastMonthData->avg(), 2) . ' U$D/MWh en ' . $month->copy()->subMonth()->locale('es_UY')->isoFormat('MMMM')
                    . ' y '
                    . round((float) $twoMonthsAgoData->avg(), 2) . ' U$D/MWh en '
                    . $month->copy()->subMonths(2)->locale('es_UY')->isoFormat('MMMM') . ')';
            } else {
                $comparison = 'más bajo que el mes anterior ('
                    . round((float) $lastMonthData->avg(), 2) . ' U$D/MWh)';
            }
        }
        $status .= $comparison . '. ';
        $status .= 'Alcanzó su valor máximo (' . round((float) $data->max(), 2) . ' U$D/MWh) ';
        $maxPricePoints = $data->filter(fn ($value) => $value === $data->max());
        $times = $maxPricePoints->count();
        $maxPriceDates = $maxPricePoints->keys()
            ->mapWithKeys(fn (int $ts) => [$ts => Carbon::createFromTimestamp($ts, 'America/Montevideo')])
            ->groupBy(fn (Carbon $date, int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('Y-m-d'), true);
        if ($times === 1) {
            /** @phpstan-ignore-next-line */
            $status .= 'el ' . $maxPriceDates->first()->first()->locale('es_UY')->isoFormat('D MMM') . ' a las ';
            /** @phpstan-ignore-next-line */
            $status .= $maxPriceDates->first()->first()->format('H:i');
        } else {
            $status .= 'en ' . $times . ' ' . Str::plural('ocasión', $times) . ', ';
            $sentences = new Collection();
            foreach ($maxPriceDates as $day => $timesThatDay) {
                $sentence = '';
                /** @var \Illuminate\Support\Collection $timesThatDay */
                $sentence .= $timesThatDay->count() . ' ' . Str::plural('vez', $timesThatDay->count());
                $sentence .= ' el ' . $timesThatDay->first()->locale('es_UY')->isoFormat('D MMM') . ' (a las ';
                $sentence .= $timesThatDay->map(fn (Carbon $maxDate) => $maxDate->format('H'))->join(', ', ' y a las ') . ')';
                $sentences->push($sentence);
            }
            $status .= $sentences->join(', ', ' y ') . '.';
        }

        $tweets = app(TwitterHelper::class)->splitTextIntoTweets($status, 260);
        $tweetResponse = false;
        if (!$this->option('dry-run')) {
            $reply = null;
            foreach ($tweets as $i => $tweet) {
                $tweetOptions = [
                    'status' => $tweet,
                ];
                if (isset($mediaResponse['media_id_string'])) {
                    $tweetOptions['media_ids'] = $mediaResponse['media_id_string'];
                    unset($mediaResponse['media_id_string']);
                }
                if (!empty($reply)) {
                    $tweetOptions['in_reply_to_status_id'] = $reply;
                    $tweetOptions['status'] = '@BigBo_Energy ' . $tweetOptions['status'];
                }

                $tweetResponse = $this->api->post('statuses/update', $tweetOptions);
                Log::debug('tweetresponse', [
                    'options' => $tweetOptions,
                    'response' => $tweetResponse,
                ]);
                if (!isset($tweetResponse['id_str'])) {
                    throw new RuntimeException('Tweet did not post');
                }
                $reply = Arr::get($tweetResponse, 'id_str');
            }

            return $tweetResponse;
        }

        $this->info($status);
        $this->line($alt);
        $filename = storage_path('app/ute/post-montly-spot-' . now()->format('y-m-d-h:i:s') . '.png');
        file_put_contents($filename, $lineGraph);
        $this->info($filename);
        return false;
    }

    private function getAltForSourceDaily(string $source, Collection $data, Collection $percs) : string
    {
        $series = $data->get($source);
        $alt = '';
        $mainSource = $percs->search($percs->max());
        switch($source) {
            case 'Biomasa':
                $alt = 'La generación por biomasa cubrió un ' . $percs[$source] .
                '% de la generación del día.';
                if ($series->stdDev() < 10) {
                    $alt .= ' Se mantuvo estable durante todo el día, '
                    . 'produciendo de media unos ' . (round($series->avg()) * 6) . 'MW/h.';
                }
                break;
            case 'Solar':
                $meaningfulData = $series->filter(fn ($val) => $val > 10);
                $startTs = $meaningfulData->keys()->first();
                $endTs = $meaningfulData->keys()->last();
                $maxTs = $meaningfulData->sort()->keys()->last();
                $max = $meaningfulData->max();
                $alt = 'La generación por energía solar cubrió un ' . $percs[$source]
                    . '% de la producción del día. Empezó a generar de manera significativa a las '
                    . Carbon::createFromTimestamp($startTs)->format('H:i') . ' y duró hasta las '
                    . Carbon::createFromTimestamp($endTs)->format('H:i') . ', alcanzando su máximo '
                    . 'a las ' . Carbon::createFromTimestamp($maxTs)->format('H:i') . ' con un pico de '
                    . round($max) . 'MW.';
                break;
            case 'Eólica':
                $hydro = $data->get('Hidráulica');
                $timeWindBiggerHydro = $series->reduce(function ($carry, $windVal, $key) use ($hydro) {
                    if ($windVal > $hydro[$key]) {
                        return ((int) $carry) + 10;
                    }
                    return (int) $carry;
                }) / 60;
                $alt = 'La generación eólica cubrió un ' . $percs[$source]
                    . '% de la producción del día';
                if ($mainSource === $source) {
                    $alt .= ' convirtiéndola así en la principal fuente para el país.';
                } elseif ($timeWindBiggerHydro > 0) {
                    $alt .= '. Durante unas ' . round($timeWindBiggerHydro)
                        . ' horas la generación eólica superó a la hidráulica como fuente principal del país.';
                } else {
                    $alt .= ' alcanzando un pico de ' . round($series->max()) . 'MW.';
                }
                break;
            case 'Hidráulica':
                $wind = $data->get('Eólica');
                $timeWindBiggerHydro = $series->reduce(function ($carry, $hydroVal, $key) use ($wind) {
                    if ($hydroVal > $wind[$key]) {
                        return ((int) $carry) + 10;
                    }
                    return (int) $carry;
                }) / 60;
                $alt = 'La generación hidráulica cubrió un ' . $percs[$source]
                    . '% de la producción del día';
                if ($mainSource === $source) {
                    $alt .= ' convirtiéndola así en la principal fuente para el país.';
                } elseif (round($timeWindBiggerHydro) > 0) {
                    $alt .= '. Durante unas ' . round($timeWindBiggerHydro)
                        . ' horas la generación hidráulica superó a la eólica como fuente principal del país.';
                } else {
                    $alt .= ' alcanzando un pico de ' . round($series->max()) . 'MW.';
                }
                break;
            case 'Térmica':
                if ($percs[$source] > 20) {
                    $alt = 'Las centrales térmicas cubrieron un ' . $percs[$source]
                        . '% de la generación eléctrica';
                }
                break;
            case 'Importación':
                if ($percs[$source] > 20) {
                    $alt = 'Hubo que importar un ' . $percs[$source]
                        . '% de la electricidad.';
                }
                break;
            default:
        }

        return $alt;
    }

    private function getAltForSourceWeekly(string $source, Collection $dayData, Collection $percs) : string
    {
        $series = $dayData->get($source);
        $alt = '';
        $mainSource = $percs->search($percs->max());
        switch($source) {
            case 'Biomasa':
                $alt = 'La generación por biomasa cubrió un ' . $percs[$source] .
                '% de la generación de la semana.';
                break;
            case 'Solar':
                $maxTs = $series->sort()->keys()->last();
                $max = $series->max();
                $alt = 'La generación por energía solar cubrió un ' . $percs[$source]
                    . '% de la producción de la semana. Alcanzó su máximo '
                    . 'el ' . Carbon::createFromTimestamp($maxTs)->locale('es_UY')->isoFormat('dddd DD MMM YYYY') .
                     ' con un pico de ' . round($max) . 'GW.';
                break;
            case 'Eólica':
                $alt = 'La generación eólica cubrió un ' . $percs[$source]
                    . '% de la producción de la semana';
                if ($mainSource === $source) {
                    $alt .= ' convirtiéndola así en la principal fuente de la semana.';
                }
                break;
            case 'Hidráulica':
                $alt = 'La generación hidráulica cubrió un ' . $percs[$source]
                    . '% de la producción de la semana';
                if ($mainSource === $source) {
                    $alt .= ' convirtiéndola así en la principal fuente de la semana';
                }
                $alt .= '.';
                break;
            case 'Térmica':
                if ($percs[$source] > 1) {
                    $alt = 'Las centrales térmicas cubrieron un ' . $percs[$source]
                        . '% de la generación eléctrica de la semana.';
                }
                break;
            case 'Importación':
                if ($percs[$source] > 1) {
                    $alt = 'Hubo que importar un ' . $percs[$source]
                        . '% de la electricidad demandada en la semana.';
                }
                break;
            default:
        }

        return $alt;
    }
}
