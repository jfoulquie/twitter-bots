<?php

namespace App\Console\Commands\BioDivLibrary;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use JeroenG\Flickr\Flickr;
use RuntimeException;

use function Safe\file_get_contents;
use function Safe\json_decode;

/**
 *
 * @phpstan-type PhotoSet array{id: string, primary: string, secret: string, count_views: string, count_photos: int, count_videos: int, title: array{_content: string}, description: array{_content: string}, date_create: string, date_update: string}
 *
 * @package App\Console\Commands\BioDivLibrary
 */
class Post extends Command
{
    private const USER_ID = '61021753@N02';
    private const USERNAME = 'BioDivLibrary';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'biodivlib:post {--photoset-file=resources/data/biodivlibrary/biodivlibrary-photosets.json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post a random picture from the Bio Diversity Library\'s Flickr account to Twitter';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Flickr $flickr)
    {
        $file = $this->option('photoset-file');
        if (!is_string($file)) {
            throw new RuntimeException('photoset-file must be a string');
        }
        /** @var array<int, PhotoSet> $json */
        $json = json_decode(file_get_contents('./' . $file), true);
        $photosets = collect($json);

        // $photoset = $photosets->random();

        /** @var PhotoSet $photoset */
        $photoset = $photosets->first();
        $page = 1;
        $response = $flickr->request('flickr.photosets.getPhotos', [
            'user_id' => self::USER_ID,
            'photoset_id' => $photoset['id'],
            'page' => $page,
            'extras' => 'license,date_upload,date_taken,owner_name,icon_server,original_format,last_update,geo,tags,machine_tags,o_dims,views,media,path_alias,url_sq,url_t,url_s,url_m,url_o',
        ]);
        // dd(data_get($response, 'contents.photoset.page'), data_get($response, 'contents.photoset.total'), data_get($response, 'contents.photoset.title'));
        $pages = data_get($response, 'contents.photoset.pages');
        if ($pages > 1) {
            // TODO: request a random page between 1 and $pages
        }
        // TODO: Filter the already tweeted!
        $photo = Arr::random(data_get($response, 'contents.photoset.photo', []));
        if (!$photo) {
            dump($response);
            return 1;
        }
        $flickrUrl = 'https://www.flickr.com/photos/' . self::USERNAME . '/' . $photo['id'] . '/in/album-' . $photoset['id'] . '/';
        $photoUrl = $photo['url_o'];
        // Get extra info
        $info = $flickr->request('flickr.photos.getInfo', ['photo_id' => $photo['id'], 'secret' => $photo['secret']]);
        $title = data_get($info, 'contents.photo.title._content');
        $description = strip_tags(data_get($info, 'contents.photo.description._content'));
        $uploadedAt = Carbon::parse(data_get($info, 'contents.photo.dates.posted'));
        $status = $title . PHP_EOL . $description . PHP_EOL . $uploadedAt->format('Y-m-d');
        dump($info);
        // TODO: Tweet the pick with the flickr link

        return 0;
    }
}
