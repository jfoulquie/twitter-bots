<?php

namespace App\Console\Commands\CDF;

use App\Services\UserAgent;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use function Safe\file_put_contents;

use function Safe\json_decode;
use function Safe\json_encode;
use Throwable;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdf:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(UserAgent $userAgent)
    {
        $url = 'https://cdf.montevideo.gub.uy/buscar/fotos?filters=entity%3Afoto%20ss_nombre_padre%3A%22Serie%20Diario%20El%20Popular%22';
        $lastPage = 60;

        $baseUrl = 'https://cdf.montevideo.gub.uy';
        $detailsUrl = 'https://cdf.montevideo.gub.uy/catalogo/foto/'; // + id
        $pictureUrl = 'https://cdf.montevideo.gub.uy/sites/cdf.montevideo.gub.uy/files/catalogo/originales/'; // + id.jpg
        $pictureHdUrl = '';
        $data = [];

        $this->output->progressStart(1214);
        for ($page = 0; $page <= $lastPage; $page++) {
            $response = Http::withHeaders([
                'User-Agent' => $userAgent->get(),
            ])->get("$url&page=$page");

            try {
                $document = new Document($response->body());
                $photos = $document->find('li.result-item');
                foreach ($photos as $photo) {
                    /** @phpstan-ignore-next-line */
                    $info = json_decode(trim($photo->first('.rs')->text()), true);
                    $picData = [
                        'nombre' => $info['c'],
                        'coleccion' => $info['np'],
                        'tecnica' => $info['pt'],
                        'digitalizacion' => $info['pi'],
                        'soporte' => $info['ps'],
                        'fecha' => $info['pf'],
                        'descripcion' => $info['pa'],
                        'productor' => $info['pp'],
                        'autor' => $info['pau'],
                        'detallesUrl' => $detailsUrl . $info['c'],
                        'archivo' => $pictureUrl . $info['c'] . '.jpg',
                        'archivoAlta' => isset($info['alta']) ? $baseUrl . $info['alta'] : '',
                    ];
                    $data[] = $picData;
                    $this->output->progressAdvance();
                }
            } catch (Throwable $e) {
                dd($e);
            }
        }

        file_put_contents('cdf-popular.json', json_encode($data));
        $this->output->progressFinish();
        return 0;
    }
}
