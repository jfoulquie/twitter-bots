<?php

namespace App\Console\Commands\CIA;

use App\Services\UserAgent;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cia:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //
        $site = 'https://www.cia.gov/readingroom/search/site/uruguay';

        /** @var \App\Services\UserAgent $userAgent */
        $userAgent = resolve(UserAgent::class);

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($site);

        if (!$response->successful()) {
            $response->throw();
        }

        $document = new Document($response->body());

        $searchResults = $document->find('.search-results li');

        dd($searchResults);

        return 0;
    }
}
