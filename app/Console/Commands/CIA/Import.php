<?php

namespace App\Console\Commands\CIA;

use App\Services\UserAgent;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use RuntimeException;

use function Safe\file_put_contents;
use function Safe\json_encode;
use function Safe\parse_url;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cia:import {--search=uruguay}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all links to CIA docs related to a given search term';

    private const CIA_SITE = 'https://www.cia.gov/readingroom/search/site/';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $term = $this->option('search');
        if (!is_string($term) || empty($term)) {
            throw new RuntimeException('A term is needed');
        }

        $page = 0;
        $document = $this->getDocument($page);
        $pagination = $document->first('.pager-last a');
        $params = [];
        parse_str(parse_url($pagination->attr('href'), PHP_URL_QUERY), $params);
        $maxPage = Arr::get($params, 'page', 0);
        $this->output->progressStart($maxPage);

        $docs = [];
        while ($page <= $maxPage) {
            $searchResults = $document->find('.search-results li');
            if (!is_array($searchResults)) {
                throw new RuntimeException('No results found');
            }
            foreach ($searchResults as $result) {
                $link = $result->first('a');
                $docs[] = $link->attr('href');
            }
            $this->output->progressAdvance();
            sleep(2);

            $page++;
            $document = $this->getDocument($page);
        }

        file_put_contents('cia-uruguay.json', json_encode($docs));

        $this->output->progressFinish();

        $this->info(count($docs) . ' docs retrieved');

        return 0;
    }

    private function getDocument(int $page)
    {
        $term = (string) $this->option('search');
        $site = self::CIA_SITE . '/' . $term . '?page=' . $page;

        /** @var \App\Services\UserAgent $userAgent */
        $userAgent = resolve(UserAgent::class);

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($site);

        if (!$response->successful()) {
            $response->throw();
        }

        return new Document($response->body());
    }
}
