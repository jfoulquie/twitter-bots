<?php

namespace App\Console\Commands\Everylot;

use App\Models\Lot;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;

class FillDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everylot:filldb {csvfile}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read a CSV file and fill up the DB with it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        $count = LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
        })->count();
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
        })->skip(1)->each(
            /** @var ?array<int, string> $line */
            function (?array $line) {
                if (!is_array($line)) {
                    return;
                }

                try {
                    $this->output->progressAdvance();
                    Lot::firstOrCreate(
                        ['punto_wkb' => $line[2]],
                        [
                            'lat' => $line[0],
                            'long' => $line[1],
                            'codigo_postal' => $line[3],
                            'nombre_via' => $line[5],
                            'num_puerta' => $line[6],
                            'letra_puerta' => $line[7],
                            'km' => $line[8],
                            'manzana' => $line[9],
                            'solar' => $line[10],
                            'nombre_inmueble' => $line[11],
                            'localidad' => $line[12],
                            'departamento' => $line[14],
                        ]
                    );
                } catch (QueryException $e) {
                    $this->error('QueryException: ' . $e->getMessage());
                    $this->error(json_encode($line));
                    return;
                }
            }
        );
        $this->output->progressFinish();

        return 0;
    }
}
