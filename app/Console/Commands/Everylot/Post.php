<?php

namespace App\Console\Commands\Everylot;

use App\DTO\Twitter\Tweet;
use App\Models\Lot;
use App\Models\Nomenclator;
use App\Services\Google\StreetView;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use RuntimeException;

use function Safe\file_get_contents;
use function Safe\unlink;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everylot:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read from a file and post the result to twitter';

    private Lot $lot;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $path = $this->getStreetViewImagePath();

        // Upload pic
        $mediaResponse = $twitterApi->uploadMedia([
            'media' => file_get_contents($path),
        ]);

        Log::debug('mediaREsponse', $mediaResponse);

        // Add alt text
        $twitterApi->addMetadata([
            'media_id' => $mediaResponse['media_id_string'],
            'alt_text' => [
                'text' => 'Captura de Street View para la dirección ' . $this->lot->human_address,
            ],
        ]);

        // Build the tweet
        $status = "🗺️ {$this->lot->twitter_address}";
        if ($date = data_get($this->lot->metadata, 'date')) {
            $status .= "\n📆 $date";
        }
        $status .= "\n🔗 {$this->lot->short_street_view_url}";

        // Compose Tweet
        $tweetOptions = [
            'status' => $status,
            'lat' => $this->lot->lat,
            'long' => $this->lot->long,
            'display_coordinates' => true,
            'media_ids' => $mediaResponse['media_id_string'],
        ];
        Log::debug('tweeting with ', $tweetOptions);
        $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);
        Log::debug('tweetResponse', Arr::wrap($tweetResponse));

        if (!is_array($tweetResponse)) {
            Log::error('Something went wrong when tweeting the image', Arr::wrap($tweetResponse));
            $this->error('Something went wrong when tweeting the image');
            return -1;
        }

        $tweet = new Tweet($tweetResponse);

        $this->lot->tweet_id = $tweet->getStatusId();
        $this->lot->user_id = $tweet->getAuthor()->getId();
        $this->lot->username = $tweet->getAuthor()->getUsername();
        $this->lot->tweeted_at = $tweet->getCreatedAt();
        $this->lot->save();

        // Delete image
        unlink($path);

        $street = Nomenclator::findStreetOnNomenclator($this->lot->nombre_via);
        if (!$street) {
            Log::notice("No street was found for {$this->lot->nombre_via}");
            return 0;
        }
        $reply = $tweet->getStatusId();
        $twitterApi->reconfigure(config('services.twitter.nomenclator'));
        $statuses = $street->getTweets();
        sleep(1);
        Log::debug('Nomenclator tweeting ' . count($statuses) . ' tweets');
        foreach ($statuses as $status) {
            $tweetOptions = [
                'status' => $status,
                'in_reply_to_status_id' => $reply,
            ];
            Log::debug('tweeting with ', $tweetOptions);
            $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);
            Log::debug('tweetResponse', Arr::wrap($tweetResponse));

            if (!is_array($tweetResponse)) {
                throw new RuntimeException('Error while tweeting');
            }
            $reply = Arr::get($tweetResponse, 'id_str');
            sleep(1);
        }

        return 0;
    }

    private function getStreetViewImagePath(int $delay = 0) : string
    {
        if ($delay > 15) {
            Log::error("Too many attempts to retrieve street view image. Let's wait till the next run");
            exit;
        }
        try {
            $this->lot = Lot::notTweetedYet()->googleVirgin()->inRandomOrder()->take(1)->firstOrFail();
            $path = app(StreetView::class)->getImageForLot($this->lot);
        } catch (Exception $e) {
            Log::warning('Failed retrieving street view image (' . $this->lot->address . '): ' . $e->getMessage());
            return $this->getStreetViewImagePath($delay + 5);
        }
        if (empty($path)) {
            Log::warning('No error but empty path returned, retrying in ' . ($delay + 5));
            return $this->getStreetViewImagePath($delay + 5);
        }
        return $path;
    }
}
