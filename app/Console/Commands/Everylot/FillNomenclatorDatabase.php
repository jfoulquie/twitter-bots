<?php

namespace App\Console\Commands\Everylot;

use App\Models\Nomenclator;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;
use function Safe\preg_replace;

class FillNomenclatorDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everylot:fillnomenclator {csvfile}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read a CSV file and fill up the DB with it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        $count = LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
        })->count();
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle, 0, ';')) {
                yield $line;
            }

            fclose($handle);
        })->skip(2)->each(
            /** @var ?array<int, string> $line */
            function (?array $line) {
                if (!is_array($line)) {
                    return;
                }

                try {
                    $this->output->progressAdvance();
                    $data = [
                        'via' => trim($line[0]),
                        'nombre_abreviado' => trim($line[1]),
                        'especificacion' => trim($line[2]),
                        'nombre' => preg_replace('/\s{2,}/', ' ', trim($line[3])),
                        'nombre_clasificacion' => trim($line[4]),
                        'significado' => trim($line[5]),
                        'comienzo_num' => trim($line[8]),
                        'fin_num' => trim($line[9]),
                    ];

                    if (!empty($data['significado'])) {
                        Nomenclator::create($data);
                    }
                } catch (QueryException $e) {
                    $this->error('QueryException: ' . $e->getMessage());
                    $this->error(json_encode($line));
                    return;
                }
            }
        );
        $this->output->progressFinish();

        return 0;
    }
}
