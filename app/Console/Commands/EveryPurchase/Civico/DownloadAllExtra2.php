<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use App\Services\EffectiveUrlMiddleware;
use App\Services\UserAgent;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

use function Safe\fclose;
use function Safe\fopen;

class DownloadAllExtra2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:download-extra2 {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all extra pages';

    protected Client $client;
    protected string $cookie = '';

    private string $accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $existing = collect();

        $stack = HandlerStack::create();
        $stack->push(EffectiveUrlMiddleware::middleware());
        $this->client = new Client([
            'handler' => $stack,
            'User-Agent' => resolve(UserAgent::class)->get(),
            'Accept-Language' => 'en-US,en;q=0.5',
            'Accept' => $this->accept,
            // 'X-Requested-With' => 'XMLHttpRequest',
            // 'Referrer' => 'https://www.comprasestatales.gub.uy/consultas/detalle/id/',
            // 'Accept-Encoding' => 'gzip, deflate, br',
        ]);

        $headers = [
            'User-Agent' => resolve(UserAgent::class)->get(),
            'Accept-Language' => 'en-US,en;q=0.5',
            'Accept' => $this->accept,
        ];
        if (!empty($this->cookie)) {
            $headers['Cookie'] = $this->cookie;
        }
        // dump($uri, $headers);
        $response = Http::withHeaders($headers)->get('https://www.comprasestatales.gub.uy/consultas/detalle/id/676728');
        $cookies = $response->cookies();
        if (empty($this->cookie)) {
            foreach ($cookies as $cookie) {
                $this->cookie .= $cookie->getName() . '=' . $cookie->getValue() . '; ';
            }
            $this->cookie = trim($this->cookie);
        }

        $requests = function () {
            $i = 0;
            $filename = $this->argument('file');
            if (!is_string($filename)) {
                throw new Exception('Invalid filename ');
            }
            $handle = fopen(storage_path('app/arce-extra2/failed-urls'), 'r');

            while (($row = fgets($handle)) !== false) {
                $i++;
                $url = trim($row);
                $headers = [
                    'User-Agent' => resolve(UserAgent::class)->get(),
                    'Accept-Language' => 'en-US,en;q=0.5 --compressed',
                    'Accept' => $this->accept,
                    'Cookie' => $this->cookie,
                    'Connection' => 'keep-alive',
                    'Cache-Control' => 'max-age=0',
                ];
                yield new Request('GET', $url, $headers);
            }
            fclose($handle);
        };
        $this->output->progressStart(13);

        $pool = new Pool($this->client, $requests(), [
            'concurrency' => 10,
            // 'options' => [
            // ],
            'fulfilled' => function (Response $response, $index) {
                $uri = Arr::first($response->getHeader('X-GUZZLE-EFFECTIVE-URL'));
                $segments = explode('/', $uri);
                $page = Arr::last($segments);
                $id = Arr::get($segments, 6);
                Storage::put("arce-extra2/$id-$page.html", (string) $response->getBody());
                // $this->info($id . ', page ' . $page . ' stored');
                $this->output->progressAdvance();
            },
            'rejected' => function (GuzzleException $reason, $index) {
                Storage::append('arce-extra2/failed', $reason->getMessage() . PHP_EOL);
                $this->error('failed, sleep for a bit');
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        $this->output->progressFinish();

        return 0;
    }
}
