<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\LazyCollection;

use function Safe\fopen;
use function Safe\fputcsv;

class CivicoExcepcion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:civico-excepcion {--localHtmlPath=} {--offset=1} {--total=10} {--continue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Civico scraper';

    private $exportHandle;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->exportFile = ;
        $mode = $this->option('continue') ? 'a' : 'w';
        $this->exportHandle = fopen(resource_path('data/everypurchase/excepciones.csv'), $mode);

        $headers = ['id', 'ocid', 'fecha', 'excepcion', 'url'];
        if (!$this->option('continue')) {
            fputcsv($this->exportHandle, $headers);
        }

        $total = $this->option('total');
        $this->output->progressStart($total);
        $localPath = $this->option('localHtmlPath');

        LazyCollection::make(function () use ($localPath) {
            // scan all files on the directory
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($localPath)) as $filename) {
                // filter out "." and ".."
                if ($filename->isDir()) {
                    continue;
                }
                // Add only the class name
                yield $filename->getFilename();
            }
        })->skip($this->option('offset'))->take($total)->each(function ($filename) use ($localPath) {
            if (!file_exists($localPath . "/$filename")) {
                throw new \Exception("File $filename does not exist");
            }
            $document = new Document($localPath . "/$filename", true);
            $this->parseExcepcionFromDocument($document, $filename);
            $this->output->progressAdvance();
        });

        $this->output->progressFinish();
        return 0;
    }

    public function parseExcepcionFromDocument(Document $document, string $filename) : void
    {
        $id = str_replace('.html', '', $filename);
        $ocid = "ocds-yfs5dr-$id";
        $url = "https://comprasestatales.gub.uy/consultas/detalle/id/$id";

        $table = $document->xpath("//strong[normalize-space() = 'Compra por Excepción']");
        $dateTitle = $document->xpath("//li[normalize-space() = 'Fecha Publicación:']");
        $date = '';
        if (isset($dateTitle[0])) {
            $date = html_entity_decode($dateTitle[0]->nextSibling()->text(), ENT_HTML5);
        }
        if (count($table) === 0) {
            return;
        }
        if (count($table) > 1) {
            if (!$this->confirm("More than one table found for $filename, continue?")) {
                exit;
            }
            dd(__LINE__, $table);
        }

        $exception = $table[0]->parent()->nextSibling()->text();
        if (empty($exception)) {
            dd(__LINE__, $table);
        }
        fputcsv($this->exportHandle, [$id, $ocid, $date, $exception, $url]);
    }
}
