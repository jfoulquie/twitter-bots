<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\LazyCollection;

use function Safe\fopen;
use function Safe\fputcsv;

class CivicoAllProviders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:civico-providers {--localHtmlPath=} {--offset=1} {--total=10} {--continue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Civico scraper';

    private $exportHandle;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->exportFile = ;
        $mode = $this->option('continue') ? 'a' : 'w';
        $this->exportHandle = fopen(resource_path('data/everypurchase/providers.csv'), $mode);

        $headers = ['id', 'ocid', 'tipo', 'nro-doc', 'nombre', 'url'];
        if (!$this->option('continue')) {
            fputcsv($this->exportHandle, $headers);
        }

        $total = $this->option('total');
        $this->output->progressStart($total);
        $localPath = $this->option('localHtmlPath');

        LazyCollection::make(function () use ($localPath) {
            // scan all files on the directory
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($localPath)) as $filename) {
                // filter out "." and ".."
                if ($filename->isDir()) {
                    continue;
                }
                // Add only the class name
                yield $filename->getFilename();
            }
        })->skip($this->option('offset'))->take($total)->each(function ($filename) use ($localPath) {
            if (!file_exists($localPath . "/$filename")) {
                throw new \Exception("File $filename does not exist");
            }
            $document = new Document($localPath . "/$filename", true);
            $this->parseProvidersFromDocument($document, $filename);
            $this->output->progressAdvance();
        });

        $this->output->progressFinish();
        return 0;
    }

    public function parseProvidersFromDocument(Document $document, string $filename) : void
    {
        $id = str_replace('.html', '', $filename);
        $ocid = "ocds-yfs5dr-$id";
        $url = "https://comprasestatales.gub.uy/consultas/detalle/id/$id";

        $tableTitle = $document->xpath("//strong[normalize-space() = 'Proveedores participantes']");
        if (count($tableTitle) === 0) {
            return;
        }
        if (count($tableTitle) > 1) {
            if (!$this->confirm("More than one table found for $filename, continue?")) {
                exit;
            }
            dd(__LINE__, $tableTitle);
        }

        $table = $tableTitle[0]->parent()->parent()->first('table');
        if (empty($table)) {
            dd(__LINE__, $tableTitle);
        }
        foreach ($table->find('tbody tr') as $tr) {
            $tds = $tr->find('td');
            if (count($tds) !== 3) {
                dd($tr);
                continue;
            }
            $tipo = $tds[0]->text();
            $nroDoc = $tds[1]->text();
            $nombre = $tds[2]->text();

            $row = [$id, $ocid, $tipo, $nroDoc, $nombre, $url];
            fputcsv($this->exportHandle, $row);
        }
    }
}
