<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use App\Services\EffectiveUrlMiddleware;
use App\Services\UserAgent;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use function Safe\fclose;
use function Safe\fopen;

class DownloadAllExtra extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:download-extra {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all extra pages';

    protected Client $client;
    protected string $cookie = '';
    protected string $accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8';
    protected string $baseUri = 'https://www.comprasestatales.gub.uy/consultas/detalle/id/';
    protected string $itemsBaseUri = 'https://www.comprasestatales.gub.uy/consultas/items/id/';
    private array $folders;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->folders = [
            'arce',
            'arce2021',
            'arce-extra2',
        ];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $existing = collect();
        // $h = fopen(resource_path('data/everypurchase/mas100.csv'), 'r');
        // while (($line = fgetcsv($h)) !== false) {
        //     dd($line);
        // }
        // fclose($h);

        // dump($existing->count());

        $stack = HandlerStack::create();
        $stack->push(EffectiveUrlMiddleware::middleware());
        $this->client = new Client([
            'handler' => $stack,
            'User-Agent' => resolve(UserAgent::class)->get(),
            'Accept-Language' => 'en-US,en;q=0.5',
            'Accept' => $this->accept,
            // 'X-Requested-With' => 'XMLHttpRequest',
            // 'Referrer' => 'https://www.comprasestatales.gub.uy/consultas/detalle/id/',
            // 'Accept-Encoding' => 'gzip, deflate, br',
        ]);

        $requests = function () {
            $i = 0;
            $filename = $this->argument('file');
            if (!is_string($filename)) {
                throw new Exception('Invalid filename ');
            }
            $handle = fopen($filename, 'r');

            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                $i++;
                if ($i <= 1) {
                    $this->output->progressAdvance();
                    continue;
                }

                // $id = Arr::last(explode('-', $row[0]));
                $id = $row[0];

                foreach ($this->folders as $folder) {
                    if (Storage::exists("$folder/$id.html") ||
                        Storage::exists("folder/$id-1.html") && Storage::exists("folder/$id-2.html")) {
                        $this->output->progressAdvance();
                        continue 2;
                    }
                }

                $headers = [
                    'User-Agent' => resolve(UserAgent::class)->get(),
                    'Accept-Language' => 'en-US,en;q=0.5 --compressed',
                    'Accept' => $this->accept,
                    'Cookie' => $this->cookie,
                    // 'Connection' => 'keep-alive',
                    'Cache-Control' => 'max-age=0',
                ];
                $uri = $this->baseUri . $id;
                $pages = $this->getMissingPages($uri, collect());
                if ($pages->isEmpty()) {
                    yield new Request('GET', $this->baseUri . $id, $headers);
                    continue;
                }

                // info($pages->count() . ' pages found for ' . $uri);
                foreach ($pages->sort()->toArray() as $page) {
                    // dump($this->itemsBaseUri . $id . '/page/' . $page, $headers);
                    yield new Request('GET', $this->itemsBaseUri . $id . '/page/' . $page, $headers);
                }
            }
            fclose($handle);
        };

        $this->output->progressStart(66684);

        $pool = new Pool($this->client, $requests(), [
            'concurrency' => 10,
            // 'options' => [
            // ],
            'fulfilled' => function (Response $response, $index) {
                $filepath = 'arce-extra2/';
                $uri = Arr::first($response->getHeader('X-GUZZLE-EFFECTIVE-URL'));
                $segments = explode('/', $uri);
                if (Str::startsWith($uri, $this->itemsBaseUri)) {
                    $page = Arr::last($segments);
                    $id = Arr::get($segments, 6);
                    $filepath .= $id . '-' . $page . '.html';
                } else {
                    $id = Arr::last($segments);
                    $filepath .= $id . '.html';
                }

                Storage::put($filepath, (string) $response->getBody());
                $this->output->progressAdvance();
            },
            'rejected' => function (GuzzleException $reason, $index) {
                Storage::append('arce2021/failed', $reason->getMessage() . PHP_EOL);
                $this->error('failed, sleep for a bit');
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        $this->output->progressFinish();

        return 0;
    }

    public function getMissingPages(string $uri, Collection $missingPages) : Collection
    {
        $headers = [
            'User-Agent' => resolve(UserAgent::class)->get(),
            'Accept-Language' => 'en-US,en;q=0.5',
            'Accept' => $this->accept,
        ];
        if (!empty($this->cookie)) {
            $headers['Cookie'] = $this->cookie;
        }

        $response = Http::withHeaders($headers)->get($uri);
        $cookies = $response->cookies();
        if (empty($this->cookie)) {
            foreach ($cookies as $cookie) {
                $this->cookie .= $cookie->getName() . '=' . $cookie->getValue() . '; ';
            }
            $this->cookie = trim($this->cookie);
        }

        $document = new Document($response->body());
        if (preg_match('#.*/id/([\w\d]+)/?#', $uri, $matches)) {
            $id = $matches[1];
        } else {
            throw new Exception('Invalid URI: ' . $uri);
        }
        $pages = $document->find('#pagination a');
        foreach ($pages as $page) {
            if (is_numeric($page->text()) && !$missingPages->contains($page->text())) {
                $missingPages->push($page->text());
            }
        }
        if (!empty($pages) && Arr::last($pages)->text() === 'Siguiente >') {
            $uri = $this->itemsBaseUri . $id . '/page/' . $missingPages->last();
            $missingPages = $this->getMissingPages($uri, $missingPages);
        }
        return $missingPages;
    }
}
