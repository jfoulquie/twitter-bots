<?php

namespace App\Console\Commands\EveryPurchase;

use App\Models\EveryPurchase\Purchase;
use Illuminate\Console\Command;
use j3j5\TwitterApio;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $p = Purchase::whereNull('tweeted_at')->has('items', '>', 1)->orderBy('bought_at')->firstOrFail();
        $tweets = $p->tweets;
        $this->info('Tweeting ' . count($tweets) . ' tweets');
        // dump($p->description);
        foreach ($tweets as $tweet) {
            dump($tweet, mb_strlen($tweet));
        }

        // $p->tweet_id = $tweet1->getStatusId();
        // $p->user_id = $tweet1->getAuthor()->getId();
        // $p->username = $tweet1->getAuthor()->getUsername();
        // $p->tweeted_at = Carbon::parse($tweet1->created_at);
        // $p->save();

        return 0;
    }
}
