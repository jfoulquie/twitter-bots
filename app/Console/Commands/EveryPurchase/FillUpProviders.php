<?php

namespace App\Console\Commands\EveryPurchase;

use App\Models\EveryPurchase\Purchase;
use Illuminate\Console\Command;

class FillUpProviders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:providers';

    protected $hidden = true;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $purchases = Purchase::whereHas('items', function ($query) {
            $query->whereNull('provider_id');
        })->get();

        if ($purchases->isEmpty()) {
            $this->info('No missing providers');
            return 0;
        }

        $this->output->progressStart($purchases->count());
        $purchases->each(function ($p) {
            Purchase::updateOrCreateFromLink($p->link);
            $this->output->progressAdvance();
        });
        $this->output->progressFinish();
        return 0;
    }
}
