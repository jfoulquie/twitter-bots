<?php

namespace App\Console\Commands\LitClock;

use Google\Service\Sheets;
use Google\Service\Sheets\BatchUpdateValuesRequest;
use Google\Service\Sheets\ValueRange;
use Illuminate\Console\Command;

class SortSpreadsheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'litclock:sort-spreadsheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(Sheets $service)
    {
        $spreadsheetId = '1YdsWAyYN_EJkHtqaZhPan8zEi_M9XgVuSnR_mt6PK4c';
        $sheet = "'litclock_annotated_es'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:F";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($spreadsheetId, $datesRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $headers = array_shift($values);
        $sorted = collect($values)->filter()->sortBy(0)->prepend($headers);
        $value_range = new ValueRange(['values' => $sorted->toArray()]);
        // Use the sheet_id as range, so the sheet is overwritten
        $value_range->setRange($sheet);

        $request = new BatchUpdateValuesRequest();
        $request->setData($value_range);
        $request->setValueInputOption('RAW');

        $service->spreadsheets_values->batchUpdate($spreadsheetId, $request);

        $this->info('done');
    }
}
