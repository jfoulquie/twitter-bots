<?php

namespace App\Console\Commands\LitClock;

use Google\Service\Sheets;
use Illuminate\Console\Command;

class MissingMinutes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'litclock:missing-minutes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(Sheets $service)
    {
        $allMinutes = collect();
        $start = now()->startofDay();
        $end = now()->endOfDay();
        $i = $start->copy();
        while($i <= $end) {
            $allMinutes->push($i->format('H:i'));
            $i->addMinute();
        }
        $spreadsheetId = '1YdsWAyYN_EJkHtqaZhPan8zEi_M9XgVuSnR_mt6PK4c';
        $sheet = "'litclock_annotated_es'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:A";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($spreadsheetId, $datesRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $existing = collect($values)->flatten()->unique();

        $allMinutes->reject(fn (string $time) => $existing->contains($time))->values()->dump();
    }
}
