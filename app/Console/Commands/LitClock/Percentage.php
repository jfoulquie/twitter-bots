<?php

namespace App\Console\Commands\LitClock;

use App\Console\Output;
use App\Services\FediApi;
use Google\Service\Sheets;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Helper\ProgressBar;

class Percentage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'litclock:percentage {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(Sheets $service, FediApi $api)
    {
        $allMinutes = collect();
        $start = now()->startofDay();
        $end = now()->endOfDay();
        $i = $start->copy();
        while($i <= $end) {
            $allMinutes->push($i->format('H:i'));
            $i->addMinute();
        }
        $spreadsheetId = '1YdsWAyYN_EJkHtqaZhPan8zEi_M9XgVuSnR_mt6PK4c';
        $sheet = "'litclock_annotated_es'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:A";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($spreadsheetId, $datesRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $existing = collect($values)->flatten()->unique();

        $message = 'Van ' . $existing->count() . ' horas completadas (con '
            . count($values) . ' citas en total) de '
            . $allMinutes->count() . ' minutos que hay en el día ' . Arr::random(['🤓', '📖', '📚', '📕', '📔']) . '.';
        $bar = new ProgressBar(app(Output::class), $allMinutes->count());
        $bar->setEmptyBarCharacter('░');
        $bar->setProgressCharacter('▒');
        $bar->setBarCharacter('█');
        $bar->setFormat('[%bar%] %percent:3s%%');
        $bar->setBarWidth(20);

        \ob_start();

        $bar->setProgress($existing->count());
        $bar->display();
        $progressString = \ob_get_clean();
        $message .= PHP_EOL . $progressString;
        if ($this->output->isVerbose()) {
            $this->info($message);
        }

        if (!$this->option('dry-run')) {
            $api->publishPost($message);
        }
    }
}
