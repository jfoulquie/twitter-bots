<?php

namespace App\Console\Commands\UVindex;

use App\Services\FediApi;
use App\Services\Inumet;
use App\Services\UVIndex\Fetcher;
use App\Services\UVIndex\Parser;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use function Safe\date_sun_info;

use function Safe\file_get_contents;
use Sentry;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uv:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update and post (if needed) the current UV Index';

    private Parser $parser;
    private FediApi $fediApi;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FediApi $fediApi, Fetcher $fetcher, Parser $parser)
    {
        $this->parser = $parser;
        $this->fediApi = $fediApi;

        $lastIndex = Storage::disk('local')->get('uvindex/last_index') ?? 0;

        $data = $fetcher->getLatestAverage(4);  // there are 4 samples on 15 minutes
        if (!isset($data['uvindex'])) {
            return -1;
        }

        Storage::disk('local')->put('uvindex/last_index', (int) round((float) $data['uvindex']));

        Log::debug('Last index ' . $lastIndex . ';  New index ' . $data['uvindex']);
        if ((int) $lastIndex === (int) round((float) $data['uvindex'])) {
            Log::debug('Same index, no tweet ' . $lastIndex . ' = ' . round($data['uvindex']));
            return 0;
        }

        // $avatar = $this->parser->getAvatarForIndex($data['uvindex']);
        $tweet = $this->getTweet($data);

        $sunInfo = date_sun_info(now()->timezone('America/Montevideo')->getTimestamp(), -34.90706976618914, -56.18601472514095);
        $sunset = Carbon::createFromTimestampUTC($sunInfo['sunset'])->timezone('America/Montevideo');

        if (round($data['uvindex']) === 0.0 && $sunset->isFuture()) {
            // Disable for now, almost 0 UV Index but still not quite
            Log::debug('UVIndex ≈ 0, ignore and not sunset yet', [$data['uvindex'], $sunset]);
            return 0;
        }

        $this->fediApi->publishPost($tweet);
        // $this->tweet($tweet);

        if ($lastIndex > 11 && round((float) $data['uvindex']) > 11) {
            // Over 11 all images are the same, so don't upload it again
            return 0;
        }

        // $this->updateAvatar($avatar);

        return 0;
    }

    // private function tweet(string $tweet) : void
    // {
    //     $tweetOptions = [
    //         'status' => $tweet,
    //         'lat' => -34.90706976618914,
    //         'long' => -56.18601472514095,
    //         'display_coordinates' => true,
    //     ];

    //     $tweetResponse = ;
    //     // Log::debug('Tweet: ', Arr::wrap($tweetResponse));
    // }

    // public function updateAvatar(string $avatar) : void
    // {
    //     $avatarResponse = $this->fediApi->postProfileImage([
    //         'image' => base64        // public function updateAvatar(string $avatar) : void
    // {
    //     $avatarResponse = $this->fediApi->postProfileImage([
    //         'image' => base64_encode(file_get_contents($avatar)),
    //     ]);
    //     // Log::debug('Avatar: ', Arr::wrap($avatarResponse));
    // }_encode(file_get_contents($avatar)),
    //     ]);
    //     // Log::debug('Avatar: ', Arr::wrap($avatarResponse));
    // }

    private function getTweet(array $data) : string
    {
        // Emoji plus UV Index value
        $emoji = $this->parser->getEmojiForIndex($data['uvindex']);
        $tweet = $emoji . ' ' . round($data['uvindex']) . PHP_EOL;

        /** @var \App\Services\Inumet $inumet */
        $inumet = resolve(Inumet::class);
        $weatherStation = 'Prado';

        try {
            // Temperature
            $temperature = $inumet->getCurrentTemperatureForStation($weatherStation);
            $tweet .= "🌡️ {$temperature}°C" . PHP_EOL;
        } catch (Exception $e) {
            Log::error('Error getting temperature: ' . $e->getMessage());
            Sentry::captureException($e);
        }
        $tweet .= '☼⥵ ' . round($data['radiation'], 2) . 'W/m²' . PHP_EOL;

        try {
            // Weather emoji plus sky status
            $weatherEmoji = $inumet->getCurrentWeatherEmojiForStation($weatherStation);
            $sky = $inumet->getCurrentSkyStatusForStation($weatherStation);
            $tweet .= "$weatherEmoji $sky" . PHP_EOL;
        } catch (Exception $e) {
            Log::error('Error getting weather data: ' . $e->getMessage());
            Sentry::captureException($e);
        }
        // Last updated time
        $tweet .= '⏰ ' . Carbon::createFromTimestampMsUTC($data['timestamp'])->timezone('America/Montevideo')->format('H:i') . PHP_EOL . PHP_EOL;

        // Risk
        $risk = $this->parser->getRiskForIndex($data['uvindex']);
        $tweet .= $risk . PHP_EOL . PHP_EOL;
        // Advice
        $advice = $this->parser->getAdviceForIndex($data['uvindex']);
        $tweet .= $advice;

        return $tweet;
    }
}
