<?php

namespace App\Console\Commands\OSE;

use App\Models\OSE\Data;
use App\Services\UserAgent;
use DiDom\Document;
use DiDom\Element;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use RuntimeException;
use Throwable;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ose:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(UserAgent $userAgent)
    {
        $url = 'http://www.ose.com.uy/agua/situacion-zona-metropolitana';

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($url);

        try {
            $document = new Document($response->body());

            $title = $document->first('div.body > p')->text();
            if (preg_match('/(?<day>\d{2})\/(?<month>\d{2})\/(?<year>\d{4})/', $title, $matches) !== 1) {
                throw new RuntimeException('No date was found on ' . $title);
            }
            $date = Carbon::createFromDate($matches['year'], $matches['month'], $matches['day'], 'America/Montevideo');

            $tables = $document->find('table');
            $data = [];
            foreach ($tables as $i => $table) {
                $data[] = match($i) {
                    0 => $this->getSodiumAndChlorideLevels($table),
                    1 => $this->getExceedAndCurrentLevel($table),
                    2 => $this->getCurrentLevelAndConsumption($table),
                    default => null,
                };
            }
        } catch (Throwable $e) {
            info($e->getMessage());
        }

        Data::firstOrcreate(
            ['observed_at' => $date->startOfDay()],
            ['record' => $data]
        );
    }

    private function getSodiumAndChlorideLevels(Element $table) : array
    {
        $lines = [];

        foreach ($table->find('tr') as $i => $tr) {
            if ($i === 0) {
                foreach($tr->find('td') as $td) {
                    $name = str_replace("\xc2\xa0", '', $td->text());
                    if ($name !== '') {
                        $lines[] = $name;
                    }
                }
                continue;
            }
            foreach($tr->find('td') as $td) {
                $dataRows[$i][] = trim($td->text());
            }
        }

        $dataRows = array_values($dataRows);
        foreach($lines as $i => $line) {
            $data[$dataRows[0][0]][$line] = $dataRows[0][$i + 1];
            $data[$dataRows[1][0]][$line] = $dataRows[1][$i + 1];
        }

        dump($data);

        return $data;
    }

    //Nivel de rebalse y actual
    private function getExceedAndCurrentLevel(Element $table) : array
    {
        $dams = [];
        $dataRows = [];

        foreach ($table->find('tr') as $i => $tr) {
            if ($i === 0) {
                foreach($tr->find('td') as $td) {
                    $name = str_replace("\xc2\xa0", '', $td->text());
                    if ($name !== '') {
                        $dams[] = $name;
                    }
                }
                continue;
            }
            foreach($tr->find('td') as $td) {
                $dataRows[$i][] = trim($td->text());
            }
        }
        $dataRows = array_values($dataRows);

        foreach ($dams as $i => $dam) {
            $data[$dam][$dataRows[0][0]] = $dataRows[0][$i + 1];
            $data[$dam][$dataRows[1][0]] = $dataRows[1][$i + 1];
        }

        dump($data);

        return $data;
    }

    private function getCurrentLevelAndConsumption(Element $table) : array
    {
        // Consumo y nivel
        foreach ($table->find('tr') as $tr) {
            $tds = $tr->find('td');
            $data[trim($tds[0]->text())] = trim($tds[1]->text());
        }

        dump($data);

        return $data;
    }
}
