<?php

namespace App\Console\Commands\IndustrialProp;

use App\DTO\Twitter\Tweet;
use App\Jobs\IndProp\CreateNiceClassInfographic;
use App\Models\IndProp\Brand;
use App\Services\MIEM\Helper;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use RuntimeException;

use function Safe\json_encode;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registra:post {--request=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the oldest branch to tweet and tweet it';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $query = Brand::tweetable()->orderBy('requestNumber');
        $requestNr = $this->option('request');
        if (!empty($requestNr) && is_numeric($requestNr)) {
            $query = Brand::where('requestNumber', $requestNr);
        }
        $brand = $query->first();

        if (!$brand instanceof Brand) {
            $this->info('No brands found!');
            return Command::SUCCESS;
        }
        $imageTweetIds = [];
        if (!empty($brand->logo)) {
            // Upload the logo
            $mediaResponse = $twitterApi->uploadMedia([
                'media' => $brand->logo_binary,
            ]);
            Log::debug('mediaResponse', $mediaResponse);
            $imageTweetIds[] = $mediaResponse['media_id_string'];

            // Add alt text
            $alt = 'Logo para el pedido de registro de marca número ' . $brand->requestNumber;
            if ($brand->brand) {
                $alt .= ' (' . $brand->brand . ')';
            }
            $twitterApi->addMetadata([
                'media_id' => $mediaResponse['media_id_string'],
                'alt_text' => [
                    'text' => $alt,
                ],
            ]);
        }

        $niceImg = CreateNiceClassInfographic::dispatchSync($brand);
        $mediaResponse = $twitterApi->uploadMedia([
            'media' => (string) $niceImg,
        ]);
        if (!isset($mediaResponse['media_id_string'])) {
            Log::error('Could not upload image', [$mediaResponse]);
            throw new RuntimeException('Could not upload image to Twitter ');
        }

        Log::debug('mediaResponse', $mediaResponse);
        $imageTweetIds[] = $mediaResponse['media_id_string'];

        // Add alt text
        $alt = 'Esta marca se registra para la siguientes categorías dentro de la clasificación de Niza:' . PHP_EOL;
        foreach ($brand->niceClasses as $class => $desc) {
            if (empty($desc)) {
                $info = app(Helper::class)->getNiceClassInfo($class);
                $alt .= "Clase $class: {$info['desc']}" . PHP_EOL;
            } else {
                $alt .= "Clase $class: {$desc}" . PHP_EOL;
            }
        }
        $twitterApi->addMetadata([
            'media_id' => $mediaResponse['media_id_string'],
            'alt_text' => [
                'text' => $alt,
            ],
        ]);

        $tweets = $brand->getTweets();

        $replyToId = '';
        foreach ($tweets as $index => $status) {
            // Only include the pic on the first tweet
            if ($index === 0) {
                $tweetOptions = [
                    'status' => $status,
                    'media_ids' => implode(',', $imageTweetIds),
                ];
            } else {
                $tweetOptions = [
                    'status' => '@RegistraBot ' . $status,
                    'in_reply_to_status_id' => $replyToId,
                ];
            }
            Log::debug('tweeting with ', $tweetOptions);
            $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);
            if (!is_array($tweetResponse)) {
                throw new Exception('Something went wrong when tweeting the image: ' . json_encode(Arr::wrap($tweetResponse)));
            }
            $tweetObj = new Tweet($tweetResponse);
            $replyToId = $tweetObj->getStatusId();

            if ($index === 0) {
                $brand->tweet_id = $tweetObj->getStatusId();
            }

            sleep(random_int(1, 5));
        }

        $brand->tweeted_at = now();
        $brand->save();

        return Command::SUCCESS;
    }
}
