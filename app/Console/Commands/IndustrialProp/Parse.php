<?php

namespace App\Console\Commands\IndustrialProp;

use App\Models\IndProp\Brand;
use App\Services\MIEM\Parser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registra:parse-miem {--by-date} {--request=} {--from=} {--to=} {--reqNumStart=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected Parser $parser;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Parser $parser)
    {
        $this->parser = $parser;

        if (!empty($this->option('request'))) {
            return $this->parseSingleRequest();
        }

        if ($this->option('by-date')) {
            $from = now()->subDay();
            $to = now();

            if (!empty($this->option('from') && is_string($this->option('from')))) {
                $from = Carbon::parse($this->option('from'));
            }

            if (!empty($this->option('to')) && is_string($this->option('to'))) {
                $to = Carbon::parse($this->option('to'));
            }
            return $this->parseFromDateRange($from, $to);
        }

        if (!empty($this->option('reqNumStart'))) {
            $start = (int) $this->option('reqNumStart');
        } else {
            $start = Brand::orderByDesc('requestNumber')->firstOrFail('requestNumber')->requestNumber;
        }

        return $this->parseFromRequestNumber($start);
    }

    private function parseFromDateRange(Carbon $from, Carbon $to) : int
    {
        $types = [
            'DEN',
            'FRA',
            'MAR',
            'COL',
            'CER',
        ];
        foreach ($types as $type) {
            $bar = $this->output->createProgressBar();
            $this->parser->setProgressBar($bar)
                ->from($from)
                ->to($to)
                ->requestType($type)
                ->getBrandsRequests();
            $bar->finish();
        }

        // Add patents
        return Command::SUCCESS;
    }

    private function parseSingleRequest() : int
    {
        $reqNumber = (int) $this->option('request');

        $this->parser->requestNumber($reqNumber)->getBrandsRequests();

        return Command::SUCCESS;
    }

    private function parseFromRequestNumber(int $requestNumberStart) : int
    {
        $start = $requestNumberStart;
        $end = $requestNumberStart + 100;
        while (true) {
            $bar = $this->output->createProgressBar();
            $brands = $this->parser->setProgressBar($bar)
                ->requestNumberRange($start, $end)
                ->getBrandsRequests();
            if ($brands->isEmpty()) {
                break;
            }
            $start = $end;
            $end += 100;
            $bar->finish();
        }

        return Command::SUCCESS;
    }
}
