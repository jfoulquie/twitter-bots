<?php

namespace App\Console\Commands\EveryFlight;

use App\DTO\Twitter\Tweet;
use App\Models\EveryFlight\FlightStatus;
use App\Services\FediApi;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyflight:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post updates to twitter every time a new flight status is found';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FediApi $api)
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\EveryFlight\FlightStatus> */
        $pendingStatuses = FlightStatus::toBeTweeted()->orderBy('created_at')->get();

        $pendingStatuses->each(function (FlightStatus $flightStatus) use ($api) {
            $tweet = $flightStatus->getTweet();

            if ($tweet === '') {
                $flightStatus->markAsTweeted('');
                return;
            }

            $tweetOptions = [
            // 'status' => $tweet,
            // Carrasco Airport Control Tower
            // 'lat' => -34.82645236101645,
            // 'long' => -56.01798659662071,
            // 'display_coordinates' => true,
            ];
            $latestSibling = $flightStatus->siblings()->orderByDesc('tweeted_at')->whereNotNull('tweet_id')->first();

            if ($latestSibling) {
                $tweetOptions['in_reply_to_id'] = $latestSibling->tweet_id;
            }

            $tweetResponse = $api->publishPost(
                $tweet,
                $tweetOptions
            );

            // if (!is_array($tweetResponse)) {
            //     Log::error('Something went wrong when tweeting the flight', Arr::wrap($tweetResponse));
            //     $this->error('Something went wrong when tweeting the flight');
            //     return -1;
            // }

            // $tweet = new Tweet($tweetResponse);

            $flightStatus->markAsTweeted($tweetResponse->json('id'));
        });
        return 0;
    }
}
