<?php

namespace App\Console\Commands\EveryFlight;

use App\Models\EveryFlight\Flight;
use App\Services\UserAgent;
use Carbon\Factory;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Console\Output\Output;
use Throwable;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyflight:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all available flights from source';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Flight::TYPES as $type) {
            $this->info('Retrieving ' . $type . ' flights...', Output::VERBOSITY_VERBOSE);
            $response = $this->getFlightsPage($type);
            $flights = $this->parseJsonData($response->json('flights'), $type);
            // $flights = $this->parseFlightsPage($response->body(), $type);
            $this->info('Found ' . count($flights) . ' flights', Output::VERBOSITY_VERBOSE);
        }

        return 0;
    }

    private function parseJsonData(array $data, string $type) : Collection
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\EveryFlight\Flight> $flights */
        $flights = new Collection();
        $factory = new Factory([
            'locale' => 'es_UY',
            'timezone' => 'America/Montevideo',
        ]);
        foreach ($data as $flightInfo) {
            $flightData = [
                'airline' => Arr::get($flightInfo, 'aerolinea'),
                'flight_number' => Arr::get($flightInfo, 'nro'),
                'status' => Arr::get($flightInfo, 'estes'),
                'airline_logo' => Arr::get($flightInfo, 'logo'),
                'type' => $type,
                'updated_at' => Carbon::parse(Arr::get($flightInfo, 'actualizado')),
            ];
            $stda = null;
            if (Arr::has($flightInfo, 'stda')) {
                try {
                    [$date, $time] = explode(' ', Arr::get($flightInfo, 'stda'));
                    $stda = $factory->parse($date)->setTimeFromTimeString($time);
                } catch (Throwable) {
                }
            }

            // Estimated Time Departure/Arrival
            $etda = null;
            if (Arr::has($flightInfo, 'etda')) {
                try {
                    [$date, $time] = explode(' ', Arr::get($flightInfo, 'etda'));
                    $etda = $factory->parse($date)->setTimeFromTimeString($time);
                } catch (Throwable) {
                }
            }

            // Actual Time Departure/Arrival
            $atda = null;
            if (Arr::has($flightInfo, 'atda')) {
                try {
                    [$date, $time] = explode(' ', Arr::get($flightInfo, 'atda'));
                    $atda = $factory->parse($date)->setTimeFromTimeString($time);
                } catch (Throwable) {
                }
            }

            if ($type === Flight::ARRIVAL) {
                $flightData['arrival_time'] = $stda;
                $flightData['departure_city'] = Arr::get($flightInfo, 'destorig');
                // Estimated Time Arrival
                $flightData['eta'] = $etda;
                // Actual Time Arrival
                $flightData['ata'] = $atda;
            } else {
                $flightData['departure_time'] = $stda;
                $flightData['arrival_city'] = Arr::get($flightInfo, 'destorig');
                // Estimated Time Departure
                $flightData['etd'] = $etda;
                // Actual Time Departure
                $flightData['atd'] = $atda;
            }

            if (isset($flightData['flight_number']) && (isset($flightData['departure_time']) || isset($flightData['arrival_time']))) {
                $flights->push(Flight::updateOrCreate([
                    'flight_number' => $flightData['flight_number'],
                    'departure_time' => Arr::get($flightData, 'departure_time'),
                    'arrival_time' => Arr::get($flightData, 'arrival_time'),
                ], $flightData));
            }
        }

        return $flights;
    }

    private function getFlightsPage(string $type) : Response
    {
        $baseUrl = 'https://www.aeropuertodecarrasco.com.uy';
        $endpoints = [
            Flight::ARRIVAL => '/aeropuertos/vuelos/mvd/arribos.json',
            Flight::DEPARTURE => '/aeropuertos/vuelos/mvd/partidas.json',
        ];
        // Get the needed cookies from the homepage
        /** @var \App\Services\UserAgent $userAgent */
        $userAgent = resolve(UserAgent::class);
        $endpointResp = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
            'X-Requested-With' => 'XMLHttpRequest',
        ])->acceptJson()->get($baseUrl . $endpoints[$type]);

        return $endpointResp->throw();
    }
}
