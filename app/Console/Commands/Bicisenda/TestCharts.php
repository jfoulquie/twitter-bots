<?php

namespace App\Console\Commands\Bicisenda;

use App\Models\Inumet\Observation;
use App\Services\Bicisenda\BiciApi;
use App\Services\Bicisenda\Graphs;
use App\Services\Inumet;
// use App\Services\UTE\Graphs;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class TestCharts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bici:test-charts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private BiciApi $biciApi;
    private Graphs $graphs;

    /**
     * Execute the console command.
     */
    public function handle(BiciApi $biciApi, Graphs $graphs)
    {
        // $start = now()->subMonths(12)->subWeek()->startOfDay();
        // $end = now()->subMonths(12)->addDays(0)->endOfDay();
        // $observations = Observation::fromStationId(211)->whereBetween('updated_at', [$start->timestamp, $end->timestamp])->get();

        // $weatherDay = $observations->groupBy(
        //     fn (Observation $observation) => Carbon::createFromTimestamp($observation->updated_at, 'America/Montevideo')->toDateString()
        // )->map(fn (EloquentCollection $dayData) => $dayData->mode('IconoEstadoActual'))
        // ->map(fn (array $icon) => count($icon) === 1 ? app(Inumet::class)->icons[$icon[0]] : app(Inumet::class)->icons[array_rand($icon)]);

        // dd($weatherDay);

        $this->biciApi = $biciApi;
        $this->graphs = $graphs;

        $chart = $this->hourly();
        file_put_contents(storage_path('app/bicisenda/hourly-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $chart);

        $chart = $this->weeklyGrouped();
        file_put_contents(storage_path('app/bicisenda/weekly-bargroup-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $chart);

        // $chart = $this->weeklyStacked();
        // file_put_contents(storage_path('app/bicisenda/weekly-barstacked-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $chart);
    }

    private function hourly()
    {
        $yesterday = now('America/Montevideo')->subDay(1);

        // Get data for the previous 4 weeks
        $start = $yesterday->copy()->subWeeks(4)->startOfWeek();
        $end = $yesterday->copy()->subWeeks(1)->endOfWeek();
        // $start = $yesterday->copy()->startOfDay();
        // $end = $yesterday->copy()->endOfDay();

        $where = 'WHERE fecha_conteo >= ' . $start->getTimestampMs();
        $where .= ' AND fecha_conteo <= ' . $end->getTimestampMs();
        $where .= " AND extract('dow' FROM fecha_conteo at time zone '-03') = " . $yesterday->dayOfWeekIso;

        $sql = <<<QUERY
            SELECT
            in_counts as sentido_ciudad_vieja,
            out_counts as sentido_tres_cruces,
            fecha_conteo
            FROM observatorio.v_omo_conteo_bicisenda
            $where
            ORDER BY fecha_conteo
        QUERY;
        $response = $this->biciApi->query(sql: $sql);

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        // Get the data for the past 4 weeks
        $pastData = collect(range(0, count($values->first()) - 1))
            // Add readable headers
            ->map(fn ($index) => $fields->combine($values->pluck($index)))
            // Get only the days we need
            ->filter(fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->dayOfWeekIso === $yesterday->dayOfWeekIso)
            // Group values by day (date)
            ->groupBy(fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->format('Y-m-d'))
            // Group values by hour (on each day)
            ->mapWithKeys(
                fn (Collection $dayData, string $date) => [$date => $dayData->groupBy(
                    fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->format('H') . ':00'
                )->sortKeys()]
                // Sum all values on each hour (to get the count by hour)
            )->mapWithKeys(
                fn (Collection $dayData, string $date) => [
                    $date => $dayData->mapWithKeys(fn (Collection $hourData, string $hour) => collect([
                        $hour => [
                            BiciApi::CIUDAD_VIEJA => $hourData->sum(BiciApi::CIUDAD_VIEJA),
                            BiciApi::TRES_CRUCES => $hourData->sum(BiciApi::TRES_CRUCES),
                        ],
                    ])),
                ]
            );

        $avgData = collect(range(0, 23))->mapWithKeys(
            fn (int $hour) => [str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00' => $pastData->map(
                fn (Collection $dayData, $day) => $dayData->get(str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00')
            )]
        );
        $avgDataCV = $avgData->map(
            fn (Collection $hourData) => $hourData->avg(BiciApi::CIUDAD_VIEJA)
        );
        $avgDataTC = $avgData->map(
            fn (Collection $hourData) => $hourData->avg(BiciApi::TRES_CRUCES)
        );

        // Get daily data
        $start = $yesterday->copy()->startOfDay();
        $end = $yesterday->copy()->endOfDay();
        $dailyData = $this->biciApi->getDataGroupByHour(start: $start, end: $end, singleValue: false);

        $ciudadVieja = $dailyData->pluck(BiciApi::CIUDAD_VIEJA);
        $tresCruces = $dailyData->pluck(BiciApi::TRES_CRUCES);

        $labels = $dailyData->map(
            fn ($hourData) => $yesterday->copy()
                ->hour($hourData['hora'])
                ->startOfHour()
                ->timestamp
        )->all();

        $dayData = collect([
            'Hacia Ciudad Vieja' => $ciudadVieja,
            'Hacia Tres Cruces' => $tresCruces,
            'Media hacia CV 4 ' . $yesterday->locale('es_UY')->isoFormat('ddd') . ' ant.' => $avgDataCV->values(),
            'Media hacia TC 4 ' . $yesterday->locale('es_UY')->isoFormat('ddd') . ' ant.' => $avgDataTC->values(),
        ]);

        $title = 'Número de bicis';
        $subtitle = '18 de Julio y Magallanes';
        $subsubtitle = ucwords($start->copy()->locale('es_UY')->isoFormat('dddd D MMMM YYYY'));

        // $formatLabels = fn (int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('H:i');

        // return $this->graphs->generateHourlyChartWithAvg($dayData, $labels, 1920, 1080, $title, $subtitle, $subsubtitle);
        return $this->graphs->generateGroupedBarChartWithLine(
            data: $dayData,
            labels: $labels,
            width: 1920,
            height: 1080,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            addWeather: true,
            type: 'hourly',
        );
    }

    private function weeklyGrouped()
    {
        $lastWeek = now('America/Montevideo')->subWeeks(1)->startOfDay();

        $start = $lastWeek->copy()->subWeeks(4)->startOfWeek();
        $end = $lastWeek->copy()->subWeeks(1)->endOfWeek();
        $avgQuery = sprintf(<<<QUERY
            SELECT
            sum(in_counts) as %s,
            sum(out_counts) as %s,
            count(*) as count,
            dia, mes, anio
            FROM observatorio.v_omo_conteo_bicisenda
            WHERE fecha_conteo >= %d AND fecha_conteo <= %d
            GROUP BY dia, mes, anio
        QUERY, BiciApi::CIUDAD_VIEJA, BiciApi::TRES_CRUCES, $start->getTimestampMs(), $end->getTimestampMs());

        $response = $this->biciApi->query(sql: $avgQuery);

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        $days = [
            'Lunes' ,
            'Martes' ,
            'Miércoles' ,
            'Jueves' ,
            'Viernes' ,
            'Sábado' ,
            'Domingo' ,
        ];

        // Get data grouped by day of the week
        $avgData = collect(range(0, count($values->first()) - 1))
            ->map(fn ($index) => $fields->combine($values->pluck($index)))
            ->groupBy(
                fn ($data) => ucfirst(Carbon::parse(
                    implode(
                        ' ',
                        [
                        $data['dia'],
                        $data['mes'],
                        $data['anio'],
                        ]
                    )
                )->locale('es')->isoFormat('dddd'))
            )->sortKeysUsing(
                fn ($a, $b) => array_search($a, $days, true) - array_search($b, $days, true)
            );

        /** @var \Illuminate\Support\Collection<string, float> $avgDataCV */
        $avgDataCV = $avgData->map->avg(BiciApi::CIUDAD_VIEJA);
        /** @var \Illuminate\Support\Collection<string, float> $avgDataTC */
        $avgDataTC = $avgData->map->avg(BiciApi::TRES_CRUCES);

        // Last week
        $start = $lastWeek->copy()->startOfWeek();
        $end = $lastWeek->copy()->endOfWeek();

        $currentData = $this->biciApi->getDataGroupByDay(start: $start, end: $end);
        $labels = $currentData->keys();
        $ciudadVieja = $currentData->pluck(BiciApi::CIUDAD_VIEJA);
        $tresCruces = $currentData->pluck(BiciApi::TRES_CRUCES);

        $dayData = collect([
            'Hacia Ciudad Vieja' => $ciudadVieja,
            'Hacia Tres Cruces' => $tresCruces,
            'Media hacia CV 4 sem. ant.' => $avgDataCV->values(),
            'Media hacia TC 4 sem. ant.' => $avgDataTC->values(),
        ]);

        $title = 'Número de bicis';
        $subtitle = '18 de Julio y Magallanes';
        $subsubtitle = 'Semana del ' . $start->locale('es_UY')->isoFormat('D');
        $subsubtitle .= ' al ' . $end->locale('es_UY')->isoFormat('LL');

        // $formatLabels = ;
        return $this->graphs->generateGroupedBarChartWithLine(
            data: $dayData,
            labels: $labels->all(),
            width: 1920,
            height: 1080,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            addWeather: true,
            type: 'daily',
            // formatLabels: $formatLabels,
        );
    }

    private function weeklyStacked()
    {
        $lastWeek = now('America/Montevideo')->subWeek()->startOfDay();

        $start = $lastWeek->copy()->subWeeks(5)->startOfWeek();
        $end = $lastWeek->copy()->subWeeks(1)->endOfWeek();
        $avgQuery = sprintf(<<<QUERY
            SELECT
            sum(in_counts) as %s,
            sum(out_counts) as %s,
            count(*) as count,
            dia, mes, anio
            FROM observatorio.v_omo_conteo_bicisenda
            WHERE fecha_conteo >= %d AND fecha_conteo <= %d
            GROUP BY dia, mes, anio
        QUERY, BiciApi::CIUDAD_VIEJA, BiciApi::TRES_CRUCES, $start->getTimestampMs(), $end->getTimestampMs());

        $response = $this->biciApi->query(sql: $avgQuery);

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        $days = [
            'Lunes' ,
            'Martes' ,
            'Miércoles' ,
            'Jueves' ,
            'Viernes' ,
            'Sábado' ,
            'Domingo' ,
        ];

        $avgData = collect(range(0, count($values->first()) - 1))
            ->map(fn ($index) => $fields->combine($values->pluck($index)))
            ->groupBy(
                fn ($data) => ucfirst(Carbon::parse(
                    implode(
                        ' ',
                        [
                        $data['dia'],
                        $data['mes'],
                        $data['anio'],
                        ]
                    )
                )->locale('es')->isoFormat('dddd'))
            )
            // Calculate average per day of
            ->sortKeysUsing(
                fn ($a, $b) => array_search($a, $days, true) - array_search($b, $days, true)
            )
            ->map(
                fn (Collection $dataDay) => $dataDay->avg(BiciApi::CIUDAD_VIEJA)
                    + $dataDay->avg(BiciApi::TRES_CRUCES)
            );

        // Last week
        $start = $lastWeek->copy()->startOfWeek();
        $end = $lastWeek->copy()->endOfWeek();

        $currentData = $this->biciApi->getDataGroupByDay(start: $start, end: $end);

        $ciudadVieja = $currentData->pluck(BiciApi::CIUDAD_VIEJA);
        $tresCruces = $currentData->pluck(BiciApi::TRES_CRUCES);

        $labels = $currentData->map(fn ($dayData) => Carbon::parse(
            implode(
                ' ',
                [
                    $dayData['dia'],
                    $dayData['mes'],
                    $dayData['anio'],
                ]
            )
        )->locale('es_UY')->isoFormat('ddd DD MMM'))->all();

        $dayData = collect([
            'Hacia Ciudad Vieja' => $ciudadVieja,
            'Hacia Tres Cruces' => $tresCruces,
            'Media 4 sem. ant.' => $avgData->values(),
        ]);

        $title = 'Número de bicis';
        $subtitle = '18 de Julio y Magallanes';
        $subsubtitle = 'Semana del ' . $start->locale('es_UY')->isoFormat('D');
        $subsubtitle .= ' al ' . $end->locale('es_UY')->isoFormat('LL');

        return $this->graphs->generateStackedBarchartWithLine($dayData, $labels, 1920, 1080, $title, $subtitle, $subsubtitle);
    }
}
