<?php

namespace App\Console\Commands\Bicisenda;

use App\Services\Bicisenda\BiciApi;
use App\Services\FediApi;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bici:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(BiciApi $api, FediApi $fediApi)
    {
        $start = now('America/Montevideo')->subDay()
            ->minute(0)
            ->second(0)
            ->microsecond(0)
            ->hour(22);
        // ->subMinutes(10);
        $end = now('America/Montevideo')->subDay()
            ->minute(0)
            ->second(0)
            ->microsecond(0)
            ->hour(23);

        $sql = <<< 'QUERY'
        SELECT
        sum(in_counts) as sentido_in,
        sum(out_counts) as sentido_out,
        hora,
        TO_CHAR((DATE_TRUNC('day', fecha_conteo at time zone '-03'))::timestamp, 'YYYY-MM-DD') as ff
        FROM observatorio.v_omo_conteo_bicisenda
            WHERE
                DATE_TRUNC('day', fecha_conteo at time zone '-03') = DATE_TRUNC('day', CURRENT_TIMESTAMP - INTERVAL '1' DAY)
            GROUP BY hora, DATE_TRUNC('day', fecha_conteo at time zone '-03')
            ORDER BY hora
        QUERY;

        // $sql = <<<'QUERY'
        //     SELECT
        //         dia, hora, min,
        //         fecha_conteo,
        //         DATE_TRUNC('day', fecha_conteo) as today,
        //         DATE_TRUNC('day', CURRENT_TIMESTAMP - INTERVAL '1' DAY) as yesterday
        //     FROM observatorio.v_omo_conteo_bicisenda
        //     WHERE
        //     QUERY;
        // $sql .= ' fecha_conteo >= ' . $start->getTimestampMs()
        //     . ' AND ' . 'fecha_conteo <= ' . $end->getTimestampMs();

        // $sql .= <<<'QUERYEND'
        //     ORDER BY hora

        // QUERYEND;

        // $sql = <<<'QUERY'
        //         SELECT
        //             in_counts, hora, min, seg
        //         FROM observatorio.v_omo_conteo_bicisenda
        //         WHERE
        //         QUERY;
        // $sql .= ' fecha_conteo >= ' . $start->getTimestampMs()
        // . ' AND ' . 'fecha_conteo <= ' . $end->getTimestampMs();
        // $sql .= ' ORDER BY hora, min, seg';
        dump($sql);
        $response = $api->query(now(), now(), $sql);
        // $response = $api->query($start, $end);
        dump($response->body());
        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');
        $data = $fields->combine($values); //->each(fn ($row) => dd($row, $fields) && $fields->combine($row));
        dd($data);

        // Last hour
        $start = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subHour();
        $end = now('America/Montevideo')->minute(0)->second(0)->microsecond(0);
        $currentData = $api->getData($start, $end);

        // Previous hour
        $start = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subHours(2);
        $end = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subHour();
        $prevHourData = $api->getData($start, $end);

        $diffPrevHour = [
            'sentido_ciudad_vieja' => $currentData['sentido_ciudad_vieja'] - $prevHourData['sentido_ciudad_vieja'],
            'sentido_tres_cruces' => $currentData['sentido_tres_cruces'] - $prevHourData['sentido_tres_cruces'],
        ];

        // Yesterday (same hour)
        $start = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subDay()->subHours(1);
        $end = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subDay()->addHour();
        $yesterdayData = $api->getData($start, $end);

        $diffYesterday = [
            'sentido_ciudad_vieja' => $currentData['sentido_ciudad_vieja'] - $yesterdayData['sentido_ciudad_vieja'],
            'sentido_tres_cruces' => $currentData['sentido_tres_cruces'] - $yesterdayData['sentido_tres_cruces'],
        ];

        // Last Month (same day and hour)
        $start = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subMonth()->subHours(1);
        $end = now('America/Montevideo')->minute(0)->second(0)->microsecond(0)->subMonth()->addHour();
        $lastMonthData = $api->getData($start, $end);

        $diffLastMonth = [
            'sentido_ciudad_vieja' => $currentData['sentido_ciudad_vieja'] - $lastMonthData['sentido_ciudad_vieja'],
            'sentido_tres_cruces' => $currentData['sentido_tres_cruces'] - $lastMonthData['sentido_tres_cruces'],
        ];

        $message = "<p>Durante la última hora ({$currentData['hora']}:00 - " . $currentData['hora'] + 1;
        $message .= ':00), el tránsito por la ciclovía de 18 de Julio (en el cruce con Magallanes) fue de:</p>';
        $message .= "<ul><li>🏛 {$currentData['sentido_ciudad_vieja']} 🚲 hacia Ciudad Vieja (";
        if ($diffPrevHour['sentido_ciudad_vieja'] > 0) {
            $message .= "+{$diffPrevHour['sentido_ciudad_vieja']}";
        } elseif ($diffPrevHour['sentido_ciudad_vieja'] < 0) {
            $message .= "{$diffPrevHour['sentido_ciudad_vieja']}";
        } else {
            $message .= '=';
        }
        $message .= ' hora previa)</li>';
        $message .= "<li>🚍 {$currentData['sentido_tres_cruces']} 🚲 hacia Tres Cruces (";
        if ($diffPrevHour['sentido_tres_cruces'] > 0) {
            $message .= "+ {$diffPrevHour['sentido_tres_cruces']}";
        } elseif ($diffPrevHour['sentido_tres_cruces'] < 0) {
            $message .= $diffPrevHour['sentido_tres_cruces'];
        } else {
            $message .= '=';
        }
        $message .= ' hora previa)</li></ul>';

        // $message .= "<p>Comparando con la hora anterior ({$prevHourData['hora']}:00 - ";
        // $message .= $prevHourData['hora'] + 1 . ':00), ';
        // $message .= in_array($diffPrevHour['sentido_ciudad_vieja'], [-1, 1]) ? 'fue ' : 'fueron ';
        // if ($diffPrevHour['sentido_ciudad_vieja'] > 0) {
        //     $message .= "{$diffPrevHour['sentido_ciudad_vieja']} más ";
        // } elseif ($diffPrevHour['sentido_ciudad_vieja'] < 0) {
        //     $message .= ($diffPrevHour['sentido_ciudad_vieja'] * -1) . ' menos ';
        // } else {
        //     $message .= 'los mismos ';
        // }
        // $message .= 'hacia Ciudad Vieja y ';
        // if ($diffPrevHour['sentido_tres_cruces'] > 0) {
        //     $message .= "{$diffPrevHour['sentido_tres_cruces']} más";
        // } elseif ($diffPrevHour['sentido_tres_cruces'] < 0) {
        //     $message .= ($diffPrevHour['sentido_tres_cruces'] * -1) . ' menos';
        // } else {
        //     $message .= 'los mismos';
        // }
        // $message .= ' hacia Tres Cruces.</p>';

        dump('current', $currentData, 'prevHour', $prevHourData, 'yesterday', $yesterdayData, 'lastMonth', $lastMonthData);

        $fediApi->publishPost($message);
        $this->info($message);
    }
}
