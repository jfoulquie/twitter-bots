<?php

namespace App\Console\Commands\Defancify;

use App\DTO\Twitter\Tweet;
use App\Repositories\Dictionaries;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;

class TestTweet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'defancify:test-tweet {tweetId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test defancification for a given tweet';

    protected Tweet $tweet;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api) : int
    {
        $tweetId = $this->argument('tweetId');
        if (!is_string($tweetId)) {
            $this->error('Tweet id must be a single value');
            return -1;
        }
        $originalArr = $api->get('statuses/show', [
            'id' => $tweetId,
            'include_entities' => true,
            'tweet_mode' => 'extended',
        ]);

        if (!is_array($originalArr)) {
            $this->error("Could not retrieve the original tweet {$tweetId}");
            return -1;
        }

        Log::debug('Response:', $originalArr);

        $this->tweet = new Tweet($originalArr);
        $translation = app(Dictionaries::class)->translate($this->tweet->getText());
        $tweets = $this->getTweets($translation);

        $this->info($translation);
        dump($tweets);
        foreach ($tweets as $tweet) {
            dump(Tweet::countChars($tweet));
        }
        // $this->info(mb_strlen($translation) . ' chars');

        return 0;
    }

    private function getTweets(string $translation) : array
    {
        $tweets = [];
        $tweet = '';
        $originalText = collect(mb_str_split('@' . $this->tweet->getAuthor()->getUsername() . ' ' . $translation))->reverse();
        while ($originalText->isNotEmpty()) {
            while (Tweet::countChars($tweet) <= 265 && $originalText->isNotEmpty()) { // Being on the safe side
                $tweet .= $originalText->pop();
            }
            // Another tweet is coming, cut the string on the previous word
            if ($originalText->isNotEmpty()) {
                $lastSpacePos = mb_strrpos($tweet, ' ');
                if ($lastSpacePos !== false) {
                    $remaining = trim(mb_substr($tweet, $lastSpacePos));
                    $tweet = mb_substr($tweet, 0, $lastSpacePos);
                }

                if (!empty($remaining)) {
                    $remainingArray = mb_str_split(Tweet::mbStrrev($remaining));
                    foreach ($remainingArray as $char) {
                        $originalText->push($char);
                    }
                }
            }
            $tweets[] = $tweet;
            $tweet = '';
        }

        if (count($tweets) > 1) {
            foreach ($tweets as $i => &$tweet) {
                $tweet .= ' (' . ($i + 1) . '/' . count($tweets) . ')';
            }
        }

        return $tweets;
    }
}
