<?php

namespace App\Console\Commands\EveryUruguayan;

use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;

class ImportData2 extends BaseImportData
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:import2 {csvfile} {--hogar_id=} {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data from CSV';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        // $count = LazyCollection::make(function () use ($csvFile) {
        //     $handle = fopen($csvFile, 'r');

        //     while (($line = fgetcsv($handle)) !== false) {
        //         yield $line;
        //     }
        //     fclose($handle);
        // })->count();

        $count = 145167;
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
            //
        })->each(function (?array $line, int $index) {
            if (!is_array($line)) {
                return;
            }

            $this->output->progressAdvance();
            if ($index === 0) {
                $this->headers = array_flip($line);
                return;
            }

            if ($index < intval($this->option('offset'))) {
                return;
            }

            if (!empty($this->option('hogar_id')) && $this->getData($line, 'numero') != $this->option('hogar_id')) {
                return;
            }

            // $tipoCentro = trim(Arr::get(
            //     $line,
            //     $this->headers['e194'],   // 3 años o más NA
            //     Arr::get($line, $this->headers['e50_cv']) // AL
            // ), "0 \n\r\t\v\0");

            $tiempoDejoEmpleo = null;
            $años = $this->getData($line, 'f118_2', true);
            $meses = $this->getData($line, 'f118_1', true);
            if ($años) {
                $tiempoDejoEmpleo = "$años año" . ($años > 1 ? 's' : '');
            }

            if ($meses) {
                if ($tiempoDejoEmpleo === null) {
                    $tiempoDejoEmpleo = "$meses mes";
                } else {
                    $tiempoDejoEmpleo .= " y $meses mes";
                }
                $tiempoDejoEmpleo .= ($meses > 1 ? 'es' : '');
            }

            try {
                Uruguayan::where('hogar_id', Arr::get($line, $this->headers['numero']))
                    ->where('persona_id', Arr::get($line, $this->headers['nper']))
                    ->update([
                        // 'tipoCentro' => $tipoCentro,

                        // Universidad
                        'tipo_universidad' => $this->getData($line, 'e219'),
                        'años_aprobados_uni' => $this->getData($line, 'e51_9'),
                        'carrera' => $this->getData($line, 'e220_1'),

                        // Formación docente
                        'asistencia_fdocente' => $this->getData($line, 'e215'),
                        'finalizo_fdocente' => $this->getData($line, 'e215_1') === 'Sí',
                        'tipo_fdocente' => $this->getData($line, 'e216'),
                        'años_aprobados_fd' => $this->getData($line, 'e51_8'),
                        'carrera_fd' => $this->getData($line, 'e217_1'),

                        // Dejaron trabajo
                        'tiempo_dejo_empleo' => $tiempoDejoEmpleo,
                        'categoria_anterior_trabajo' => $this->getData($line, 'f121'),
                        'aportaba_caja' => $this->getData($line, 'f123') === 'Sí',
                        'trabajo_antes' => $this->getData($line, 'f116') === 'Sí',

                ]);
            } catch (QueryException $e) {
                $this->error('QueryException: ' . $e->getMessage());
                $this->error(json_encode($line));
                dd('error');
            }
        });

        $this->output->progressFinish();
        return 0;
    }

    // private function getAccesoInstiSalud(array $line) : ?string
    // {
    //     $fields = [
    //         'e45_1_1_cv',
    //         // 'e45_1_1_1_cv',
    //         'e45_2_1_cv',
    //         // 'e45_2_1_1_cv',
    //         'e45_3_1_cv',
    //         // 'e45_3_1_1_cv',
    //         'e45_4_1_cv',
    //         // 'e45_4_1_1_cv'
    //     ];
    //     foreach ($fields as $field) {
    //         if ($acceso = $this->getData($line, $field)) {
    //             return $acceso;
    //         }
    //     }
    //     return null;
    // }
}
