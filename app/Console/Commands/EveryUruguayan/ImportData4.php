<?php

namespace App\Console\Commands\EveryUruguayan;

use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;

class ImportData4 extends BaseImportData
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:import4 {csvfile} {--hogar_id=} {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data from CSV';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        $count = 145167;
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
            //
        })->each(function (?array $line, int $index) {
            if (!is_array($line)) {
                return;
            }

            $this->output->progressAdvance();
            if ($index === 0) {
                $this->headers = array_flip($line);
                return;
            }

            if ($index < intval($this->option('offset'))) {
                return;
            }

            if (!empty($this->option('hogar_id')) && $this->getData($line, 'numero') != $this->option('hogar_id')) {
                return;
            }

            try {
                Uruguayan::where('hogar_id', Arr::get($line, $this->headers['numero']))
                    ->where('persona_id', Arr::get($line, $this->headers['nper']))
                    ->update([
                        'parentesco' => $this->getData($line, 'e30'),

                        'cantidad_trabajos' => $this->getData($line, 'f70'),
                        'categoria_trabajo' => $this->getData($line, 'f73'),
                        'sector_publico' => $this->getData($line, 'f74'),
                        'tamano_empresa' => $this->getData($line, 'f77'),
                        'lugar_de_trabajo' => $this->getData($line, 'f78'),
                        'ciuo_08_trabajo' => $this->getData($line, 'f71_2'),
                        'forma_tributacion' => $this->getData($line, 'f265'),

                        'bachillerato_tecnologico_curso' => $this->getData($line, 'e209_1'),
                        'asistio_ed_tecnica' => $this->getData($line, 'e212'),
                        'finalizo_ed_tecnica' => $this->getData($line, 'e212_1') === 'Sí',
                        'tipo_centro_ed_tecnica' => $this->getData($line, 'e213'),
                        'años_aprobados_ed_tecnica' => $this->getData($line, 'e51_7', true),
                        'ed_media_curso' => $this->getData($line, 'e214_1'),
                        'asistio_terciario' => $this->getData($line, 'e221'),
                        // 'finalizo_terciario' => $this->getData($line, 'e221_1') === 'Sí',
                        'tipo_centro_terciario' => $this->getData($line, 'e222'),
                        'años_aprobados_terciario' => $this->getData($line, 'e51_10', true),
                        'terciario_curso' => $this->getData($line, 'e223_1'),
                        'asistio_posgrado' => $this->getData($line, 'e224'),
                        'finalizo_posgrado' => $this->getData($line, 'e224_1') === 'Sí',
                        'tipo_centro_posgrado' => $this->getData($line, 'e225'),
                        'años_aprobados_posgrado' => $this->getData($line, 'e51_11', true),
                        'posgrado_curso' => $this->getData($line, 'e226_1'),
                ]);
            } catch (QueryException $e) {
                $this->error('QueryException: ' . $e->getMessage());
                $this->error(json_encode($line));
                dd('error');
            }
        });

        $this->output->progressFinish();
        return 0;
    }
}
