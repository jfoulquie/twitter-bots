<?php

namespace App\Console\Commands\EveryUruguayan;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;

abstract class BaseImportData extends Command
{
    protected array $headers;

    protected function getData(array $line, string $header, bool $allowZero = false) : ?string
    {
        $data = trim(Arr::get($line, $this->headers[$header]), "\n\r\t\v\0");

        if (!$allowZero && in_array($data, ['0', 0], true)) {
            $data = null;
        }
        if ($data === '') {
            $data = null;
        }

        return $data;
    }
}
