<?php

namespace App\Console\Commands\PoliticosAlert;

use App\Jobs\PoliticosAlert\UpdateFriendsList;
use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pol-alert:update {--only-new} {--no-friends}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update info from members of the list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        $params = [
            'list_id' => '1442938234577711104',     // Política Alert
            'skip_status' => true,
            'count' => 5000,    // max
        ];
        $response = $api->get('lists/members', $params);
        if (!is_array($response) || !isset($response['users'])) {
            Log::warning('The list seems to have no members', Arr::wrap($response));
            return -1;
        }
        /** @var array<string, mixed> $userTweet */
        foreach ($response['users'] as $userTweet) {
            Log::debug('processing @' . $userTweet['screen_name']);
            $user = TwitterUser::createOrUpdateFromTweet($userTweet);

            if ($this->option('only-new') && $user->friends()->exists()) {
                continue;
            }
            // Add the job with a random delay so they aren't processed in the same order always
            if (!$this->option('no-friends')) {
                UpdateFriendsList::dispatch($user);
            }
        }
        return 0;
    }
}
