<?php

namespace App\Console\Commands\IPAP;

use App\Services\Google\GoogleSheets;
use Exception;
use Google\Service\Sheets;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\EmbeddedChart;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\UpdateChartSpecRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipap:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->ipapFilesBasePath = storage_path('app/ipap/');
        $this->spreadsheetId = '1f8_95ZXiB_Cz1ZVznkpd34O3O0zO8Huvs5O7bhUXZ2U';
        $this->ipapId = '1277472891';
        $this->chartsId = '143654460';
        $this->preciosUYId = '1582206519';
        $this->preciosMediosId = '34493036';
        $this->indicePreciosId = '1446640599';
        $this->papasChipId = '1755359182';

        $this->charts = [
            // "IPAP vs Inflación", (Variaciones interanuales, serie histórica)
            48278545 => [
                'domain' => true,
                'series' => true,
            ],
            // "IPAP por rubro", (Variación interanual, último mes)
            2041588925 => [
                'domain' => true,
                'series' => true,
            ],
            // "IPAP vs Inflación", (Variación interanual)
            1464077539 => [
                'domain' => true,
                'series' => true,
            ],
            // "Evolución Precio de Asado con Picadita", (Histórico)
            913607062 => [
                'domain' => true,
                'series' => true,
            ],
            // "Evolución Incidencia de rubros en el precio del Asado con Picadita",
            458060236 => [
                'domain' => false,
                'series' => true,
            ],
            // "Precio Asado con Picadita por Rubro" (Mes anterior vs Actual),
            1636679044 => [
                'domain' => false,
                'series' => true,
            ],
            // "Evolución Precio de Asado con Picadita",
            2055932017 => [
                'domain' => true,
                'series' => true,
            ],
            // "Precio Asado con Picadita por Rubro" (Un año atrás vs Actual),
            1632418179 => [
                'domain' => false,
                'series' => true,
            ],
            // "Incidencia por rubro del Asado con Picadita" (Mes actual),
            211202585 => [
                'domain' => true,
                'series' => true,
            ],
          ];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Sheets $service)
    {
        $sheet = "'IPAP'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:B";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
        $allColumns = collect($response->getValues());
        $columnsWithData = $allColumns->where(1, '!=', 0);
        $lastRowWithData = $columnsWithData->keys()->last() + 1; // Add 1 because the array is zero-indexed
        $lastDateWithData = GoogleSheets::getDateFromSerialNumber($columnsWithData->last()[0]);
        $spreadsheet = $service->spreadsheets->get($this->spreadsheetId);
        /** @var \Google\Service\Sheets\EmbeddedChart[] */
        $charts = [];
        foreach ($spreadsheet->sheets as $sheet) {
            $charts = array_merge($charts, $sheet->charts);
        }
        $chart = $charts[0];
        $chartsInfo = array_map(function (EmbeddedChart $chart) {
            $title = $chart->getSpec()->getTitle() . ' (' . $chart->getSpec()->getSubtitle() . ')';
            if ($chart->getSpec()->getBasicChart()) {
                return [
                    'id' => $chart->getChartId(),
                    'title' => $title,
                    // 'domains' => $chart->getSpec()->getBasicChart() ?? $chart->getSpec()->getPieChart(),
                    // 'domains' => Arr::pluck(
                    //     $chart->getSpec()->getBasicChart()->getDomains(),
                    //     'domain.sourceRange.sources'
                    // ),
                    // 'series' => Arr::pluck(
                    //     $chart->getSpec()->getBasicChart()->getSeries(),
                    //     'series.sourceRange.sources'
                    // ),
                    // 'domains' => Arr::pluck($chart->getSpec()->getBasicChart()->getDomains(), 'sourceRange.sources'),
                ];
            } elseif ($chart->getSpec()->getPieChart()) {
                return [
                    'id' => $chart->getChartId(),
                    'title' => $title,
                    // 'domains' => $chart->getSpec()->getPieChart()->getDomain()->getSourceRange()->getSources(),
                ];
            } else {
                throw new Exception('unknown chart');
            }
        }, $charts);
        // dd($chart->getSpec()->getBasicChart()->getDomains());
        dd($chartsInfo);
        $spec = $chart->getSpec();
        // dump(
        // $spec->getTitle(),
        // $spec->getSubtitle(),
        //  $spec->getBasicChart()->getSeries()
        // );
        // dump('++++++++++++++++++++++++++++++');

        $basicChart = $spec->getBasicChart();

        // Update domains
        $domains = $basicChart->getDomains();
        foreach ($domains as &$basicChartDomain) {
            $sources = $basicChartDomain->getDomain()->getSourceRange()->getSources();
            foreach ($sources as &$source) {
                $source->setEndRowIndex($lastRowWithData);
            }
            unset($source);
            $domain = $basicChartDomain->getDomain();
            $sourceRange = $domain->getSourceRange();
            $sourceRange->setSources($sources);
            $domain->setSourceRange($sourceRange);
            $basicChartDomain->setDomain($domain);
        }
        unset($basicChartDomain);

        // Update Series
        $basicSeries = $basicChart->getSeries();
        foreach ($basicSeries as &$basicSerie) {
            $series = $basicSerie->getSeries();
            $sourceRange = $series->getSourceRange();
            $sources = $sourceRange->getSources();
            foreach ($sources as &$source) {
                $source->setEndRowIndex($lastRowWithData);
            }
            unset($source);
            $sourceRange->setSources($sources);
            $series->setSourceRange($sourceRange);
            $basicSerie->setSeries($series);
        }
        unset($basicSerie);

        // Update spec
        $basicChart->setDomains($domains);
        $basicChart->setSeries($basicSeries);

        $spec->setBasicChart($basicChart);

        // dd($spec->getBasicChart()->getSeries());

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);

        $request = new Request();
        $request->setUpdateChartSpec($updateChartSpec);

        $batchUpdate = new BatchUpdateSpreadsheetRequest();
        $batchUpdate->setRequests($request);
        $response = $service->spreadsheets->batchUpdate($this->spreadsheetId, $batchUpdate);
        dump($response);
        // $service->spreadsheets->batchUpdate($this->spreadsheetId);

        // foreach ($charts as $chart) {

        // }
        dump($charts);
        return 0;
    }
}
