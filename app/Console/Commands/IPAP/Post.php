<?php

namespace App\Console\Commands\IPAP;

use App\Services\IPAP;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use j3j5\TwitterApio;
use RuntimeException;
use Symfony\Component\Console\Output\OutputInterface;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipap:post {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post the latest IPAP to twitter';

    private IPAP $ipap;
    private Carbon $currentMonth;
    private string $lastTweetedFile = 'ipap/last_tweeted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(IPAP $ipap, TwitterApio $twitterApi)
    {
        $this->ipap = $ipap;
        $date = $this->option('date');
        if (!is_string($date) && !is_null($date)) {
            throw new RuntimeException('Invalid date');
        }

        $this->currentMonth = $this->ipap->init($date);

        if ($date === null) {
            // Check last tweeted, after init, if last tweeted === $this->currentMonth
            $lastTweetedTs = Storage::get($this->lastTweetedFile);
            if (!is_string($lastTweetedTs) || !is_numeric($lastTweetedTs)) {
                throw new RuntimeException('Invalid last tweeted file, empty??');
            }
            $lastTweeted = Carbon::createFromTimestampUTC($lastTweetedTs);

            if ($this->currentMonth->year === $lastTweeted->year && $this->currentMonth->month === $lastTweeted->month) {
                $this->info('Already tweeted', OutputInterface::VERBOSITY_VERBOSE);
                return 0;
            }
        }

        $charts = collect($this->ipap->generateCharts())->split(2);
        $tweets = $this->getTweets();

        foreach ($tweets as $i => $tweet) {
            $tweetImgs = [];
            $images = $charts->get($i);
            if (!$images instanceof Collection) {
                throw new RuntimeException('Something went wrong while parsing the generated graphs');
            }

            $images->each(function ($img, $graphName) use ($twitterApi, &$tweetImgs) {
                $mediaResponse = $twitterApi->uploadMedia([
                    'media' => $img,
                ]);

                $altText = $this->getAltText($graphName);
                // Add alt text
                $twitterApi->addMetadata([
                    'media_id' => $mediaResponse['media_id_string'],
                    'alt_text' => [
                        'text' => $altText,
                    ],
                ]);
                $tweetImgs[] = $mediaResponse['media_id_string'];
            });
            $tweetOptions = [
                'status' => $tweet,
                'lat' => '-34.90495052870264',
                'long' => '-56.19292752903653',
                'display_coordinates' => true,
                'media_ids' => implode(',', $tweetImgs),
            ];

            if (isset($tweetResponse, $tweetResponse['id_str'])) {
                $tweetOptions['in_reply_to_status_id'] = $tweetResponse['id_str'];
                $tweetOptions['status'] = '@IPAPBot ' . $tweetOptions['status'];
            }

            $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);
        }

        Storage::put($this->lastTweetedFile, (string) $this->currentMonth->timestamp);

        return 0;
    }

    private function getTweets() : array
    {
        $tweets = [];
        // Build tweets with the prices, the IPAP levels and the charts
        $totalCurrentPrice = $this->ipap->getPrice($this->currentMonth, 'Asado con Picadita');
        $totalLastMonthPrice = $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Asado con Picadita');
        $totalLastYearPrice = $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Asado con Picadita');
        $diffLastMonth = $totalCurrentPrice - $totalLastMonthPrice;
        $diffLastYear = $totalCurrentPrice - $totalLastYearPrice;

        $statusPrices = mb_strtoupper($this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY')) . PHP_EOL . PHP_EOL;
        $statusPrices .= 'En el mes de ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . ' ';
        $statusPrices .= "el precio de un asado con picadita fue de \${$totalCurrentPrice}. ";
        $statusPrices .= 'Esto es $' . abs($diffLastMonth) . ' más ' . ($diffLastMonth > 0 ? 'caro' : 'barato') .
                         ' que el mes anterior (' . $this->currentMonth->copy()->subMonth()->locale('es_UY')->isoFormat('MMM YYYY');
        $statusPrices .= ') y $' . abs($diffLastYear) . ' más ' . ($diffLastYear > 0 ? 'caro' : 'barato') . ' que el año anterior (' .
                        $this->currentMonth->copy()->subYear()->locale('es_UY')->isoFormat('MMM YYYY') . ')' . PHP_EOL . PHP_EOL;
        // Bebida: string, Picadita: string, Asado: string, Ensalada: string, Postre: string, 'Asado con Picadita': string}
        $statusPrices .= '🍖🥩 $' . $this->ipap->getPrice($this->currentMonth, 'Asado') . PHP_EOL;
        $statusPrices .= '🍷🍺 $' . $this->ipap->getPrice($this->currentMonth, 'Bebida') . PHP_EOL;
        $statusPrices .= '🧀🥖 $' . $this->ipap->getPrice($this->currentMonth, 'Picadita') . PHP_EOL;
        $statusPrices .= '🍅🥬 $' . $this->ipap->getPrice($this->currentMonth, 'Ensalada') . PHP_EOL;
        $statusPrices .= '🍧🍦 $' . $this->ipap->getPrice($this->currentMonth, 'Postre') . PHP_EOL;

        $tweets[] = $statusPrices;

        $ipapCurrent = $this->ipap->getInflationRate($this->currentMonth, 'IPAP');
        $inflationCurrent = $this->ipap->getInflationRate($this->currentMonth, 'Inflación');
        $statusRates = mb_strtoupper($this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY')) . PHP_EOL . PHP_EOL;
        $statusRates .= 'En el mes de ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . ' ';
        $statusRates .= 'el IPAP interanual fue de ' . $ipapCurrent . '% ';
        $statusRates .= '(la inflación en el mismo mes fue de ' . $inflationCurrent . '%)' . PHP_EOL . PHP_EOL;
        $statusRates .= '🍖🥩 ' . $this->ipap->getInflationRate($this->currentMonth, 'Asado') . '% ' . PHP_EOL;
        $statusRates .= '🍷🍺 ' . $this->ipap->getInflationRate($this->currentMonth, 'Bebida') . '% ' . PHP_EOL;
        $statusRates .= '🧀🥖 ' . $this->ipap->getInflationRate($this->currentMonth, 'Picadita') . '% ' . PHP_EOL;
        $statusRates .= '🍅🥬 ' . $this->ipap->getInflationRate($this->currentMonth, 'Ensalada') . '% ' . PHP_EOL;
        $statusRates .= '🍧🍦 ' . $this->ipap->getInflationRate($this->currentMonth, 'Postre') . '% ' . PHP_EOL;
        $tweets[] = $statusRates;

        return $tweets;
    }

    private function getAltText(string $name) : string
    {
        switch($name) {
            case 'precio-por-rubro-año':
                $text = 'Gráfico de barras que muestra el precio del asado con picadita separado por rubro. Cada rubro tiene dos barras que muestran el precio hace un año y el precio actual.';
                $text .= ' Los precios son los siguientes (' . $this->currentMonth->copy()->subYear()->locale('es_UY')->isoFormat('MMMM \d\e YYYY');
                $text .= '  vs ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . '):' . PHP_EOL . PHP_EOL;
                $text .= 'Asado: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Asado') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Asado') . PHP_EOL;
                $text .= 'Bebida: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Bebida') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Bebida') . PHP_EOL;
                $text .= 'Picadita: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Picadita') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Picadita') . PHP_EOL;
                $text .= 'Ensalada: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Ensalada') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Ensalada') . PHP_EOL;
                $text .= 'Postre: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Postre') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Postre') . PHP_EOL;
                $text .= 'Asado con Picadita: $' . $this->ipap->getPrice($this->currentMonth->copy()->subYear(), 'Asado con Picadita') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Asado con Picadita') . PHP_EOL;
                return $text;
            case 'precio-por-rubro-mes':
                $text = 'Gráfico de barras que muestra el precio del asado con picadita separado por rubro. Cada rubro tiene dos barras que muestran el precio hace un mes y el precio actual.';
                $text .= ' Los precios son los siguientes (' . $this->currentMonth->copy()->subMonth()->locale('es_UY')->isoFormat('MMMM \d\e YYYY');
                $text .= '  vs ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . '):' . PHP_EOL . PHP_EOL;
                $text .= 'Asado: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Asado') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Asado') . PHP_EOL;
                $text .= 'Bebida: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Bebida') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Bebida') . PHP_EOL;
                $text .= 'Picadita: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Picadita') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Picadita') . PHP_EOL;
                $text .= 'Ensalada: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Ensalada') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Ensalada') . PHP_EOL;
                $text .= 'Postre: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Postre') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Postre') . PHP_EOL;
                $text .= 'Asado con Picadita: $' . $this->ipap->getPrice($this->currentMonth->copy()->subMonth(), 'Asado con Picadita') . ' vs $' . $this->ipap->getPrice($this->currentMonth, 'Asado con Picadita') . PHP_EOL;
                return $text;
            case 'evolucion-precios':
                $text = 'Gráfico de áreas apiladas (stacked line chart) que muestra la evolución del precio del asado con picadita.';
                $text .= ' en general, es un área ascendente que va desde Diciembre de 2010, donde el asado con picadita costaba $931';
                $text .= ' hasta ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . ' que la canasta completa cuesta';
                $text .= ' $' . $this->ipap->getPrice($this->currentMonth, 'Asado con Picadita');
                return $text;
            case 'incidencia':
                $text = 'Gráfico de torta que muestra la incidencia de cada rubro en el total del precio de un asado con picadita.' . PHP_EOL . PHP_EOL;
                $text .= 'Asado: ' . $this->ipap->getIncidencia('Asado') . '%' . PHP_EOL;
                $text .= 'Bebida: ' . $this->ipap->getIncidencia('Bebida') . '%' . PHP_EOL;
                $text .= 'Picadita: ' . $this->ipap->getIncidencia('Picadita') . '%' . PHP_EOL;
                $text .= 'Ensalada: ' . $this->ipap->getIncidencia('Ensalada') . '%' . PHP_EOL;
                $text .= 'Postre: ' . $this->ipap->getIncidencia('Postre') . '%' . PHP_EOL;
                return $text;
            case 'ipap-x-rubro':
                $text = 'Gráfico de barras que muestra los niveles de inflación para los rubros incluidos en el IPAP así como el total y la inflación general para el mes de ' .
                        $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY') . '.' . PHP_EOL . PHP_EOL;
                $text .= 'Asado: ' . $this->ipap->getInflationRate($this->currentMonth, 'Asado') . PHP_EOL;
                $text .= 'Bebida: ' . $this->ipap->getInflationRate($this->currentMonth, 'Bebida') . PHP_EOL;
                $text .= 'Picadita: ' . $this->ipap->getInflationRate($this->currentMonth, 'Picadita') . PHP_EOL;
                $text .= 'Ensalada: ' . $this->ipap->getInflationRate($this->currentMonth, 'Ensalada') . PHP_EOL;
                $text .= 'Postre: ' . $this->ipap->getInflationRate($this->currentMonth, 'Postre') . PHP_EOL;
                $text .= 'IPAP: ' . $this->ipap->getInflationRate($this->currentMonth, 'IPAP') . PHP_EOL;
                $text .= 'Inflación: ' . $this->ipap->getInflationRate($this->currentMonth, 'Inflación') . PHP_EOL;
                return $text;
            case 'ipap-vs-inflacion':
                $text = 'Gráfico de líneas que muestra la evolución temporal de la inflación y del IPAP desde Diciembre de 2011 hasta ' . $this->currentMonth->locale('es_UY')->isoFormat('MMMM \d\e YYYY');
                return $text;
            default:
                return '';
        }
    }
}
