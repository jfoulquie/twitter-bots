<?php

namespace App\Console\Commands\IPAP;

use App\Services\Google\GoogleSheets;
use App\Traits\Graphs\GeneratesGraphs;
use Exception;
use Google\Service\Sheets;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\ChartSpec;
use Google\Service\Sheets\EmbeddedChart;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\UpdateChartSpecRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class UpdateCharts extends Command
{
    use GeneratesGraphs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipap:update-charts {--latestMonth=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update graphs on Google Sheets';

    private Carbon $firstMonth;
    private Carbon $latestMonth;
    private int $lastRowWithData;
    /** @var array<int, array> */
    private array $chartFunctions;
    private string $spreadsheetId;
    /** @var \Illuminate\Support\Collection<int, \Google\Service\Sheets\EmbeddedChart> */
    private Collection $charts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->spreadsheetId = '1f8_95ZXiB_Cz1ZVznkpd34O3O0zO8Huvs5O7bhUXZ2U';

        $this->chartFunctions = [
            // "IPAP vs Inflación", (Variaciones interanuales, serie histórica)
            48278545 => [$this, 'ipapVsInflacionHistorico'],
            // "IPAP por rubro", (Variación interanual, último mes)
            2041588925 => [$this, 'ipapPorRubroUltimoMes'],
            // "IPAP vs Inflación", (Variación interanual)
            // 1464077539 => [$this, 'ipapVsInflacionUltimoAño'],
            // "Evolución Precio de Asado con Picadita", (Histórico)
            913607062 => [$this, 'evolucionPrecioIpapHistorico'],
            // "Evolución Precio de Asado con Picadita (Último Año)",
            // 2055932017 => [$this, 'evolucionIpapUltimoAño'],
            // "Evolución Incidencia de rubros en el precio del Asado con Picadita",
            458060236 => [$this, 'evolucionIncidenciaPrecioPorRubro'],
            // "Precio Asado con Picadita por Rubro" (Mes anterior vs Actual),
            1636679044 => [$this, 'precioIpapMesActualVsMesAnterior'],
            // "Precio Asado con Picadita por Rubro" (Un año atrás vs Actual),
            1632418179 => [$this, 'precioIpapAñoActualVsAñoAnterior'],
            // "Incidencia por rubro del Asado con Picadita" (Mes actual),
            211202585 => [$this, 'incidenciaPorRubroIpapMesActual'],
          ];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Sheets $service)
    {
        /** @phpstan-ignore-next-line */
        app()->log->pushProcessor(static function (array $record) {
            $record['message'] = "[IPAP][UPDATE-SHEETS] {$record['message']}";
            return $record;
        });

        $sheet = "'IPAP'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:B";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $allColumns = collect($values);
        $this->firstMonth = Carbon::parse('2011-12-01');
        $columnsWithData = $allColumns->where('1', '!=', 0);
        if (is_string($this->option('latestMonth')) && !empty($this->option('latestMonth'))) {
            $this->latestMonth = Carbon::parse($this->option('latestMonth'));
            $dateNumber = GoogleSheets::getSerialNumberFromDate($this->latestMonth);
            $row = $columnsWithData->firstWhere('0', $dateNumber);
            if ($row === null) {
                throw new Exception('Month not found');
            }
            $this->lastRowWithData = (int) $columnsWithData->search($row) + 1;
        } else {
            $this->lastRowWithData = (int) $columnsWithData->keys()->last() + 1; // Add 1 because the array is zero-indexed
            $lastColumn = $columnsWithData->last();
            if (!isset($lastColumn[0])) {
                throw new Exception('No months found');
            }
            $this->latestMonth = GoogleSheets::getDateFromSerialNumber((float) $lastColumn[0]);
        }

        // Make sure the lastRowWithData makes sense
        if ($this->lastRowWithData < 2) {
            $this->error("No data found in $sheet");
            return 1;
        }

        $spreadsheet = $service->spreadsheets->get($this->spreadsheetId);
        /** @var \Google\Service\Sheets\Sheet $sheet */
        $sheet = Arr::first($spreadsheet->getSheets(), function ($sheet) {
            return $sheet->getProperties()->getTitle() === 'Charts';
        });
        $this->charts = collect($sheet->getCharts());

        $requests = [];

        $this->charts->each(function ($chart) use (&$requests) {
            if (isset($this->chartFunctions[$chart->getChartId()])) {
                list($class, $method) = $this->chartFunctions[$chart->getChartId()];
                $fn = [$class, $method];
                if (method_exists($class, $method) && is_callable($fn)) {
                    $updateChartSpec = call_user_func($fn, $chart);

                    $request = new Request();
                    $request->setUpdateChartSpec($updateChartSpec);
                    $requests[] = $request;
                }
            }
        });
        $batchUpdate = new BatchUpdateSpreadsheetRequest();
        $batchUpdate->setRequests($requests);
        $response = $service->spreadsheets->batchUpdate($this->spreadsheetId, $batchUpdate);

        return 0;
    }

    // IPAP vs Inflación (Variaciones interanuales, serie histórica)
    private function ipapVsInflacionHistorico(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();

        $subtitle = 'Variaciones interanuales, serie histórica (' . $this->firstMonth->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);

        // Update domains
        $spec = $this->updateDomains($spec, null, $this->lastRowWithData);
        $spec = $this->updateSeries($spec, null, $this->lastRowWithData);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function ipapPorRubroUltimoMes(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();

        $subtitle = 'Variación interanual, último mes (' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);

        // Update domains
        // $spec = $this->updateDomains($spec, null, $this->lastRowWithData);
        $spec = $this->updateSeries($spec, $this->lastRowWithData - 1, $this->lastRowWithData);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    // NOT USED yet, issues with legened on the graph
    private function ipapVsInflacionUltimoAño(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = 'Variación Interanual (' . $this->latestMonth->copy()->subYear()->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);
        // Update domains
        $spec = $this->updateDomains($spec, $this->lastRowWithData - 12, $this->lastRowWithData);
        $spec = $this->updateSeries($spec, $this->lastRowWithData - 12, $this->lastRowWithData);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function evolucionPrecioIpapHistorico(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = 'Histórico (' . $this->firstMonth->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);

        // Update domains
        $spec = $this->updateDomains($spec, null, $this->lastRowWithData);
        $spec = $this->updateSeries($spec, null, $this->lastRowWithData);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function evolucionIpapUltimoAño(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = 'Último año (' . $this->latestMonth->copy()->subYear()->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);

        // Update domains
        $spec = $this->updateDomains($spec, $this->lastRowWithData - 12, $this->lastRowWithData);
        $spec = $this->updateSeries($spec, $this->lastRowWithData - 12, $this->lastRowWithData);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }
    // private function evolucionIncidenciaPrecioPorRubro(EmbeddedChart $chart) : UpdateChartSpecRequest
    // {
    // }
    private function precioIpapMesActualVsMesAnterior(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = $this->latestMonth->copy()->subMonth()->locale('es_UY')->isoFormat('MMM YYYY') . ' vs ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY');
        $spec->setSubtitle($subtitle);

        // Update Series
        $spec = $this->updateSeries($spec, $this->lastRowWithData - 1, $this->lastRowWithData);
        $basicChart = $spec->getBasicChart();

        // Set previous month
        $basicSeries = $basicChart->getSeries();
        $series = $basicSeries[0]->getSeries();
        $sourceRange = $series->getSourceRange();
        $sources = $sourceRange->getSources();
        foreach ($sources as &$source) {
            $source->setStartRowIndex($this->lastRowWithData - 2);
            $source->setEndRowIndex($this->lastRowWithData - 1);
        }
        unset($source);
        $sourceRange->setSources($sources);
        $series->setSourceRange($sourceRange);
        $basicSeries[0]->setSeries($series);

        // Update spec
        $basicChart->setSeries($basicSeries);
        $spec->setBasicChart($basicChart);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function precioIpapAñoActualVsAñoAnterior(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = $this->latestMonth->copy()->subYear()->locale('es_UY')->isoFormat('MMM YYYY') . ' vs ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY');
        $spec->setSubtitle($subtitle);

        // Update Series
        $spec = $this->updateSeries($spec, $this->lastRowWithData - 1, $this->lastRowWithData);
        $basicChart = $spec->getBasicChart();

        // Set previous month
        $basicSeries = $basicChart->getSeries();
        $series = $basicSeries[0]->getSeries();
        $sourceRange = $series->getSourceRange();
        $sources = $sourceRange->getSources();
        foreach ($sources as &$source) {
            $source->setStartRowIndex($this->lastRowWithData - 13);
            $source->setEndRowIndex($this->lastRowWithData - 12);
        }
        unset($source);
        $sourceRange->setSources($sources);
        $series->setSourceRange($sourceRange);
        $basicSeries[0]->setSeries($series);

        // Update spec
        $basicChart->setSeries($basicSeries);
        $spec->setBasicChart($basicChart);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function incidenciaPorRubroIpapMesActual(EmbeddedChart $chart) : UpdateChartSpecRequest
    {
        $spec = $chart->getSpec();
        $subtitle = 'Mes Actual (' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $spec->setSubtitle($subtitle);

        $pieChart = $spec->getPieChart();

        $series = $pieChart->getSeries();
        $sourceRange = $series->getSourceRange();
        $sources = $sourceRange->getSources();
        foreach ($sources as &$source) {
            $source->setStartRowIndex($this->lastRowWithData - 1);
            $source->setEndRowIndex($this->lastRowWithData);
        }
        unset($source);
        $sourceRange->setSources($sources);
        $series->setSourceRange($sourceRange);

        // Update spec
        $pieChart->setSeries($series);
        $spec->setPieChart($pieChart);

        $updateChartSpec = new UpdateChartSpecRequest();
        $updateChartSpec->setChartId($chart->getChartId());
        $updateChartSpec->setSpec($spec);
        return $updateChartSpec;
    }

    private function updateDomains(ChartSpec $spec, ?int $startRow, ?int $endRow) : ChartSpec
    {
        $basicChart = $spec->getBasicChart();

        $domains = $basicChart->getDomains();
        foreach ($domains as &$basicChartDomain) {
            $sources = $basicChartDomain->getDomain()->getSourceRange()->getSources();
            foreach ($sources as &$source) {
                if ($startRow) {
                    $source->setStartRowIndex($startRow);
                }
                if ($endRow) {
                    $source->setEndRowIndex($endRow);
                }
            }
            unset($source);
            $domain = $basicChartDomain->getDomain();
            $sourceRange = $domain->getSourceRange();
            $sourceRange->setSources($sources);
            $domain->setSourceRange($sourceRange);
            $basicChartDomain->setDomain($domain);
        }
        unset($basicChartDomain);

        $basicChart->setDomains($domains);
        $spec->setBasicChart($basicChart);

        return $spec;
    }

    private function updateSeries(ChartSpec $spec, ?int $startRow, ?int $endRow) : ChartSpec
    {
        $basicChart = $spec->getBasicChart();

        // Update Series
        $basicSeries = $basicChart->getSeries();
        foreach ($basicSeries as &$basicSerie) {
            $series = $basicSerie->getSeries();
            $sourceRange = $series->getSourceRange();
            $sources = $sourceRange->getSources();
            foreach ($sources as &$source) {
                if ($startRow) {
                    $source->setStartRowIndex($startRow);
                }
                if ($endRow) {
                    $source->setEndRowIndex($endRow);
                }
            }
            unset($source);
            $sourceRange->setSources($sources);
            $series->setSourceRange($sourceRange);
            $basicSerie->setSeries($series);
        }
        unset($basicSerie);

        // Update spec
        $basicChart->setSeries($basicSeries);
        $spec->setBasicChart($basicChart);
        return $spec;
    }
}
