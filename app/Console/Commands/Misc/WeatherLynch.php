<?php

namespace App\Console\Commands\Misc;

use App\Services\FediApi;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

use function Safe\preg_match;

class WeatherLynch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lynch:weather {--import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(FediApi $api)
    {
        if ((bool) $this->option('import')) {
            $this->importData();
            return Command::SUCCESS;
        }

        $this->post($api);
        return Command::SUCCESS;
    }

    private function post(FediApi $api)
    {
        $data = collect(json_decode(file_get_contents(storage_path('app/LynchWeather/weather-lynch.json')), true));

        $videos = $data->filter(function ($video) : bool {
            $videoDate = Carbon::parse($video['date']);

            return $videoDate->month === now()->month && $videoDate->day === now()->day;
        });

        $text = "<p>On this day, here's how the weather was in L.A. on " . $videos->pluck('date')->map(fn (string $date) => Carbon::parse($date)->year)->join(', ', ' and ') . ' :<br><ul>';

        $videos->each(function (array $video) use (&$text) : void {
            $text .= '<li><a href="' . $video['url'] . '" target="_blank">' . Carbon::parse($video['date'])->toFormattedDateString() . '</a></li>';
        });
        $text .= '</ul><br>#DavidLynch #WeatherReport #OnThisDay</p>';

        $api->publishPost($text);
    }

    private function importData(): void
    {
        $fp = @fopen(storage_path('app/LynchWeather/David Lynch - Weather reports.m3u'), 'r');
        $titleRegex = '/^#EXTINF:\d+,(?<title>.+)$/';
        $tracks = collect();

        if ($fp) {
            while (($line = fgets($fp, 4096)) !== false) {
                if (preg_match($titleRegex, $line, $matches)) {
                    $data = [
                        'title' => $matches['title'],
                        'url' => trim(fgets($fp, 4096)),
                    ];
                    if (preg_match('/(?<month>\d{1,2})\/(?<day>\d{1,2})\/(?<year>\d{2})/', $data['title'], $match)) {
                        $data['date'] = Carbon::createFromDate('20' . $match['year'], $match['month'], $match['day'])->toDateString();
                    }
                    $tracks->push($data);
                }
            }

            if (!feof($fp)) {
                $this->line("Error: unexpected fgets() fail\n");
            }

            fclose($fp);
        }

        $this->info($tracks->toJson());
    }
}
