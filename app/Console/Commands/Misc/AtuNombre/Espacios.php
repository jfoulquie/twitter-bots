<?php

namespace App\Console\Commands\Misc\AtuNombre;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;

use function Safe\file_get_contents;
use function Safe\json_decode;
use function Safe\json_encode;

use Shapefile\ShapefileReader;

class Espacios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atunombre:add-espacios';

    protected $hidden = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private Proj4php $projector;
    private ShapefileReader $hoodsShape;
    private Proj $hoodsProjection;
    private Proj $googleProjection;
    private Collection $espaciosInfo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Open Shapefile
        $this->projector = app(Proj4php::class);
        $this->hoodsShape = app()->makeWith(ShapefileReader::class, [
            'files' => resource_path('shapes/atunombre/v_sig_espacios_publicos/v_sig_espacios_publicos.shp'),
        ])->setCharset('UTF-8');

        // Your initial projection information from your .prj file
        $this->hoodsProjection = new Proj($this->hoodsShape->getPRJ(), $this->projector);
        // WGS84 (for google map)
        $this->googleProjection = new Proj('EPSG:4326', $this->projector);

        $espacios = $this->readEspaciosCSV();

        $espaciosGeojson = [
            'type' => 'FeatureCollection',
            'crs' => [
                'type' => 'name',
                'properties' => [
                    'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84',
                ],
            ],
            'features' => [],
        ];

        $mujeresGeoJson = json_decode(file_get_contents(resource_path('shapes/atunombre/mujeres.geojson')), true);

        // Read all the records
        $i = 0;
        while ($hoodPolygonShape = $this->hoodsShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($hoodPolygonShape->isDeleted()) {
                continue;
            }

            $data = $hoodPolygonShape->getDataArray();

            if ($data['COD_NOM_ES'] === '***********') {
                continue;
            }

            $espacio = $this->espaciosInfo->firstWhere('cod_nom_es', $data['COD_NOM_ES']);
            if ($espacio['CATEGORIA'] !== 'Mujer') {
                continue;
            }
            $data = array_merge($data, Arr::only($espacio, ['CATEGORIA', 'SUBCATEGORIA', 'OTROS']));

            $geojson = $hoodPolygonShape->getGeoJSON();
            $geo = json_decode($geojson, true);

            // BBox
            unset($geo['bbox']);
            // Coordinates
            foreach ($geo['coordinates'] as &$lines) {
                foreach ($lines as &$point) {
                    $srcPoint = new Point($point[0], $point[1], $this->hoodsProjection);
                    // New point in WGS84 projection for Google Maps
                    $dstPoint = $this->projector->transform($this->googleProjection, $srcPoint);
                    $point = [$dstPoint->x, $dstPoint->y];
                }
                unset($point);
            }
            unset($lines);
            $feature = [
                'type' => 'Feature',
                'properties' => $data,
                'geometry' => $geo,
            ];

            $espaciosGeojson['features'][] = $feature;
            $mujeresGeoJson['features'][] = $feature;
        }

        file_put_contents(resource_path('shapes/atunombre/espacios-only.geojson'), json_encode($espaciosGeojson));
        file_put_contents(resource_path('shapes/atunombre/espacios-plus-calles.geojson'), json_encode($mujeresGeoJson));

        return 0;
    }

    private function readEspaciosCSV()
    {
        $this->espaciosInfo = collect();
        $csvFile = resource_path('shapes/atunombre/ESPACIOSPUBLICOS_CATEGORIAS_24_8_21.csv');
        $handle = fopen($csvFile, 'r');

        $i = 0;
        $keys = [];
        while (($line = fgetcsv($handle)) !== false) {
            if ($i === 0) {
                $keys = $line;
                $i++;
                continue;
            }
            $data = array_combine($keys, $line);
            $this->espaciosInfo->push($data);
        }
        fclose($handle);
    }
}
