<?php

namespace App\Console\Commands\Misc\AtuNombre;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;

use function Safe\json_decode;
use function Safe\json_encode;

use Shapefile\ShapefileReader;

class Espacios3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atunombre:espacios3';

    protected $hidden = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private Proj4php $projector;
    private ShapefileReader $hoodsShape;
    private Proj $hoodsProjection;
    private Proj $googleProjection;
    private Collection $espaciosInfo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Open Shapefile
        $this->projector = app(Proj4php::class);
        $this->hoodsShape = app()->makeWith(ShapefileReader::class, [
            // 'files' => resource_path('shapes/atunombre/v_sig_espacios_publicos/v_sig_espacios_publicos.shp'),
            // 'files' => resource_path('shapes/atunombre/v_sig_espacios_publicos/v_sig_espacios_publicos.shp'),
            'files' => resource_path('shapes/atunombre/bibliomun/bibliomun.shp'),
        ]);
        // ])->setCharset('UTF-8');

        // Your initial projection information from your .prj file
        $this->hoodsProjection = new Proj($this->hoodsShape->getPRJ(), $this->projector);
        // WGS84 (for google map)
        $this->googleProjection = new Proj('EPSG:4326', $this->projector);

        // $espacios = $this->readEspaciosCSV();

        $espaciosGeojson = [
            'type' => 'FeatureCollection',
            'crs' => [
                'type' => 'name',
                'properties' => [
                    'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84',
                ],
            ],
            'features' => [],
        ];

        // Read all the records
        $i = 0;
        $categories = [];
        while ($hoodPolygonShape = $this->hoodsShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($hoodPolygonShape->isDeleted()) {
                continue;
            }
            // if ($i > 5) {
            // break;
            // }

            $data = $hoodPolygonShape->getDataArray();
            $geojson = $hoodPolygonShape->getGeoJSON();
            // dd($data, $geojson);
            // if (in_array($data['DESC_TIPO_'], ['Espacio libre', 'Cantero', 'Separador vial'])) {
            // continue;
            // }

            // $espacio = $this->espaciosInfo->firstWhere('cod_nom_es', $data['COD_NOM_ES']);
            // if ($espacio['CATEGORIA'] !== 'Mujer') {
            //     continue;
            // }
            // $data = array_merge($data, Arr::only($espacio, ['CATEGORIA', 'SUBCATEGORIA', 'OTROS']));

            $geo = json_decode($geojson, true);
            // BBox
            // unset($geo['bbox']);
            // Coordinates
            // foreach ($geo['coordinates'] as &$lines) {
            $srcPoint = new Point($geo['coordinates'][0], $geo['coordinates'][1], $this->hoodsProjection);
            // New point in WGS84 projection for Google Maps
            $dstPoint = $this->projector->transform($this->googleProjection, $srcPoint);
            $geo['coordinates'] = [$dstPoint->x, $dstPoint->y];
            // foreach ($geo['coordinates'] as &$point) {
            // foreach ($lines as &$point) {
            // dd($point);
            // }
            // unset($point);
            // }
            $feature = [
                'type' => 'Feature',
                'properties' => $data,
                'geometry' => $geo,
            ];
            // parada
            $espaciosGeojson['features'][] = $feature;

            //círculo
            // $radius = 0.001;    // 100m
            $radius = 0.005;    // 500m

            // $espaciosGeojson['features'][] = $this->getcircle($radius, $geo['coordinates'], 3, '#8ff0a4', 0.5);
            // $espaciosGeojson['features'][] = $this->getcircle($radius * 2, $geo['coordinates'], 3, '#f9f06b', 0.4);
            // $espaciosGeojson['features'][] = $this->getcircle($radius * 3, $geo['coordinates'], 3, '#ffbe6f', 0.3);
            // $this->info($i);
            $i++;
            // $mujeresGeoJson['features'][] = $feature;
        }

        file_put_contents(resource_path('shapes/atunombre/bibliotecas.geojson'), json_encode($espaciosGeojson));
        // file_put_contents(resource_path('shapes/atunombre/paradas-test.geojson'), json_encode($espaciosGeojson));
        // file_put_contents(resource_path('shapes/atunombre/espacios-plus-calles.geojson'), json_encode($mujeresGeoJson));

        return 0;
    }

    private function getCircle(float $radius, array $center, int $precision = 1, string $color = '#555', float $opacity = 1) : array
    {
        $coordinates = [];
        for ($j = 1; $j <= 360; $j = $j + $precision) {
            $angle = $j * pi() / 180;
            $ptx = $center[0] + $radius * cos($angle);
            $pty = $center[1] + $radius * sin($angle);
            $coordinates[] = [$ptx, $pty];
        }

        $geometry = [
            'type' => 'Polygon',
            'coordinates' => [
                $coordinates,
            ],
        ];
        return [
            'type' => 'Feature',
            'properties' => [
                'Distancia' => '100m',
                'stroke' => '#555555',
                'stroke-width' => 1,
                'stroke-opacity' => 0.8,
                'fill' => $color,
                'fill-opacity' => $opacity,
            ],
            'geometry' => $geometry,
        ];
    }

    private function readEspaciosCSV()
    {
        $this->espaciosInfo = collect();
        $csvFile = resource_path('shapes/atunombre/ESPACIOSPUBLICOS_CATEGORIAS_24_8_21.csv');
        $handle = fopen($csvFile, 'r');

        $i = 0;
        $keys = [];
        while (($line = fgetcsv($handle)) !== false) {
            if ($i === 0) {
                $keys = $line;
                $i++;
                continue;
            }
            $data = array_combine($keys, $line);
            $this->espaciosInfo->push($data);
        }
        fclose($handle);
    }
}
