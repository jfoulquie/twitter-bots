<?php

namespace App\Console\Commands\Misc\AtuNombre;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;

use function Safe\file_get_contents;
use function Safe\json_decode;
use function Safe\json_encode;

use Shapefile\ShapefileReader;

class Espacios2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atunombre:espacios2';

    protected $hidden = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private Proj4php $projector;
    private ShapefileReader $hoodsShape;
    private Proj $hoodsProjection;
    private Proj $googleProjection;
    private Collection $espaciosInfo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Open Shapefile
        $this->projector = app(Proj4php::class);
        $this->hoodsShape = app()->makeWith(ShapefileReader::class, [
            // 'files' => resource_path('shapes/atunombre/v_sig_espacios_publicos/v_sig_espacios_publicos.shp'),
            // 'files' => resource_path('shapes/atunombre/v_sig_espacios_publicos/v_sig_espacios_publicos.shp'),
            'files' => resource_path('shapes/atunombre/v_uptu_paradas/v_uptu_paradas.shp'),
            ])->setCharset('UTF-8');

        // Your initial projection information from your .prj file
        $this->hoodsProjection = new Proj($this->hoodsShape->getPRJ(), $this->projector);
        // WGS84 (for google map)
        $this->googleProjection = new Proj('EPSG:4326', $this->projector);

        // $espacios = $this->readEspaciosCSV();

        $espaciosGeojson = [
            'type' => 'FeatureCollection',
            'crs' => [
                'type' => 'name',
                'properties' => [
                    'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84',
                ],
            ],
            'features' => [],
        ];

        // $mujeresGeoJson = json_decode(file_get_contents(resource_path('shapes/atunombre/mujeres.geojson')), true);

        // Read all the records
        $i = 0;
        $categories = [];
        while ($hoodPolygonShape = $this->hoodsShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($hoodPolygonShape->isDeleted()) {
                continue;
            }
            // if ($i > 5) {
            // break;
            // }

            $data = $hoodPolygonShape->getDataArray();
            // if (in_array($data['DESC_TIPO_'], ['Espacio libre', 'Cantero', 'Separador vial'])) {
            // continue;
            // }

            // $espacio = $this->espaciosInfo->firstWhere('cod_nom_es', $data['COD_NOM_ES']);
            // if ($espacio['CATEGORIA'] !== 'Mujer') {
            //     continue;
            // }
            // $data = array_merge($data, Arr::only($espacio, ['CATEGORIA', 'SUBCATEGORIA', 'OTROS']));

            $geojson = $hoodPolygonShape->getGeoJSON();
            $geo = json_decode($geojson, true);
            // BBox
            // unset($geo['bbox']);
            // Coordinates
            // foreach ($geo['coordinates'] as &$lines) {
            $srcPoint = new Point($geo['coordinates'][0], $geo['coordinates'][1], $this->hoodsProjection);
            // New point in WGS84 projection for Google Maps
            $dstPoint = $this->projector->transform($this->googleProjection, $srcPoint);
            $geo['coordinates'] = [$dstPoint->x, $dstPoint->y];
            // foreach ($geo['coordinates'] as &$point) {
            // foreach ($lines as &$point) {
            // dd($point);
            // }
            // unset($point);
            // }
            $feature = [
                'type' => 'Feature',
                'properties' => $data,
                'geometry' => $geo,
            ];
            // parada
            $espaciosGeojson['features'][] = $feature;

            //círculo
            $radius = 0.001;
            $coordinates = [];
            for ($j = 1; $j <= 360; $j++) {
                $angle = $j * pi() / 180;
                $ptx = $geo['coordinates'][0] + $radius * cos($angle);
                $pty = $geo['coordinates'][1] + $radius * sin($angle);
                $coordinates[] = [$ptx, $pty];
            }

            $geometry = [
                'type' => 'Polygon',
                'coordinates' => [
                    $coordinates,
                ],
            ];
            $circle = [
                'type' => 'Feature',
                'properties' => [
                    'Distancia' => '100m',
                    'stroke' => '#555555',
                    'stroke-width' => 1,
                    'stroke-opacity' => 0.8,
                    'fill' => '#8ff0a4',
                    'fill-opacity' => 0.6,
                ],
                'geometry' => $geometry,
            ];
            $espaciosGeojson['features'][] = $circle;
            // $this->info($i);
            $i++;
            // $mujeresGeoJson['features'][] = $feature;
        }

        file_put_contents(resource_path('shapes/atunombre/paradas-test.geojson'), json_encode($espaciosGeojson));
        // file_put_contents(resource_path('shapes/atunombre/espacios-plus-calles.geojson'), json_encode($mujeresGeoJson));

        return 0;
    }

    private function readEspaciosCSV()
    {
        $this->espaciosInfo = collect();
        $csvFile = resource_path('shapes/atunombre/ESPACIOSPUBLICOS_CATEGORIAS_24_8_21.csv');
        $handle = fopen($csvFile, 'r');

        $i = 0;
        $keys = [];
        while (($line = fgetcsv($handle)) !== false) {
            if ($i === 0) {
                $keys = $line;
                $i++;
                continue;
            }
            $data = array_combine($keys, $line);
            $this->espaciosInfo->push($data);
        }
        fclose($handle);
    }
}
