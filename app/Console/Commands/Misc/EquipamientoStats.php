<?php

namespace App\Console\Commands\Misc;

use App\Services\PolygonHelper;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;
use Shapefile\Shapefile;
use Shapefile\ShapefileReader;

class EquipamientoStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'im:stats';

    protected $hidden = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Proj4php $projector;
    private ShapefileReader $hoodsShape;
    private ShapefileReader $espaciosShape;
    private Proj $espacioProjection;
    private Proj $hoodsProjection;
    private Proj $googleProjection;
    private Collection $hoodsPolygons;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->projector = app(Proj4php::class);
        // Open Shapefiles
        $this->hoodsShape = app()->makeWith(ShapefileReader::class, [
        'files' => resource_path('shapes/ine_barrios_mvd/ine_barrios_mvd.shp'),
        ])->setCharset('UTF-8');

        $this->espaciosShape = app()->makeWith(ShapefileReader::class, [
            // 'files' => resource_path('shapes/atunombre/v_ep_bancos/v_ep_bancos.shp'),
            'files' => resource_path('shapes/atunombre/v_ep_banios/v_ep_banios.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_bebederos/v_ep_bebederos.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_relojes_termometros/v_ep_relojes_termometros.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_soportes_bicicletas/v_ep_soportes_bicicletas.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_papeleras/v_ep_papeleras.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_papeleras/v_ep_papeleras.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_papeleras/v_ep_papeleras.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_papeleras/v_ep_papeleras.shp'),
            // 'files' => resource_path('shapes/atunombre/v_ep_papeleras/v_ep_papeleras.shp'),
            'options' => [Shapefile::OPTION_DBF_ALLOW_FIELD_SIZE_255 => true],
        ])->setCharset('UTF-8');

        // Your initial projection information from your .prj file
        $this->hoodsProjection = new Proj($this->hoodsShape->getPRJ(), $this->projector);
        $this->espacioProjection = new Proj($this->espaciosShape->getPRJ(), $this->projector);
        // WGS84 (for google map)
        $this->googleProjection = new Proj('EPSG:4326', $this->projector);

        $this->getHoodsPolygons();
        $hoods = [];

        while ($espacioShape = $this->espaciosShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($espacioShape->isDeleted()) {
                continue;
            }

            $data = $espacioShape->getDataArray();
            $geo = $espacioShape->getArray();

            $srcPoint = new Point($geo['x'], $geo['y'], $this->espacioProjection);
            $dstPoint = $this->projector->transform($this->googleProjection, $srcPoint);
            $hood = $this->getHood($dstPoint);
            if (isset($hoods[$hood])) {
                $hoods[$hood] += intval(Arr::get($data, 'CANTIDAD', 1));
            } else {
                $hoods[$hood] = intval(Arr::get($data, 'CANTIDAD', 1));
            }
        }
        arsort($hoods);
        dd($hoods);

        return 0;
    }

    private function getHood(Point $point)
    {
        $filter = $this->hoodsPolygons->filter(function ($polygons, $name) use ($point) {
            return $polygons->reduce(function ($carry, $polygon) use ($point) {
                $onPolygon = app(PolygonHelper::class)->pointInPolygon($point, $polygon);
                return $carry || $onPolygon;
            });
        });
        return $filter->keys()->first();
    }

    private function getHoodsPolygons()
    {
        $this->hoodsPolygons = collect();
        while ($hoodPolygonShape = $this->hoodsShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($hoodPolygonShape->isDeleted()) {
                continue;
            }

            $data = $hoodPolygonShape->getDataArray();
            $geometry = $hoodPolygonShape->getArray();
            $this->hoodsPolygons->put($data['NOMBBARR'], collect());
            if (isset($geometry['parts'])) {
                foreach ($geometry['parts'] as $part) {
                    foreach ($part['rings'] as $ring) {
                        $points = [];
                        foreach ($ring['points'] as $point) {
                            $srcPoint = new Point($point['x'], $point['y'], $this->hoodsProjection);
                            $points[] = $this->projector->transform($this->googleProjection, $srcPoint);
                        }
                        $this->hoodsPolygons->get($data['NOMBBARR'])->push($points);
                    }
                }
            } elseif (isset($geometry['rings'])) {
                foreach ($geometry['rings'] as $ring) {
                    $points = [];
                    foreach ($ring['points'] as $point) {
                        $srcPoint = new Point($point['x'], $point['y'], $this->hoodsProjection);
                        $points[] = $this->projector->transform($this->googleProjection, $srcPoint);
                    }
                    $this->hoodsPolygons->get($data['NOMBBARR'])->push($points);
                }
            } else {
                throw new Exception('Unknown structure on array for hoodPolygonShape: ' . json_encode($geometry));
            }
        }
    }

    private function isOnPolygon(Point $point, array $polygon) : bool
    {
        $polygonHood = collect($polygon)
            ->map(function (array $points) {
                // Your original point
                $srcPoint = new Point($points['x'], $points['y'], $this->hoodsProjection);
                // New point in WGS84 projection for Google Maps
                return $this->projector->transform($this->googleProjection, $srcPoint);
            })->filter();

        if (app(PolygonHelper::class)->pointInPolygon($point, $polygonHood->toArray())) {
            return true;
        }
        return false;
    }
}
