<?php

namespace App\Dictionaries;

use Illuminate\Support\Fluent;
use RuntimeException;

class Dictionary extends Fluent
{
    //

    public function hasChar(string $char) : bool
    {
        if (mb_strlen($char) !== 1) {
            throw new RuntimeException($char . ' is not a char but a string or empty');
        }

        return isset($this->$char);
    }
}
