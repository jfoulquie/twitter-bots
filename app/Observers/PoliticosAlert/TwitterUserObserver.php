<?php

namespace App\Observers\PoliticosAlert;

use App\Jobs\PoliticosAlert\RecordEvent;
use App\Models\PoliticosAlert\Event;
use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Support\Facades\Log;

class TwitterUserObserver
{
    /**
     * Handle the TwitterUser "created" event.
     *
     * @param  \App\Models\PoliticosAlert\TwitterUser  $twitterUser
     * @return void
     */
    public function created(TwitterUser $twitterUser)
    {
        //
    }

    /**
     * Handle the TwitterUser "updated" event.
     *
     * @param  \App\Models\PoliticosAlert\TwitterUser  $twitterUser
     * @return void
     */
    public function updated(TwitterUser $twitterUser)
    {
        if ($twitterUser->wasRecentlyCreated) {
            return;
        }
        foreach ($twitterUser->getDirty() as $key => $changed) {
            /** @var string $key */
            switch ($key) {
                case 'id':
                case 'twitter_id':
                case 'followers_count':
                case 'friends_count':
                case 'listed_count':
                case 'created_at':
                case 'favourites_count':
                case 'statuses_count':
                    // ignore
                    break;
                case 'username':
                case 'name':
                case 'description':
                case 'location':
                case 'url':
                case 'avatar':
                case 'banner':
                case 'protected':
                case 'verified':
                case 'withheld_in_countries':
                    Log::debug("$key has changed", ['user' => $twitterUser, 'original' => $twitterUser->getOriginal($key)]);
                    RecordEvent::dispatch($twitterUser, Event::PROFILE, $key, $twitterUser->getOriginal($key));
                    break;
                default:
                    Log::info('Untracked action on Twitter model.', ['key' => $key, 'original' => $twitterUser->getOriginal($key), 'changed' => $changed]);
                    break;
            }
        }
    }

    /**
     * Handle the TwitterUser "deleted" event.
     *
     * @param  \App\Models\PoliticosAlert\TwitterUser  $twitterUser
     * @return void
     */
    public function deleted(TwitterUser $twitterUser)
    {
        //
    }

    /**
     * Handle the TwitterUser "restored" event.
     *
     * @param  \App\Models\PoliticosAlert\TwitterUser  $twitterUser
     * @return void
     */
    public function restored(TwitterUser $twitterUser)
    {
        //
    }

    /**
     * Handle the TwitterUser "force deleted" event.
     *
     * @param  \App\Models\PoliticosAlert\TwitterUser  $twitterUser
     * @return void
     */
    public function forceDeleted(TwitterUser $twitterUser)
    {
        //
    }
}
