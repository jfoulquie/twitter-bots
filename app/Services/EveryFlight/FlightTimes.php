<?php

namespace App\Services\EveryFlight;

use App\Models\EveryFlight\Flight;
use App\Models\EveryFlight\FlightStatus;
use Carbon\CarbonInterval;
use Illuminate\Support\Carbon;

class FlightTimes
{
    private array $times;

    // See https://www.distance24.org/route.json?stops=Montevideo|Buenos%20Aires

    public function __construct()
    {
        $this->times = [
            'Aeroparque' => CarbonInterval::hours(3),
            'Buenos Aires (Aeroparque)' => CarbonInterval::hours(3),
            'Asunción' => CarbonInterval::hours(3),
            'Bogotá' => CarbonInterval::hours(3),
            'Ciudad de Panamá' => CarbonInterval::hours(3),
            'Lima' => CarbonInterval::hours(3),
            'Madrid' => CarbonInterval::hours(3),
            'Miami' => CarbonInterval::hours(3),
            'Porto Alegre' => CarbonInterval::hours(1.5),
            'San Pablo' => CarbonInterval::hours(3),
            'Santiago de Chile' => CarbonInterval::hours(3),
        ];
    }

    public function time(string $city): CarbonInterval
    {
        return $this->times[$city] ?? CarbonInterval::fromString('');
    }

    public function isLive(Flight $flight) : bool
    {
        // Cancelled flights are never live
        if (in_array($flight->status, [FlightStatus::STATUS_CANCELLED])) {
            return false;
        }

        $flightTime = $this->time($flight->city);

        if ($flight->isDeparture()) {
            // It can only be live after it's departed
            if ($flight->status !== FlightStatus::STATUS_DEPARTED) {
                return false;
            }
            // For the tweets, this doesn't really matter, there's only one tweet after
            // departing (not for arrival on the destination airport) but I'm adding it
            // for accuracy's sake
            return $flight->carbon_most_accurate_time->copy()->add($flightTime)->isFuture();
        }

        // Once it's landed, it can't be live
        if ($flight->status === FlightStatus::STATUS_LANDED) {
            return false;
        }
        // If we're between the departure time (approx) and the arrival time, we're live
        return Carbon::parse(now()->timezone('America/Montevideo')->toDateTimeString()) // This mess is because the times are stored on the DB as UTC but in reality they are in UTC-3, so this is the way to convert
            ->isBetween(
                $flight->carbon_most_accurate_time->copy()->sub($flightTime),
                $flight->carbon_most_accurate_time
            );
    }

    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::                                                                         :*/
    /*::  This routine calculates the distance between two points (given the     :*/
    /*::  latitude/longitude of those points). It is being used to calculate     :*/
    /*::  the distance between two locations using GeoDataSource(TM) Products    :*/
    /*::                                                                         :*/
    /*::  Definitions:                                                           :*/
    /*::    South latitudes are negative, east longitudes are positive           :*/
    /*::                                                                         :*/
    /*::  Passed to function:                                                    :*/
    /*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
    /*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
    /*::    unit = the unit you desire for results                               :*/
    /*::           where: 'M' is statute miles (default)                         :*/
    /*::                  'K' is kilometers                                      :*/
    /*::                  'N' is nautical miles                                  :*/
    /*::  Worldwide cities and other features databases with latitude longitude  :*/
    /*::  are available at http://www.geodatasource.com                          :*/
    /*::                                                                         :*/
    /*::  For enquiries, please contact sales@geodatasource.com                  :*/
    /*::                                                                         :*/
    /*::  Official Web site: http://www.geodatasource.com                        :*/
    /*::                                                                         :*/
    /*::         GeoDataSource.com (C) All Rights Reserved 2015                  :*/
    /*::                                                                         :*/
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public function distance(float $lat1, float $lon1, float $lat2, float $lon2, string $unit = 'K') : float
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == 'K') {
            return ($miles * 1.609344);
        } elseif ($unit == 'N') {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
