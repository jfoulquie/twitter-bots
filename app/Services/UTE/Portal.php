<?php

namespace App\Services\UTE;

use App\Services\UserAgent;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class Portal
{
    private array $cookies;

    public function __construct(private UserAgent $userAgent)
    {
        //
    }

    private function init() : void
    {
        $pageUrl = 'https://apps.ute.com.uy/sge/portal/nucleo/paginas/portal_utei.aspx?c=GerEyM_ComposicionEnergetica';
        $response = Http::withHeaders([
            'User-Agent' => $this->userAgent->get(),
        ])->head($pageUrl);
        $this->cookies = collect($response->cookies()->toArray())
            ->mapWithKeys(fn ($cookie) => [$cookie['Name'] => $cookie['Value']])
            ->toArray();
    }

    public function getInstantPower() : Collection
    {
        if (!isset($this->cookies)) {
            $this->init();
        }

        $jsonUrl = 'https://apps.ute.com.uy/sge/portal/EyMWeb/ServiciosLocales/LocalConsultas_EyM.svc/CUPotenciasDeGeneracion';
        $response = Http::acceptJson()
            ->withCookies($this->cookies, 'apps.ute.com.uy')
            ->post($jsonUrl, $this->getInstantPowerRequestData());

        /** @phpstan-ignore-next-line */
        $rawData = collect(Arr::get($response->json(), 'Items'))
            ->map(fn (array $item) => $item['Row'])
            // Get level 0 items only
            ->filter(fn (array $item) => $item[$this->getColumnPosition('Level')] === '0')
            // Add keys
            ->mapWithKeys(
                fn (array $item) => [$item[$this->getColumnPosition('Instalacion')] => $item[$this->getColumnPosition('PotenciaActiva')]]
            );
        // ->mapWithKeys(fn (array $item) => [$item[$this->getColumnPosition('Instalacion')] => [
        //     'Potencia Activa (MW)' => $item[$this->getColumnPosition('PotenciaActiva')],
        //     // 'Potencia Reactiva (MVAr)' => $item[$this->getColumnPosition('PotenciaReactiva')]
        // ]]);

        // Replace strings with floats
        /** @var \Illuminate\Support\Collection<string, float> */
        $powerData = new Collection();
        /** @var string $key */
        /** @var string $value */
        foreach ($rawData as $key => $value) {
            // Change from "spanish" notation to float ('.' are removed and THEN ',' become '.')
            $power = floatval(str_replace(['.', ','], ['', '.'], $value));
            if ($power > 0) {
                $powerData[str_replace(['Generación ', 'Fotovoltaica'], ['', 'Solar'], $key)] = $power;
            }
        }

        return $powerData;
    }

    public function getInstantDemand() : Collection
    {
        if (!isset($this->cookies)) {
            $this->init();
        }

        $jsonUrl = 'https://apps.ute.com.uy/sge/portal/EyMWeb/ServiciosLocales/LocalEnergia_Y_Mercado.svc/CUObtenerPuntosEstadisticos';
        $response = Http::acceptJson()
            ->withCookies($this->cookies, 'apps.ute.com.uy')
            ->post($jsonUrl, $this->getInstantDemandRequestData());

        return $response->throw()->collect();
    }

    private function getInstantPowerRequestData() : array
    {
        return [
            'filtro' => [
                'Consulta' => '',
                'Comando' => 'ComandoPotenciaDeGeneracionPorTipoDeGeneracion',
            ],
            'gridData' => [
                'Id' => 'IdGrilla',
                'PageSize' => 10000,
                'CurrentPage' => 1,
                // 'SortColumn' => 'Orden',
                // 'SortOrder' => 'asc',
                'TreeGridInfo' => [
                    'NodeId' => null,
                    'Parent' => null,
                    'Level' => null,
                    'RowId' => null,
                    'ParentRowId' => null,
                ],
                'LoadOnce' => true,
                // 'Title' => '',
                'ExportInfo' => [],
                'Columns' => $this->getInstantPowerColumns()->toArray(),
                // 'GroupHeaders' => [],
            ],
        ];
    }

    /**
     *
     * @return \Illuminate\Support\Collection<int, array{Name: string, Caption?: string, Hidden: bool}>
     */
    private function getInstantPowerColumns() : Collection
    {
        /** @phpstan-ignore-next-line */
        return collect([
           [
               'Name' => 'Instalacion',
               'Caption' => 'Instalación',
               'Hidden' => false,
           ],
           [
               'Name' => 'PotenciaActiva',
               // 'Caption' => 'Potencia Activa (MW)',
               'Hidden' => false,
           ],
           [
               'Name' => 'PotenciaReactiva',
               // 'Caption' => 'P Reactiva (MVAr)',
               'Hidden' => false,
           ],
           [
               'Name' => 'Level',
               'Hidden' => true,
           ],
        ]);
    }

    private function getColumnPosition(string $columnName) : int|bool
    {
        /** @phpstan-ignore-next-line */
        return $this->getInstantPowerColumns()
            ->search(fn (array $column) => $column['Name'] === $columnName);
    }

    private function getInstantDemandRequestData() : array
    {
        return [
            'filtro' => [
                'Consulta' => '',
                'Comando' => 'ComandoPotenciaDeGeneracionPorTipoDeGeneracion',
            ],
        ];
    }
}
