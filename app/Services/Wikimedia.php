<?php

namespace App\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

/**
 * @see https://wikimedia.org/api/rest_v1/#/Pageviews%20data
 * @see https://wikitech.wikimedia.org/wiki/Analytics/AQS/Pageviews
 */
class Wikimedia
{
    public readonly string $baseUrl;
    public readonly string $userAgent;

    private array $parameters = [];

    public function __construct(string $languageDomain = 'en')
    {

        $this->baseUrl = 'https://wikimedia.org/api/rest_v1/';
        $this->userAgent = 'fedibots by @j3j5@hachyderm.io | https://gitlab.com/j3j5 | https://j3j5.uy';

        $this->parameters = [

        ];
    }

    public function parameter(string $key, mixed $value) : self
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function parameters(array $parameters = null) : array|self
    {
        if ($parameters === null) {
            return $this->parameters;
        }
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }

    /**
     *
     * @param string $path
     * @throws \Illuminate\Http\Client\RequestException
     * @return \Illuminate\Http\Client\Response
     */
    public function get(string $path) : Response
    {
        if (Str::startsWith($path, '/')) {
            $path = mb_substr($path, 1);
        }
        return Http::withHeaders([
                'User-Agent' => $this->userAgent,
                'Content-Encoding' => 'UTF-8',
                'Accept-Encoding' => 'gzip, deflate, br',
            ])->acceptJson()
            ->get($this->baseUrl . $path, $this->parameters)
            ->throw();
    }

    /**
     * Lists the 1000 most viewed articles for a given project and timespan
     * (month or day). You can filter by access method.
     *
     * @param string $project The domain of any Wikimedia project, for example
     *              'en.wikipedia.org', 'www.mediawiki.org' or 'commons.wikimedia.org'.
     * @param string $access Options: all-access, desktop, mobile-app, mobile-web
     * @param string $year YYYY format
     * @param string $month MM format
     * @param string $day DD format
     */
    public function topViews(string $project, string $access, string $year, string $month, string $day) : Response
    {
        $path = "/metrics/pageviews/top/$project/$access/$year/$month/$day";
        return $this->get($path);
    }

    /**
     * Lists the pageviews to this project, split by country of origin for a given
     * month. Because of privacy reasons, pageviews are given in a bucketed format,
     * and countries with less than 100 views do not get reported.
     *
     * @param string $project The domain of any Wikimedia project, for example
     *              'en.wikipedia.org', 'www.mediawiki.org' or 'commons.wikimedia.org'.
     * @param string $access Options: all-access, desktop, mobile-app, mobile-web
     * @param string $year YYYY format
     * @param string $month MM format
     */
    public function topByCountry(string $project, string $access, string $year, string $month) : Response
    {
        $path = "/metrics/pageviews/top-by-country/$project/$access/$year/$month";
        return $this->get($path);
    }

}
