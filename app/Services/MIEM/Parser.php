<?php

namespace App\Services\MIEM;

use App\Models\IndProp\Brand;
use App\Services\UserAgent;
use Carbon\Carbon;
use DiDom\Document;
use DiDom\Element;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use RuntimeException;
use function Safe\array_combine;

use function Safe\preg_match;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * @phpstan-type BrandArray array{requestNumber: int, brand: string, niceClasses: string|array, requestType: string, receptionDate: string|\Carbon\Carbon, status: string, owner: string, ownerNationality?: string, logo?: string, requestSubType?: string, signType?: string, ownerAddress?: string, requestUrl?: string}
 */
class Parser
{
    public const ORIGIN = 'https://dnpispweb-test.miem.gub.uy';
    public const REFERER_BRAND = 'https://dnpispweb-test.miem.gub.uy/pamp/mark/index';
    public const BRAND_URL = 'https://dnpispweb-test.miem.gub.uy/pamp/mark/searchFilteredResults';
    public const BRAND_SHOW = 'https://dnpispweb-test.miem.gub.uy/pamp/mark/show';
    public const BRAND_DOWNLOAD = 'https://dnpispweb-test.miem.gub.uy/pamp/mark/downloadEDoc';
    public const REFERER_PATENT = 'https://dnpispweb-test.miem.gub.uy/pamp/patent/index';
    public const PATENT_URL = 'https://dnpispweb-test.miem.gub.uy/pamp/patent/searchFilteredResults';
    public const PATENT_SHOW = 'https://dnpispweb-test.miem.gub.uy/pamp/patent/show';

    /** @var array<string,string> */
    private readonly array $requestTypes;
    /** @var array<string,string> */
    private readonly array $signTypes;

    /** @var array<string, string> */
    private array $data;

    private ProgressBar $bar;

    public function __construct(private UserAgent $ua)
    {
        $this->signTypes = [
            'D' => 'Denominativa',
            'F' => 'Figurativa',
            'M' => 'Mixta',
            'T' => 'Tridimensional',
            'S' => 'Sonora',
            'O' => 'Olfativa',
        ];

        $this->requestTypes = [
            'DEN' => 'Denominación de Origen',
            'FRA' => 'Frase Publicitaria',
            'MAR' => 'Marca',
            'COL' => 'Marca Colectiva',
            'CER' => 'Marca de Certificación o Garantía',
        ];

        $this->data = [
            // Filter by name
            'markName' => '',
            // Filter by request number or range of them
            'requestNumber1' => '',
            'requestNumber2' => '',
            // Filter by registration number or range of them
            'registrationNumber1' => '',
            'registrationNumber2' => '',
            // Request date
            'dateFrom' => '',
            'dateTo' => '',
            // Filter by owner or requester data
            'ownerName' => '',
            'ownerNationality' => '',
            'representativeName' => '',
            // Type of brand and resource
            'signType' => '',
            'requestType' => '',
            'requestSubType' => '',
        ];
    }

    /**
     * Possible values:
     *  DEN -> Denominación de Origen
     *  FRA -> Frase Publicitaria
     *  MAR -> Marca
     *  COL -> Marca Colectiva
     *  CER -> Marca de Certificación o Garantía
     */
    public function requestType(string $type) : self
    {
        $this->data['requestType'] = $type;

        return $this;
    }

    public function from(Carbon $from) : self
    {
        $this->data['dateFrom'] = $from->format('d/m/Y');

        return $this;
    }

    public function to(Carbon $to) : self
    {
        $this->data['dateTo'] = $to->format('d/m/Y');

        return $this;
    }

    /**
     * Possible values:
     *  "D" -> Denominativa
     *  "F" -> Figurativa
     *  "M" -> Mixta
     *  "T" -> Tridimensional
     *  "S" -> Sonora
     *  "O" -> Olfativa
     *
     */
    public function signType(string $type) : self
    {
        $this->data['signType'] = $type;

        return $this;
    }

    public function name(string $name) : self
    {
        $this->data['markName'] = $name;

        return $this;
    }

    public function requestNumber(int $number) : self
    {
        $this->data['requestNumber1'] = (string) $number;

        return $this;
    }

    public function requestNumberRange(int $number1, int $number2) : self
    {
        $this->data['requestNumber1'] = (string) $number1;
        $this->data['requestNumber2'] = (string) $number2;

        return $this;
    }

    public function setData(string $key, string $value) : self
    {
        $this->data[$key] = $value;

        return $this;
    }

    public function setProgressBar(ProgressBar $bar) : self
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     *
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Safe\Exceptions\ArrayException
     * @return \Illuminate\Support\Collection<int, BrandArray>
     *
     */
    public function getBrandsRequests() : Collection
    {
        $validator = Validator::make($this->data, $this->rulesForBrands());

        $validator->validate();

        $response = Http::withHeaders([
            'User-Agent' => $this->ua->get(),
            'Referer' => self::REFERER_BRAND,
            'X-Requested-With' => 'XMLHttpRequest',
            'Origin' => self::ORIGIN,
        ])->withBody(
            http_build_query($this->data),
            'application/x-www-form-urlencoded; charset=UTF-8'
        )->post(self::BRAND_URL);

        $body = $response->throw()->body();
        if (empty($body)) {
            throw new RuntimeException('The website at MIEM returned a ' . $response->status() . ' but the body is empty');
        }
        $brands = $this->getBrandsFromHTML($body);

        if (isset($this->bar) && $brands->count() > 0) {
            $this->bar->setMaxSteps($brands->count());
        }

        $brands->transform(function (array $brandData) {
            $brandData = $this->getExtraInfoFromBrandPage($brandData);
            Brand::updateOrCreate(
                ['requestNumber' => $brandData['requestNumber']],
                $brandData
            );
            if (isset($this->bar)) {
                $this->bar->advance();
            }

            return $brandData;
        });

        return $brands;
    }

    /**
     *
     * @param string $body
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Safe\Exceptions\ArrayException
     * @return \Illuminate\Support\Collection<int, BrandArray>
     */
    private function getBrandsFromHTML(string $body) : Collection
    {
        $document = new Document($body);
        $rows = $document->find('tbody tr');
        if (!is_array($rows)) {
            throw new RuntimeException('No rows were found when looking for marks');
        }
        $headers = [
            'requestNumber',
            'brand',
            'niceClasses',
            'requestType',
            'receptionDate',
            'status',
            'owner',
        ];
        $brands = new Collection();
        foreach ($rows as $row) {
            /** @var array<int, \DiDom\Element> $cells */
            $cells = $row->find('th,td');   /** @phpstan-ignore-line */
            if (!is_array($cells)) {
                /** @phpstan-ignore-next-line */
                throw new RuntimeException('No cells were found when looking for marks on this row: ' . $row->html());
            }
            $brandData = [];
            foreach ($cells as $cell) {
                $brandData[] = $cell->text();
            }
            if (count($brandData) !== count($headers)) {
                /** @phpstan-ignore-next-line */
                throw new RuntimeException('The row seems to have the wrong number of cells: ' . $row->html());
            }

            $brandData = array_combine($headers, $brandData);
            // Ignore this as they are not brands
            if (in_array($brandData['brand'], ['RECIBIDO POR ERROR'])) {
                continue;
            }
            $brands->push($brandData);
        }

        return $brands;
    }

    /**
     *
     * @param BrandArray $brandData
     * @return BrandArray
     */
    private function getExtraInfoFromBrandPage(array $brandData) : array
    {
        // Normalize keys
        $niceClasses = explode(' ', $brandData['niceClasses']);  /** @phpstan-ignore-line */
        $niceClasses = array_filter($niceClasses, 'is_numeric');

        $brandData['niceClasses'] = array_combine($niceClasses, array_fill(0, count($niceClasses), ''));
        $matches = [];
        if (preg_match('/(?<name>.*)\s\[(?<natl>.*)\]/', $brandData['owner'], $matches)) {
            $brandData['owner'] = (string) $matches['name'];
            $brandData['ownerNationality'] = (string) $matches['natl'];
        }
        $date = Carbon::createFromFormat('d/m/Y', $brandData['receptionDate']);
        if (!$date instanceof Carbon) {
            throw new RuntimeException('Invalid date format ' . $brandData['receptionDate'] . '. Expected d/m/Y');
        }
        $brandData['receptionDate'] = $date
            ->timezone('UTC')
            ->startOfDay();
        // Now try to get extra info from the brand's details page
        $query = [
            'seq' => 'MA',
            'type' => 'M',
            'series' => '1',
            'number' => $brandData['requestNumber'],
        ];

        $body = null;

        /** @phpstan-ignore-next-line */
        retry([5000, 10000, 15000], function () use (&$body, $query) {
            $brandReqPageResp = Http::withHeaders([
                'User-Agent' => $this->ua->get(),
            ])->get(self::BRAND_SHOW, $query);
            $body = $brandReqPageResp->throw()->body();
            if (empty($body)) {
                Log::notice('failed to download doc', [self::BRAND_SHOW . '?' . http_build_query($query)]);
                throw new RuntimeException('Empty body');
            }
        });

        if (empty($body)) {
            Log::notice('empty body', [self::BRAND_SHOW . '?' . http_build_query($query)]);
            return $brandData;
        }

        $document = new Document($body);
        $title = $document->first('title');
        if (!$title instanceof Element || $title->text() === 'Error') {
            Log::notice('error page', [self::BRAND_SHOW . '?' . http_build_query($query)]);
            return $brandData;
        }

        $logo = $document->first('.mainContainer > img.img-thumbnail');
        if ($logo instanceof Element) {
            $brandData['logo'] = $logo->attr('src');
        }

        $dls = collect($document->find('dl.dl-horizontal'));
        $subType = $dls->firstWhere(
            fn ($dl) => $dl->first('dt')->text() === 'Subtipo de solicitud'
        );

        if ($subType instanceof Element) {
            $dd = $subType->first('dd');
            if ($dd instanceof Element) {
                $brandData['requestSubType'] = $dd->text();
            }
        }

        $signType = $dls->firstWhere(
            fn ($dl) => $dl->first('dt')->text() === 'Tipo de signo'
        );

        if ($signType instanceof Element) {
            $dd = $signType->first('dd');
            if ($dd instanceof Element) {
                $brandData['signType'] = $dd->text();
            }
        }

        $panels = collect($document->find('div.panel-info'));
        // Parse custome niceClasses
        $niceClassesPanel = $panels->get(0);
        if ($niceClassesPanel instanceof Element) {
            $rows = $niceClassesPanel->find('tr');
            foreach ($rows as $row) {
                if (!$row instanceof Element) {
                    continue;
                }
                $th = $row->first('th');
                if ($th instanceof Element) {
                    $class = (int) $th->text();
                    $td = $row->first('td');
                    if ($td instanceof Element) {
                        $desc = str_replace(["\r\n", '  '], [' ', ' '], $td->text());
                        $brandData['niceClasses'][$class] = $desc;
                    }
                }
            }
        }

        $ownersPanel = $panels->get(1);
        if ($ownersPanel instanceof Element) {
            /** @phpstan-ignore-next-line */
            $brandData['ownerAddress'] = $ownersPanel->first('tbody td')->text();
        }

        /** @phpstan-ignore-next-line */
        $link = (string) $document->first('a.btn-primary')->attr('href');
        if (preg_match('/javascript:downloadEDoc\((?<attrs>.*)\)/', $link, $matches)) {
            $downloadData = explode(', ', str_replace("'", '', $matches['attrs']));
            $vars = [
                'eDocId',
                'seq',
                'type',
                'series',
                'number',
                'docType',
            ];
            $brandData['requestUrl'] = self::BRAND_DOWNLOAD . '?' . http_build_query(array_combine($vars, $downloadData));
        }

        /** @phpstan-ignore-next-line */
        return $brandData;
    }

    private function rulesForBrands() : array
    {
        return [
            'markName' => 'present|string',
            // Filter by request number or range of them
            'requestNumber1' => 'present|numeric|required_with:requestNumber2',
            'requestNumber2' => 'present|numeric',
            // Filter by registration number or range of them
            'registrationNumber1' => 'present|numeric|required_with:registrationNumber2',
            'registrationNumber2' => 'present|numeric',
            // Request date
            'dateFrom' => 'present|date_format:d/m/Y',
            'dateTo' => 'present|date_format:d/m/Y',
            // Filter by owner or requester data
            'ownerName' => 'present|string',
            'ownerNationality' => 'present|string',
            'representativeName' => 'present|string',
            // Type of brand and resource
            'signType' => 'present|in:' . implode(',', array_keys($this->signTypes)),
            'requestType' => 'present|in:' . implode(',', array_keys($this->requestTypes)),
            'requestSubType' => 'present|in:P,R',
        ];
    }
}
