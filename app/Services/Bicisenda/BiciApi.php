<?php

namespace App\Services\Bicisenda;

use App\Services\UserAgent;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class BiciApi
{
    public const CIUDAD_VIEJA = 'sentido_ciudad_vieja';
    public const TRES_CRUCES = 'sentido_tres_cruces';
    public const QUERY_URL = 'https://graf.montevideo.gub.uy/graf/api/ds/query';

    public function getDataGroupByHour(Carbon $start, Carbon $end, bool $singleValue = true) : Collection
    {
        $response = $this->query(start: $start, end: $end, groupby: 'hora');

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        if ($singleValue) {
            return $fields->combine($values->pluck('0'));
        }

        return collect(range(0, count($values->first()) - 1))->map(fn ($index) => $fields->combine($values->pluck($index)));
    }

    public function getDataGroupByDay(Carbon $start, Carbon $end)
    {
        $response = $this->query(start: $start, end: $end, groupby: 'dia, mes, anio');

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        return collect(range(0, count($values->first()) - 1))
            ->mapWithKeys(function ($index) use ($fields, $values) {
                $combined = $fields->combine($values->pluck($index));
                $date = Carbon::parse("{$combined['dia']} {$combined['mes']} {$combined['anio']}", 'America/Montevideo');
                return [
                    $date->timestamp => $combined,
                ];
            })->sortKeys();
    }

    public function query(Carbon $start = null, Carbon $end = null, string $sql = null, string $groupby = null) : Response
    {
        if ($sql === null) {
            $select = sprintf(<<<SELECT
                sum(in_counts) as %s,
                sum(out_counts) as %s,
                count(*) as count
                SELECT, self::CIUDAD_VIEJA, self::TRES_CRUCES);

            $group = '';
            if($groupby !== null) {
                $select .= ", $groupby";
                $group .= " GROUP BY $groupby";
                $group .= " ORDER BY $groupby";
            }
            $sql = <<<"QUERY"
                SELECT
                    $select
                FROM observatorio.v_omo_conteo_bicisenda
                WHERE
                QUERY;
            $sql .= ' fecha_conteo >= ' . $start->getTimestampMs()
            . ' AND ' . 'fecha_conteo <= ' . $end->getTimestampMs();
            $sql .= $group;
        }
        // dump($sql);
        $query = [
            'queries' => [
                [
                    'refId' => 'A',
                    'datasource' => [
                        'uid' => 'Ys8zmZ2nz',
                        'type' => 'grafana-postgresql-datasource',
                    ],
                    'rawSql' => $sql,
                    'format' => 'table',
                    'datasourceId' => 112,
                    // 'intervalMs' => 43200000, // 12h
                    'maxDataPoints' => 5000,
                ],
            ],
        ];

        return Http::withHeaders([
            'User-Agent' => resolve(UserAgent::class)->get(),
        ])->post(self::QUERY_URL, $query);
    }
}
