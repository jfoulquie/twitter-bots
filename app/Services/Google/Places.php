<?php

namespace App\Services\Google;

use Illuminate\Support\Facades\Http;

use function Safe\unlink;

class Places
{
    private const BASE_URL = 'https://maps.googleapis.com/maps/api/place';
    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function findPlace(string $input, ?string $bias = null, string $fields = 'geometry,photos,place_id,type') : array
    {
        $query = [
            'input' => $input,
            'inputtype' => 'textquery',
            'fields' => $fields,
            'language' => 'en',
            'key' => $this->apiKey,
        ];

        if ($bias !== null) {
            $query['locationbias'] = $bias;
        }

        $response = Http::get(self::BASE_URL . '/findplacefromtext/json', $query);
        return $response->json();
    }

    public function getDetails(string $placeId, string $fields = 'geometry,photos') : array
    {
        $query = [
            'place_id' => $placeId,
            'fields' => $fields,
            'key' => $this->apiKey,
            'language' => 'es-419', // Spanish - Latin America
        ];

        $response = Http::get(self::BASE_URL . '/details/json', $query);
        return $response->json();
    }

    /**
     *
     * @param array<string> $photoReferences
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     * @return array<string> An array with all the paths to the downloaded photos
     */
    public function getImages(string $name, array $photoReferences, int $limit) : array
    {
        $files = [];
        foreach ($photoReferences as $reference) {
            $query = [
                'maxwidth' => 800,
                'photoreference' => $reference,
                'key' => $this->apiKey,
            ];

            $filename = storage_path('app/google/' . $name . '-' . count($files) . '.jpg');

            $response = Http::withOptions([
                'sink' => $filename,
            ])->get(self::BASE_URL . '/photo', $query);

            if ($response->ok()) {
                $files[$reference] = $filename;
            } else {
                unlink($filename);
            }

            // Stop once we reach the limit
            if (count($files) >= $limit) {
                break;
            }
        }
        return $files;
    }
}
