<?php

namespace App\Services\Google;

use Google\Client;
use Google\Service\Sheets;
use Illuminate\Support\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class GoogleSheets
{
    /**
     * @var \Google\Service\Sheets
     */
    public $api;

    public function __construct(Client $client)
    {
        $this->api = new Sheets($client);
    }

    public static function getDateFromSerialNumber(float $number, string $tz = null) : Carbon
    {
        return new Carbon(Date::excelToDateTimeObject($number, $tz));
    }

    public static function getSerialNumberFromDate(Carbon $date) : float
    {
        return Date::dateTimeToExcel($date);
    }
}
