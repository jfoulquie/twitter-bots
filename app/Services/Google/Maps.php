<?php

namespace App\Services\Google;

use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Http;

class Maps
{
    private const BASE_URL = 'https://maps.googleapis.com/maps/api/staticmap';
    private string $apiKey;
    private string $urlSecret;

    public function __construct(string $apiKey, string $urlSecret)
    {
        $this->apiKey = $apiKey;
        $this->urlSecret = $urlSecret;
    }

    public function getImage(string $filename, array $viewport) : string
    {
        $query = [
            'maptype' => 'satellite',
            'size' => '640x640',   // 4:3
            'scale' => '2',
            'visible' => implode(',', $viewport['northeast']) . '|' . implode(',', $viewport['southwest']),
            'key' => $this->apiKey,
        ];
        $filepath = storage_path('app/google/' . $filename . '.png');
        /** @var \App\Services\Google\GoogleURLSigner */
        $urlSigner = app()->makeWith(GoogleURLSigner::class, ['secret' => $this->urlSecret]);
        $response = Http::withMiddleware(Middleware::mapRequest([$urlSigner, 'signBeforeSend']))
            ->withOptions([
                'sink' => $filepath,
            ])->get(self::BASE_URL, $query);

        return $filepath;
    }
}
