<?php

namespace App\Services\Google;

use App\Models\Lot;
use GuzzleHttp\Middleware;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

use function Safe\unlink;

class StreetView
{
    private const BASE_URL = 'https://maps.googleapis.com/maps/api/streetview';

    private string $apiKey;
    private string $urlSecret;

    public function __construct(string $apiKey, string $urlSecret)
    {
        $this->apiKey = $apiKey;
        $this->urlSecret = $urlSecret;
    }

    public function getImageForLot(Lot $lot) : string
    {
        $meta = $this->getMetadata($lot);
        $lot->metadata = $meta;

        $query = [
            'key' => $this->apiKey,
            'return_error_code' => 'true',
            'size' => '1280x960',   // 4:3
            'source' => 'outdoor',
        ];
        $query = array_merge($query, $this->aimCamera($lot->getFloors()));
        $query = array_merge($query, ['location' => $lot->getLocation()]);
        /** @var \App\Services\Google\GoogleURLSigner */
        $urlSigner = app()->makeWith(GoogleURLSigner::class, ['secret' => $this->urlSecret]);
        $response = Http::withMiddleware(Middleware::mapRequest([$urlSigner, 'signBeforeSend']))
            ->withOptions([
                'sink' => $lot->filename,
            ])->get(self::BASE_URL, $query);
        $lot->maps_tried = true;
        if (!$response->successful()) {
            unlink($lot->filename);
            $lot->save();
            $response->throw();
        }

        $lot->geocode_lat = Arr::first(explode(',', $query['location']));
        $lot->geocode_long = Arr::last(explode(',', $query['location']));

        $lot->save();

        return $lot->filename;
    }

    public function getMetadata(Lot $place) : array
    {
        $query = [
            'key' => $this->apiKey,
            'location' => $place->getLocation(),
        ];
        $query = array_merge($query, $this->aimCamera($place->getFloors()));
        /** @var \App\Services\Google\GoogleURLSigner */
        $urlSigner = app()->makeWith(GoogleURLSigner::class, ['secret' => $this->urlSecret]);
        $response = Http::withMiddleware(Middleware::mapRequest([$urlSigner, 'signBeforeSend']))
            ->get(self::BASE_URL . '/metadata', $query);

        $place->maps_tried = true;
        if (!$response->successful()) {
            $response->throw();
        }

        return $response->json();
    }

    /**
     * Thanks @everylot https://github.com/fitnr/everylotbot/blob/529826f01820b925803d13aa458baf3cc778f9c8/everylot/everylot.py#L70
     *
     * @param int $floors
     * @return array
     */
    private function aimCamera(int $floors = 2) : array
    {
        if ($floors < 0) {
            $floors = 0;
        }

        switch ($floors) {
            case 0:
            case 1:
            case 2:
                return [
                    'fov' => 65,
                    'pitch' => 10,
                ];
            case 3:
                return [
                    'fov' => 72,
                    'pitch' => 10,
                ];
            case 4:
                return [
                    'fov' => 76,
                    'pitch' => 15,
                ];
            case 5:
                return [
                    'fov' => 81,
                    'pitch' => 20,
                ];
            case 6:
            case 7:
                return [
                    'fov' => 86,
                    'pitch' => 20,
                ];
            case 8:
            case 9:
                return [
                    'fov' => 90,
                    'pitch' => 25,
                ];
            case 10:
            default:
                return [
                    'fov' => 90,
                    'pitch' => 25,
                ];
        }
    }
}
