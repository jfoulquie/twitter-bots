<?php

namespace App\Services\Google;

use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Geocode
{
    private const BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json';

    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function getCoordinatesFromAddress(string $address) : array
    {
        $query = [
            'address' => $address,
            'key' => $this->apiKey,
        ];
        $response = Http::acceptJson()->get(self::BASE_URL, $query);

        if (!$response->successful()) {
            $response->throw();
        }

        Log::debug('Geocoding response', $response->json());

        $location = data_get($response->json(), 'results.0.geometry.location');
        if ($location === null) {
            throw new HttpClientException('Invalid response data');
        }
        return $location;
    }
}
