<?php

namespace App\Services\Google;

use App\Services\UserAgent;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

use function Safe\json_decode;

class MapsShortener
{
    private const BASE_URL = 'https://www.google.com/maps/rpc/shorturl';

    public function shorten(string $url) : string
    {
        $query = [
            'authuser' => '0',
            'hl' => 'en',
            'gl' => 'uy',
            'pb' => '!1s' . $url,
        ];

        $response = Http::withHeaders([
            'User-Agent' => resolve(UserAgent::class)->get(),
            'Referer' => 'https://www.google.com/',
            'DNT' => '1',
        ])->get(self::BASE_URL, $query);

        if ($response->failed()) {
            $response->throw();
        }

        // Fix Google's broken json
        $body = str_replace(")]}'\n", '', $response->body());
        $data = json_decode($body, true);

        return Arr::first($data);
    }
}
