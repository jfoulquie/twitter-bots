<?php

namespace App\Services\Meteobot;

use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Plot\IconPlot;
use Amenadiel\JpGraph\Plot\PlotLine;
use App\Graphs\Themes\MeteoBotTheme;
use App\Traits\Graphs\GeneratesGraphs;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use RuntimeException;

/**
 * @phpstan-type Station array{id: int, name: string, location: array{lat: numeric-string, long: numeric-string}, department: string}
 */
class Graphs
{
    use GeneratesGraphs;

    private const MIN_SAMPLES_FOR_GRAPH = 10;

    private Collection $data;
    private Carbon $date;

    public function __construct(private AmbienteUY $ambiente, Carbon $date = null)
    {
        $this->data = new Collection();
        if ($date === null) {
            $this->date = now()->timezone('America/Montevideo')->subDay();
        }
    }

    public function setDate(Carbon $date)
    {
        $this->date = $date;
    }

    public function getAmbienteService() : AmbienteUY
    {
        return $this->ambiente;
    }

    public function getStoredData() : Collection
    {
        return $this->data;
    }

    public function generateGraphs() : array
    {
        $charts = [];
        // Ambiente graphs
        $ambGraphs = [
            'humidity' => AmbienteUY::HUMEDAD,
            'pressure' => AmbienteUY::PRESION,
            // last data point, Sept 2020 for Barradas
            // 'rain' => AmbienteUY::LLUVIA,
            'temperature' => AmbienteUY::TEMP,
            'wind' => AmbienteUY::VIENTO_INT,
        ];
        foreach ($this->ambiente->getStations() as $stationData) {
            $ambienteData = new Collection();
            foreach ($ambGraphs as $func => $name) {
                switch($name) {
                    case AmbienteUY::HUMEDAD:
                        $dates = collect([
                            $this->date->copy(),
                        ]);
                        $ambienteData = $this->getData($stationData['id'], $dates);
                        break;
                    case AmbienteUY::PRESION:
                        $dates = collect([
                            $this->date->copy(),
                        ]);
                        $ambienteData = $this->getData($stationData['id'], $dates);
                        break;
                    case AmbienteUY::VIENTO_INT:
                        $dates = collect([
                            $this->date->copy(),
                        ]);
                        $ambienteData = $this->getData($stationData['id'], $dates);
                        break;
                    case AmbienteUY::TEMP:
                        $dates = collect([
                            $this->date->copy()->subYears(10),
                            $this->date->copy()->subYears(9),
                            $this->date->copy()->subYears(8),
                            $this->date->copy()->subYears(7),
                            $this->date->copy()->subYears(6),
                            $this->date->copy()->subYears(5),
                            $this->date->copy()->subYears(4),
                            $this->date->copy()->subYears(3),
                            $this->date->copy()->subYears(2),
                            $this->date->copy()->subYear(),
                            $this->date->copy(),
                        ]);
                        $ambienteData = $this->getData($stationData['id'], $dates);
                        break;
                    case AmbienteUY::LLUVIA:
                        // Make bar plot with line plot of accumulating in the month
                        continue 2;
                }

                $data = [];

                $ambienteData->each(function (Collection $series, string $key) use ($name, &$data, $stationData) {
                    // dump($key);
                    switch($name) {
                        case AmbienteUY::VIENTO_INT:
                            $seriesData = $series->mapWithKeys(fn (Collection $row) => [
                                $row['Fecha'] => [$row->get(AmbienteUY::VIENTO_DIR), $row->get(AmbienteUY::VIENTO_INT)],
                            ]);
                            // Make sure there are enough datapoints to add the series to a graph
                            if ($seriesData->pluck(1)->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
                                Log::warning("«{$name}» data for {$stationData['name']} does not contain enough data points", Arr::wrap($seriesData));
                                return;
                            }
                            $data[] = $seriesData;
                            break;
                        case AmbienteUY::HUMEDAD:
                        case AmbienteUY::PRESION:
                            $seriesData = $series->pluck($name, 'Fecha');
                            if ($seriesData->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
                                Log::warning("«{$name}» data for {$stationData['name']} does not contain enough data points", Arr::wrap($seriesData));
                                return;
                            }
                            $data[] = $seriesData->all();
                            break;
                        case AmbienteUY::LLUVIA:
                        case AmbienteUY::TEMP:
                        default:
                            $seriesData = $series->pluck($name, 'Fecha');
                            if ($seriesData->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
                                Log::warning("«{$name}» data for {$stationData['name']} does not contain enough data points", Arr::wrap($seriesData));
                                return;
                            }
                            $dateCarbon = Carbon::parse($key);
                            $dateStr = $dateCarbon->year === $this->date->year
                                ? Carbon::parse($key)->locale('es_ES')->isoFormat('D MMM YYYY ')
                                : Carbon::parse($key)->isoFormat('YYYY ');
                            $dateStr = ucwords($dateStr);
                            $data[$dateStr] = $seriesData->all();
                            break;
                    }
                });

                if (empty($data)) {
                    continue;
                }

                $charts[$stationData['name']][$func] = $this->$func($stationData, $data);
            }
        }
        return $charts;
    }

    public function getData(int $station, Collection $dates) : Collection
    {
        $dates->each(function ($date) use ($station) {
            if ($this->data->has($station . '.' . $date->format('Y-m-d'))) {
                return;
            }
            $this->data->put(
                $station,
                Collection::wrap($this->data->get($station))->put(      /** @phpstan-ignore-line */
                    $date->format('Y-m-d'),
                    $this->ambiente->getData(
                        $station,
                        $date->copy()->subDay()->hour(23)->minute(50),
                        $date->copy()->hour(23)->minute(50)
                    )
                )
            );
        });

        // Normalize series
        $this->data->transform(function (Collection $stationData) {
            $stationData->transform(function (Collection $series) {
                // All series must have 24 values
                if ($series->count() === 24) {
                    return $series;
                }

                if ($series->isEmpty()) {
                    return;
                }

                $keys = $series->first()->keys();
                $day = Carbon::parse($series->first()->get('Fecha'));
                $copy = $series->keyBy('Fecha');
                $hours = range(0, 23);
                foreach ($hours as $hour) {
                    if (!$copy->has($day->hour($hour)->format('Y-m-d H:i:s'))) {
                        // Create null-filled entry
                        $value = $keys->combine(array_fill(0, $keys->count(), null));
                        // Fill in the date
                        $value->put('Fecha', $day->format('Y-m-d H:i:s'));
                        // Insert into series
                        $copy->put($day->format('Y-m-d H:i:s'), $value);
                    }
                }
                // Sort the times so they are in proper order
                return $copy->sortKeys()->values();
            });
            // remove empty series
            return $stationData->filter();
        });
        // Sort the dates
        $this->data->transform(fn ($stationData) => $stationData->sortKeys());

        return $this->data->get($station)->only($dates->map(fn (Carbon $date) => $date->format('Y-m-d'))->toArray());
    }

    /**
     *
     * @param Station $stationData
     * @param array<int, array<string, array>> $data
     * @throws \Carbon\Exceptions\NotLocaleAwareException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Carbon\Exceptions\InvalidTypeException
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Safe\Exceptions\OutcontrolException
     * @throws \RuntimeException
     * @return string
     */
    private function humidity(array $stationData, array $data) : string
    {
        $this->theme = new MeteoBotTheme();
        $this->theme->setTotalPlots(count($data));

        /** @var array<string, array> $latestSeries */
        $latestSeries = Arr::last($data);
        $labels = collect($latestSeries)->keys()->map(
            /** @phpstan-ignore-next-line */
            fn ($date) => Carbon::createFromFormat('Y-m-d H:i:s', $date)
                ->timezone('America/Montevideo')
                ->timestamp
        )->toArray();

        /** @var array<int, array<int, string>> $data */
        $data = Arr::map($data, fn (array $series) => array_values($series));
        /** @var array<int, string> */
        $series = reset($data);
        if (!is_array($series)) {
            throw new RuntimeException('Wrong format for data');
        }
        if (collect($series)->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
            Log::warning("Humidity data for {$stationData['name']} does not contain enough data points", $data);
            return '';
        }

        $title = 'Evolución de Humedad Relativa';
        $subtitle = $stationData['name'] . ', ' . $stationData['department'];
        $subsubtitle = Carbon::createFromTimestamp($labels[(int) count($labels) / 2])->locale('es_ES')->isoFormat('D \d\e MMMM YYYY');

        $graph = $this->createLinePlot($data, $labels, $title, $subtitle, $subsubtitle, 'datlin');
        $graph->yscale->SetAutoMin(0);
        $graph->yscale->SetAutoMax(100);
        $graph = $this->styleGraph($graph, $labels, '%d%%');

        return $this->getGraphImage($graph);
    }

    private function pressure(array $stationData, array $data) : string
    {
        $this->theme = new MeteoBotTheme();
        $this->theme->setTotalPlots(count($data));

        /** @var array<string, array> $latestSeries */
        $latestSeries = Arr::last($data);
        $labels = collect($latestSeries)->keys()->map(
            /** @phpstan-ignore-next-line */
            fn ($date) => Carbon::createFromFormat('Y-m-d H:i:s', $date)
                ->timezone('America/Montevideo')
                ->timestamp
        )->toArray();

        /** @var array<int, array<int, string>> $data */
        $data = Arr::map($data, fn (array $series) => array_values($series));
        /** @var array<int, string> */
        $series = reset($data);
        if (!is_array($series)) {
            throw new RuntimeException('Wrong format for data');
        }
        if (collect($series)->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
            Log::warning("Pressure data for {$stationData['name']} does not contain enough data points", $data);
            return '';
        }
        $title = 'Evolución de la Presión Atmosférica';
        $subtitle = $stationData['name'] . ', ' . $stationData['department'];
        $subsubtitle = Carbon::createFromTimestamp($labels[(int) count($labels) / 2])->locale('es_ES')->isoFormat('D \d\e MMMM YYYY');

        $graph = $this->createLinePlot($data, $labels, $title, $subtitle, $subsubtitle, 'datlin');

        // Add pressure at sea level (1013.25 hPa)
        $avgLine = new PlotLine(0, 1013.25, '#16161d', 5);
        $avgLine->SetLineStyle('dotted');
        $graph->Add($avgLine);

        $graph = $this->styleGraph($graph, $labels, '%d');
        $graph->yaxis->SetTitle(AmbienteUY::PRESION, 'middle');
        if (min(Arr::first($data)) > 1000) {
            $graph->yscale->SetAutoMin(1000);
        }

        if (max(Arr::first($data)) < 1014) {
            $graph->yscale->SetAutoMax(1020);
        }

        return $this->getGraphImage($graph);
    }

    private function temperature(array $stationData, array $data) : string
    {
        $this->theme = new MeteoBotTheme();
        $this->theme->setTotalPlots(count($data));

        /** @var array<string, array> $latestSeries */
        $latestSeries = Arr::last($data);
        $labels = collect($latestSeries)->keys()->map(
            /** @phpstan-ignore-next-line */
            fn ($date) => Carbon::createFromFormat('Y-m-d H:i:s', $date)
                ->timezone('America/Montevideo')
                ->timestamp
        )->toArray();

        /** @var array<int, array<int, string>> $data */
        $data = Arr::map($data, fn (array $series) => array_values($series));
        /** @var array<int, string> */
        $series = reset($data);
        if (!is_array($series)) {
            throw new RuntimeException('Wrong format for data');
        }
        if (collect($series)->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
            Log::warning("Temperature data for {$stationData['name']} does not contain enough data points", $data);
            return '';
        }

        $title = 'Evolución de la Temperatura';
        $subtitle = $stationData['name'] . ', ' . $stationData['department'];
        $subsubtitle = Carbon::createFromTimestamp($labels[(int) count($labels) / 2])->locale('es_ES')->isoFormat('D \d\e MMMM YYYY') . PHP_EOL;
        $subsubtitle .= 'Comparativa con el mismo día en años anteriores';

        $graph = $this->createLinePlot($data, $labels, $title, $subtitle, $subsubtitle, 'datlin');

        $graph = $this->styleGraph($graph, $labels, '%dºC');
        $graph->yaxis->SetTitle(AmbienteUY::TEMP, 'middle');

        return $this->getGraphImage($graph);
    }

    private function wind(array $stationData, array $data) : string
    {
        $this->theme = new MeteoBotTheme();
        $this->theme->setTotalPlots(count($data));

        /** @var \Illuminate\Support\Collection<string, array<int, string>> $latestSeries */
        $latestSeries = Arr::last($data);
        $labels = collect($latestSeries)->keys()->map(
            /** @phpstan-ignore-next-line */
            fn ($date) => Carbon::createFromFormat('Y-m-d H:i:s', $date)
                ->timezone('America/Montevideo')
                ->timestamp
        )->toArray();

        $directionData = $latestSeries->mapWithKeys(
            fn (array $row, string $date) => [
            /** @phpstan-ignore-next-line */
                Carbon::createFromFormat('Y-m-d H:i:s', $date)->timezone('America/Montevideo')->timestamp => $row[0],
            ]
        );
        $windData = Arr::map($data, fn (Collection $series) => $series->pluck(1));
        if ($latestSeries->filter()->count() <= self::MIN_SAMPLES_FOR_GRAPH) {
            Log::warning("Temperature data for {$stationData['name']} does not contain enough data points", $data);
            return '';
        }

        $title = 'Evolución de la Velocidad y Dirección del Viento';
        $subtitle = $stationData['name'] . ', ' . $stationData['department'];
        $subsubtitle = Carbon::createFromTimestamp($labels[(int) count($labels) / 2])->locale('es_ES')->isoFormat('D \d\e MMMM YYYY');

        $xScale = [min($labels), Carbon::createFromTimestamp(max($labels))->addHour()->timestamp];
        $graph = $this->createLinePlot($windData, $labels, $title, $subtitle, $subsubtitle, 'datlin', $xScale);
        $graph->yscale->SetGrace(10);

        // Add an empty image mark as default
        $graph->plots[0]->mark->SetType(MARK_IMG, resource_path('images/empty.png'), 1);
        // Add direction marks
        $graph->plots[0]->mark->SetCallbackYX(function ($y, $x) use ($directionData) {
            $angle = $directionData[$x];

            $width = '';
            $color = '';
            $fcolor = '';
            $imgscale = 0.4;
            $filename = '';
            if (empty($angle) || !is_numeric($angle) || $angle === $directionData->first()) {
                return [$width, $color, $fcolor, $filename, $imgscale];
            }

            $rounded = $this->findClosestAngle((float) $angle);
            $filename = resource_path("images/meteobot/compass-$rounded.png");
            return [$width, $color, $fcolor, $filename, $imgscale];
        });

        $graph = $this->styleGraph($graph, $labels, '%01.1f');
        $graph->yscale->SetAutoMin(0);
        $graph->yaxis->SetTitle(AmbienteUY::VIENTO_INT, 'middle');

        return $this->getGraphImage($graph);
    }

    // private function radiation(array $stationData, array $data) : string
    // {
    //     $this->theme = new MeteoBotTheme();
    //     $this->theme->setTotalPlots(count($data));

    //     $labels = collect(Arr::last($data))->keys()->map(fn ($ts) => Carbon::createFromTimestampMs($ts)->timestamp)->toArray();
    //     $data = Arr::map($data, fn (array $series) => array_values($series));

    //     $title = 'Comparativa de la Irradiación Solar ';
    //     $subtitle = 'Evolución a lo largo del día';
    //     $subsubtitle = 'Hace 6 meses, hace un mes,' . PHP_EOL;
    //     $subsubtitle .= 'la semana pasada, ayer y hoy' . PHP_EOL;
    //     $graph = $this->createLinePlot($data, $labels, $title, $subtitle, $subsubtitle, 'datlin');

    //     // Get manual tick every second hour
    //     list($majTickPos, $minTickPos) = $this->getHourlyTicks($labels);
    //     collect($majTickPos)->map(fn ($ts) => Carbon::createFromTimestamp($ts)->timezone('America/Montevideo')->format('H:i'));
    //     $graph->SetTickDensity(TICKD_SPARSE);

    //     $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);

    //     $graph->xaxis->SetLabelFormatCallback(fn ($ts) => Carbon::createFromTimestamp($ts)->timezone('America/Montevideo')->format('H:i'));
    //     $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%01.1f W/m²', $tick));

    //     $license = $this->getLicenseIcon();
    //     $graph->AddIcon($license);

    //     $logo = $this->getLogoIcon();
    //     $graph->AddIcon($logo);

    //     return $this->getGraphImage($graph);
    // }

    private function styleGraph(Graph $graph, array $labels, string $yFormat) : Graph
    {
        // Get manual tick every second hour
        list($majTickPos, $minTickPos) = $this->getHourlyTicks($labels);
        $graph->SetTickDensity(TICKD_VERYSPARSE);

        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);

        $graph->xaxis->SetLabelFormatCallback(fn ($ts) => Carbon::createFromTimestamp($ts)->format('H:i'));
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf($yFormat, $tick));

        $license = $this->getLicenseIcon();
        $graph->AddIcon($license);

        $logo = $this->getLogoIcon();
        $graph->AddIcon($logo);

        return $graph;
    }

    public function findClosestAngle(float $angle) : int
    {
        $milestones = collect([0, 45, 90, 135, 180, 225, 270, 315]);
        $distances = $milestones->map(fn ($milestone) => abs($angle - $milestone));
        $smallestDistanceKey = $distances->search($distances->min());
        if (!is_int($smallestDistanceKey)) {
            throw new RuntimeException('Unknown error when finding the closest angle of ' . $angle);
        }
        $closest = $milestones->get($smallestDistanceKey);
        if (!is_int($closest)) {
            throw new RuntimeException('Unexpected error finding closest angle');
        }
        return $closest;
    }

    private function getLicenseIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/cc-by-sa.png'));
        $icon->SetScale(0.35);
        $icon->SetMix(90);
        $icon->SetAnchor('right', 'top');
        $icon->SetPos(1200, 40);

        return $icon;
    }

    private function getLogoIcon() : IconPlot
    {
        // Add logo icon
        $icon = new IconPlot(resource_path('images/meteobot/logo-dwi.png'));
        $icon->SetScale(0.12);
        $icon->SetMix(90);
        $icon->SetAnchor('left', 'top');
        $icon->SetPos(140, 35);

        return $icon;
    }
}
