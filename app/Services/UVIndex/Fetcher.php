<?php

namespace App\Services\UVIndex;

use App\Services\UserAgent;
use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class Fetcher
{
    public const QUERY_URL = 'https://graf.montevideo.gub.uy/graf/api/ds/query';
    private array $keys = ['timestamp', 'radiation', 'temperature', 'uvindex'];

    public function getLatestValues() : array
    {
        $response = $this->query(1);

        return collect($this->keys)->combine(Arr::flatten(
            Arr::get($response->json(), 'results.A.frames.0.data.values')
        ))->toArray();
    }

    public function getLatestAverage(int $samples = 5) : array
    {
        $response = $this->query($samples + 1);
        $values = Arr::get($response->json(), 'results.A.frames.0.data.values', [[]]);

        $data = new Collection();

        /** @var \Illuminate\Support\Collection<int, array<string, mixed>> $data */
        $result = call_user_func_array([$data, 'zip'], $values);

        // Skip the first result, for some reason is a strange data point in the future
        $data = $result->skip(1)
        // Convert to a handier format
            ->map->filter(fn ($datapoint) => !is_null($datapoint))
            ->map->values()
            ->map->toArray()
            ->map(
                /** @var array<int, mixed> $datapoing */
                fn (array $datapoint) => collect($this->keys)
                    ->combine($datapoint)
                    ->toArray()
            );

        $timestamp = data_get($data, '0.timestamp');
        if (!Carbon::createFromTimestampMsUTC($timestamp)->isToday()) {
            $timestamp = $data->filter(fn ($row) => Carbon::createFromTimestampMsUTC($row['timestamp'])->isToday())->pluck('timestamp')->first();
        }

        // Transform to array of averages (and latest timestamp)
        return collect($this->keys)->combine([
            $timestamp,
            $data->avg('radiation'),
            $data->avg('temperature'),
            $data->avg('uvindex'),
        ])->toArray();
    }

    public function getValuesForTheDay(Carbon $start, Carbon $end) : Collection
    {
        $response = $this->query(100000, $start, $end, 'time', 'ASC');
        $values = Arr::get($response->json(), 'results.A.frames.0.data.values', [[]]);

        $data = new Collection();

        /** @var \Illuminate\Support\Collection<int, array<string, mixed>> $data */
        $result = call_user_func_array([$data, 'zip'], $values);

        // Skip the first result, for some reason is a strange data point in the future
        $data = $result->skip(1)
        // Convert to a handier format
            ->map->filter(fn ($datapoint) => !is_null($datapoint))
            ->map->values()
            ->map->toArray()
            ->filter(fn ($row) => count($row) === 4)
            ->map(
                /** @var array<int, mixed> $datapoint */
                fn (array $row) => collect($this->keys)
                    ->combine($row)
                    ->toArray()
            );

        // $data->map(function($row) {
        //         $row['timestamp'] = Carbon::createFromTimestampMs($row['timestamp'])->format('H:i');
        //         return $row;
        // })->dump();

        return $data;
    }

    public function query(int $samples, ?Carbon $start = null, ?Carbon $end = null, string $orderby = 'time', string $order = 'desc') : Response
    {
        $order = mb_strtoupper($order);
        $sql = <<<'QUERY'
            SELECT
                $__time(time_index),
                solarradiation * 10 AS "radiación solar (W/m2)",
                temperature AS "Tint (°C)",
                uvindex AS "UV2"
            FROM
                mtdefault.etweatherobserved
            WHERE
                entity_id = 'MVD:WeatherObserved:00002'
            QUERY;

        if ($start !== null) {
            $sql .= ' AND time_index >= ' . $start->getTimestampMs() . PHP_EOL;
        }

        if ($end !== null) {
            $sql .= ' AND time_index <= ' . $end->getTimestampMs() . PHP_EOL;
        }

        $sql .= <<<QUERY
            ORDER BY $orderby $order
            LIMIT
            QUERY;
        $sql .= " $samples;";

        $query = [
            'queries' => [
                [
                    'refId' => 'A',
                    'datasource' => [
                        'uid' => 'pgwpCUVnz',
                        'type' => 'postgres',
                    ],
                    'rawSql' => $sql,
                    'format' => 'table',
                    'datasourceId' => 90,
                    'intervalMs' => 6000,
                    'maxDataPoints' => 5000,
                ],
            ],
        ];

        return Http::withHeaders([
            'User-Agent' => resolve(UserAgent::class)->get(),
        ])->post(self::QUERY_URL, $query);
    }
}
