<?php

namespace App\Services\UVIndex;

use Amenadiel\JpGraph\Plot\IconPlot;
use Amenadiel\JpGraph\Plot\PlotBand;
use App\Graphs\Themes\UVIndexTheme;
use App\Models\Inumet\Observation;
use App\Traits\Graphs\GeneratesGraphs;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use RuntimeException;

use function Safe\date_sun_info;

class Graphs
{
    use GeneratesGraphs;

    public Collection $data;
    public Collection $dates;

    public function __construct(private Fetcher $imFetcher)
    {
        $this->dates = collect([
            // 'lastYear' => now()->timezone('America/Montevideo')->subDay(1)->subYear()->locale('es_UY'),
            // '6monthsAgo' => now()->timezone('America/Montevideo')->subDay(1)->subMonths(6)->locale('es_UY'),
            // 'lastMonth' => now()->timezone('America/Montevideo')->subDay(1)->subMonth()->locale('es_UY'),
            // 'lastWeek' => now()->timezone('America/Montevideo')->subDays(8)->locale('es_UY'),
            // 'yesterday' => now()->timezone('America/Montevideo')->subDays(2)->locale('es_UY'),
            'today' => now()->timezone('America/Montevideo')->locale('es_ES'),
        ]);
    }

    public function generateGraphs() : array
    {
        // IM Graphs
        $this->data = $this->getData();
        $imGraphs = [
            'uvindex',
            'radiation',
        ];
        $charts = [];
        $data = [];
        foreach ($imGraphs as $name) {
            $this->data->each(function (Collection $series) use ($name, &$data) {
                $data[] = $series->pluck($name, 'timestamp')->all();
            });
        }
        $charts['radiation'] = $this->uvIndexGraph($data);
        return $charts;
    }

    public function getData() : Collection
    {
        $earliestSunrise = $this->dates->mapWithKeys(function ($date, $key) {
            $sunInfo = date_sun_info($date->timezone('America/Montevideo')->getTimestamp(), -34.90706976618914, -56.18601472514095);
            $sunrise = Carbon::createFromTimestamp($sunInfo['sunrise'])->timezone('America/Montevideo');
            // $sunset = Carbon::createFromTimestamp($sunInfo['sunset'])->timezone('America/Montevideo');
            return [$key => $sunrise];
        })->sort(fn ($date1, $date2) => $date1->format('Hi') < $date2->format('Hi') ? -1 : 1)->first();

        $latestSunset = $this->dates->mapWithKeys(function ($date, $key) {
            $sunInfo = date_sun_info($date->timezone('America/Montevideo')->getTimestamp(), -34.90706976618914, -56.18601472514095);
            $sunset = Carbon::createFromTimestamp($sunInfo['sunset'])->timezone('America/Montevideo');
            return [$key => $sunset];
        })->sort(fn ($date1, $date2) => $date1->format('Hi') > $date2->format('Hi') ? -1 : 1)->first();

        if (!$earliestSunrise instanceof Carbon || !$latestSunset instanceof Carbon) {
            throw new RuntimeException('Wrong dates');
        }

        // Make the dates look rounded
        $latestSunset->addHour();
        $fullData = new Collection();
        $this->dates->each(
            fn ($date, $key) => $fullData->put(
                $key,
                $this->imFetcher->getValuesForTheDay(
                    $date->copy()->hour($earliestSunrise->hour)->minute(0),
                    $date->copy()->hour($latestSunset->hour)->minute(0)
                )
            )
        );

        // Do they all have the same datapoints?
        $minDataPoints = $fullData->reduce(
            fn (?int $carry, Collection $series) => is_null($carry)
                ? $series->count()
                : min($carry, count($series))
        );

        // Normalize
        $incorrectDataPoints = $fullData->filter(fn (Collection $series) => $series->count() !== $minDataPoints);
        if ($incorrectDataPoints->isNotEmpty()) {
            // Some series need fixing so the plots have the same datapoints
            $baseSerieskey = $fullData->search(fn (Collection $series) => $series->count() === $minDataPoints);
            /** @phpstan-ignore-next-line */
            $baseline = $fullData->get($baseSerieskey)->map(function (array $values) {
                $values['timestamp'] = Carbon::createFromTimestampMs($values['timestamp'])->timezone('America/Montevideo')->format('H:i');
                return $values;
            });

            $incorrectDataPoints->map(
                fn (Collection $series) => $series->map(function (array $values) {
                    $values['timestamp'] = Carbon::createFromTimestampMs($values['timestamp'])->timezone('America/Montevideo')->format('H:i');
                    return $values;
                })
            )->each(function (Collection $series, string $key) use ($baseline, &$fullData) {
                $diff = $series->pluck('timestamp')->diff($baseline->pluck(['timestamp']));
                $diff->each(fn ($value, $index) => $series->forget($index));
                $fullData->put($key, $series);
            });
        }

        return $fullData;
    }

    private function uvIndexGraph(array $data) : string
    {
        $this->theme = new UVIndexTheme();

        /** @var array<int, float> $series */
        $series = Arr::last($data);
        $labels = collect($series)->keys()->map(fn ($ts) => Carbon::createFromTimestampMs($ts)->timestamp)->toArray();

        // Add weather icon
        $hour = -1;
        $weatherSeries = collect($labels)->mapWithKeys(function (int $ts) use (&$hour) {
            $date = Carbon::createFromTimestamp($ts)->minute(0)->seconds(0);
            if ($hour === $date->hour) {
                return[$ts => false];
            }
            $hour = $date->hour;
            // Prado Station
            $o = Observation::fromStationId(211)->where('updated_at', $date->timestamp)->first('IconoEstadoActual');
            if ($o === null) {
                return [$ts => false];
            }

            return [$ts => storage_path('app/inumet/icons/' . $o->iconName)];
        })->filter();

        $data = Arr::map($data, fn (array $series) => array_values($series));
        $title = 'Radiación Solar en Montevideo';
        $subtitle = 'Evolución a lo largo del día';
        $subsubtitle = ucwords($this->dates->last()->isoFormat('dddd D MMMM YYYY'));
        $graph = $this->createLinePlotMultiAxis($data, $labels, $title, $subtitle, $subsubtitle, 'datlin');

        // Add color bands for UV Index levels
        $bandsData = [
            ['min' => 0, 'max' => 3, 'color' => ['green@0.65', 'yellow@0.55']],
            ['min' => 3, 'max' => 6, 'color' => ['yellow@0.55', 'orange@0.45']],
            ['min' => 6, 'max' => 8, 'color' => ['orange@0.45', 'red@0.65']],
            ['min' => 8, 'max' => 11, 'color' => ['red@0.65', 'red@0.45']],
            ['min' => 11, 'max' => 20, 'color' => ['purple@0.65', 'purple@0.65']],
        ];
        foreach ($bandsData as $data) {
            $band = new PlotBand(HORIZONTAL, BAND_SOLID, $data['min'], $data['max'], reset($data['color']));
            // $band = new PlotBand(HORIZONTAL, BAND_SOLID, $data['min'], $data['max'], $data['color']);
            $band->ShowFrame(false);
            $graph->Add($band);
        }

        // Get manual tick every second hour
        list($majTickPos, $minTickPos) = $this->getHourlyTicks($labels);
        collect($majTickPos)->map(fn ($ts) => Carbon::createFromTimestamp($ts)->timezone('America/Montevideo')->format('H:i'));
        $graph->SetTickDensity(TICKD_SPARSE);

        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);

        $graph->xaxis->SetLabelFormatCallback([$this, 'formatTimeLabel']);
        $graph->yaxis->SetLabelFormatCallback([$this, 'formatUVIndex']);
        $graph->yaxis->SetTitle('Índice UV', 'middle');
        $graph->ynaxis[0]->SetLabelFormatCallback([$this, 'formatRadiation']);

        // Add an empty image mark as default
        $graph->plots[0]->mark->SetType(MARK_IMG, resource_path('images/empty.png'), 1);

        // Add direction marks
        $graph->plots[0]->mark->SetCallbackYX(function ($y, $x) use ($weatherSeries) {
            $icon = $weatherSeries->get($x);

            $width = '';
            $color = '';
            $fcolor = '';
            $imgscale = 1;
            $filename = '';
            if (empty($icon)) {
                return [$width, $color, $fcolor, $filename, $imgscale];
            }

            return [$width, $color, $fcolor, $icon, $imgscale];
        });

        $license = $this->getLicenseIcon();

        $license->SetPos($graph->img->original_width - $graph->img->right_margin - 40, 40);
        $graph->AddIcon($license);

        $logo = $this->getLogoIcon();
        $graph->AddIcon($logo);

        return $this->getGraphImage($graph);

        // file_put_contents(storage_path('app/uvindex/radiation-' . date('Y-M-d H:i:s') . '.png'), $chart);
    }

    public function formatTimeLabel(int $ts) : string
    {
        return Carbon::createFromTimestamp($ts)->timezone('America/Montevideo')->format('H:i');
    }

    public function formatRadiation(float $radiation) : string
    {
        return sprintf('%01.1f W/m²', $radiation);
    }

    public function formatUVIndex(float $uvIndex) : string
    {
        return sprintf('%d', $uvIndex);
    }

    private function getLicenseIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/cc-by-sa.png'));
        $icon->SetScale(0.35);
        $icon->SetMix(90);
        $icon->SetAnchor('right', 'top');

        return $icon;
    }

    private function getLogoIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/uvindex/logo-dwi.png'));
        $icon->SetScale(0.12);
        $icon->SetMix(90);
        $icon->SetAnchor('left', 'top');
        $icon->SetPos(140, 35);

        return $icon;
    }
}
