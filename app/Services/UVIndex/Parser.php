<?php

namespace App\Services\UVIndex;

use Illuminate\Support\Arr;

class Parser
{
    private array $ranges;
    private array $images;

    public function __construct()
    {
        $this->ranges = [
            0 => 'Buenas noches, hasta mañana.',
            1 => 'Bajo peligro de los rayos UV del sol para una persona promedio.',
            2 => 'Bajo peligro de los rayos UV del sol para una persona promedio.',
            3 => '👒🕶️👕 Riesgo moderado de daño por exposición al sol sin protección.',
            4 => '👒🕶️👕 Riesgo moderado de daño por exposición al sol sin protección.',
            5 => '👒🕶️👕 Riesgo moderado de daño por exposición al sol sin protección.',
            6 => '👒🕶️👕 Riesgo alto de daño por exposición al sol sin protección.',
            7 => '👒🕶️👕 Riesgo alto de daño por exposición al sol sin protección.',
            8 => '👒🕶️👕⛱️ Riesgo muy alto de daño por exposición al sol sin protección.',
            9 => '👒🕶️👕⛱️ Riesgo muy alto de daño por exposición al sol sin protección.',
            10 => '👒🕶️👕⛱️ Riesgo muy alto de daño por exposición al sol sin protección.',
            11 => '👒🕶️👕⛱️ Riesgo extremo de daño por exposición al sol sin protección.',
        ];
        $basePath = resource_path('images/uvindex/');
        $this->images = [$basePath . 'night.png'];
        for ($i = 1; $i <= 11; $i++) {
            $this->images[$i] = $basePath . $i . '.png';
        }
        $this->images[] = $basePath . '11+.png';
    }

    public function getRiskForIndex(float $index) : string
    {
        return Arr::get($this->ranges, (int) round($index), $this->ranges[11]);
    }

    public function getAvatarForIndex(float $index) : string
    {
        return Arr::get($this->images, (int) round($index), $this->images[12]);
    }

    public function getEmojiForIndex(?float $index) : string
    {
        $index = round((float) $index);
        if (empty($index)) {
            return '🌃';
        } elseif ($index > 0 && $index < 3) {
            return '☀️🟢';
        } elseif ($index >= 3 && $index < 6) {
            return '☀️🟡';
        } elseif ($index >= 6 && $index < 8) {
            return '☀️🟠';
        } elseif ($index >= 8 && $index <= 10) {
            return '☀️🔴';
        } else {
            return '☀️🟣';
        }
    }

    public function getAdviceForIndex(?float $index) : string
    {
        $index = round((float) $index);
        if (empty($index)) {
            return '';
        } elseif ($index > 0 && $index < 3) {
            return '¡Puede permanecer en el exterior sin riesgo!';
        } elseif ($index >= 3 && $index <= 7) {
            return '¡Póngase camisa, protector solar y sombrero!';
        } elseif ($index >= 7 && $index <= 11) {
            return '¡Busque la sombra! y recuerde, ¡camisa, protector y sombrero son IMPRESCINDIBLES!';
        } else {
            return '🌞🔥❤️‍🔥😶‍🌫️ ¡Busque la sombra por lo que más quiera!';
        }
    }
}
