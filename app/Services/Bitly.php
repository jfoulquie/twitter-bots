<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Bitly
{
    private string $token;
    private const BASE_URL = 'https://api-ssl.bitly.com';

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function shorten(string $url) : string
    {
        $path = '/v4/shorten';
        $data = [
            'long_url' => $url,
            // 'group_guid' => '',
        ];
        $response = Http::withToken($this->token)->asJson()->post(self::BASE_URL . $path, $data);
        if (!$response->successful()) {
            $response->throw();
        }
        return $response->json('link');
    }
}
