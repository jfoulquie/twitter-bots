<?php

namespace App\Providers;

use App\Listeners\EveryFlight\FlightsEventSubscriber;
use App\Models\PoliticosAlert\TwitterUser;
use App\Observers\PoliticosAlert\TwitterUserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<string, array<int, string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array<int, string>
     */
    protected $subscribe = [
        FlightsEventSubscriber::class,
    ];

    /**
    * Determine if events and listeners should be automatically discovered.
    *
    * @return bool
    */
    public function shouldDiscoverEvents() : bool
    {
        return false;
    }

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot() : void
    {
        TwitterUser::observe(TwitterUserObserver::class);
    }
}
