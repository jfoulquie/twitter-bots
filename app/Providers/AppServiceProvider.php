<?php

namespace App\Providers;

use App\Repositories\Dictionaries;
use App\Services\Bicisenda\BiciApi;
use App\Services\Bicisenda\Graphs as BicisendaGraphs;
use App\Services\Bitly;
use App\Services\FediApi;
use App\Services\Google\Geocode;
use App\Services\Google\Maps;
use App\Services\Google\Places;
use App\Services\Google\StreetView;
use App\Services\Inumet;
use App\Services\IPAP;
use App\Services\Meteobot;
use App\Services\MIEM\Helper;
use App\Services\TwitterHelper;
use App\Services\UserAgent;
use App\Services\UTE\ADME;
use App\Services\UTE\Graphs;
use App\Services\UVIndex;
use App\Services\UVIndex\Fetcher;
use App\Services\UVIndex\Parser;
use App\Services\Wikimedia;
use App\Services\Wikipedia;
use Google\Service\Sheets;
use Illuminate\Support\ServiceProvider;
use j3j5\TwitterApio;
use JeroenG\Flickr\Flickr;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\TagProcessor;
use Monolog\Processor\UidProcessor;
use RuntimeException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->app->singleton(Dictionaries::class, function ($app) {
            return new Dictionaries();
        });

        // Defancify
        $this->app->bindMethod('App\Console\Commands\Defancify\TestTweet@handle', function ($job, $app) {
            $this->addLogTag('DEFANCIFY');
            $api = new TwitterApio($app->config->get('services.twitter.defancify'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\Defancify\TestResponse@handle', function ($job, $app) {
            $this->addLogTag('DEFANCIFY');
            $api = new TwitterApio($app->config->get('services.twitter.defancify'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Jobs\Defancify\ProcessTweet@handle', function ($job, $app) {
            $this->addLogTag('DEFANCIFY');
            $api = new TwitterApio($app->config->get('services.twitter.defancify'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\Defancify\MentionsStream@handle', function ($job, $app) {
            $this->addLogTag('DEFANCIFY');
            $api = new TwitterApio($app->config->get('services.twitter.defancify'));
            return $job->handle($api);
        });

        // Everylot MVD
        $this->app->bindMethod('App\Console\Commands\Everylot\Test@handle', function ($job, $app) {
            $this->addLogTag('EVERYLOT');
            $api = new TwitterApio($app->config->get('services.twitter.everylotmvd'));
            return $job->handle($api);
        });
        $this->app->bindMethod('App\Console\Commands\Everylot\Post@handle', function ($job, $app) {
            $this->addLogTag('EVERYLOT');
            $api = new TwitterApio($app->config->get('services.twitter.everylotmvd'));
            return $job->handle($api);
        });

        // Autores.uy
        $this->app->bindMethod('App\Console\Commands\AutoresUY\Post@handle', function ($job, $app) {
            $this->addLogTag('AUTORES.UY');
            $api = new TwitterApio($app->config->get('services.twitter.autores'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\PoliticosAlert\Test@handle', function ($job, $app) {
            $this->addLogTag('POL-ALERT');
            $api = new TwitterApio($app->config->get('services.twitter.compras_watcher'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\PoliticosAlert\Update@handle', function ($job, $app) {
            $this->addLogTag('POL-ALERT');
            $api = new TwitterApio($app->config->get('services.twitter.polalert'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Jobs\PoliticosAlert\UpdateFriendsList@handle', function ($job, $app) {
            $api = \Atymic\Twitter\Facade\Twitter::forApiV2()->getQuerier();
            if (mt_rand(0, 1) === 1) {
                $api = $api->usingCredentials(
                    $app->config->get('services.twitter.polalert.token'),
                    $app->config->get('services.twitter.polalert.secret'),
                    $app->config->get('services.twitter.polalert.consumer_key'),
                    $app->config->get('services.twitter.polalert.consumer_secret'),
                );
            } else {
                $api = $api->usingCredentials(
                    $app->config->get('services.twitter.autores.token'),
                    $app->config->get('services.twitter.autores.secret'),
                    $app->config->get('services.twitter.autores.consumer_key'),
                    $app->config->get('services.twitter.autores.consumer_secret'),
                );
            }

            return $job->handle($api);
        });

        $this->app->bindMethod('App\Jobs\PoliticosAlert\RecordEvent@handle', function ($job, $app) {
            $api = new TwitterApio($app->config->get('services.twitter.polalert'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\Misc\TweetedAgo@handle', function ($job, $app) {
            $this->addLogTag('PETECO');
            $api = new TwitterApio($app->config->get('services.twitter.peteco'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\EveryUruguayan\Post@handle', function ($job, $app) {
            $this->addLogTag('URUGUAYANS');
            $api = new TwitterApio($app->config->get('services.twitter.everyuruguayan'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\EveryFlight\Post@handle', function ($job, $app) {
            $this->addLogTag('EVERYFLIGHT');
            date_default_timezone_set('America/Montevideo');
            $api = new FediApi($app->config->get('services.fediApi.everyflight.host'), $app->config->get('services.fediApi.everyflight.apikey'));
            return $job->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\EveryFlight\Test@handle', function ($job, $app) {
            $this->addLogTag('EVERYFLIGHT');
            date_default_timezone_set('America/Montevideo');
            return $job->handle();
        });

        $this->app->bindMethod('App\Console\Commands\UVindex\Post@handle', function ($job, $app) {
            $fediApi = new FediApi(
                $app->config->get('services.fediApi.uvindex.host'),
                $app->config->get('services.fediApi.uvindex.apikey')
            );

            $fetcher = new Fetcher();
            $parser = new Parser();
            return $job->handle($fediApi, $fetcher, $parser);
        });

        $this->app->bindMethod('App\Console\Commands\UVindex\PostChart@handle', function ($command, $app) {
            $fediApi = new FediApi(
                $app->config->get('services.fediApi.uvindex.host'),
                $app->config->get('services.fediApi.uvindex.apikey')
            );
            return $command->handle($app->make(UVIndex\Graphs::class), $fediApi);
        });

        $this->app->bindMethod('App\Console\Commands\EveryAirport\Post@handle', function ($command, $app) {
            $api = new TwitterApio($app->config->get('services.twitter.everyairport'));
            return $command->handle($app->make(Places::class), $app->make(Maps::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\RandomBakeries\Post@handle', function ($command, $app) {
            $api = new TwitterApio($app->config->get('services.twitter.randombakeries'));
            return $command->handle($app->make(Places::class), $api, $app->make(TwitterHelper::class));
        });

        $this->app->bindMethod('App\Console\Commands\CDF\Post@handle', function ($command, $app) {
            $this->addLogTag('CDF');
            $api = new FediApi($app->config->get('services.fediApi.cdf.host'), $app->config->get('services.fediApi.cdf.apikey'));
            return $command->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\IPAP\Post@handle', function ($command, $app) {
            $this->addLogTag('IPAP');
            $api = new TwitterApio($app->config->get('services.twitter.ipap'));
            return $command->handle($app->make(IPAP::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\Meteobot\Post@handle', function ($command, $app) {
            $this->addLogTag('METEOBOT');
            $api = new FediApi($app->config->get('services.fediApi.meteobot.host'), $app->config->get('services.fediApi.meteobot.apikey'));
            return $command->handle($app->make(Meteobot\Graphs::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\IndustrialProp\Post@handle', function ($command, $app) {
            $this->addLogTag('INDUSTRIAL PROPERTY');
            $api = new TwitterApio($app->config->get('services.twitter.indprop'));
            return $command->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\Misc\WebMonitor@handle', function ($command, $app) {
            $apiV1 = \Atymic\Twitter\Facade\Twitter::forApiV1();
            $apiV1 = $apiV1->usingCredentials(
                $app->config->get('services.twitter.webmonitor.token'),
                $app->config->get('services.twitter.webmonitor.secret'),
                $app->config->get('services.twitter.webmonitor.consumer_key'),
                $app->config->get('services.twitter.webmonitor.consumer_secret'),
            );

            $querier = \Atymic\Twitter\Facade\Twitter::forApiV2()->getQuerier();
            $querier = $querier->usingCredentials(
                $app->config->get('services.twitter.webmonitor.token'),
                $app->config->get('services.twitter.webmonitor.secret'),
                $app->config->get('services.twitter.webmonitor.consumer_key'),
                $app->config->get('services.twitter.webmonitor.consumer_secret'),
            );
            return $command->handle($apiV1, $querier);
        });

        $this->app->bindMethod('App\Console\Commands\UTE\UpdateCharts@handle', function ($command, $app) {
            $this->addLogTag('UTE');
            $api = new TwitterApio($app->config->get('services.twitter.ute'));
            return $command->handle($app->make(Graphs::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\UTE\Post@handle', function ($command, $app) {
            $this->addLogTag('UTE');

            $api = new FediApi($app->config->get('services.fediApi.ute.host'), $app->config->get('services.fediApi.ute.apikey'));
            return $command->handle($api, $app->make(Graphs::class), $app->make(ADME::class));
        });

        $this->app->bindMethod('App\Console\Commands\LaDiaria\Update@handle', function ($command, $app) {
            $this->addLogTag('LaDiaria');
            $api = new FediApi($app->config->get('services.fediApi.ladiaria.host'), $app->config->get('services.fediApi.ladiaria.apikey'));

            return $command->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\LaDiaria\Covers@handle', function ($command, $app) {
            $this->addLogTag('LaDiaria');
            $api = new FediApi($app->config->get('services.fediApi.ladiaria.host'), $app->config->get('services.fediApi.ladiaria.apikey'));

            return $command->handle($api);
        });

        $this->app->bindMethod('App\Console\Commands\SciHub\UpdateDomainList@handle', function ($command, $app) {
            $this->addLogTag('SciHub');
            $api = new FediApi($app->config->get('services.fediApi.scihub.host'), $app->config->get('services.fediApi.scihub.apikey'));
            return $command->handle($app->make(UserAgent::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\LitClock\Percentage@handle', function ($command, $app) {
            $this->addLogTag('LitClock');
            $api = new FediApi($app->config->get('services.fediApi.reloj.host'), $app->config->get('services.fediApi.reloj.apikey'));
            return $command->handle($app->make(Sheets::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\LitClock\PostTime@handle', function ($command, $app) {
            $this->addLogTag('LitClock');
            $api = new FediApi($app->config->get('services.fediApi.reloj.host'), $app->config->get('services.fediApi.reloj.apikey'));
            return $command->handle($app->make(Sheets::class), $api);
        });

        $this->app->bindMethod('App\Console\Commands\Wikipedia\NotabilityDeletion@handle', function ($command, $app) {
            $this->addLogTag('Wikipedia');
            $api = new FediApi($app->config->get('services.fediApi.wikipediaEn.host'), $app->config->get('services.fediApi.wikipediaEn.apikey'));
            return $command->handle($api, $app->make(Wikipedia::class));
        });

        $this->app->bindMethod('App\Console\Commands\Wikipedia\TopViews@handle', function ($command, $app) {
            $this->addLogTag('Wikipedia');
            $api = new FediApi($app->config->get('services.fediApi.wikipediaEsTop.host'), $app->config->get('services.fediApi.wikipediaEsTop.apikey'));
            return $command->handle(fediApi: $api, wikiApi: $app->make(Wikimedia::class));
        });

        $this->app->bindMethod('App\Console\Commands\Bicisenda\Test@handle', function ($command, $app) {
            $this->addLogTag('bici');
            $api = new FediApi($app->config->get('services.fediApi.test.host'), $app->config->get('services.fediApi.test.apikey'));
            return $command->handle(fediApi: $api, api: $this->app->make(BiciApi::class));
        });

        $this->app->bindMethod('App\Console\Commands\Bicisenda\Post@handle', function ($command, $app) {
            $this->addLogTag('bici');
            $api = new FediApi($app->config->get('services.fediApi.ciclovia18.host'), $app->config->get('services.fediApi.ciclovia18.apikey'));
            return $command->handle(
                fediApi: $api,
                api: $this->app->make(BiciApi::class),
                graphs: $this->app->make(BicisendaGraphs::class),
            );
        });

        $this->app->bindMethod('App\Console\Commands\Misc\WeatherLynch@handle', function ($command, $app) {
            $this->addLogTag('lynch');
            $api = new FediApi($app->config->get('services.fediApi.lynch.host'), $app->config->get('services.fediApi.lynch.apikey'));
            return $command->handle(api: $api);
        });

        // Every Purchase
        $this->app->bind(StreetView::class, function ($app) {
            return new StreetView(
                $app->config->get('services.google.streetview.apikey'),
                $app->config->get('services.google.streetview.urlsecret')
            );
        });
        $this->app->bind(Geocode::class, function ($app) {
            return new Geocode(
                $app->config->get('services.google.geocode.apikey')
            );
        });
        $this->app->bind(Places::class, function ($app) {
            return new Places(
                $app->config->get('services.google.maps.apikey')
            );
        });
        $this->app->bind(Maps::class, function ($app) {
            return new Maps(
                $app->config->get('services.google.maps.apikey'),
                $app->config->get('services.google.maps.urlsecret')
            );
        });

        $this->app->bind(Bitly::class, function ($app) {
            return new Bitly($app->config->get('services.bitly.token'));
        });

        $this->app->bind(Sheets::class, function ($app) {
            $client = new \Google\Client([
                'quota_project' => $app->config->get('services.google.sheets.project'),
                'application_name' => $app->config->get('services.google.sheets.application_name'),
            ]);
            $client->setScopes($app->config->get('services.google.sheets.scopes'));
            $client->setAuthConfig($app->config->get('services.google.sheets.service_account'));

            return new Sheets($client);
        });

        // The package does not bind the class to the facadefn($app) => $app->make('flickr')
        $this->app->bind(Flickr::class, 'flickr');

        $this->app->singleton(Inumet::class);
        $this->app->singleton(TwitterHelper::class);
        $this->app->singleton(Helper::class);
    }

    public function addLogTag(string $id) : void
    {
        $this->app->log->pushProcessor(new TagProcessor([$id]));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->app->log->pushProcessor(new UidProcessor(16));
        $this->app->log->pushProcessor(new MemoryUsageProcessor());
        $this->app->log->pushProcessor(new MemoryPeakUsageProcessor());
    }
}
