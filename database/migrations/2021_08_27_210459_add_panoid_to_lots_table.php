<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPanoidToLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everylot')->table('lots', function (Blueprint $table) {
            $table->json('metadata')->nullable();
            $table->string('gshort_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everylot')->table('lots', function (Blueprint $table) {
            $table->dropColumn('gshort_url');
        });
        Schema::connection('everylot')->table('lots', function (Blueprint $table) {
            $table->dropColumn('metadata');
        });
    }
}
