<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToUruguayansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            //
            //RELACIÓN DE PARENTESCO	e30
            $table->string('parentesco')->nullable()->comment('e30');

            $table->integer('cantidad_trabajos')->nullable()->comment('f70');
            $table->string('categoria_trabajo')->nullable()->comment('f73');
            $table->string('sector_publico')->nullable()->comment('f74');
            $table->string('tamano_empresa')->nullable()->comment('f77');
            // DÓNDE REALIZA SU TRABAJO	f78
            $table->string('lugar_de_trabajo')->nullable()->comment('f78');
            $table->integer('ciuo_08_trabajo')->nullable()->comment('f71_2');
            $table->string('forma_tributacion')->nullable()->comment('f265');

            // Bachillerato tecnológico
            /*
            CURSO O CARRERA QUE ESTUDIA / ESTUDIÓ e209_1
            */
            $table->string('bachillerato_tecnologico_curso')->nullable()->comment('e209_1');

            // Educación Técnica
            /*
            ASISTENCIA A EDUCACIÓN TÉCNICA	e212
            FINALIZÓ EL NIVEL	e212_1
            TIPO DE CENTRO EDUCATIVO	e213
            AÑOS APROBADOS EN EDUCACIÓN TÉCNICA	e51_7
            CURSO O CARRERA QUE ESTUDIA / ESTUDIÓ	e214_1
            */
            $table->string('asistio_ed_tecnica')->nullable()->comment('e212');
            $table->boolean('finalizo_ed_tecnica')->default(false)->comment('e212_1');
            $table->string('tipo_centro_ed_tecnica')->nullable()->comment('e213');
            $table->string('años_aprobados_ed_tecnica')->nullable()->comment('e51_7');
            $table->string('ed_media_curso')->nullable()->comment('e214_1');

            // TERCIARIO NO UNIVERSITARIO
            /*
            ASISTENCIA A TERCIARIO NO UNIVERSITARIO	e221
            FINALIZÓ EL NIVEL	e221_1
            TIPO DE CENTRO EDUCATIVO	e222
            AÑOS APROBADOS EN TERCIARIO NO UNIVERSITARIO	e51_10
            CURSO O CARRERA QUE ESTUDIA/ESTUDIÓ	e223_1
            */
            $table->string('asistio_terciario')->nullable()->comment('e221');
            // $table->boolean('finalizo_terciario')->default(false)->comment('e221_1');
            $table->string('tipo_centro_terciario')->nullable()->comment('e222');
            $table->string('años_aprobados_terciario')->nullable()->comment('e51_10');
            $table->string('terciario_curso')->nullable()->comment('e223_1');

            // POSGRADO
            /*
            ASISTENCIA A POSGRADO	e224
            FINALIZÓ EL NIVEL	e224_1
            TIPO DE CENTRO EDUCATIVO	e225
            AÑOS APROBADOS EN POSGRADO	e51_11
            CURSO O CARRERA QUE ESTUDIA/ESTUDIÓ	e226_1
            */
            $table->string('asistio_posgrado')->nullable()->comment('e224');
            $table->boolean('finalizo_posgrado')->default(false)->comment('e224_1');
            $table->string('tipo_centro_posgrado')->nullable()->comment('e225');
            $table->string('años_aprobados_posgrado')->nullable()->comment('e51_11');
            $table->string('posgrado_curso')->nullable()->comment('e226_1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('parentesco');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_trabajos');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('categoria_trabajo');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('sector_publico');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tamano_empresa');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('lugar_de_trabajo');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('ciuo_08_trabajo');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('bachillerato_tecnologico_curso');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_ed_tecnica');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('finalizo_ed_tecnica');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_centro_ed_tecnica');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_ed_tecnica');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('ed_media_curso');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_terciario');
        });
        // Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
        //     $table->dropColumn('finalizo_terciario');
        // });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_centro_terciario');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_terciario');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('terciario_curso');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_posgrado');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('finalizo_posgrado');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_centro_posgrado');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_posgrado');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('posgrado_curso');
        });
    }
}
