<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everypurchase')->create('providers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('doc');
            $table->string('country')->nullable();
            $table->text('address')->nullable();
            $table->string('status')->nullable();

            $table->unique('doc');
        });

        Schema::connection('everypurchase')->table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('provider_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everypurchase')->table('items', function (Blueprint $table) {
            $table->dropColumn('provider_id');
        });
        Schema::connection('everypurchase')->dropIfExists('providers');
    }
}
