<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUruguayansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyuruguayan')->create('uruguayans', function (Blueprint $table) {
            $table->id();

            // Hogar
            $table->integer('hogar_id')->comment('numero');
            $table->integer('persona_id')->comment('nper');
            $table->string('identificacion')->comment('e557, IDENTIFICACIÓN DEL INFORMANTE');
            $table->string('departamento')->comment('dpto'); //
            $table->string('barrio')->nullable()->comment('nombarrio, solo Montevideo');
            $table->string('personas_vivienda')->comment('ht19');
            $table->string('region')->comment('region_4');
            // $table->string('tenencia')->comment('H5_CV');

            // Estudios
            $table->boolean('estudia')->comment('e49_cv');
            $table->string('tipo_centro')->comment('e50_cv, e239, e194');
            $table->boolean('beca')->default(false)->comment('e562, e562_cv');
            $table->string('tipo_beca')->nullable()->comment('e562, e562_cv');
            $table->string('alfabetizacion')->comment('e48');
            $table->string('universidad')->nullable()->comment('e218');
            $table->boolean('termino_universidad')->default(false)->comment('e218_1');

            $table->string('actividad')->nullable()->comment('f124_1,f124_2,f124_3,f124_4,f124_5');

            // Identidad
            // $table->string('estrato')->
            $table->string('ascendencia')->comment('e29_6');
            $table->string('identidad_genero')->nullable()->comment('e563');
            $table->integer('edad')->comment('e27');
            $table->string('sexo')->comment('e26');

            // Economía
            $table->string('condicion_actividad')->nullable()->comment('pobpcoac');
            // $table->string('ocupacion')->nullable()->comment('e564');
            $table->boolean('pobre')->default(false)->comment('pobre_06');
            $table->boolean('indigente')->default(false)->comment('indigente_06');
            $table->string('jornada_laboral')->nullable()->comment();
            $table->string('ingresos')->nullable()->comment('pt4');
            $table->string('razon_dejar_trabajo')->nullable()->comment('f122');

            // Salud
            $table->string('insti_salud')->nullable()->comment('e45_CV, INSTITUCIÓN EN DONDE CUENTA CON DERECHOS PARA ATENDER SU SALUD');
            $table->string('acceso_insti_salud')->nullable()->comment('e45_1_1_CV, e45_1_1_1_CV, e45_2_1_CV, e45_2_1_1_CV, e45_3_1_CV, e45_3_1_1_CV, e45_4_1_CV, e45_4_1_1_CV');
            $table->boolean('emergencia')->default(false)->comment('e46_CV');
            $table->string('acceso_emergencia')->nullable()->comment('e47_CV');

            // Jubilación
            $table->boolean('aporta_caja')->default(false)->comment('f82');
            $table->string('caja_aportes')->nullable()->comment('f83');

            // Twitter
            $table->string('user_id')->nullable();
            $table->string('username')->nullable();
            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->unique(['hogar_id', 'persona_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everyuruguayan')->dropIfExists('uruguayans');
    }
}
