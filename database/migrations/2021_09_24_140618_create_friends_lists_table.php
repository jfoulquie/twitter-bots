<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFriendsListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pol-alert')->create('friends_lists', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->json('list');
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('twitter_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pol-alert')->dropIfExists('friends_lists');
    }
}
