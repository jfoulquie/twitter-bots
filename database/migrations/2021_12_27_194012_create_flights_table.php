<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyflight')->create('flights', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type')->comment('arrival/departure'); // arrival/departure
            $table->timestamp('departure_time')->nullable();
            $table->timestamp('etd')->nullable();
            $table->timestamp('atd')->nullable();
            $table->timestamp('arrival_time')->nullable();
            $table->timestamp('eta')->nullable();
            $table->timestamp('ata')->nullable();
            $table->string('departure_city')->nullable();
            $table->string('arrival_city')->nullable();
            $table->string('airline');
            $table->string('airline_logo')->nullable();
            $table->string('flight_number');
            $table->string('status');

            $table->unique(['flight_number', 'departure_time', 'arrival_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everyflight')->dropIfExists('flights');
    }
}
