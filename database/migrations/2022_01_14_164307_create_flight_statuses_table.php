<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyflight')->create('flight_statuses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('flight_id');
            $table->string('old_status')->nullable();
            $table->string('new_status');
            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->foreign('flight_id')->references('id')->on('flights')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everyflight')->dropIfExists('flight_statuses');
    }
}
